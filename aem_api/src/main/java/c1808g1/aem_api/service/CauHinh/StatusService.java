package c1808g1.aem_api.service.CauHinh;

import java.util.List;
import java.util.Optional;

import c1808g1.aem_api.models.CauHinh.StatusModel;

public interface StatusService {
	List<StatusModel> findAllStatus();
	List<StatusModel> findStatusByGroupTypeActiveTrue(int group_type);
	List<StatusModel> findStatusByGroupType(int group_type);
	StatusModel findStatusById(String id);
	StatusModel findStatusByName(String name_status);
	void save(StatusModel stat);
	void remove(StatusModel stat);
}
