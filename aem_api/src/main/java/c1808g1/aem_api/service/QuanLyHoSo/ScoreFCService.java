package c1808g1.aem_api.service.QuanLyHoSo;

import java.util.List;
import java.util.Optional;

import org.springframework.data.repository.query.Param;

import c1808g1.aem_api.models.QuanLyChuongTrinhHoc.Subject;
import c1808g1.aem_api.models.QuanLyHoSo.FCModel;
import c1808g1.aem_api.models.QuanLyHoSo.ScoreFCModel;

public interface ScoreFCService {
	List<ScoreFCModel> ListAllScoreFC();
	ScoreFCModel ListScoreFCById(Integer id);
	List<ScoreFCModel> getScoreFCOrderByDatecreateDESC();
	List<ScoreFCModel> getScoreFCByMultipleParameter(String subject_id,String fc_id);
	List<ScoreFCModel> getScoreFCBySearchValue(String searchValue);
	List<ScoreFCModel> getScoreFCBySubjectIdAndSearchValue(String subject_id,String searchValue);
	List<ScoreFCModel> getScoreFCByFCIdAndSearchValue(String fc_id,String searchValue);
	void save(ScoreFCModel sfcm);
	void delete(ScoreFCModel sfcm);
}
