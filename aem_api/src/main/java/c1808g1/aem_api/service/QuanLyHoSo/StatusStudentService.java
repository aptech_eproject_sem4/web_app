package c1808g1.aem_api.service.QuanLyHoSo;

import java.util.List;

import c1808g1.aem_api.models.QuanLyHoSo.StatusStudentModel;

public interface StatusStudentService {
	List<StatusStudentModel> findAllStatusStudent();
	StatusStudentModel findStatusStudentById(Integer id);
	StatusStudentModel findLatestByStudentIdAndStatusId(String student_id, String status_id);
	StatusStudentModel findLatestByStudentId(String student_id);
	List<StatusStudentModel> findStatusStudentByIdStudent(String id_student);
	void save(StatusStudentModel stastu);
	void remove(StatusStudentModel stastu);
}
