package c1808g1.aem_api.service.QuanLyHoSo;

import java.util.Date;
import java.util.List;


import c1808g1.aem_api.models.QuanLyHoSo.StudentModel;

public interface StudentService {
	List<StudentModel> ListAllStudent();
	StudentModel ListStudentByEmailAndPassword(String email, String password);
	StudentModel ListStudentById(String id_student);
	StudentModel getStudentByEmail(String email);
	StudentModel ListStudentByMobileMac(String mobile);
	void updateMobileMac(String id_student, String mobile_mac);
	void save(StudentModel student);
	void delete(StudentModel student);
	public List<StudentModel> GetListStudentFilter();
	public List<StudentModel> GetListStudentFilter(
	String course_family,
	String course_id,	
	String current_class,
	String name_search,
	String from_date,
	String to_date,
	int page,
	String status
	);
}
