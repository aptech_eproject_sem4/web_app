package c1808g1.aem_api.service.QuanLyHoSo;

import java.util.List;
import java.util.Optional;

import c1808g1.aem_api.models.QuanLyHoSo.EmployeeModel;
import c1808g1.aem_api.models.QuanLyHoSo.FCModel;

public interface FCService {
	List<FCModel> ListAllFC();
	FCModel ListFCById(String id_fc);
	FCModel getFCByEmail(String email);
	FCModel ListFCByEmailAndPassword(String email,String password);
	List<FCModel> checkFCByValue(String searchValue);
	List<FCModel> getFCByMultipleParameter(String status,String searchValue);
	void save(FCModel fc);
	void delete(FCModel fc);
}
