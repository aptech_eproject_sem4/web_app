package c1808g1.aem_api.service.QuanLyHoSo;

import java.util.List;


import c1808g1.aem_api.models.QuanLyHoSo.EmployeeModel;

public interface EmployeeService {
	List<EmployeeModel> ListAllEmployee();
	EmployeeModel ListEmployeeById(String id_emp);
	EmployeeModel getEmployeeByEmail(String email);
	EmployeeModel ListEmployeeByEmailAndPassword(String email,String password);
	List<EmployeeModel> getEmployeeByMultipleParameter(Boolean status,String searchValue);
	void save(EmployeeModel employee);
	void delete(EmployeeModel employee);
}