package c1808g1.aem_api.service.QuanLyLichHoc;

import java.util.List;
import c1808g1.aem_api.models.QuanLyLichHoc.ScheduleModel;

public interface ScheduleService {
	List<ScheduleModel> ListAllSchedule();
	ScheduleModel ListScheduleById(Integer id);
	void save(ScheduleModel sm);
	void delete(ScheduleModel sm);
	public List<ScheduleModel> GetListScheduleFilter(
			String filterClass,
			String filterFc
			);
}
