package c1808g1.aem_api.service.implement.QuanLyChuongTrinhHoc;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import c1808g1.aem_api.models.QuanLyChuongTrinhHoc.Course;
import c1808g1.aem_api.repository.QuanLyChuongTrinhHoc.CourseRepository;
import c1808g1.aem_api.service.QuanLyChuongTrinhHoc.CourseServices;

import java.util.List;
import java.util.Optional;

@Service
public class CourseServiceImplement implements CourseServices {
	private CourseRepository cosRepo;
	
	@Autowired
	public CourseServiceImplement (CourseRepository cosRepo) {
		this.cosRepo = cosRepo;
	}
	
	@Override
	public List<Course> findAllCourse(){
		return (List<Course>) cosRepo.findAll();
	}
	
	@Override
	public List<Course> findAllCourseByDateCreate(){
		return (List<Course>) cosRepo.findAllCourseByDateCreate();
	}

	
	@Override
	public Course findById(String id){
		return cosRepo.findById(id).get();
	}
	
	@Override
	public void	save(Course Course) {
		cosRepo.save(Course);
	}
	
	@Override
	public void remove(Course Course) {
		cosRepo.delete(Course);
	}
	@Override
	public List<Course> findByCourseRootIsNull(){
		return (List<Course>) cosRepo.findByCourse_root();
	}
	@Override
	public List<Course> findByCourseRootNotNull(){
		return (List<Course>) cosRepo.findByCourseNotNull();
	}
	@Override
	public List<Course> findCourseByCourseroot(String courseroot){
		return (List<Course>) cosRepo.findCourseByCourseroot(courseroot);
	}
}
