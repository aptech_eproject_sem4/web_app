package c1808g1.aem_api.service.implement.QuanLyHoSo;

import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import c1808g1.aem_api.common.ParseDate;
import c1808g1.aem_api.models.QuanLyHoSo.StudentModel;
import c1808g1.aem_api.repository.QuanLyHoSo.StudentRepository;
import c1808g1.aem_api.service.QuanLyHoSo.StudentService;

@Service
public class StudentServiceImplement implements StudentService {
	private StudentRepository StuRepo;

	@PersistenceContext
	private EntityManager entityManager;

	@Autowired
	public StudentServiceImplement(StudentRepository StuRepo) {
		this.StuRepo = StuRepo;
	}

	@Override
	public List<StudentModel> ListAllStudent() {
		return (List<StudentModel>) StuRepo.findAll();
	}

	@Override
	public StudentModel ListStudentById(String id_student) {
		return StuRepo.findById(id_student).get();
	}

	@Override
	public StudentModel getStudentByEmail(String email) {
		return StuRepo.getStudentByEmail(email);
	}

	@Override
	public StudentModel ListStudentByEmailAndPassword(String email, String password) {
		return StuRepo.findByEmailAndPassword(email, password);
	}

	@Override
	public StudentModel ListStudentByMobileMac(String mobile) {
		return StuRepo.findByMobileMac(mobile);
	}

	@Override
	public void updateMobileMac(String id_student, String mobile_mac) {
		StuRepo.updateMobileMac(id_student, mobile_mac);
	}

	@Override
	public void save(StudentModel student) {
		StuRepo.save(student);
	}

	@Override
	public void delete(StudentModel student) {
		StuRepo.delete(student);
	}

	@Override
	public List<StudentModel> GetListStudentFilter() {
		try {

			StoredProcedureQuery queryStore = entityManager
					.createNamedStoredProcedureQuery("getStudentIndexFilterNonParams");

			var results = queryStore.getResultList();
			List<StudentModel> studentModels = results;

			return studentModels;
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			this.entityManager.close();
		}
		return null;
	}
	
	@Override
	public List<StudentModel> GetListStudentFilter(
		String course_family,
		String course_id ,
		String current_class,
		String name_search,
		String from_date,
		String to_date,
		int page,
		String status
	) {
	
		try {
			SimpleDateFormat formatter = new SimpleDateFormat("yyyy/MM/dd");  

//			if(from_date=="") from_date="2000/01/01";
//			if(to_date=="") to_date =formatter.format(new Date());
	
			StoredProcedureQuery queryStore = entityManager.createNamedStoredProcedureQuery("getStudentIndexFilter");
			queryStore.setParameter("COURSE_FAMILY", course_family);
			queryStore.setParameter("COURSE_ID", course_id);
			queryStore.setParameter("CURRENT_CLASS", current_class);
			queryStore.setParameter("NAME_SEARCH", name_search);
			queryStore.setParameter("FROM_DATE",from_date);
			queryStore.setParameter("TO_DATE",to_date);
//			queryStore.setParameter("FROM_DATE",ParseDate.ParseToSQLDate(from_date));
//			queryStore.setParameter("TO_DATE",ParseDate.ParseToSQLDate(to_date));
			queryStore.setParameter("PAGE", page);
			queryStore.setParameter("STATUS", status);
			
			var results = queryStore.getResultList();
			List<StudentModel> studentModels = results;

				return studentModels;
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			this.entityManager.close();
		}
		return null;
	}
}