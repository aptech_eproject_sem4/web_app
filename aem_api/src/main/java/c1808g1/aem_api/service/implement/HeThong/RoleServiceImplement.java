package c1808g1.aem_api.service.implement.HeThong;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import c1808g1.aem_api.models.HeThong.RoleModel;
import c1808g1.aem_api.repository.HeThong.RoleRepository;
import c1808g1.aem_api.service.HeThong.RoleService;

	@Service
	public class RoleServiceImplement implements RoleService {
		private RoleRepository rrepo;

		@Autowired
		public RoleServiceImplement(RoleRepository rrepo) {
			this.rrepo = rrepo;
		}
		@Override
		public List<RoleModel> findAllRole() {
			 
			return (List<RoleModel>) rrepo.findAll();
		}
		
		@Override
		public List<RoleModel> getAllOrderbyASC(){
			return (List<RoleModel>) rrepo.getAllOrderbyASC();
		};

		@Override
		public RoleModel findRoleById(Integer id) {
			 
			return rrepo.findById(id).get();
		}

		@Override
		public void save(RoleModel r) {
			 
			rrepo.save(r);
		}

		@Override
		public void remove(RoleModel r) {
			 
			rrepo.delete(r);
		}

}
