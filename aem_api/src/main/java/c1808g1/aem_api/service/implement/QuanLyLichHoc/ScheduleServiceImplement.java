package c1808g1.aem_api.service.implement.QuanLyLichHoc;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Optional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import c1808g1.aem_api.models.QuanLyLichHoc.ScheduleModel;
import c1808g1.aem_api.repository.QuanLyLichHoc.ScheduleRepository;
import c1808g1.aem_api.service.QuanLyLichHoc.ScheduleService;

@Service
public class ScheduleServiceImplement implements ScheduleService{
	private ScheduleRepository SRepo;

	@PersistenceContext
	private EntityManager entityManager;
	
	@Autowired
	public ScheduleServiceImplement(ScheduleRepository sRepo) {
		SRepo = sRepo;
	}

	@Override
	public List<ScheduleModel> ListAllSchedule() {
		return (List<ScheduleModel>) SRepo.findAll();
	}

	@Override
	public ScheduleModel ListScheduleById(Integer id) {
		return  SRepo.findById(id).get();
	}

	@Override
	public void save(ScheduleModel sm) {
		SRepo.save(sm);
	}

	@Override
	public void delete(ScheduleModel sm) {
		SRepo.delete(sm);
	}
	
	@Override
	public List<ScheduleModel> GetListScheduleFilter(
		String filterClass,
		String filterFc 
	) {
	
		try {
			SimpleDateFormat formatter = new SimpleDateFormat("yyyy/MM/dd");  

//			if(from_date=="") from_date="2000/01/01";
//			if(to_date=="") to_date =formatter.format(new Date());
	
			StoredProcedureQuery queryStore = entityManager.createNamedStoredProcedureQuery("getScheduleIndexFilter");
			queryStore.setParameter("FILTER_CLASS", filterClass);
			queryStore.setParameter("FILTER_FC", filterFc);
			
			var results = queryStore.getResultList();
			List<ScheduleModel> ScheduleModels = results;

			return ScheduleModels;
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			this.entityManager.close();
		}
		return null;
	}
}

