package c1808g1.aem_api.service.implement.QuanLyHoSo;

import java.util.List;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Service;

import c1808g1.aem_api.models.QuanLyChuongTrinhHoc.Subject;
import c1808g1.aem_api.models.QuanLyHoSo.FCModel;
import c1808g1.aem_api.models.QuanLyHoSo.ScoreFCModel;
import c1808g1.aem_api.repository.QuanLyHoSo.ScoreFCRepository;
import c1808g1.aem_api.service.QuanLyHoSo.ScoreFCService;

@Service
public class ScoreFCServiceImplement implements ScoreFCService{
	private ScoreFCRepository SFCRepo;
	
	@Autowired
	public ScoreFCServiceImplement(ScoreFCRepository SFCRepo) {
		this.SFCRepo=SFCRepo;
	}
	
	@Override
	public List<ScoreFCModel> ListAllScoreFC() {
		return (List<ScoreFCModel>) SFCRepo.findAll();
	}
	
	@Override
	public List<ScoreFCModel> getScoreFCOrderByDatecreateDESC() {
		return (List<ScoreFCModel>) SFCRepo.getScoreFCOrderByDatecreateDESC();
	}
	
	@Override
	public List<ScoreFCModel> getScoreFCByMultipleParameter(String subject_id,String fc_id) {
		return (List<ScoreFCModel>) SFCRepo.getScoreFCByMultipleParameter(subject_id, fc_id);
	}
	
	@Override
	public List<ScoreFCModel> getScoreFCBySearchValue(String searchValue) {
		return (List<ScoreFCModel>) SFCRepo.getScoreFCBySearchValue(searchValue);
	}
	
	@Override
	public List<ScoreFCModel> getScoreFCBySubjectIdAndSearchValue(String subject_id,String searchValue) {
		return (List<ScoreFCModel>) SFCRepo.getScoreFCBySubjectIdAndSearchValue(subject_id, searchValue);
	}
	
	@Override
	public List<ScoreFCModel> getScoreFCByFCIdAndSearchValue(String fc_id,String searchValue) {
		return (List<ScoreFCModel>) SFCRepo.getScoreFCByFCIdAndSearchValue(fc_id, searchValue);
	}

	@Override
	public ScoreFCModel ListScoreFCById(Integer id) {
		return SFCRepo.findById(id).get();
	}

	@Override
	public void save(ScoreFCModel sfcm) {
		SFCRepo.save(sfcm);
	}

	@Override
	public void delete(ScoreFCModel sfcm) {
		SFCRepo.delete(sfcm);
	}

}
