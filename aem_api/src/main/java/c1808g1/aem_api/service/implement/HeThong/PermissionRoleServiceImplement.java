package c1808g1.aem_api.service.implement.HeThong;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import c1808g1.aem_api.models.HeThong.*;
import c1808g1.aem_api.repository.HeThong.PermissionRoleRepository;
import c1808g1.aem_api.service.HeThong.PermissionRoleService;

@Service
public class PermissionRoleServiceImplement implements PermissionRoleService {
	private PermissionRoleRepository prRepo;

	@Autowired
	public PermissionRoleServiceImplement(PermissionRoleRepository prRepo) {
		this.prRepo = prRepo;
	}

	
	@Override
	public List<PermissionRoleModel> findAllPermissionRole(){
		return (List<PermissionRoleModel>) prRepo.findAll();
	}
	
	@Override
	public PermissionRoleModel findPermissionRoleById(Integer id){
		return prRepo.findById(id).get();
	}
	
	@Override
	public void save(PermissionRoleModel holi){
		prRepo.save(holi);
	}
	
	@Override
	public void remove(PermissionRoleModel holi){
		prRepo.delete(holi);
	}
	
	@Override
	public List<PermissionRoleModel> deletePermissionRoleByRoleid(Integer role_id){
		return prRepo.deletePermissionRoleByRoleid(role_id);
	}
	
	@Override
	public List<PermissionRoleModel> deletePermissionRoleByControllerid(String controller_id){
		return prRepo.deletePermissionRoleByControllerid(controller_id);
	}

}
