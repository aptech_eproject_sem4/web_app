package c1808g1.aem_api.service.HeThong;

import java.util.List;
import java.util.Optional;

import c1808g1.aem_api.models.HeThong.*;

public interface RoleService {
	List<RoleModel> findAllRole();
	RoleModel findRoleById(Integer id);
	List<RoleModel> getAllOrderbyASC();
	void save(RoleModel rs);
	void remove(RoleModel rs);
}
