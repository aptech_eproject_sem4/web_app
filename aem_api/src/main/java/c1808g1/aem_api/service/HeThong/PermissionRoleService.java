package c1808g1.aem_api.service.HeThong;

import java.sql.ResultSet;
import java.util.List;
import java.util.Optional;

import c1808g1.aem_api.models.HeThong.*;


public interface PermissionRoleService {
	List<PermissionRoleModel> findAllPermissionRole();
	PermissionRoleModel findPermissionRoleById(Integer id);
	void save(PermissionRoleModel pr);
	void remove(PermissionRoleModel pr);
	List<PermissionRoleModel> deletePermissionRoleByRoleid(Integer role_id);
	List<PermissionRoleModel> deletePermissionRoleByControllerid(String controller_id);
}
