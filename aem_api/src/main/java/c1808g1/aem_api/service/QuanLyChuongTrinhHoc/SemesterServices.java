package c1808g1.aem_api.service.QuanLyChuongTrinhHoc;
import java.util.List;
import java.util.Optional;

import org.springframework.data.repository.query.Param;

import c1808g1.aem_api.models.QuanLyChuongTrinhHoc.Semester;

public interface SemesterServices {
	List<Semester> findAllSemester();
	Semester findById(Integer id);
	List<Semester> findSemesterByIdcourse(String idCourse);
	void save(Semester semester);
	void remove(Semester semester);
	List<Semester> findAllSemeByOrderNumber();
}
