package c1808g1.aem_api.service.QuanLyChuongTrinhHoc;
import java.util.List;
import java.util.Optional;

import c1808g1.aem_api.models.QuanLyChuongTrinhHoc.Subject;

public interface SubjectServices {
	List<Subject> findAllSubject();
	Subject findById(String id);
	List<Subject> getSubjectByMultipleParameter(int idSemester,String idStatus,String searchValue);
	List<Subject> getSubjectOrderByOrderNumberASC();
	List<Subject> checkSubjectByValue(String searchValue);
	void save(Subject subject);
	void remove(Subject subject);
}