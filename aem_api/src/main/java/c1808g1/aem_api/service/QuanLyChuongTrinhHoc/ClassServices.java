package c1808g1.aem_api.service.QuanLyChuongTrinhHoc;
import java.util.List;
import java.util.Optional;

import c1808g1.aem_api.models.CauHinh.HolidayModel;
import c1808g1.aem_api.models.QuanLyChuongTrinhHoc.Class;

public interface ClassServices {
	List<Class> findAllClass();
	Class findById(String id);
	List<Class> DateSort();
	void save(Class classV);
	void remove(Class classV);
}
