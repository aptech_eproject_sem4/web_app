package c1808g1.aem_api.controller.QuanLyHoSo;

import java.text.ParseException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import c1808g1.Models.CauHinh.StatusDTO;
import c1808g1.Models.QuanLiHoSo.ScoreStudentDTO;
import c1808g1.Models.QuanLiHoSo.StatusStudentDTO;
import c1808g1.Models.QuanLiHoSo.StudentDTO;
import c1808g1.aem_api.common.ParseDate;
import c1808g1.aem_api.config.ModelMapperConfig;
import c1808g1.aem_api.models.QuanLyHoSo.EmployeeModel;
import c1808g1.aem_api.models.QuanLyHoSo.ScoreStudentModel;
import c1808g1.aem_api.models.QuanLyHoSo.StudentModel;
import c1808g1.aem_api.service.CauHinh.StatusService;
import c1808g1.aem_api.service.QuanLyHoSo.ScoreStudentService;
import c1808g1.aem_api.service.QuanLyHoSo.StatusStudentService;
import c1808g1.aem_api.service.QuanLyHoSo.StudentService;

@RestController
@RequestMapping("/api/quanlyhoso/sinhvienapi")
public class SinhVienController {
	private ScoreStudentService SSSv;
	private StudentService StuSv;
	private StatusStudentService statusStudentService;

	private StatusService statusService;

	@Autowired
	public SinhVienController(ScoreStudentService SSSv, StudentService StuSv, StatusStudentService statusStudentService,
			StatusService statusService) {
		this.SSSv = SSSv;
		this.StuSv = StuSv;
		this.statusStudentService = statusStudentService;
		this.statusService = statusService;
	}

	@RequestMapping(value = "/getAllScoreStudent", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<ScoreStudentDTO>> ListAllScoreStudent() {
		List<ScoreStudentModel> ssm = SSSv.ListAllScoreStudent();
		List<ScoreStudentDTO> ssdto = ModelMapperConfig.mapList(ssm, ScoreStudentDTO.class);
		if (ssdto.isEmpty()) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
		return new ResponseEntity<>(ssdto, HttpStatus.OK);
	}

	@RequestMapping(value = "/getScoreStudentById/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ScoreStudentDTO> ListScoreStudentById(@PathVariable("id") Integer id) {
		var data = SSSv.ListScoreStudentById(id);
		ScoreStudentDTO ssdto = ModelMapperConfig.modelMapper.map(data, ScoreStudentDTO.class);
		if (ssdto == null) {
			return new ResponseEntity<>(ssdto, HttpStatus.NO_CONTENT);
		}
		return new ResponseEntity<>(ssdto, HttpStatus.OK);
	}

	@RequestMapping(value = "/createScoreStudent", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ScoreStudentDTO> CreateScoreStudent(@RequestBody ScoreStudentDTO ssdto,
			UriComponentsBuilder builder) {
		ScoreStudentModel ssm = ModelMapperConfig.modelMapper.map(ssdto, ScoreStudentModel.class);
		SSSv.save(ssm);
		ssdto.setId(String.valueOf(ssm.getId()));
		HttpHeaders headers = new HttpHeaders();
		headers.setLocation(builder.path("/create/{id}").buildAndExpand(ssdto.getId()).toUri());
		return new ResponseEntity<>(ssdto, HttpStatus.CREATED);
	}

	@RequestMapping(value = "/updateScoreStudent", method = RequestMethod.PUT)
	public ResponseEntity<ScoreStudentDTO> updateScoreStudent(@RequestBody ScoreStudentDTO ss) {
		ScoreStudentModel scorestudent = ModelMapperConfig.modelMapper.map(ss, ScoreStudentModel.class);
		if (scorestudent == null) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
		SSSv.save(scorestudent);
		return new ResponseEntity<>(ss, HttpStatus.OK);
	}

	@RequestMapping(value = "/deleteScoreStudent/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<ScoreStudentDTO> deleteScoreStudent(@PathVariable("id") Integer id) {
		ScoreStudentModel ss = SSSv.ListScoreStudentById(id);
		ScoreStudentDTO h = ModelMapperConfig.modelMapper.map(ss, ScoreStudentDTO.class);
		if (ss == null) {
			return new ResponseEntity<ScoreStudentDTO>(h, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		SSSv.delete(ss);
		return new ResponseEntity<ScoreStudentDTO>(h, HttpStatus.OK);
	}

	// Student

	@RequestMapping(value = "/getAllStudent", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<StudentDTO>> ListAllStudent() {
		List<StudentModel> sm = StuSv.ListAllStudent();
		List<StudentDTO> sdto = ModelMapperConfig.mapList(sm, StudentDTO.class);
		if (sdto.isEmpty()) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
		return new ResponseEntity<>(sdto, HttpStatus.OK);
	}

	@RequestMapping(value = "/getStudentById/{id_student}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<StudentDTO> ListStudentById(@PathVariable("id_student") String id_student) {
		try {
			var data = StuSv.ListStudentById(id_student);
			StudentDTO sdto = ModelMapperConfig.modelMapper.map(data, StudentDTO.class);
			if (sdto == null) {
				return new ResponseEntity<>(sdto, HttpStatus.NO_CONTENT);
			}
			return new ResponseEntity<>(sdto, HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
	}

	@RequestMapping(value = "/getStudentBySchoolEmail/{email_school}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<StudentDTO> ListStudentByEmail(@PathVariable("email_school") String email_school) {
		try {
			StudentModel data = null;
			for (StudentModel student : StuSv.ListAllStudent()) {
				if (student.getEmail_school().equals(email_school)) {
					data = student;
					break;
				}
			}
			StudentDTO sdto = ModelMapperConfig.modelMapper.map(data, StudentDTO.class);
			if (sdto == null) {
				return new ResponseEntity<>(sdto, HttpStatus.NO_CONTENT);
			}
			return new ResponseEntity<>(sdto, HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
	}

	@RequestMapping(value = "/getStudentByEmail/{email}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<StudentDTO> getStudentByEmail(@PathVariable("email") String email) {
		var data = StuSv.getStudentByEmail(email);
		StudentDTO sdto = ModelMapperConfig.modelMapper.map(data, StudentDTO.class);
		if (sdto == null) {
			return new ResponseEntity<>(sdto, HttpStatus.NO_CONTENT);
		}
		return new ResponseEntity<>(sdto, HttpStatus.OK);
	}

	@RequestMapping(value = "/getStudentByMobileMac/{mobile}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<StudentDTO> ListScoreStudentByMobileMac(@PathVariable("mobile") String mobile) {
		var data = StuSv.ListStudentByMobileMac(mobile);
		StudentDTO sdto = ModelMapperConfig.modelMapper.map(data, StudentDTO.class);
		if (sdto == null) {
			return new ResponseEntity<>(sdto, HttpStatus.NO_CONTENT);
		}
		return new ResponseEntity<>(sdto, HttpStatus.OK);
	}

	@RequestMapping(value = "/createStudent", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<StudentDTO> CreateStudent(@RequestBody StudentDTO sdto, UriComponentsBuilder builder) {
		StudentModel em = ModelMapperConfig.modelMapper.map(sdto, StudentModel.class);
		StuSv.save(em);
		sdto.setId_student(em.getId_student());

		HttpHeaders headers = new HttpHeaders();
		headers.setLocation(builder.path("/getStudentById/{id_student}").buildAndExpand(sdto.getId_student()).toUri());
		return new ResponseEntity<>(sdto, HttpStatus.CREATED);
	}

	// add list students json
	@RequestMapping(value = "/createStudentList", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<StudentDTO>> CreateStudentList(@RequestBody List<StudentDTO> sdtos,
			UriComponentsBuilder builder) {
		sdtos.forEach(sdto -> {
			StudentModel sm = ModelMapperConfig.modelMapper.map(sdto, StudentModel.class);
			StuSv.save(sm);
			sdto.setId_student(sm.getId_student());
			System.out.println("successed");
		});
		return new ResponseEntity<List<StudentDTO>>(sdtos, HttpStatus.CREATED);
	}

	@RequestMapping(value = "/updateStudent", method = RequestMethod.PUT)
	public ResponseEntity<StudentDTO> updateStudent(@RequestBody StudentDTO sdto) {
		StudentModel student = ModelMapperConfig.modelMapper.map(sdto, StudentModel.class);
		if (student == null) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
		StuSv.save(student);

		return new ResponseEntity<>(sdto, HttpStatus.OK);
	}

	@RequestMapping(value = "/updateMobileMac/{id}/{mobile_mac}", method = RequestMethod.PUT)
	public ResponseEntity<StudentDTO> updateMobileMac(@PathVariable("id") String id_student,
			@PathVariable("mobile_mac") String mobile_mac) {
		if (id_student == null || mobile_mac == null) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} else {
			StudentModel getStu = StuSv.ListStudentById(id_student);
			getStu.setMobile_mac(mobile_mac);
			StuSv.save(getStu);
			StudentDTO s = ModelMapperConfig.modelMapper.map(getStu, StudentDTO.class);
			return new ResponseEntity<>(s, HttpStatus.OK);
		}
	}

	@RequestMapping(value = "/deleteStudent/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<StudentDTO> deleteStudent(@PathVariable("id") String id) {
		StudentModel s = StuSv.ListStudentById(id);
		StudentDTO h = ModelMapperConfig.modelMapper.map(s, StudentDTO.class);
		if (s == null) {
			return new ResponseEntity<StudentDTO>(h, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		StuSv.delete(s);
		return new ResponseEntity<StudentDTO>(h, HttpStatus.OK);
	}

	// get index user
	@RequestMapping(value = "/getListStudentFilter", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<StudentDTO>> getListStudentFilter() {

		var list = ModelMapperConfig.mapList(StuSv.GetListStudentFilter(), StudentDTO.class);

		var listStatus = ModelMapperConfig.mapList(this.statusService.findAllStatus(), StatusDTO.class);
		// cong kenh vl
		for (StudentDTO studentDTO : list) {
			var studentStatus = statusStudentService.findLatestByStudentIdAndStatusId(studentDTO.getId_student(),
					studentDTO.getStatus_id());
			if (studentStatus != null) {
				StatusStudentDTO studentstatusData = ModelMapperConfig.modelMapper.map(studentStatus,
						StatusStudentDTO.class);
				for (StatusDTO statusDTO : listStatus) {
					if (statusDTO.getId_status().equals(studentStatus.getStatus_id()))
						studentstatusData.setStatusDTO(statusDTO);
				}
				studentDTO.setStatusStudentDTO(studentstatusData);
			}
		}

		if (list.isEmpty()) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
		return new ResponseEntity<>(list, HttpStatus.OK);

	}

	// get index user
	@RequestMapping(value = "/getListStudentFilterParams", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<StudentDTO>> getListStudentFilterParams(
			@RequestParam(name = "course_family", required = false) String course_family,
			@RequestParam(name = "course_id", required = false) String course_id,
			@RequestParam(name = "current_class", required = false) String current_class,
			@RequestParam(name = "name_search", required = false) String name_search,
			@RequestParam(name = "from_date", required = false) String from_date,
			@RequestParam(name = "to_date", required = false) String to_date,
			@RequestParam(name = "page", required = false) int page,
			@RequestParam(name = "status", required = false) String status) throws ParseException {
		try {
			if (course_family == null || course_family.equals(null)) {
				course_family = "";
			} else {

			}
			if (course_id == null || course_id.equals(null)) {
				course_id = "";
			} else {

			}
			if (current_class == null || current_class.equals(null)) {
				current_class = "";
			} else {

			}
			if (name_search == null || name_search.equals(null)) {
				name_search = "";
			} else {

			}
			if (from_date == null || from_date.equals(null)) {
				from_date = "";
			} else {

			}
			if (to_date == null || to_date.equals(null)) {
				to_date = "";
			} else {

			}
			if (status == null || status.equals(null)) {
				status = "";
			} else {

			}
//			var list = ModelMapperConfig.mapList(StuSv.GetListStudentFilter((course_family==null||course_family.equals(null)) ? "" : course_family,
//					(course_id==null||course_id.equals(null)) ? "" : course_id, (current_class==null||current_class.equals(null)) ? "" : current_class,
//					(name_search==null||name_search.equals(null)) ? "" : name_search,
//					(from_date==null||from_date.equals(null)) ? "" : from_date, (to_date==null||to_date.equals(null)) ? "" : to_date, page,
//					(status==null||status.equals(null)) ? "" : status), StudentDTO.class);
			var list = ModelMapperConfig.mapList(StuSv.GetListStudentFilter(course_family, course_id, current_class,
					name_search, from_date, to_date, page, status), StudentDTO.class);

			var listStatus = ModelMapperConfig.mapList(this.statusService.findAllStatus(), StatusDTO.class);
			// cong kenh vl
			for (StudentDTO studentDTO : list) {
				var studentStatus = statusStudentService.findLatestByStudentIdAndStatusId(studentDTO.getId_student(),
						studentDTO.getStatus_id());
				if (studentStatus != null) {
					StatusStudentDTO studentstatusData = ModelMapperConfig.modelMapper.map(studentStatus,
							StatusStudentDTO.class);
					for (StatusDTO statusDTO : listStatus) {
						if (statusDTO.getId_status().equals(studentStatus.getStatus_id()))
							studentstatusData.setStatusDTO(statusDTO);
					}
					studentDTO.setStatusStudentDTO(studentstatusData);
				}
			}
			if (list.isEmpty()) {
				return new ResponseEntity<>(HttpStatus.NO_CONTENT);
			}
			return new ResponseEntity<>(list, HttpStatus.OK);

		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
	}

	// get index user
	@RequestMapping(value = "/getListParentStudent", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<StudentDTO> getListParentStudent(String studentId, String parrentInfo) {
		var student = StuSv.ListStudentById(studentId);
		if (student == null) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		student.setHo_so(parrentInfo);
		StuSv.save(student);

		return new ResponseEntity<>(ModelMapperConfig.modelMapper.map(student, StudentDTO.class), HttpStatus.OK);

	}

}