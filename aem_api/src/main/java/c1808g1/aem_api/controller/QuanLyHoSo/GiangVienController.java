package c1808g1.aem_api.controller.QuanLyHoSo;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import c1808g1.Models.QuanLiHoSo.FCDTO;
import c1808g1.Models.QuanLiHoSo.ScoreFCDTO;

import c1808g1.aem_api.common.ParseDate;
import c1808g1.aem_api.config.ModelMapperConfig;
import c1808g1.aem_api.models.QuanLyHoSo.FCModel;
import c1808g1.aem_api.models.QuanLyHoSo.ScoreFCModel;
import c1808g1.aem_api.service.QuanLyHoSo.FCService;
import c1808g1.aem_api.service.QuanLyHoSo.ScoreFCService;

@RestController
@RequestMapping("/api/quanlyhoso/giangvienapi")
public class GiangVienController {
	private ScoreFCService SFCSv;
	private FCService FCSv;

	@Autowired
	public GiangVienController(ScoreFCService SFCSv, FCService FCSv) {
		this.SFCSv = SFCSv;
		this.FCSv = FCSv;
	}

//	@Autowired
//	public GiangVienController () {
//		
//	}

	// ScoreFC
	@RequestMapping(value = "/getAllScoreFC", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<ScoreFCDTO>> ListAllScoreFC() {
		List<ScoreFCModel> sfcm = SFCSv.ListAllScoreFC();
		List<ScoreFCDTO> sfcdto = ModelMapperConfig.mapList(sfcm, ScoreFCDTO.class);
		if (sfcdto.isEmpty()) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
		return new ResponseEntity<>(sfcdto, HttpStatus.OK);
	}

	@RequestMapping(value = "/getScoreFCOrderByDatecreateDESC", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<ScoreFCDTO>> getScoreFCOrderByDatecreateDESC() {
		List<ScoreFCModel> sfcm = SFCSv.getScoreFCOrderByDatecreateDESC();
		List<ScoreFCDTO> sfcdto = ModelMapperConfig.mapList(sfcm, ScoreFCDTO.class);
		if (sfcdto.isEmpty()) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
		return new ResponseEntity<>(sfcdto, HttpStatus.OK);
	}

	@RequestMapping(value = "/getScoreFCByMultipleParameter/{subject_id}/{fc_id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<ScoreFCDTO>> getScoreFCByMultipleParameter(@PathVariable("subject_id") String subject_id,
			@PathVariable("fc_id") String fc_id) {
		List<ScoreFCModel> sfcm = SFCSv.getScoreFCByMultipleParameter(subject_id, fc_id);
		List<ScoreFCDTO> sfcdto = ModelMapperConfig.mapList(sfcm, ScoreFCDTO.class);
		if (sfcdto.isEmpty()) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
		return new ResponseEntity<>(sfcdto, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/getScoreFCBySearchValue/{searchValue}" , method = RequestMethod.GET , produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<ScoreFCDTO>> getScoreFCBySearchValue(@PathVariable("searchValue") String searchValue){
		List<ScoreFCModel> sfcm = SFCSv.getScoreFCBySearchValue(searchValue);
		List<ScoreFCDTO> sfcdto = ModelMapperConfig.mapList(sfcm, ScoreFCDTO.class);
		if (sfcdto.isEmpty()) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
		return new ResponseEntity<>(sfcdto,HttpStatus.OK);
	}
	
	@RequestMapping(value = "/getScoreFCBySubjectIdAndSearchValue/{subject_id}/{searchValue}" , method = RequestMethod.GET , produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<ScoreFCDTO>> getScoreFCBySubjectIdAndSearchValue(@PathVariable("subject_id") String subject_id,@PathVariable("searchValue") String searchValue){
		List<ScoreFCModel> sfcm = SFCSv.getScoreFCBySubjectIdAndSearchValue(subject_id, searchValue);
		List<ScoreFCDTO> sfcdto = ModelMapperConfig.mapList(sfcm, ScoreFCDTO.class);
		if (sfcdto.isEmpty()) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
		return new ResponseEntity<>(sfcdto, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/getScoreFCByFCIdAndSearchValue/{fc_id}/{searchValue}" , method = RequestMethod.GET , produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<ScoreFCDTO>> getScoreFCByFCIdAndSearchValue(@PathVariable("fc_id") String fc_id,@PathVariable("searchValue") String searchValue){
		List<ScoreFCModel> sfcm = SFCSv.getScoreFCByFCIdAndSearchValue(fc_id, searchValue);
		List<ScoreFCDTO> sfcdto = ModelMapperConfig.mapList(sfcm, ScoreFCDTO.class);
		if (sfcdto.isEmpty()) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
		return new ResponseEntity<>(sfcdto,HttpStatus.OK);
	}


	@RequestMapping(value = "/getScoreFCById/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ScoreFCDTO> ListScoreFCById(@PathVariable("id") Integer id) {
		var data = SFCSv.ListScoreFCById(id);
		ScoreFCDTO sfcdto = ModelMapperConfig.modelMapper.map(data, ScoreFCDTO.class);
		if (sfcdto == null) {
			return new ResponseEntity<>(sfcdto, HttpStatus.NO_CONTENT);
		}
		return new ResponseEntity<>(sfcdto, HttpStatus.OK);
	}

	@RequestMapping(value = "/createScoreFC", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ScoreFCDTO> CreateScoreFC(@RequestBody ScoreFCDTO sfcdto, UriComponentsBuilder builder) {
		ScoreFCModel sfcm = ModelMapperConfig.modelMapper.map(sfcdto, ScoreFCModel.class);
		SFCSv.save(sfcm);
		sfcdto.setId(String.valueOf(sfcm.getId()));
		HttpHeaders headers = new HttpHeaders();
		headers.setLocation(builder.path("/create/{id}").buildAndExpand(sfcdto.getId()).toUri());
		return new ResponseEntity<>(sfcdto, HttpStatus.CREATED);
	}

	@RequestMapping(value = "/updateScoreFC", method = RequestMethod.PUT)
	public ResponseEntity<ScoreFCDTO> updateScoreFC(@RequestBody ScoreFCDTO sfc) {
		ScoreFCModel scorefc = ModelMapperConfig.modelMapper.map(sfc, ScoreFCModel.class);
		if (scorefc == null) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
		SFCSv.save(scorefc);
		return new ResponseEntity<>(sfc, HttpStatus.OK);
	}

	@RequestMapping(value = "/deleteScoreFC/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<ScoreFCDTO> deleteScoreFC(@PathVariable("id") Integer id) {
		ScoreFCModel sfc = SFCSv.ListScoreFCById(id);
		ScoreFCDTO h = ModelMapperConfig.modelMapper.map(sfc, ScoreFCDTO.class);
		if (sfc == null) {
			return new ResponseEntity<ScoreFCDTO>(h, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		SFCSv.delete(sfc);
		return new ResponseEntity<ScoreFCDTO>(h, HttpStatus.OK);
	}

	// FC

	@RequestMapping(value = "/getAllFC", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<FCDTO>> FindAllFC() {
		List<FCModel> fcm = FCSv.ListAllFC();
		List<FCDTO> fcdto = ModelMapperConfig.mapList(fcm, FCDTO.class);
		if (fcdto.isEmpty()) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
		return new ResponseEntity<>(fcdto, HttpStatus.OK);
	}

	@RequestMapping(value = "/getFCById/{id_fc}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<FCDTO> FindFCById(@PathVariable("id_fc") String id_fc) {
		var data = FCSv.ListFCById(id_fc);
		FCDTO fcdto = ModelMapperConfig.modelMapper.map(data, FCDTO.class);
		if (fcdto == null) {
			return new ResponseEntity<>(fcdto, HttpStatus.NO_CONTENT);
		}
		return new ResponseEntity<>(fcdto, HttpStatus.OK);
	}

	@RequestMapping(value = "/getFCByEmail/{email}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<FCDTO> getFCByEmail(@PathVariable("email") String email) {
		var data = FCSv.getFCByEmail(email);
		FCDTO fcdto = ModelMapperConfig.modelMapper.map(data, FCDTO.class);
		if (fcdto == null) {
			return new ResponseEntity<>(fcdto, HttpStatus.NO_CONTENT);
		}
		return new ResponseEntity<>(fcdto, HttpStatus.OK);
	}

	@RequestMapping(value = "/checkFCByValue/{searchValue}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<FCDTO>> checkFCByValue(@PathVariable("searchValue") String searchValue) {
		List<FCModel> sfcm = FCSv.checkFCByValue(searchValue);
		List<FCDTO> sfcdto = ModelMapperConfig.mapList(sfcm, FCDTO.class);
		if (sfcdto.isEmpty()) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
		return new ResponseEntity<>(sfcdto, HttpStatus.OK);
	}

	@RequestMapping(value = "/createFC", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<FCDTO> CreateFC(@RequestBody FCDTO fcdto, UriComponentsBuilder builder) {
		FCModel fcm = ModelMapperConfig.modelMapper.map(fcdto, FCModel.class);
		FCSv.save(fcm);
		fcdto.setId_fc(fcm.getId_fc());
		HttpHeaders headers = new HttpHeaders();
		headers.setLocation(builder.path("/create/{id_fc}").buildAndExpand(fcdto.getId_fc()).toUri());
		return new ResponseEntity<>(fcdto, HttpStatus.CREATED);
	}

	@RequestMapping(value = "/updateFC", method = RequestMethod.PUT)
	public ResponseEntity<FCDTO> updateFC(@RequestBody FCDTO f) {
		FCModel fc = ModelMapperConfig.modelMapper.map(f, FCModel.class);
		if (fc == null) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
		FCSv.save(fc);
		return new ResponseEntity<>(f, HttpStatus.OK);
	}

	@RequestMapping(value = "/deleteFC/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<FCDTO> deleteFC(@PathVariable("id") String id) {
		FCModel f = FCSv.ListFCById(id);
		FCDTO h = ModelMapperConfig.modelMapper.map(f, FCDTO.class);
		if (f == null) {
			return new ResponseEntity<FCDTO>(h, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		FCSv.delete(f);
		return new ResponseEntity<FCDTO>(h, HttpStatus.OK);
	}

	@RequestMapping(value = "/getFCByMultipleParameter/{status}/{searchValue}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<FCDTO>> getFcByMultipleParameter(@PathVariable("status") String status,
			@PathVariable("searchValue") String searchValue) {
		List<FCModel> em = FCSv.getFCByMultipleParameter(status, searchValue);
		List<FCDTO> edto = ModelMapperConfig.mapList(em, FCDTO.class);

		if (edto.isEmpty()) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
		return new ResponseEntity<>(edto, HttpStatus.OK);
	}

}
