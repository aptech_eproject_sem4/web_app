package c1808g1.aem_api.controller.QuanLyHoSo;


import java.util.ArrayList;
import java.util.List;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import c1808g1.Models.CauHinh.StatusDTO;
import c1808g1.Models.QuanLiHoSo.StatusStudentDTO;
import c1808g1.aem_api.common.ParseDate;
import c1808g1.aem_api.config.ModelMapperConfig;
import c1808g1.aem_api.models.QuanLyHoSo.StatusStudentModel;
import c1808g1.aem_api.models.QuanLyHoSo.StudentModel;
import c1808g1.aem_api.service.CauHinh.StatusService;
import c1808g1.aem_api.service.QuanLyHoSo.StatusStudentService;
import c1808g1.aem_api.service.QuanLyHoSo.StudentService;

@RestController
@RequestMapping("/api/quanlyhoso/status_student_api")
public class StatusStudentController {
	private StatusStudentService ssSv;
	private StatusService statusService;
	private StudentService studentService;

	@Autowired
	public StatusStudentController(StatusStudentService ssSv, StatusService statusService,StudentService studentService) {
		this.ssSv = ssSv;
		this.statusService = statusService;
		this.studentService =  studentService;
	}

	@RequestMapping(value = "/getAll", method = RequestMethod.GET)
	public ResponseEntity<List<StatusStudentDTO>> findAllss() {
		List<StatusStudentModel> listss = ssSv.findAllStatusStudent();
		
		
		var listStatus = statusService.findAllStatus();

		// mapper từ list entity -> list DTO
		List<StatusStudentDTO> lsss = ModelMapperConfig.mapList(listss, StatusStudentDTO.class);

		if (lsss.isEmpty()) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
		return new ResponseEntity<>(lsss, HttpStatus.OK);
	}

	@RequestMapping(value = "/getStatusStudentById/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<StatusStudentDTO> getssById(@PathVariable("id") Integer id) {
		var data = ssSv.findStatusStudentById(id);
		// mapper từ entity -> DTO
		StatusStudentDTO ss = ModelMapperConfig.modelMapper.map(data, StatusStudentDTO.class);

		if (ss == null) {
			return new ResponseEntity<>(ss, HttpStatus.NO_CONTENT);
		}
		return new ResponseEntity<>(ss, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/getLatestStatusStudentByStudentId/{student_id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<StatusStudentDTO> getLatestStatusStudentById(@PathVariable("student_id") String student_id) {
		var data = ssSv.findLatestByStudentId(student_id);
		// mapper từ entity -> DTO
		StatusStudentDTO ss = ModelMapperConfig.modelMapper.map(data, StatusStudentDTO.class);

		if (ss == null) {
			return new ResponseEntity<>(ss, HttpStatus.NO_CONTENT);
		}
		return new ResponseEntity<>(ss, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/getLatestStatusStudentByStudentIdAndStatusId/{student_id}/{status_id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<StatusStudentDTO> getLatestStatusStudentByIdAndStatusId(@PathVariable("student_id") String student_id,@PathVariable("status_id") String status_id) {
		var data = ssSv.findLatestByStudentIdAndStatusId(student_id, status_id);
		// mapper từ entity -> DTO
		StatusStudentDTO ss = ModelMapperConfig.modelMapper.map(data, StatusStudentDTO.class);

		if (ss == null) {
			return new ResponseEntity<>(ss, HttpStatus.NO_CONTENT);
		}
		return new ResponseEntity<>(ss, HttpStatus.OK);
	}

	@RequestMapping(value = "/findStatusStudentByIdStudent/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<StatusStudentDTO>> findStatusStudentByIdStudent(@PathVariable("id") String id) {
		var data = ssSv.findStatusStudentByIdStudent(id);
		// mapper từ entity -> DTO
		List<StatusStudentDTO> ss = ModelMapperConfig.mapList(data, StatusStudentDTO.class);

		if (ss == null) {
			return new ResponseEntity<>(ss, HttpStatus.NO_CONTENT);
		}
		return new ResponseEntity<>(ss, HttpStatus.OK);
	}

	
	@RequestMapping(value = "/create", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<StatusStudentDTO> createss(@RequestBody StatusStudentDTO ss, UriComponentsBuilder builder) {
		// mapper từ DTO -> entity
		
		StatusStudentModel em = ModelMapperConfig.modelMapper.map(ss, StatusStudentModel.class);
		ssSv.save(em);
		ss.setId(String.valueOf(em.getId()));
		
		HttpHeaders headers = new HttpHeaders();
		headers.setLocation(builder.path("/findStatusStudentByIdStudent/{id}").buildAndExpand(ss.getId()).toUri());
		return new ResponseEntity<>(ss, HttpStatus.CREATED);
	}

	@RequestMapping(value = "/update", method = RequestMethod.PUT)
	public ResponseEntity<StatusStudentDTO> updatess(@RequestBody StatusStudentDTO ss) {
		StatusStudentModel statusstudent = ModelMapperConfig.modelMapper.map(ss, StatusStudentModel.class);
		if (statusstudent == null) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
		ssSv.save(statusstudent);
		return new ResponseEntity<>(ss, HttpStatus.OK);
	}

	@RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<StatusStudentDTO> deletess(@PathVariable("id") Integer id) {
		StatusStudentModel ss = ssSv.findStatusStudentById(id);
		StatusStudentDTO h = ModelMapperConfig.modelMapper.map(ss, StatusStudentDTO.class);
		if (ss == null) {
			return new ResponseEntity<StatusStudentDTO>(h, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		ssSv.remove(ss);
		return new ResponseEntity<StatusStudentDTO>(h, HttpStatus.OK);
	}

	@RequestMapping(value = "/getListStudentStatusByStudentId/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<StatusStudentDTO>> getListStudentStatusStudentById(@PathVariable("id") String id) {

		try {
			var data = ModelMapperConfig.mapList(ssSv.findAllStatusStudent(), StatusStudentDTO.class);
			var statusDtos = ModelMapperConfig.mapList(statusService.findAllStatus(), StatusDTO.class);

			var listStatusStudentById = new ArrayList<StatusStudentDTO>();

			for (StatusStudentDTO statusStudentModel : data) {
				if (statusStudentModel.getStudent_id().equals(id))
					listStatusStudentById.add(statusStudentModel);
			}

			for (StatusStudentDTO statusStudentModel : listStatusStudentById) {
				for (StatusDTO statusDTO : statusDtos) {
					if (statusStudentModel.getStatus_id().equals(statusDTO.getId_status()))
						statusStudentModel.setStatusDTO(statusDTO);
					break;
				}
			}

			return new ResponseEntity<>(listStatusStudentById, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);

		}
	}

}
