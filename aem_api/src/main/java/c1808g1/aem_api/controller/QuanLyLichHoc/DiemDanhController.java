package c1808g1.aem_api.controller.QuanLyLichHoc;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;


import c1808g1.Models.QuanLyLichHoc.AttendanceDTO;
import c1808g1.Models.QuanLyLichHoc.AttendanceFCDTO;
import c1808g1.Models.QuanLyLichHoc.AttendanceStudentDTO;
import c1808g1.aem_api.config.ModelMapperConfig;
import c1808g1.aem_api.models.QuanLyLichHoc.AttendanceFCModel;
import c1808g1.aem_api.models.QuanLyLichHoc.AttendanceModel;
import c1808g1.aem_api.models.QuanLyLichHoc.AttendanceStudentModel;
import c1808g1.aem_api.service.QuanLyLichHoc.AttendanceFCService;
import c1808g1.aem_api.service.QuanLyLichHoc.AttendanceService;
import c1808g1.aem_api.service.QuanLyLichHoc.AttendanceStudentService;

@RestController
@RequestMapping("/api/quanlylichhoc/diemdanhapi")
public class DiemDanhController {
	private AttendanceFCService AFCSv;
	private AttendanceService ASv;
	private AttendanceStudentService ASSv;

	@Autowired
	public DiemDanhController(AttendanceFCService AFCSv,AttendanceService ASv,AttendanceStudentService ASSv) {
		this.AFCSv = AFCSv;
		this.ASv = ASv;
		this.ASSv = ASSv;
	}
//	@Autowired
//	public DiemDanhController() {
//		
//	}
//	@Autowired
//	public DiemDanhController() {
//		
//	}
	
	//Attendance
	
	@RequestMapping(value = "/getAllAttendance" , method = RequestMethod.GET , produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<AttendanceDTO>> ListAllAttendance(){
		List<AttendanceModel> am = ASv.ListAllAttendance();
		List<AttendanceDTO> adto =  ModelMapperConfig.mapList(am, AttendanceDTO.class);
		if (adto.isEmpty()) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
		return new ResponseEntity<>(adto,HttpStatus.OK);
	}
	
	@RequestMapping(value = "/getAttendanceById/{id}" , method = RequestMethod.GET , produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<AttendanceDTO> ListAttendanceById(@PathVariable("id") Integer id){
		var data = ASv.ListAttendanceById(id);
		AttendanceDTO adto = ModelMapperConfig.modelMapper.map(data , AttendanceDTO.class);
		if(adto == null){
			return new ResponseEntity<>(adto ,HttpStatus.NO_CONTENT);
		}
		return new ResponseEntity<>(adto ,HttpStatus.OK);
	}
	
	@RequestMapping(value = "/createAttendance" , method = RequestMethod.POST , produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<AttendanceDTO> CreateAttendance(@RequestBody AttendanceDTO adto, UriComponentsBuilder builder){
		AttendanceModel am = ModelMapperConfig.modelMapper.map(adto, AttendanceModel.class);
		ASv.save(am);
		adto.setId(String.valueOf(am.getId()));
		HttpHeaders headers = new HttpHeaders();
		headers.setLocation(builder.path("/create/{id}").buildAndExpand(adto.getId()).toUri());
		return new ResponseEntity<>(adto,HttpStatus.CREATED);
	}

	@RequestMapping(value = "/updateAttendance", method = RequestMethod.PUT)
	public ResponseEntity<AttendanceDTO> updateAttendance(@RequestBody AttendanceDTO a) {
		AttendanceModel attendance = ModelMapperConfig.modelMapper.map(a, AttendanceModel.class);
		if (attendance == null) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
		ASv.save(attendance);
		return new ResponseEntity<>(a, HttpStatus.OK);
	}

	@RequestMapping(value = "/deleteAttendance/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<AttendanceDTO> deleteAttendance(@PathVariable("id") Integer id) {
		AttendanceModel a = ASv.ListAttendanceById(id);
		AttendanceDTO h= ModelMapperConfig.modelMapper.map(a,AttendanceDTO.class);
		if (a == null) {
			return new ResponseEntity<AttendanceDTO>(h,HttpStatus.INTERNAL_SERVER_ERROR);
		}
		ASv.delete(a);
		return new ResponseEntity<AttendanceDTO>(h,HttpStatus.OK);
	}
	
	//AttendanceFC
	
	@RequestMapping(value = "/getAllAttendanceFC" , method = RequestMethod.GET , produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<AttendanceFCDTO>> ListAllAttendanceFC(){
		List<AttendanceFCModel> afcm = AFCSv.ListAllAttendanceFC();
		List<AttendanceFCDTO> afcdto = ModelMapperConfig.mapList(afcm, AttendanceFCDTO.class);
		if (afcdto.isEmpty()) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
		return new ResponseEntity<>(afcdto,HttpStatus.OK);
	}
	
	@RequestMapping(value = "/getAttendanceFCById/{id}" , method = RequestMethod.GET , produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<AttendanceFCDTO> ListAttendanceFCById(@PathVariable("id") Integer id){
		var data = AFCSv.ListAttendanceFCById(id);
		AttendanceFCDTO afcdto = ModelMapperConfig.modelMapper.map(data, AttendanceFCDTO.class);
		if(afcdto == null) {
			return new ResponseEntity<>(afcdto ,HttpStatus.NO_CONTENT);
		}
		return new ResponseEntity<>(afcdto, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/createAttendanceFC" , method = RequestMethod.POST , produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<AttendanceFCDTO> CreateAttendanceFC(@RequestBody AttendanceFCDTO afcdto, UriComponentsBuilder builder){
		AttendanceFCModel afcm = ModelMapperConfig.modelMapper.map(afcdto, AttendanceFCModel.class);
		AFCSv.save(afcm);
		afcdto.setId(String.valueOf(afcm.getId()));
		HttpHeaders headers = new HttpHeaders();
		headers.setLocation(builder.path("/create/{id}").buildAndExpand(afcdto.getId()).toUri());
		return new ResponseEntity<>(afcdto,HttpStatus.CREATED);
	}

	@RequestMapping(value = "/updateAttendanceFC", method = RequestMethod.PUT)
	public ResponseEntity<AttendanceFCDTO> updateAttendanceFC(@RequestBody AttendanceFCDTO afc) {
		AttendanceFCModel attendancefc = ModelMapperConfig.modelMapper.map(afc, AttendanceFCModel.class);
		if (attendancefc == null) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
		AFCSv.save(attendancefc);
		return new ResponseEntity<>(afc, HttpStatus.OK);
	}

	@RequestMapping(value = "/deleteAttendanceFC/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<AttendanceFCDTO> deleteAttendanceFC(@PathVariable("id") Integer id) {
		AttendanceFCModel afc = AFCSv.ListAttendanceFCById(id);
		AttendanceFCDTO h= ModelMapperConfig.modelMapper.map(afc,AttendanceFCDTO.class);
		if (afc == null) {
			return new ResponseEntity<AttendanceFCDTO>(h,HttpStatus.INTERNAL_SERVER_ERROR);
		}
		AFCSv.delete(afc);
		return new ResponseEntity<AttendanceFCDTO>(h,HttpStatus.OK);
	}
	
	//AttendanceStudent
	
	@RequestMapping(value = "/getAllAttendanceStudent" , method = RequestMethod.GET , produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<AttendanceStudentDTO>> ListAllAttendanceStudent(){
		List<AttendanceStudentModel> asm = ASSv.getAllAttendanceStudent();
		List<AttendanceStudentDTO> asdto = ModelMapperConfig.mapList(asm, AttendanceStudentDTO.class);
		if (asdto.isEmpty()) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
		return new ResponseEntity<>(asdto,HttpStatus.OK);
	}
	
	@RequestMapping(value = "/getAttendanceStudentById/{id}" , method = RequestMethod.GET , produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<AttendanceStudentDTO> ListAttendanceStudentById(@PathVariable("id") Integer id){
		var data = ASSv.getAttendanceStudentById(id);
		AttendanceStudentDTO asdto = ModelMapperConfig.modelMapper.map(data , AttendanceStudentDTO.class);
		if(asdto == null){
			return new ResponseEntity<>(asdto ,HttpStatus.NO_CONTENT);
		}
		return new ResponseEntity<>(asdto,HttpStatus.OK);
	}
	
	@RequestMapping(value = "/createAttendanceStudent" , method = RequestMethod.POST , produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<AttendanceStudentDTO> CreateAttendanceStudent(@RequestBody AttendanceStudentDTO asdto, UriComponentsBuilder builder){
		AttendanceStudentModel asm = ModelMapperConfig.modelMapper.map(asdto, AttendanceStudentModel.class);
		ASSv.save(asm);
		asdto.setId(String.valueOf(asm.getId()));
		HttpHeaders headers = new HttpHeaders();
		headers.setLocation(builder.path("/create/{id}").buildAndExpand(asdto.getId()).toUri());
		return new ResponseEntity<>(asdto,HttpStatus.CREATED);
	}

	@RequestMapping(value = "/updateAttendanceStudent", method = RequestMethod.PUT)
	public ResponseEntity<AttendanceStudentDTO> updateAttendanceStudent(@RequestBody AttendanceStudentDTO as) {
		AttendanceStudentModel attendancestudent = ModelMapperConfig.modelMapper.map(as, AttendanceStudentModel.class);
		if (attendancestudent == null) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
		ASSv.save(attendancestudent);
		return new ResponseEntity<>(as, HttpStatus.OK);
	}

	@RequestMapping(value = "/deleteAttendanceStudent/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<AttendanceStudentDTO> deleteAttendanceStudent(@PathVariable("id") Integer id) {
		AttendanceStudentModel as = ASSv.getAttendanceStudentById(id);
		AttendanceStudentDTO h= ModelMapperConfig.modelMapper.map(as,AttendanceStudentDTO.class);
		if (as == null) {
			return new ResponseEntity<AttendanceStudentDTO>(h,HttpStatus.INTERNAL_SERVER_ERROR);
		}
		ASSv.delete(as);
		return new ResponseEntity<AttendanceStudentDTO>(h,HttpStatus.OK);
	}
	
}