package c1808g1.aem_api.controller.QuanLyLichHoc;

import java.text.ParseException;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import c1808g1.Models.QuanLyLichHoc.ScheduleDTO;
import c1808g1.aem_api.config.ModelMapperConfig;
import c1808g1.aem_api.models.QuanLyLichHoc.ScheduleModel;
import c1808g1.aem_api.service.QuanLyLichHoc.ScheduleService;


@RestController
@RequestMapping("/api/quanlylichhoc/lichhocapi")
public class LichHocController {
	private ScheduleService SSv;

	@Autowired
	public LichHocController (ScheduleService SSv) {
		this.SSv = SSv;
	}
	
	@RequestMapping(value = "/getAll" , method = RequestMethod.GET , produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<ScheduleDTO>> ListAllSchedule(){
		List<ScheduleModel> sm = SSv.ListAllSchedule();
		List<ScheduleDTO> sdto = ModelMapperConfig.mapList(sm, ScheduleDTO.class);
		if (sdto.isEmpty()) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
		return new ResponseEntity<>(sdto,HttpStatus.OK);
	}
	
	@RequestMapping(value = "/getScheduleById/{id}" , method = RequestMethod.GET , produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ScheduleDTO> ListScheduleById(@PathVariable("id") Integer id){
		var data = SSv.ListScheduleById(id);
		ScheduleDTO sdto = ModelMapperConfig.modelMapper.map(data , ScheduleDTO.class);
		if(sdto == null){
			return new ResponseEntity<>(sdto ,HttpStatus.NO_CONTENT);
		}
		return new ResponseEntity<>(sdto , HttpStatus.OK);
	}
	
	@RequestMapping(value = "/create" , method = RequestMethod.POST , produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ScheduleDTO> CreateSchedule(@RequestBody ScheduleDTO sdto, UriComponentsBuilder builder){
		ScheduleModel sm = ModelMapperConfig.modelMapper.map(sdto, ScheduleModel.class);
		SSv.save(sm);
		sdto.setId(String.valueOf(sm.getId()));
		HttpHeaders headers = new HttpHeaders();
		headers.setLocation(builder.path("/create/{id}").buildAndExpand(sdto.getId()).toUri());
		return new ResponseEntity<>(sdto,HttpStatus.CREATED);
	}

	@RequestMapping(value = "/update", method = RequestMethod.PUT)
	public ResponseEntity<ScheduleDTO> updates(@RequestBody ScheduleDTO s) {
		ScheduleModel schedule = ModelMapperConfig.modelMapper.map(s, ScheduleModel.class);
		if (schedule == null) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
		SSv.save(schedule);
		return new ResponseEntity<>(s, HttpStatus.OK);
	}

	@RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<ScheduleDTO> deletes(@PathVariable("id") Integer id) {
		ScheduleModel s = SSv.ListScheduleById(id);
		ScheduleDTO h= ModelMapperConfig.modelMapper.map(s,ScheduleDTO.class);
		if (s == null) {
			return new ResponseEntity<ScheduleDTO>(h,HttpStatus.INTERNAL_SERVER_ERROR);
		}
		SSv.delete(s);
		return new ResponseEntity<ScheduleDTO>(h,HttpStatus.OK);
	}
	
	@RequestMapping(value = "/getListScheduleFilterParams", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<ScheduleDTO>> getListScheduleFilterParams(
			@RequestParam(name = "filterClass", required = false) String filterClass,
			@RequestParam(name = "filterFc", required = false) String filterFc) throws ParseException {
		try {
			if (filterClass == null || filterClass.equals(null)) {
				filterClass = "";
			} else {

			}
			if (filterFc == null || filterFc.equals(null)) {
				filterFc = "";
			} else {

			}

			var list = ModelMapperConfig.mapList(SSv.GetListScheduleFilter(filterClass, filterFc), ScheduleDTO.class);

			if (list.isEmpty()) {
				return new ResponseEntity<>(HttpStatus.NO_CONTENT);
			}
			return new ResponseEntity<>(list, HttpStatus.OK);

		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
	}
}
