package c1808g1.aem_api.controller.QuanLyChuongTrinhHoc;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import c1808g1.Models.QuanLyChuongTrinhHoc.ClassDTO;
import c1808g1.Models.QuanLyChuongTrinhHoc.SubjectDTO;
import c1808g1.aem_api.config.ModelMapperConfig;
import c1808g1.aem_api.models.QuanLyChuongTrinhHoc.Class;
import c1808g1.aem_api.models.QuanLyChuongTrinhHoc.Semester;
import c1808g1.aem_api.models.QuanLyChuongTrinhHoc.Subject;
import c1808g1.aem_api.service.QuanLyChuongTrinhHoc.SubjectServices;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/quanlychuongtrinhhoc/subjectapi")
public class MonHocController {
	private SubjectServices subSv;
	
	@Autowired
	public MonHocController(SubjectServices subSv) {
		this.subSv = subSv;
	}
	
	@RequestMapping(value = "/getAll", method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<List<SubjectDTO>> findAllSubject(){
		List<Subject> listsub = subSv.findAllSubject();
		List<SubjectDTO> lssub = ModelMapperConfig.mapList(listsub, SubjectDTO.class);
		if (lssub.isEmpty()) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
		return new ResponseEntity<>(lssub, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/getSubjectOrderByOrderNumberASC", method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<List<SubjectDTO>> getSubjectOrderByOrderNumberASC(){
		List<Subject> listsub = subSv.getSubjectOrderByOrderNumberASC();
		List<SubjectDTO> lssub = ModelMapperConfig.mapList(listsub, SubjectDTO.class);
		if (lssub.isEmpty()) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
		return new ResponseEntity<>(lssub, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/getSubjectById/{id_subject}", method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<SubjectDTO> getSubjectById(@PathVariable("id_subject") String id_subject){
		var data = subSv.findById(id_subject);
		SubjectDTO sub = ModelMapperConfig.modelMapper.map(data, SubjectDTO.class);
		
		if(sub == null) {
			return new ResponseEntity<>(sub, HttpStatus.NO_CONTENT);
		}
		return new ResponseEntity<>(sub, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/checkSubjectByValue/{searchValue}" , method = RequestMethod.GET , produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<SubjectDTO>> checkSubjectByValue(@PathVariable("searchValue") String searchValue){
		List<Subject> sfcm = subSv.checkSubjectByValue(searchValue);
		List<SubjectDTO> sfcdto = ModelMapperConfig.mapList(sfcm, SubjectDTO.class);
		if (sfcdto.isEmpty()) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
		return new ResponseEntity<>(sfcdto,HttpStatus.OK);
	}
	
	@RequestMapping(value = "/getSubjectByMultipleParameter/{idSemester}/{idStatus}/{searchValue}", method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<List<SubjectDTO>> getSubjectByMultipleParameter(@PathVariable("idSemester") Integer idSemester,@PathVariable("idStatus") String idStatus,@PathVariable("searchValue") String searchValue){
		List<Subject> listsub = subSv.getSubjectByMultipleParameter(idSemester, idStatus, searchValue);
		List<SubjectDTO> lssub = ModelMapperConfig.mapList(listsub, SubjectDTO.class);
		if (lssub.isEmpty()) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
		return new ResponseEntity<>(lssub, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/create", method = RequestMethod.POST, produces = "application/json")
	public ResponseEntity<SubjectDTO> createSubject(@RequestBody SubjectDTO sub, UriComponentsBuilder builder){
		Subject SubjectModel = ModelMapperConfig.modelMapper.map(sub, Subject.class);
		subSv.save(SubjectModel);
		sub.setId_subject(SubjectModel.getId_subject());
		HttpHeaders headers = new HttpHeaders();
		headers.setLocation(builder.path("/subject/{id_subject}").buildAndExpand(sub.getId_subject()).toUri());
		return new ResponseEntity<>(sub, HttpStatus.CREATED);
	}
	
	@RequestMapping(value = "/update", method = RequestMethod.PUT)
	public ResponseEntity<SubjectDTO> updates(@RequestBody SubjectDTO s) {
		Subject subject = ModelMapperConfig.modelMapper.map(s, Subject.class);
		if (subject == null) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
		subSv.save(subject);
		return new ResponseEntity<>(s, HttpStatus.OK);
	}

	@RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<SubjectDTO> deletes(@PathVariable("id") String id) {
		Subject s = subSv.findById(id);
		SubjectDTO h= ModelMapperConfig.modelMapper.map(s,SubjectDTO.class);
		if (s == null) {
			return new ResponseEntity<SubjectDTO>(h,HttpStatus.INTERNAL_SERVER_ERROR);
		}
		subSv.remove(s);
		return new ResponseEntity<SubjectDTO>(h,HttpStatus.OK);
	}
}
