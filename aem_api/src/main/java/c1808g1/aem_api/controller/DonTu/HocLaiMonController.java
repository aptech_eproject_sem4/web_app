package c1808g1.aem_api.controller.DonTu;

import java.text.ParseException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import c1808g1.Models.DonTu.FormPayDTO;
import c1808g1.aem_api.config.ModelMapperConfig;
import c1808g1.aem_api.models.DonTu.FormPayModel;
import c1808g1.aem_api.service.DonTu.FormPayService;

@RestController
@RequestMapping("/api/dontu/hoclaimonapi")
public class HocLaiMonController {
	private FormPayService FPSv;

	@Autowired
	public HocLaiMonController (FormPayService FPSv) {
		this.FPSv = FPSv;
	}
	
	@RequestMapping(value = "/getAll" , method = RequestMethod.GET , produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<FormPayDTO>> ListAllFormPay(){
		List<FormPayModel> fpm = FPSv.ListAllFormPay();
		List<FormPayDTO> fpdto = ModelMapperConfig.mapList(fpm, FormPayDTO.class);
		if (fpdto.isEmpty()) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
		return new ResponseEntity<>(fpdto,HttpStatus.OK);
	}
	
	@RequestMapping(value = "/getFormPayById/{id}" , method = RequestMethod.GET , produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<FormPayDTO> ListFormPayById(@PathVariable("id") Integer id){
		var data = FPSv.ListFormPayById(id);
		FormPayDTO fpdto = ModelMapperConfig.modelMapper.map(data, FormPayDTO.class);
		if(fpdto == null){
			return new ResponseEntity<>(fpdto,HttpStatus.NO_CONTENT);
		}
		return new ResponseEntity<>(fpdto,HttpStatus.OK);
	}
	
	@RequestMapping(value = "/create" , method = RequestMethod.POST , produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<FormPayDTO> CreateFP(@RequestBody FormPayDTO fpdto, UriComponentsBuilder builder){
		FormPayModel fpm = ModelMapperConfig.modelMapper.map(fpdto, FormPayModel.class);
		FPSv.save(fpm);
		fpdto.setId(String.valueOf(fpm.getId()));
		HttpHeaders headers = new HttpHeaders();
		headers.setLocation(builder.path("/create/{id}").buildAndExpand(fpdto.getId()).toUri());
		return new ResponseEntity<>(fpdto,HttpStatus.CREATED);
	}

	@RequestMapping(value = "/update", method = RequestMethod.PUT)
	public ResponseEntity<FormPayDTO> updatefp(@RequestBody FormPayDTO fp) {
		FormPayModel formpay = ModelMapperConfig.modelMapper.map(fp, FormPayModel.class);
		if (formpay == null) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
		FPSv.save(formpay);
		return new ResponseEntity<>(fp, HttpStatus.OK);
	}

	@RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<FormPayDTO> deletefp(@PathVariable("id") Integer id) {
		FormPayModel fp = FPSv.ListFormPayById(id);
		FormPayDTO h= ModelMapperConfig.modelMapper.map(fp,FormPayDTO.class);
		if (fp == null) {
			return new ResponseEntity<FormPayDTO>(h,HttpStatus.INTERNAL_SERVER_ERROR);
		}
		FPSv.delete(fp);
		return new ResponseEntity<FormPayDTO>(h,HttpStatus.OK);
	}
}