package c1808g1.aem_api.controller.DonTu;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import c1808g1.Models.DonTu.RegisExamDTO;
import c1808g1.aem_api.config.ModelMapperConfig;
import c1808g1.aem_api.models.DonTu.RegisExamModel;
import c1808g1.aem_api.service.DonTu.RegisExamService;

@RestController
@RequestMapping("/api/dontu/thilaimonapi")
public class ThiLaiMonController {
	private RegisExamService RESv;

	@Autowired
	public ThiLaiMonController (RegisExamService RESv) {
		this.RESv = RESv;
	}
	
	@RequestMapping(value = "/getAll" , method = RequestMethod.GET , produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<RegisExamDTO>> ListAllRegisExam(){
		List<RegisExamModel> rem = RESv.ListAllRegisExam();
		List<RegisExamDTO> redto = ModelMapperConfig.mapList(rem, RegisExamDTO.class);
		if (redto.isEmpty()) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
		return new ResponseEntity<>(redto,HttpStatus.OK);
	}
	
	@RequestMapping(value = "/getRegisExamById/{id}" , method = RequestMethod.GET , produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<RegisExamDTO> ListRegisExamById(@PathVariable("id") Integer id){
		var data = RESv.ListRegisExamById(id);
		RegisExamDTO redto = ModelMapperConfig.modelMapper.map(data, RegisExamDTO.class);
		if(redto == null) {
			return new ResponseEntity<>(redto, HttpStatus.NO_CONTENT);
		}
		return new ResponseEntity<>(redto, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/create" , method = RequestMethod.POST , produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<RegisExamDTO> CreateRE(@RequestBody RegisExamDTO redto, UriComponentsBuilder builder){
		RegisExamModel rem = ModelMapperConfig.modelMapper.map(redto, RegisExamModel.class);
		RESv.save(rem);
		redto.setId(String.valueOf(rem.getId()));
		HttpHeaders headers = new HttpHeaders();
		headers.setLocation(builder.path("/create/{id}").buildAndExpand(redto.getId()).toUri());
		return new ResponseEntity<>(redto,HttpStatus.CREATED);
	}

	@RequestMapping(value = "/update", method = RequestMethod.PUT)
	public ResponseEntity<RegisExamDTO> updatere(@RequestBody RegisExamDTO re) {
		RegisExamModel regis = ModelMapperConfig.modelMapper.map(re, RegisExamModel.class);
		if (regis == null) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
		RESv.save(regis);
		return new ResponseEntity<>(re, HttpStatus.OK);
	}

	@RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<RegisExamDTO> deletere(@PathVariable("id") Integer id) {
		RegisExamModel re = RESv.ListRegisExamById(id);
		RegisExamDTO h= ModelMapperConfig.modelMapper.map(re,RegisExamDTO.class);
		if (re == null) {
			return new ResponseEntity<RegisExamDTO>(h,HttpStatus.INTERNAL_SERVER_ERROR);
		}
		RESv.delete(re);
		return new ResponseEntity<RegisExamDTO>(h,HttpStatus.OK);
	}
}