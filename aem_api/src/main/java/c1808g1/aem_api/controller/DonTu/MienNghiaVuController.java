package c1808g1.aem_api.controller.DonTu;

import java.util.List;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import c1808g1.Models.DonTu.ExemptionMsDTO;
import c1808g1.aem_api.config.ModelMapperConfig;
import c1808g1.aem_api.models.DonTu.ExemptionMs;

import c1808g1.aem_api.service.DonTu.ExemptionMsService;


@RestController
@RequestMapping("/api/dontu/exemptionmsapi")
public class MienNghiaVuController {
	private ExemptionMsService emsSv;

	@Autowired
	public MienNghiaVuController(ExemptionMsService emsSv) {
		this.emsSv = emsSv;
	}

	@RequestMapping(value = "/getAll", method = RequestMethod.GET)
	public ResponseEntity<List<ExemptionMsDTO>> findAllExemptionMs() {
		List<ExemptionMs> listems = emsSv.findAllExemptionMs();

		//mapper từ list entity -> list DTO
		List<ExemptionMsDTO> listemsd = ModelMapperConfig.mapList(listems, ExemptionMsDTO.class);
		if (listemsd.isEmpty()) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
		return new ResponseEntity<>(listemsd, HttpStatus.OK);
	}
	

	@RequestMapping(value = "/getExemptionMsById/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ExemptionMsDTO> getExemptionMsById(@PathVariable("id") Integer id) {
		var data = emsSv.findExemptionMsById(id);
		//mapper từ entity -> DTO
		ExemptionMsDTO emsd = ModelMapperConfig.modelMapper.map(data, ExemptionMsDTO.class);

		if (emsd == null) {
			return new ResponseEntity<>(emsd, HttpStatus.NO_CONTENT);
		}
		return new ResponseEntity<>(emsd, HttpStatus.OK);
	}

	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public ResponseEntity<ExemptionMsDTO> createExemptionMs(@RequestBody ExemptionMsDTO emsd, UriComponentsBuilder builder) {
		ExemptionMs ems = ModelMapperConfig.modelMapper.map(emsd, ExemptionMs.class);
		emsSv.save(ems);
		emsd.setId(String.valueOf(ems.getId()));
		HttpHeaders headers = new HttpHeaders();
		headers.setLocation(builder.path("/emsd/{id}").buildAndExpand(emsd.getId()).toUri());
		return new ResponseEntity<>(emsd, HttpStatus.CREATED);
	}

	@RequestMapping(value = "/update", method = RequestMethod.PUT)
	public ResponseEntity<ExemptionMsDTO> updateex(@RequestBody ExemptionMsDTO ex) {
		ExemptionMs exemption = ModelMapperConfig.modelMapper.map(ex, ExemptionMs.class);
		if (exemption == null) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
		emsSv.save(exemption);
		return new ResponseEntity<>(ex, HttpStatus.OK);
	}

	@RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<ExemptionMsDTO> deleteex(@PathVariable("id") Integer id) {
		ExemptionMs ex = emsSv.findExemptionMsById(id);
		ExemptionMsDTO h= ModelMapperConfig.modelMapper.map(ex,ExemptionMsDTO.class);
		if (ex == null) {
			return new ResponseEntity<ExemptionMsDTO>(h,HttpStatus.INTERNAL_SERVER_ERROR);
		}
		emsSv.remove(ex);
		return new ResponseEntity<ExemptionMsDTO>(h,HttpStatus.OK);
	}

}
