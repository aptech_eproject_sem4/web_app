package c1808g1.aem_api.controller.CauHinh;

import java.sql.Date;
import java.util.List;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import c1808g1.Models.CauHinh.HolidayDTO;
import c1808g1.aem_api.config.ModelMapperConfig;
import c1808g1.aem_api.models.CauHinh.HolidayModel;
import c1808g1.aem_api.service.CauHinh.HolidayService;

@RestController
@RequestMapping("/api/cauhinh/holidayapi")
public class NgayNghiController {
	private HolidayService holiSv;

	@Autowired
	public NgayNghiController(HolidayService holiSv) {
		this.holiSv = holiSv;
	}

	@RequestMapping(value = "/getAll", method = RequestMethod.GET)
	public ResponseEntity<List<HolidayDTO>> findAllholi() {
		List<HolidayModel> listHoli = holiSv.findAllHoliday();
		List<HolidayDTO> lsHoli = ModelMapperConfig.mapList(listHoli, HolidayDTO.class);
		if (lsHoli.isEmpty()) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
		return new ResponseEntity<>(lsHoli, HttpStatus.OK);
	}

	@RequestMapping(value = "/getHolidayById/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<HolidayDTO> getholiById(@PathVariable("id") Integer id) {
		var data = holiSv.findHolidayById(id);
		// mapper từ entity -> DTO
		HolidayDTO holi = ModelMapperConfig.modelMapper.map(data, HolidayDTO.class);

		if (holi == null) {
			return new ResponseEntity<>(holi, HttpStatus.NO_CONTENT);
		}
		return new ResponseEntity<>(holi, HttpStatus.OK);
	}

	@RequestMapping(value = "/getHolidayByDateOff/{date}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<HolidayDTO> getholiByDate_off(@PathVariable("date") Date date) {
		var data = holiSv.findHolidayByDateOff(date);
		// mapper từ entity -> DTO
		HolidayDTO holi = ModelMapperConfig.modelMapper.map(data, HolidayDTO.class);
		if (holi == null) {
			return new ResponseEntity<>(holi, HttpStatus.NO_CONTENT);
		}
		return new ResponseEntity<>(holi, HttpStatus.OK);
	}

	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public ResponseEntity<HolidayDTO> createholi(@RequestBody HolidayDTO holi, UriComponentsBuilder builder) {
		// mapper từ DTO -> entity
		HolidayModel holiModel = ModelMapperConfig.modelMapper.map(holi, HolidayModel.class);
		holiSv.save(holiModel);
		holi.setId(String.valueOf(holiModel.getId()));
		HttpHeaders headers = new HttpHeaders();
		headers.setLocation(builder.path("/holi/{id}").buildAndExpand(holi.getId()).toUri());
		return new ResponseEntity<>(holi, HttpStatus.CREATED);
	}

	@RequestMapping(value = "/update", method = RequestMethod.PUT)
	public ResponseEntity<HolidayDTO> updateholi(@RequestBody HolidayDTO holi) {
		HolidayModel holiday = ModelMapperConfig.modelMapper.map(holi, HolidayModel.class);
		if (holiday == null) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
		holiSv.save(holiday);
		return new ResponseEntity<>(holi, HttpStatus.OK);
	}

	@RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<HolidayDTO> deleteholi(@PathVariable("id") Integer id) {
		HolidayModel holi = holiSv.findHolidayById(id);
		HolidayDTO h= ModelMapperConfig.modelMapper.map(holi,HolidayDTO.class);
		if (holi == null) {
			return new ResponseEntity<HolidayDTO>(h,HttpStatus.INTERNAL_SERVER_ERROR);
		}
		holiSv.remove(holi);
		return new ResponseEntity<HolidayDTO>(h,HttpStatus.OK);
	}
	
	@RequestMapping(value = "/getHolidayBy", method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<List<HolidayDTO>> findListHoliday(){
		List<HolidayModel> listholi = holiSv.DateSort();
		List<HolidayDTO> lsholi = ModelMapperConfig.mapList(listholi, HolidayDTO.class);
		if (lsholi.isEmpty()) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
		return new ResponseEntity<>(lsholi, HttpStatus.OK);
	}
}