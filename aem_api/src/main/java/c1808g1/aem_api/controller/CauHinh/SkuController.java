package c1808g1.aem_api.controller.CauHinh;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import c1808g1.Models.CauHinh.SkuDTO;	
import c1808g1.aem_api.config.ModelMapperConfig;
import c1808g1.aem_api.models.CauHinh.SkuModel;
import c1808g1.aem_api.service.CauHinh.SkuService;


@RestController
@RequestMapping("/api/cauhinh/skuapi")
public class SkuController {
	private SkuService skuSv;
    @Autowired
	public SkuController(SkuService skuSv) {
		this.skuSv = skuSv;
	}
	
	@RequestMapping(value = "/getAll", method = RequestMethod.GET)
	public ResponseEntity<List<SkuDTO>> findAllsku() {
		List<SkuModel> listsku = skuSv.findAllSku();
		// List<SkuDTO> lssku = listsku.stream().map(sku -> ModelMapperConfig.modelMapper.map(sku, SkuDTO.class))
		// 		.collect(Collectors.toList());

		//mapper từ list entity -> list DTO
		List<SkuDTO> lssku = ModelMapperConfig.mapList(listsku, SkuDTO.class);
		if (lssku.isEmpty()) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
		return new ResponseEntity<>(lssku, HttpStatus.OK);
	}

	@RequestMapping(value = "/getSkuById/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<SkuDTO> getskuById(@PathVariable("id") Integer id) {
		var data = skuSv.findSkuById(id);
		//mapper từ entity -> DTO
		SkuDTO sku = ModelMapperConfig.modelMapper.map(data, SkuDTO.class);

		if (sku == null) {
			return new ResponseEntity<>(sku, HttpStatus.NO_CONTENT);
		}
		return new ResponseEntity<>(sku, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/getSkuByType/{type}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<SkuDTO>> getSkuByType(@PathVariable("type") Boolean type) {
		List<SkuModel> listsku = skuSv.getSkuByType(type);
		List<SkuDTO> lssku = ModelMapperConfig.mapList(listsku, SkuDTO.class);
		if (lssku.isEmpty()) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
		return new ResponseEntity<>(lssku, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/create", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<SkuDTO> createsku(@RequestBody SkuDTO sku, UriComponentsBuilder builder) {
		//mapper từ DTO -> entity
		SkuModel skuModel = ModelMapperConfig.modelMapper.map(sku, SkuModel.class);
		skuSv.save(skuModel);
		sku.setId(String.valueOf(skuModel.getId()));
		HttpHeaders headers = new HttpHeaders();
		headers.setLocation(builder.path("/sku/{id}").buildAndExpand(sku.getId()).toUri());
		return new ResponseEntity<>(sku, HttpStatus.CREATED);
	}

	@RequestMapping(value = "/update", method = RequestMethod.PUT)
	public ResponseEntity<SkuDTO> updates(@RequestBody SkuDTO s) {
		SkuModel sku = ModelMapperConfig.modelMapper.map(s, SkuModel.class);
		if (sku == null) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
		skuSv.save(sku);
		return new ResponseEntity<>(s, HttpStatus.OK);
	}

	@RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<SkuDTO> deletes(@PathVariable("id") Integer id) {
		SkuModel s = skuSv.findSkuById(id);
		SkuDTO h= ModelMapperConfig.modelMapper.map(s,SkuDTO.class);
		if (s == null) {
			return new ResponseEntity<SkuDTO>(h,HttpStatus.INTERNAL_SERVER_ERROR);
		}
		skuSv.remove(s);
		return new ResponseEntity<SkuDTO>(h,HttpStatus.OK);
	}
}
