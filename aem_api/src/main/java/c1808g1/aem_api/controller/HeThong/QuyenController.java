package c1808g1.aem_api.controller.HeThong;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;


import c1808g1.Models.HeThong.RoleDTO;
import c1808g1.Models.HeThong.PermissionRoleDTO;
import c1808g1.aem_api.config.ModelMapperConfig;
import c1808g1.aem_api.models.HeThong.RoleModel;
import c1808g1.aem_api.service.HeThong.RoleService;



@RestController
@RequestMapping("/api/hethong/roleapi")
public class QuyenController {
	private RoleService rSv;

	@Autowired
	public QuyenController(RoleService rSv) {
		this.rSv = rSv;
	}

	@RequestMapping(value = "/getAll", method = RequestMethod.GET)
	public ResponseEntity<List<RoleDTO>> findAllRole() {
		List<RoleModel> listr = rSv.findAllRole();

		//mapper từ list entity -> list DTO
		List<RoleDTO> listrd = ModelMapperConfig.mapList(listr, RoleDTO.class);
		if (listrd.isEmpty()) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
		return new ResponseEntity<>(listrd, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/getAllOrderbyASC", method = RequestMethod.GET)
	public ResponseEntity<List<RoleDTO>> getAllOrderbyASC() {
		List<RoleModel> listr = rSv.getAllOrderbyASC();

		//mapper từ list entity -> list DTO
		List<RoleDTO> listrd = ModelMapperConfig.mapList(listr, RoleDTO.class);
		if (listrd.isEmpty()) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
		return new ResponseEntity<>(listrd, HttpStatus.OK);
	}

	@RequestMapping(value = "/getRoleById/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<RoleDTO> getRoleById(@PathVariable("id") Integer id) {
		var data = rSv.findRoleById(id);
		//mapper từ entity -> DTO
		RoleDTO rd = ModelMapperConfig.modelMapper.map(data, RoleDTO.class);

		if (rd == null) {
			return new ResponseEntity<>(rd, HttpStatus.NO_CONTENT);
		}
		return new ResponseEntity<>(rd, HttpStatus.OK);
	}

	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public ResponseEntity<RoleDTO> createRole(@RequestBody RoleDTO rd, UriComponentsBuilder builder) {
		RoleModel r = ModelMapperConfig.modelMapper.map(rd, RoleModel.class);
		rSv.save(r);
		rd.setId(String.valueOf(r.getId()));
		HttpHeaders headers = new HttpHeaders();
		headers.setLocation(builder.path("/rd/{id}").buildAndExpand(rd.getId()).toUri());
		return new ResponseEntity<>(rd, HttpStatus.CREATED);
	}

	@RequestMapping(value = "/update", method = RequestMethod.PUT)
	public ResponseEntity<RoleDTO> updater(@RequestBody RoleDTO r) {
		RoleModel role = ModelMapperConfig.modelMapper.map(r, RoleModel.class);
		if (role == null) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
		rSv.save(role);
		return new ResponseEntity<>(r, HttpStatus.OK);
	}

	@RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<RoleDTO> deleter(@PathVariable("id") Integer id) {
		RoleModel r = rSv.findRoleById(id);
		RoleDTO h= ModelMapperConfig.modelMapper.map(r,RoleDTO.class);
		if (r == null) {
			return new ResponseEntity<RoleDTO>(h,HttpStatus.INTERNAL_SERVER_ERROR);
		}
		rSv.remove(r);
		return new ResponseEntity<RoleDTO>(h,HttpStatus.OK);
	}
}
