package c1808g1.aem_api.controller.HeThong;

import java.sql.ResultSet;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import c1808g1.Models.HeThong.PermissionRoleDTO;
import c1808g1.aem_api.config.ModelMapperConfig;
import c1808g1.aem_api.models.HeThong.*;
import c1808g1.aem_api.service.HeThong.PermissionRoleService;

@RestController
@RequestMapping("/api/hethong/permission_role_api")
public class PermissionRoleController {
	private PermissionRoleService prSv;

	@Autowired
	public PermissionRoleController(PermissionRoleService prSv) {
		this.prSv = prSv;
	}

	@RequestMapping(value = "/getAll", method = RequestMethod.GET)
	public ResponseEntity<List<PermissionRoleDTO>> findAllRole() {
		List<PermissionRoleModel> listr = prSv.findAllPermissionRole();

		// mapper từ list entity -> list DTO
		List<PermissionRoleDTO> listrd = ModelMapperConfig.mapList(listr, PermissionRoleDTO.class);
		if (listrd.isEmpty()) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
		return new ResponseEntity<>(listrd, HttpStatus.OK);
	}



	@RequestMapping(value = "/getPermissionRoleById/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<PermissionRoleDTO> getPermissionRoleById(@PathVariable("id") Integer id) {
		var data = prSv.findPermissionRoleById(id);
		//mapper từ entity -> DTO
		PermissionRoleDTO prd = ModelMapperConfig.modelMapper.map(data, PermissionRoleDTO.class);
		if (prd == null) {
			return new ResponseEntity<>(prd, HttpStatus.NO_CONTENT);
		}
		return new ResponseEntity<>(prd, HttpStatus.OK);
	}

	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public ResponseEntity<PermissionRoleDTO> createPermissionRole(@RequestBody PermissionRoleDTO prd, UriComponentsBuilder builder) {
		PermissionRoleModel prm = ModelMapperConfig.modelMapper.map(prd, PermissionRoleModel.class);
		prSv.save(prm);
		prd.setId(String.valueOf(prm.getId()));
		HttpHeaders headers = new HttpHeaders();
		headers.setLocation(builder.path("/prd/{id}").buildAndExpand(prd.getId()).toUri());
		return new ResponseEntity<>(prd, HttpStatus.CREATED);
	}

	@RequestMapping(value = "/update", method = RequestMethod.PUT)
	public ResponseEntity<PermissionRoleDTO> updatpr(@RequestBody PermissionRoleDTO r) {
		PermissionRoleModel prm = ModelMapperConfig.modelMapper.map(r, PermissionRoleModel.class);
		if (prm == null) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
		prSv.save(prm);
		return new ResponseEntity<>(r, HttpStatus.OK);
	}

	@RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<PermissionRoleDTO> deletepr(@PathVariable("id") Integer id) {
		PermissionRoleModel prm = prSv.findPermissionRoleById(id);
		PermissionRoleDTO h= ModelMapperConfig.modelMapper.map(prm,PermissionRoleDTO.class);
		if (prm == null) {
			return new ResponseEntity<PermissionRoleDTO>(h,HttpStatus.INTERNAL_SERVER_ERROR);
		}
		prSv.remove(prm);
		return new ResponseEntity<PermissionRoleDTO>(h,HttpStatus.OK);
	}
	@RequestMapping(value = "/getPermissionByRoleid/{role_id}", method = RequestMethod.GET)
	public ResponseEntity<List<PermissionRoleDTO>> deleteprcontroller(@PathVariable("role_id") Integer role_id) {
		List<PermissionRoleModel> prm = prSv.deletePermissionRoleByRoleid(role_id);
		// mapper từ list entity -> list DTO
		List<PermissionRoleDTO> listrd = ModelMapperConfig.mapList(prm, PermissionRoleDTO.class);
		if (listrd.isEmpty()) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
		return new ResponseEntity<>(listrd, HttpStatus.OK);
	}
	@RequestMapping(value = "/deletePermissionRoleByRoleid/{role_id}", method = RequestMethod.DELETE)
	public ResponseEntity<List<PermissionRoleDTO>> deletePermissionRoleByRoleid(@PathVariable("role_id") Integer role_id) {
		List<PermissionRoleModel> prm = prSv.deletePermissionRoleByRoleid(role_id);
		List<PermissionRoleDTO> listrd = ModelMapperConfig.mapList(prm, PermissionRoleDTO.class);
		if (prm.isEmpty()) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		for(PermissionRoleModel s:prm) {
			prSv.remove(s);
		}
		return new ResponseEntity<>(listrd,HttpStatus.OK);
	}
	
	@RequestMapping(value = "/deletePermissionRoleByControllerid/{controller_id}", method = RequestMethod.DELETE)
	public ResponseEntity<List<PermissionRoleDTO>> deletepr(@PathVariable("controller_id") String controller_id) {
		List<PermissionRoleModel> prm = prSv.deletePermissionRoleByControllerid(controller_id);
		List<PermissionRoleDTO> listrd = ModelMapperConfig.mapList(prm, PermissionRoleDTO.class);
		if (prm.isEmpty()) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		for(PermissionRoleModel s:prm) {
			prSv.remove(s);
		}
		return new ResponseEntity<>(listrd,HttpStatus.OK);
	}
}