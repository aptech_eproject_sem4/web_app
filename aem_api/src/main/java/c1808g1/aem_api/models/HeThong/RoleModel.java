package c1808g1.aem_api.models.HeThong;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
@Entity
@Table(name="role")
public class RoleModel {
	private int id;
	private String name_role;
	private Boolean allow_edit;
	private Boolean allow_delete;
	private int order_number;
	
	public RoleModel() {
		
	}
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id",nullable=false)
	public int getId() {
		return id;
	}
	public void setId(int id) {	
		this.id = id;
	}
	@Column(name="name_role",nullable=false)
	public String getname_role() {
		return name_role;
	}
	public void setname_role(String name_role) {
		this.name_role = name_role;
	}
	@Column(name="allow_edit",nullable=true)
	public Boolean isallow_edit() {
		return allow_edit;
	}
	public void setallow_edit(Boolean allow_edit) {
		this.allow_edit = allow_edit;
	}
	@Column(name="allow_delete",nullable=true)
	public Boolean isallow_delete() {
		return allow_delete;
	}
	public void setallow_delete(Boolean allow_delete) {
		this.allow_delete = allow_delete;
	}
	@Column(name="order_number",nullable=true)
	public int getOrder_number() {
		return order_number;
	}

	public void setOrder_number(int order_number) {
		this.order_number = order_number;
	}
	public RoleModel(int id, String name_role, Boolean allow_edit, Boolean allow_delete,int order_number) {
		super();
		this.id = id;
		this.name_role = name_role;
		this.allow_edit = allow_edit;
		this.allow_delete = allow_delete;
		this.order_number = order_number;
	}
	
}
