package c1808g1.aem_api.models.HeThong;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name="controller")
public class Controller {
	private String idcontroller;
	private String namecontroller;
	private Boolean active;
	private String menuroot;
	private int ordernumber;
	
	@Id
	@Column(name="id_controller",nullable=false)
	public String getIdcontroller() {
		return idcontroller;
	}
	public void setIdcontroller(String idcontroller) {
		this.idcontroller = idcontroller;
	}
	@Column(name="name_controller",nullable=false)
	public String getNamecontroller() {
		return namecontroller;
	}
	public void setNamecontroller(String namecontroller) {
		this.namecontroller = namecontroller;
	}
	@Column(name="active",nullable=true)
	public Boolean getActive() {
		return active;
	}
	public void setActive(Boolean active) {
		this.active = active;
	}
	@Column(name="menu_root",nullable=true)
	public String getMenuroot() {
		return menuroot;
	}	
	public void setMenuroot(String menuroot) {
		this.menuroot = menuroot;
	}
	@Column(name="order_number",nullable=true)
	public int getOrdernumber() {
		return ordernumber;
	}
	public void setOrdernumber(int ordernumber) {
		this.ordernumber = ordernumber;
	}
	
	public Controller() {
	}

}
