package c1808g1.aem_api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AemApiApplication {
	public static void main(String[] args) {
		SpringApplication.run(AemApiApplication.class, args);
	}

}
