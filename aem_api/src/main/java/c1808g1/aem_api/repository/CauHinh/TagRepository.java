package c1808g1.aem_api.repository.CauHinh;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import c1808g1.aem_api.models.CauHinh.TagModel;

@Repository
public interface TagRepository extends JpaRepository<TagModel, String>{
	@Query(value="select * FROM tag_content t WHERE UPPER(t.id_tag) LIKE %?#{[0].toUpperCase()}%",nativeQuery=true)
	List<TagModel> findByIdContaining(@Param("td_tag") String id);
}
