package c1808g1.aem_api.repository.CauHinh;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import c1808g1.aem_api.models.CauHinh.SkuModel;


@Repository
public interface SkuRepository extends JpaRepository<SkuModel, Integer>{
	@Query(value="select * from sku u where u.type =:type", nativeQuery=true)
	List<SkuModel> getSkuByType(@Param("type") Boolean type);
}
