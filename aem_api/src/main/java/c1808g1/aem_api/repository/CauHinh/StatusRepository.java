package c1808g1.aem_api.repository.CauHinh;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import c1808g1.aem_api.models.CauHinh.StatusModel;

@Repository
public interface StatusRepository extends JpaRepository<StatusModel, String>{
	@Query(value="select * from status u where u.group_type =:type and u.active='true' order by u.order_number asc", nativeQuery=true)
	List<StatusModel> findByGroup_TypeActiveTrue(@Param("type") int group_type);
	@Query(value="select * from status u where u.group_type =:type order by u.order_number asc", nativeQuery=true)
	List<StatusModel> findByGroup_Type(@Param("type") int group_type);
	@Query(value = "CALL sp_CauHinh_DanhMucTinhTrang_getStatusByName(:name);", nativeQuery = true)
	StatusModel findByName(@Param("name") String name_status);
}
