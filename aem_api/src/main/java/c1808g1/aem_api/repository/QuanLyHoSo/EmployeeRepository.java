package c1808g1.aem_api.repository.QuanLyHoSo;


import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import c1808g1.aem_api.models.QuanLyChuongTrinhHoc.Subject;
import c1808g1.aem_api.models.QuanLyHoSo.EmployeeModel;


@Repository
public interface EmployeeRepository extends JpaRepository<EmployeeModel,String>{
	@Query(value = "select * from employee s where s.email_emp=:email and s.password=:password", nativeQuery = true)
	EmployeeModel findByEmailAndPassword(@Param("email") String email,@Param("password") String password);
	@Query(value = "select * from employee s where (ISNULL(:status,'')=NULL or s.status=:status)and((ISNULL(:searchValue,'')='0' or s.id_emp LIKE CONCAT('%',:searchValue,'%')) or (ISNULL(:searchValue,'')='0' or s.name_emp LIKE CONCAT('%',:searchValue,'%')) or (ISNULL(:searchValue,'')='0' or s.email_emp LIKE CONCAT('%',:searchValue,'%')) or (ISNULL(:searchValue,'')='0' or s.phone_emp LIKE CONCAT('%',:searchValue,'%')))", nativeQuery = true)
	List<EmployeeModel> getEmployeeByMultipleParameter(@Param("status") Boolean status,@Param("searchValue") String searchValue);
	@Query(value = "select * from employee s where s.email_emp=:email", nativeQuery = true)
	EmployeeModel getEmployeeByEmail(@Param("email") String email);
}