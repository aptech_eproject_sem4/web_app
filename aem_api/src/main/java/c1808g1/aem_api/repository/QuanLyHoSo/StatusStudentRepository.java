package c1808g1.aem_api.repository.QuanLyHoSo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import c1808g1.aem_api.models.QuanLyHoSo.StatusStudentModel;

@Repository
public interface StatusStudentRepository extends JpaRepository<StatusStudentModel, Integer> {
    @Query(value = "select top 1 * from status_student s where s.student_id like :id_student and s.status_id = :id_status ORDER BY s.id desc", nativeQuery = true)
    StatusStudentModel findLatestStatusByStudentIdAndStatusId(@Param("id_student") String id_student, @Param("id_status") String id_status );
    
    @Query(value = "select top 1 * from status_student s where s.student_id like :id_student ORDER BY s.id desc", nativeQuery = true)
    StatusStudentModel findLatestStatusByStudentId(@Param("id_student") String id_student);
    
    @Query(value = "select * from status_student s where s.student_id=:id_student ORDER BY s.id desc", nativeQuery = true)
    List<StatusStudentModel> findStatusStudentByIdStudent(@Param("id_student") String id_student);

}
