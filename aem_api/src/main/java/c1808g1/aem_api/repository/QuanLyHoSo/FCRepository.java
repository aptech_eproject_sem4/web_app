package c1808g1.aem_api.repository.QuanLyHoSo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import c1808g1.aem_api.models.QuanLyHoSo.EmployeeModel;
import c1808g1.aem_api.models.QuanLyHoSo.FCModel;

@Repository
public interface FCRepository extends JpaRepository<FCModel,String>{
	@Query(value = "select * from fc s where s.email_school=:email and s.password=:password order by s.date_create desc", nativeQuery = true)
	FCModel findByEmailAndPassword(@Param("email") String email,@Param("password") String password);
	@Query(value="select * from fc s where (ISNULL(:searchValue,'')='0' or (s.id_fc LIKE CONCAT('%',:searchValue,'%'))) or (ISNULL(:searchValue,'')='0' or (s.name_fc LIKE CONCAT('%',:searchValue,'%'))) order by s.date_create desc",nativeQuery=true)
	List<FCModel> checkFCByValue(@Param("searchValue") String searchValue);
	@Query(value = "select * from fc s where s.email_school=:email order by s.date_create desc", nativeQuery = true)
	FCModel getFCByEmail(@Param("email") String email);
	
	@Query(value = "select * from fc s where  "
			+ "(ISNULL(:status,'')='0' or s.status_id=:status) "
			+ "and((ISNULL(:searchValue,'')='0' or s.id_fc LIKE CONCAT('%',:searchValue,'%'))  "
			+ "or (ISNULL(:searchValue,'')='0' or s.name_fc LIKE CONCAT('%',:searchValue,'%'))  "
			+ "or (ISNULL(:searchValue,'')='0' or s.email_fc LIKE CONCAT('%',:searchValue,'%')) "
			+ "or (ISNULL(:searchValue,'')='0' or s.phone_fc LIKE CONCAT('%',:searchValue,'%')) "
			+ "or (ISNULL(:searchValue,'')='0' or s.email_school LIKE CONCAT('%',:searchValue,'%'))) order by s.date_create desc ", nativeQuery = true)
	List<FCModel> getFCByMultipleParameter(@Param("status") String status,@Param("searchValue") String searchValue);
}	




