package c1808g1.aem_api.repository.QuanLyHoSo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import c1808g1.aem_api.models.QuanLyHoSo.GradeSalaryFcModel;

@Repository
public interface GradeSalaryFcRepository extends JpaRepository<GradeSalaryFcModel, Integer>{
	@Query(value = "select * from grade_salary_fc gsfc where gsfc.fc_id=:id ", nativeQuery = true)
	List<GradeSalaryFcModel> findGradeFCByFCId(@Param("id") String id);
}
