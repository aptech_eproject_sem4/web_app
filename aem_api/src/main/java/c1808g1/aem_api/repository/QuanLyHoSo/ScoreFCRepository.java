package c1808g1.aem_api.repository.QuanLyHoSo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import c1808g1.aem_api.models.QuanLyHoSo.ScoreFCModel;

@Repository
public interface ScoreFCRepository extends JpaRepository<ScoreFCModel, Integer> {
	@Query(value = "select * from score_fc s order by s.date_create desc", nativeQuery = true)
	List<ScoreFCModel> getScoreFCOrderByDatecreateDESC();

	@Query(value = "select * from score_fc s "
			+ "where (ISNULL(:subject_id,'')='0' or s.subject_id=:subject_id ) "
			+ "and (ISNULL(:fc_id,'')='0' or s.fc_id =:fc_id) "
			+ "order by s.date_create desc", nativeQuery = true)
	List<ScoreFCModel> getScoreFCByMultipleParameter(@Param("subject_id") String subject_id,
			@Param("fc_id") String fc_id);
	
	@Query(value = "select * from score_fc "
			+ "where exists (select * from subject where ((ISNULL(:searchValue,'')='' or (subject.id_subject LIKE CONCAT('%',:searchValue,'%')))"
			+ "or (ISNULL(:searchValue,'')='' or (subject.name_subject LIKE CONCAT('%',:searchValue,'%')))"
			+ "or (ISNULL(:searchValue,'')='' or (subject.sort_name LIKE CONCAT('%',:searchValue,'%'))))"
			+ "and subject.id_subject=score_fc.subject_id) "
			+ "or exists (select * from fc "
			+ "where (ISNULL(:searchValue,'')='' "
			+ "or (fc.name_fc LIKE CONCAT('%',:searchValue,'%'))) "
			+ "and fc.id_fc=score_fc.fc_id)"
			+ "order by score_fc.date_create desc", nativeQuery = true)
	List<ScoreFCModel> getScoreFCBySearchValue(@Param("searchValue") String searchValue);
	
	@Query(value = "select * from score_fc "
			+ "where (ISNULL(:subject_id,'')='0' or subject_id =:subject_id) "
			+ "and exists (select * from fc where (ISNULL(:searchValue,'')='0' or (fc.name_fc LIKE CONCAT('%',:searchValue,'%')))"
			+ "and fc.id_fc=score_fc.fc_id) "
			+ "order by score_fc.date_create desc", nativeQuery = true)
	List<ScoreFCModel> getScoreFCBySubjectIdAndSearchValue(@Param("subject_id") String subject_id, @Param("searchValue") String searchValue);
	
	@Query(value = "select * from score_fc "
			+ "where (ISNULL(:fc_id,'')='0' or fc_id =:fc_id) "
			+ "and exists (select * from subject where ((ISNULL(:searchValue,'')='0' or (subject.id_subject LIKE CONCAT('%',:searchValue,'%')))"
			+ "or (ISNULL(:searchValue,'')='0' or (subject.name_subject LIKE CONCAT('%',:searchValue,'%')))"
			+ "or (ISNULL(:searchValue,'')='0' or (subject.sort_name LIKE CONCAT('%',:searchValue,'%'))))"
			+ "and subject.id_subject=score_fc.subject_id) "
			+ "order by score_fc.date_create desc", nativeQuery = true)
	List<ScoreFCModel> getScoreFCByFCIdAndSearchValue(@Param("fc_id") String fc_id,@Param("searchValue") String searchValue);
}
