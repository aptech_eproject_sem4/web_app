package c1808g1.aem_api.repository.QuanLyLichHoc;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.query.Procedure;
import org.springframework.stereotype.Repository;

import c1808g1.aem_api.models.QuanLyLichHoc.ScheduleModel;

@Repository
public interface ScheduleRepository extends JpaRepository<ScheduleModel,Integer>{
	@Procedure(name="getScheduleIndexFilter")
	List<ScheduleModel> getScheduleIndexFilter();
}
