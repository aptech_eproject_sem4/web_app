package c1808g1.aem_api.repository.QuanLyChuongTrinhHoc;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import c1808g1.aem_api.models.QuanLyChuongTrinhHoc.Class;

@Repository
public interface ClassRepository extends JpaRepository<Class, String> {
    @Query(value="select * from class h order by h.date_create desc",nativeQuery = true)
    List<Class> DateSort();
}
