package c1808g1.aem_api.repository.QuanLyChuongTrinhHoc;

import java.sql.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import c1808g1.aem_api.models.CauHinh.StatusModel;
import c1808g1.aem_api.models.QuanLyChuongTrinhHoc.Course;

@Repository
public interface CourseRepository extends JpaRepository<Course, String> {
	@Query(value="select * from course c where c.course_root = ''" ,nativeQuery = true)
	List<Course> findByCourse_root();
	@Query(value="select * from course c where c.course_root <>''",nativeQuery = true)
	List<Course> findByCourseNotNull();
	@Query(value="select * from course c where c.course_root=:courseroot order by c.date_create desc",nativeQuery = true)
	List<Course> findCourseByCourseroot(@Param("courseroot")String courseroot);
	
	@Query(value="select * from course c order by date_create desc" ,nativeQuery = true)
	List<Course> findAllCourseByDateCreate();

}
