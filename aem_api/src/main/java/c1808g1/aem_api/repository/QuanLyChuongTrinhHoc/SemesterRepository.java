package c1808g1.aem_api.repository.QuanLyChuongTrinhHoc;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import c1808g1.aem_api.models.QuanLyChuongTrinhHoc.Course;
import c1808g1.aem_api.models.QuanLyChuongTrinhHoc.Semester;

@Repository
public interface SemesterRepository extends JpaRepository<Semester, Integer> {
	@Query(value="select * from semester s where s.course_id=:idCourse",nativeQuery = true)
	List<Semester> findSemesterByIdcourse(@Param("idCourse")String idCourse);
	
	@Query(value="select * from semester s order by order_number asc",nativeQuery = true)
	List<Semester> findAllSemeByOrderNumber();
}
