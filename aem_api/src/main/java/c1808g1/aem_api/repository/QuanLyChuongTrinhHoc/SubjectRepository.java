package c1808g1.aem_api.repository.QuanLyChuongTrinhHoc;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import c1808g1.aem_api.models.QuanLyChuongTrinhHoc.Subject;

@Repository
public interface SubjectRepository extends JpaRepository<Subject, String> {
	// @Query(value = "CALL
	// sp_QuanLyChuongTrinhHoc_MonHoc_getSubjectByMultipleParam(:idSemester,:idStatus,:searchValue);",
	// nativeQuery = true)
	@Query(value = "select * from subject s where (ISNULL(:idSemester,'')=0 or s.seme_id=:idSemester) and(ISNULL(:idStatus,'')='0' or s.type_subject_id=:idStatus)and((ISNULL(:searchValue,'')='0' or s.id_subject LIKE CONCAT('%',:searchValue,'%')) or (ISNULL(:searchValue,'')='0' or s.name_subject LIKE CONCAT('%',:searchValue,'%')) or (ISNULL(:searchValue,'')='0' or s.sort_name LIKE CONCAT('%',:searchValue,'%')) or (ISNULL(:searchValue,'')='0' or s.note LIKE CONCAT('%',:searchValue,'%'))) order by s.order_number asc", nativeQuery = true)
	List<Subject> getSubjectByMultipleParameter(@Param("idSemester") int idSemester, @Param("idStatus") String idStatus,@Param("searchValue") String searchValue);
	@Query(value ="select * from subject s order by s.order_number asc" , nativeQuery = true)
	List<Subject> getSubjectOrderByOrderNumberASC();
	@Query(value="select * from subject s where (ISNULL(:searchValue,'')='0' or (s.id_subject LIKE CONCAT('%',:searchValue,'%'))) or (ISNULL(:searchValue,'')='0' or (s.name_subject LIKE CONCAT('%',:searchValue,'%'))) or (ISNULL(:searchValue,'')='0' or (s.sort_name LIKE CONCAT('%',:searchValue,'%')))",nativeQuery=true)
	List<Subject> checkSubjectByValue(@Param("searchValue") String searchValue);
}
