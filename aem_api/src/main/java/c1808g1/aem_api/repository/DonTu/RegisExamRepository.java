package c1808g1.aem_api.repository.DonTu;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import c1808g1.aem_api.models.DonTu.RegisExamModel;
import c1808g1.aem_api.models.QuanLyHoSo.FCModel;

@Repository
public interface RegisExamRepository extends JpaRepository<RegisExamModel,Integer>{
//	@Query(value = "select * from registion_exemption_ms s where  "
//			+ "(ISNULL(:student_id,'')='0' or s.student_id=:student_id) "
//			+ "and((ISNULL(:searchValue,'')='0' or s.id_fc LIKE CONCAT('%',:searchValue,'%'))  "
//			+ "or (ISNULL(:searchValue,'')='0' or s.name_fc LIKE CONCAT('%',:searchValue,'%'))  "
//			+ "or (ISNULL(:searchValue,'')='0' or s.email_fc LIKE CONCAT('%',:searchValue,'%')) "
//			+ "or (ISNULL(:searchValue,'')='0' or s.phone_fc LIKE CONCAT('%',:searchValue,'%')) "
//			+ "or (ISNULL(:searchValue,'')='0' or s.email_school LIKE CONCAT('%',:searchValue,'%'))) order by s.date_create desc ", nativeQuery = true)
//	List<FCModel> getFCByMultipleParameter(@Param("student_id") String student_id,@Param("searchValue") String searchValue);
}
