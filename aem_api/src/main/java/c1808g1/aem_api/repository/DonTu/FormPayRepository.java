package c1808g1.aem_api.repository.DonTu;

import java.util.List;

import org.springframework.data.jdbc.repository.query.Query;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import c1808g1.aem_api.models.DonTu.FormPayModel;

@Repository
public interface FormPayRepository extends JpaRepository<FormPayModel, Integer>{
    
}
