package c1808g1.aem_api.repository.HeThong;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;


import c1808g1.aem_api.models.HeThong.RoleModel;

@Repository
public interface RoleRepository extends JpaRepository<RoleModel,Integer>{

	@Query(value = "select * from role r order by r.order_number asc", nativeQuery = true)
	List<RoleModel> getAllOrderbyASC();

}
