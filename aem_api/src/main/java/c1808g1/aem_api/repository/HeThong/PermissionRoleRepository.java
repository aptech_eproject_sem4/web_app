package c1808g1.aem_api.repository.HeThong;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import c1808g1.aem_api.models.HeThong.*;

@Repository
public interface PermissionRoleRepository extends JpaRepository<PermissionRoleModel, Integer>{
	@Query(value="select * from permission_role p where p.role_id=:role_id",nativeQuery=true)
	List<PermissionRoleModel> deletePermissionRoleByRoleid(@Param("role_id") Integer role_id);
	@Query(value="select * from permission_role p where p.controller_id=:controller_id",nativeQuery=true)
	List<PermissionRoleModel> deletePermissionRoleByControllerid(@Param("controller_id") String controller_id);
}
