package c1808g1.aem_api.common;

import java.sql.Date;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class ParseDate {
	// dd//MM/YYYY
	public static Date ParseToSQLDate(String dateData) {
		try {
			java.util.Date utilDate = new SimpleDateFormat("yyyy/MM/dd").parse(dateData);
			return new java.sql.Date(utilDate.getTime());

		} catch (Exception e) {
			e.printStackTrace();
			return new java.sql.Date(Calendar.getInstance().getTime().getTime());
		}
	}

	public static Date ParseToSQLDate1(String dateData) {
		try {
			java.util.Date utilDate = new SimpleDateFormat("dd/MM/yyyy").parse(dateData);
			return new java.sql.Date(utilDate.getTime());

		} catch (Exception e) {
			e.printStackTrace();
			return new java.sql.Date(Calendar.getInstance().getTime().getTime());
		}
	}

	public static java.util.Date ParseToUltilDate(String dateData) {
		try {

			return new SimpleDateFormat("yyyy/MM/dd").parse(dateData);

		} catch (Exception e) {
			return new java.util.Date();
		}
	}

	public static Date getDateTimeNow() {
		Timestamp timestamp = new Timestamp(System.currentTimeMillis());
		return new Date(timestamp.getTime());
	}
}
