package c1808g1.aem_api.common;

import java.util.ArrayList;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class ConverStreamList {
    public static <T> ArrayList<T> 
    getArrayListFromStream(Stream<T> stream) 
    { 
  
        // Convert the Stream to ArrayList 
        ArrayList<T> 
            arrayList = stream 
                            .collect(Collectors 
                            .toCollection(ArrayList::new)); 
  
        // Return the ArrayList 
        return arrayList; 
    } 
}
