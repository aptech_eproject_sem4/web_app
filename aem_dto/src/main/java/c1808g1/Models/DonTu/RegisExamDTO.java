package c1808g1.Models.DonTu;

import lombok.Data;

@Data
public class RegisExamDTO {
	public RegisExamDTO(String id, String class_id, String subject_id, String date_exam, String brand_id, String shift_id,
			String note_status, String type_exam,String disable) {
		super();
		this.id = id;
		this.class_id = class_id;
		this.subject_id = subject_id;
		this.date_exam = date_exam;
		this.brand_id = brand_id;
		this.shift_id = shift_id;
		this.note_status = note_status;
		this.type_exam = type_exam;
		this.disable=disable;
	}
	public RegisExamDTO() {
		super();
	}
	private String id;	
	private String class_id;
	private String subject_id;
	private String date_exam;
	private String brand_id;
	private String shift_id;
	private String note_status;
	private String type_exam;
	private String disable;
}
