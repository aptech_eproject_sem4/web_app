package c1808g1.Models.DonTu;

import java.sql.Date;


import lombok.Data;
@Data
public class TransferClassDTO {
	private String id;
	private String studentid;
	private String classtransfer;
	private String subjectid;
	private String numbertransfer;
	private String pathfile;
	private String creator;
	private String datecreate;
	private String confirmed;
	private String creatorconfirm;
	private String dateconfirm;
	public TransferClassDTO(String id, String studentid, String classtransfer, String subjectid, String numbertransfer,
			String pathfile, String creator, String datecreate, String confirmed, String creatorconfirm, String dateconfirm) {
		super();
		this.id = id;
		this.studentid = studentid;
		this.classtransfer = classtransfer;
		this.subjectid = subjectid;
		this.numbertransfer = numbertransfer;
		this.pathfile = pathfile;
		this.creator = creator;
		this.datecreate = datecreate;
		this.confirmed = confirmed;
		this.creatorconfirm = creatorconfirm;
		this.dateconfirm = dateconfirm;
	}

	public TransferClassDTO() {
		super();
	}
}
