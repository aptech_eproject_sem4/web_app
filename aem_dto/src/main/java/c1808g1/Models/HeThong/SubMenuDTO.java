package c1808g1.Models.HeThong;


import lombok.Data;

@Data
public class SubMenuDTO {
	private String id_per;
	private String id_controller;
	private String name_controller;
	private String list_action;
	private String role_id;
	private String per_index;
	private String per_create;
	private String per_delete;
	private String per_export;

	public SubMenuDTO(String id_per , String role_id , String id_controller, String name_controller, String per_index
	, String per_create, String per_delete, String per_export, String list_action) {
		super();
		this.id_per = id_per;
		this.role_id = role_id;
		this.id_controller = id_controller;
		this.name_controller = name_controller;
		this.per_index = per_index;
		this.per_create = per_create;
		this.per_delete = per_delete;
		this.per_export = per_export;
		this.list_action = list_action;
	}
	public SubMenuDTO() {
		super();
	}
	
}
