package c1808g1.Models.HeThong;

import lombok.Data;

@Data
public class NotifyDTO {
	public NotifyDTO(String id, String url, String receiver_id, String title, String seen, String note,String hide,String date_create) {
		super();
		this.id = id;
		this.url = url;
		this.receiver_id = receiver_id;
		this.title = title;
		this.seen = seen;
		this.note = note;
		this.hide=hide;
		this.date_create=date_create;
	}
	public NotifyDTO() {
		super();
	}
	private String id;
	private String url;
	private String receiver_id;
	private String title;
	private String seen;
	private String note;
	private String hide;
	private String date_create;
}
