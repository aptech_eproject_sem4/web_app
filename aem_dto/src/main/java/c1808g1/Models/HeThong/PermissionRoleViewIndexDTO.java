package c1808g1.Models.HeThong;

import java.util.List;

import lombok.Data;

@Data
public class PermissionRoleViewIndexDTO {
	private String menu_root;
	private String menu_name;
	private String stt;
	private List<SubMenuDTO> list_submenu;
	public PermissionRoleViewIndexDTO(String menu_root , String menu_name , String stt,List<SubMenuDTO> list_submenu) {
		super();
		this.menu_root = menu_root;
		this.menu_name = menu_name;
		this.stt = stt;
		this.list_submenu = list_submenu;
	}
	public PermissionRoleViewIndexDTO() {
		super();
	}
	
}
