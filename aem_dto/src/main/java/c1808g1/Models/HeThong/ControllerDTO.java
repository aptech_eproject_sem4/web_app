package c1808g1.Models.HeThong;

import lombok.Data;

@Data
public class ControllerDTO {
	public ControllerDTO(String idcontroller, String namecontroller, String active, String menuroot,String ordernumber) {
		super();
		this.idcontroller = idcontroller;
		this.namecontroller = namecontroller;
		this.active = active;
		this.menuroot = menuroot;
		this.ordernumber=ordernumber;
	}
	public ControllerDTO() {
		super();
	}
	private String idcontroller;
	private String namecontroller;
	private String active;
	private String menuroot;
	private String ordernumber;
}
