package c1808g1.Models.HeThong;


import lombok.Data;

@Data
public class RoleDTO {
	private String id;
	private String name_role;
	private String allow_edit;
	private String allow_delete;
	private String order_number;
	
	public RoleDTO(String id, String name_role, String allow_edit, String allow_delete, String order_number) {
		super();
		this.id = id;
		this.name_role = name_role;
		this.allow_edit = allow_edit;
		this.allow_delete = allow_delete;
		this.order_number = order_number;
	}
	public RoleDTO() {
		super();
	}
}
