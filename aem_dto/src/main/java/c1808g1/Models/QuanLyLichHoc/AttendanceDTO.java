package c1808g1.Models.QuanLyLichHoc;

import java.sql.Date;

import lombok.Data;

@Data
public class AttendanceDTO {
	private String id;	
	private String schedule_id;
	private String shift_id;
	private String brand_id;
	private String date_attendance;
	private String status_id;
	private String disable;
	private String note;
	public AttendanceDTO(String id, String schedule_id, String shift_id, String brand_id, String date_attendance,
			String status_id, String disable, String note) {
		super();
		this.id = id;
		this.schedule_id = schedule_id;
		this.shift_id = shift_id;
		this.brand_id = brand_id;
		this.date_attendance = date_attendance;
		this.status_id = status_id;
		this.disable = disable;
		this.note = note;
	}
	public AttendanceDTO() {
		super();
	}
}
