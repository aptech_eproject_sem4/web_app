package c1808g1.Models.QuanLyLichHoc;

import java.sql.Date;

import lombok.Data;

@Data
public class ScheduleDTO {
	private String id;
	private String list_fc;
	private String subject_id;
	private String class_id;
	private String shift_id;
	private String brand_id;
	private String start_date;
	private String end_date;
	private String note;
	private String list_student;
	private String coef_salary;
	private String current_session;
	private String max_session;
	private String number_session;

	public ScheduleDTO(String id, String list_fc, String subject_id, String class_id, String shift_id, String brand_id,
			String start_date, String end_date, String note, String list_student, String coef_salary,
			String current_session, String max_session, String number_session) {
		super();
		this.id = id;
		this.list_fc = list_fc;
		this.subject_id = subject_id;
		this.class_id = class_id;
		this.shift_id = shift_id;
		this.brand_id = brand_id;
		this.start_date = start_date;
		this.end_date = end_date;
		this.note = note;
		this.list_student = list_student;
		this.coef_salary = coef_salary;
		this.current_session = current_session;
		this.max_session = max_session;
		this.number_session = number_session;
	}

	public ScheduleDTO() {
		super();
	}
}
