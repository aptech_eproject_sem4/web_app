package c1808g1.Models.QuanLyLichHoc;

import java.sql.Date;

import lombok.Data;

@Data
public class AttendanceStudentDTO {
	public String id;	
	public String attendance_id;
	public String student_id;
	public String check_in;
	public String check_out;
	public String minute_late;
	public String minute_leave_early;
	public String status_id;
	public String note;
	public AttendanceStudentDTO(String id, String attendance_id, String student_id, String check_in, String check_out,
			String minute_late, String minute_leave_early, String status_id, String note) {
		super();
		this.id = id;
		this.attendance_id = attendance_id;
		this.student_id = student_id;
		this.check_in = check_in;
		this.check_out = check_out;
		this.minute_late = minute_late;
		this.minute_leave_early = minute_leave_early;
		this.status_id = status_id;
		this.note = note;
	}
	public AttendanceStudentDTO() {
		super();
	}
}
