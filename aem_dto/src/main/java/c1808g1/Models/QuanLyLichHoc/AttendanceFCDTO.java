package c1808g1.Models.QuanLyLichHoc;

import lombok.Data;

@Data
public class AttendanceFCDTO {
	private String id;	
	private String attendance_id;
	private String fc_id;
	private String status_id;
	private String note;
	public AttendanceFCDTO(String id, String attendance_id, String fc_id, String status_id, String note) {
		super();
		this.id = id;
		this.attendance_id = attendance_id;
		this.fc_id = fc_id;
		this.status_id = status_id;
		this.note = note;
	}
	public AttendanceFCDTO() {
		super();
	}
}
