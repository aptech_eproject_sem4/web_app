package c1808g1.Models.CauHinh;

import java.sql.Time;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class ShiftStudyDTO {

	private String id;
	private String name_shift;
	private String start_time;
	private String end_time;
	private String allow_late;
	private String allow_leave_early;
	private String even_or_odd;
	private String note;
	
	public ShiftStudyDTO(String id, String name_shift, String start_time, String end_time, String allow_late,
			String allow_leave_early, String even_or_odd, String note) {
		super();
		this.id = id;
		this.name_shift = name_shift;
		this.start_time = start_time;
		this.end_time = end_time;
		this.allow_late = allow_late;
		this.allow_leave_early = allow_leave_early;
		this.even_or_odd = even_or_odd;
		this.note = note;
	}

	public ShiftStudyDTO() {
		super();
	}
	
	
}
