package c1808g1.Models.CauHinh;

import lombok.Data;

@Data
public class HolidayDTO{
    private String id;
	private String name_holiday;
    private String date_off;
    public HolidayDTO(String id, String name_holiday,String date_off) {
        super();
        this.id=id;
        this.name_holiday=name_holiday;
        this.date_off=date_off;
    }
    public HolidayDTO() {
        super();
    }
}
