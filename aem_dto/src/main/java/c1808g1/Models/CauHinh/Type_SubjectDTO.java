package c1808g1.Models.CauHinh;

import lombok.Data;

@Data
public class Type_SubjectDTO {
	private String id;
	private String name_type;
	private String active_type;
	public Type_SubjectDTO(String id, String name_type, String active_type) {
		super();
		this.id = id;
		this.name_type = name_type;
		this.active_type = active_type;
	}
	public Type_SubjectDTO() {
		super();
	}
	
	
}
