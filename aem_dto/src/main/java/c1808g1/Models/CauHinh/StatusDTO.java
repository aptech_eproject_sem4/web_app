package c1808g1.Models.CauHinh;

import lombok.Data;

@Data
public class StatusDTO {
	private String id_status;
	private String name_status;
	private String group_type;
	private String active;
	private String order_number;
	private String color;
	private String note;
	public StatusDTO(String id_status, String name_status, String group_type, String active,String order_number,String color,String note) {
		super();
		this.id_status = id_status;
		this.name_status = name_status;
		this.group_type = group_type;
		this.active = active;
		this.order_number=order_number;
		this.color=color;
		this.note=note;
	}
	public StatusDTO() {
		super();
	}
	
	
}
