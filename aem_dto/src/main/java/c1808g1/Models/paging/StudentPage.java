package c1808g1.Models.paging;

import lombok.Data;

@Data
public class StudentPage {
    
    private Integer page;
    private String course_family;
    private String course_id;
    private String current_class;
    private String form_date;
    private String to_date;
    private String searchValue;
    private String status;
}
