package c1808g1.Models.QuanLyChuongTrinhHoc;

import lombok.Data;

@Data
public class ClassDTO {
	private String id_class;
	private String name_class;
	private String slot_total;
	private String slot_regis;
	private String date_create;
	public ClassDTO(String id_class, String name_class, String slot_total, String slot_regis, String date_create) {
		super();
		this.id_class = id_class;
		this.name_class = name_class;
		this.slot_total = slot_total;
		this.slot_regis = slot_regis;
		this.date_create = date_create;
	}
    public ClassDTO() {
        super();
    }
}
