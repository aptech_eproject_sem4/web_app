package c1808g1.Models.QuanLyChuongTrinhHoc;

import lombok.Data;

@Data
public class SemesterDTO {
	private String id;
	private String name_seme;
	private String course_id;
	private String note;
	private String order_number;
	public SemesterDTO(String id, String name_seme, String course_id, String note,String order_number) {
		super();
		this.id = id;
		this.name_seme = name_seme;
		this.course_id = course_id;
		this.note = note;
		this.order_number=order_number;
	}
    public SemesterDTO() {
        super();
    }
}
