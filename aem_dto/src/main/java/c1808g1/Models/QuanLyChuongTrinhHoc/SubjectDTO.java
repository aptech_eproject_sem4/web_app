package c1808g1.Models.QuanLyChuongTrinhHoc;

import lombok.Data;

@Data
public class SubjectDTO {
	private String id_subject;
	private String name_subject;
	private String sort_name;
	private String hour_study;
	private String seme_id;
	private String number_session;
	private String money_subject;
	private String sku_id;
	private String type_subject_id;
	private String note;
	private String factor;
	private String point;
	private String date_create;
	private String order_number;
	public SubjectDTO(String id_subject, String name_subject, String sort_name, String hour_study, String seme_id,
			String number_session, String money_subject, String sku_id, String type_subject_id, String note, String factor,
			String point,String date_create,String order_number) {
		super();
		this.id_subject = id_subject;
		this.name_subject = name_subject;
		this.sort_name = sort_name;
		this.hour_study = hour_study;
		this.seme_id = seme_id;
		this.number_session = number_session;
		this.money_subject = money_subject;
		this.sku_id = sku_id;
		this.type_subject_id = type_subject_id;
		this.note = note;
		this.factor = factor;
		this.point = point;
		this.date_create=date_create;
		this.order_number=order_number;
	}
	public SubjectDTO() {
		super();
	}
}
