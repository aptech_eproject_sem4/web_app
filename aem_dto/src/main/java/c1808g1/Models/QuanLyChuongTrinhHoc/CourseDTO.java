package c1808g1.Models.QuanLyChuongTrinhHoc;



import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonFormat.Shape;

import lombok.Data;

@Data
public class CourseDTO {
	private String id_course;
	private String name_course;
	private String sort_name;
	private String price_course;
	private String course_root;
	private String note;
	@JsonFormat(pattern="yyyy-MM-dd",shape =Shape.STRING)
	private String date_create;
	public CourseDTO(String id_course, String name_course, String sort_name, String price_course, String course_root,
			String note,String date_create) {
		super();
		this.id_course = id_course;
		this.name_course = name_course;
		this.sort_name = sort_name;
		this.price_course = price_course;
		this.course_root = course_root;
		this.note = note;
		this.date_create=date_create;
	}
    public CourseDTO() {
        super();
    }

}
