package c1808g1.Models.QuanLiHoSo;

import lombok.Data;

@Data
public class ParrentStudentDTO {
	private int id;
	private String name;
	private String job;
	private String address;
	private String phone;
}
