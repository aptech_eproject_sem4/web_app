package c1808g1.Models.QuanLiHoSo;


import c1808g1.Models.CauHinh.StatusDTO;
import lombok.Data;

@Data
public class StatusStudentDTO {
	private String id;
	private String student_id;
	private String status_id;
	private String start_date;
	private String end_date;
	private String note;
	private String creator;
	private String date_create;
	
	//status
	private StatusDTO statusDTO;

	public StatusStudentDTO( String id , String student_id , String status_id , String start_date , String end_date , String note , String creator ,
			String date_create) {
		super();
		this.id = id;
		this.status_id = status_id;
		this.start_date = start_date;
		this.end_date = end_date;
		this.note = note;
		this.creator = creator;
		this.date_create = date_create;
	}
	public StatusStudentDTO() {}
}
