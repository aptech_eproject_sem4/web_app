package c1808g1.Models.QuanLiHoSo;

import lombok.Data;

@Data
public class HosoStudentDTO {
	private String idHoso;
	private String idQuanhe;
	private String nameQuanhe;
	private String nghenghiepQuanhe;
	private String sdtQuanhe;
	private String diachiQuanhe;
	private String macdinhQuanhe;
}
