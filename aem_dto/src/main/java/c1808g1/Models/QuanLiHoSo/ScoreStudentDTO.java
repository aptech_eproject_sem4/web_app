package c1808g1.Models.QuanLiHoSo;

import lombok.Data;

@Data
public class ScoreStudentDTO {
	private String id;
	private String regis_exam_id;
	private String student_id;
	private String pass_exam;
	private String status_id;
	private String score_percent;
	private String score_number;
	private String form_pay_id_refer;
	private String path_file;
	private String creator;
	private String date_create;
	private String note;
	private String disable;
	public ScoreStudentDTO(String id, String regis_exam_id, String student_id, String pass_exam, String status_id,
			String score_percent, String score_number, String type_exam, String path_file, String creator,
			String date_create, String note,String disable) {
		super();
		this.id = id;
		this.regis_exam_id = regis_exam_id;
		this.student_id = student_id;
		this.pass_exam = pass_exam;
		this.status_id = status_id;
		this.score_percent = score_percent;
		this.score_number = score_number;
		this.form_pay_id_refer = form_pay_id_refer;
		this.path_file = path_file;
		this.creator = creator;
		this.date_create = date_create;
		this.note = note;
		this.disable = disable;
	}
	public ScoreStudentDTO() {
		super();
	}
}
