package c1808g1.Models.QuanLiHoSo;

import java.sql.Date;

import lombok.Data;

@Data
public class ScoreFCDTO {
	private String id;
	private String subject_id;
	private String fc_id;
	private String score_percent;
	private String score_number;
	private String date_create;
	
	public ScoreFCDTO(String id, String subject_id, String fc_id, String score_percent, String score_number,
			String date_create) {
		super();
		this.id = id;
		this.subject_id = subject_id;
		this.fc_id = fc_id;
		this.score_percent = score_percent;
		this.score_number = score_number;
		this.date_create = date_create;
	}		
		public ScoreFCDTO() {
			super();
	}
}
