package c1808g1.Models.QuanLiHoSo;

import java.sql.Date;

import lombok.Data;

@Data
public class GradeSalaryFcDTO {
	private String id;
	private String fc_id;
	private String hour_salary;
	private String start_date;
	private String date_create;
	public GradeSalaryFcDTO(String id , String fc_id , String hour_salary , String start_date , String date_create) {
		super();
		this.id = id;
		this.fc_id = fc_id;
		this.hour_salary = hour_salary;
		this.start_date = start_date;
		this.date_create = date_create;
	}
		
	public GradeSalaryFcDTO() {
		super();
	}
}	
