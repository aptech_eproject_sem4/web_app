package aem_trickcode.Enums;

public class Selected_Menu {
    public final static String CENTER = "CENTER";
    public final static String ACADEMICS = "ACADEMICS";
    public final static String EXAMINATION = "EXAMINATION";
    public final static String EMPLOYEE_MANAGEMENT = "EMPLOYEE MANAGEMENT";
    public final static String E_PROJECT = "E-PROJECT";
    public final static String ONLINEVARSITY = "ONLINEVARSITY";
}
