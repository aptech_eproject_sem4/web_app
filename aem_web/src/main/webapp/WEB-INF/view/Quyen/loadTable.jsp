<%@ page contentType="text/html;charset=UTF-8" language="java"
	isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<table id="table" class="table table-bordered table-striped">
	<thead>
		<tr>
			<th class="text-center" width="45">STT</th>
			<th class="text-center" width="45">Tên Quyền</th>
			<!-- 			<th class="text-center" width="45">Cho phép Chỉnh Sửa</th> -->
			<!-- 			<th class="text-center" width="45">Cho phép Xóa</th> -->
			<th class="text-center" width="45">Thứ Tự</th>
			<th class="text-center" width="45">Thao Tác</th>
		</tr>
	</thead>
	<tbody class="tblChiTiet">
		<c:forEach var="item" items="${listR}">
			<tr class="parent">
				<td class="text-center" width="45">${item.id}</td>
				<td class="text-center" width="45">${item.name_role}</td>
				<%-- 				  <td class="text-center" width="45">${item.allow_edit}</td> --"%>
<%-- 				  <td class="text-center" width="45">${item.allow_delete}</td> --%>
				<td class="text-center" width="45">${item.order_number}</td>
				<td class="text-center" width="45"><c:choose>
						<c:when test="${item.allow_edit==true}">
							<button type="button" class="btn-edit"
								onclick="editModal('${item.id}','${item.name_role}','${item.order_number}')">
								<i class="fa fa-pencil"></i>
							</button>
						</c:when>
						<c:otherwise>
						</c:otherwise>
					</c:choose> <c:choose>
						<c:when test="${item.allow_delete==true}">
							<button type="button" class="btn-del"
								onclick="Delete('${item.id}','/Quyen/Delete',function(){search()})">
								<i class="fa fa-trash-o"></i>
							</button>
						</c:when>
						<c:otherwise>
						</c:otherwise>
					</c:choose></td>
			</tr>
		</c:forEach>
	</tbody>

	<tfoot>
		<tr>
			<th class="text-center" width="45">STT</th>
			<th class="text-center" width="45">Tên Quyền</th>
			<!-- 			<th class="text-center" width="45">Cho phép Chỉnh Sửa</th> -->
			<!-- 			<th class="text-center" width="45">Cho phép Xóa</th> -->
			<th class="text-center" width="45">Thứ Tự</th>
			<th class="text-center" width="45">Thao Tác</th>
		</tr>
	</tfoot>
</table>