<%@ page contentType="text/html;charset=UTF-8" language="java"
	isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ include file="../base/_headerMain.jspf"%>
<!-- Content Header (Page header) -->
<section class="content-header">
	<h1>Quản lý Quyền</h1>
<!-- 	<div class="m-auto"> -->
<!-- 		<select class="form-control select2" style="width: 100%;" -->
<!-- 			id="listGroup" name="listGroup"> -->
<!-- 			<option selected="selected" value="0">Chọn Quyền</option> -->
<%-- 			<c:forEach var="item" items="${listR}"> --%>
<%-- 				<option value="${item.id}">${item.name_role}</option> --%>
<%-- 			</c:forEach> --%>
<!-- 		</select> -->
<!-- 	</div> -->
	<div class="wrap-btn-header">
		<button type="button" class="btn bg-maroon" id="btnCreate">
			<i class="fa fa-plus"></i> Thêm
		</button>
	</div>
</section>
<!-- Main content -->
<section class="content">
	<!-- Main row -->
	<div class="row">

		<!--table-->
		<div class="col-lg-12">
			<div class="filter-table"></div>
			<div class="wrap-table" id="getData">
				<%@ include file="loadTable.jsp"%>
			</div>
		</div>
		<!--table-->
		<div></div>
	</div>

</section>
<!-- /.content -->
<%@ include file="modalAction.jsp"%>
<%@ include file="../base/_footer.jspf"%>
<script>
	var modal=$("#modal-roleQ");
	$("#btnCreate").click(function(){
		$("#title-popup").html("Thêm quyền");
		modal.find('input').val(null);
		modal.modal("show");
	});
	
	$("#btnSave").click(function(){
		s_Save('/Quyen/addOrUpdate',$("#form-role"),function(data){
			if(data=="true"){
				ThongBao_ThanhCong("Lưu thành công");
				modal.modal("hide");
				search();
				
			}else{
				Thong_BaoLoi("Lưu thất bại");
				Thong_BaoLoi(data);//thông báo check trùng date off hiện lỗi return
			}			
		},null)
	})
	
	function editModal(id,name_role,order_number){
		$("#title-popup").html("Sửa quyền");
		modal.find("#id").val(id);
		modal.find("#name_role").val(name_role);
		modal.find("#order_number").val(order_number);
		modal.modal("show");
	}
	// function editModal(obj){
	// 	debugger
	// 	var tr=$(obj).closest("tr");
	// 	var id=tr.find("#id").val();
	// 	modal.find("#id").val(id);
	// }
	function search(){
		var url="/Quyen/Index?ajaxLoad=table";
		$("#getData").load(url,function(){
// 			$("#table").DataTable({
// 				ordering: false,
// 			});
		InitDataTable();
			// console.log("đã load lại table");
		})
	}

  </script>