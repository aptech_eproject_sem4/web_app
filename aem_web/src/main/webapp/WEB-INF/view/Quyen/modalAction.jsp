<%@ page contentType="text/html;charset=UTF-8" language="java"
	isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<div class="modal fade" id="modal-roleQ">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"
					aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h4 class="modal-title" id="title-popup">Thêm Quyền</h4>
			</div>
			<div class="modal-body">
				<form id="form-role" action="/Quyen/addOrUpdate" method="POST" enctype="multipart/form-data">
					<input type="hidden" id="id" name="id" >
						<div class="col-lg-6">
							<div class="form-group">
								<label for="name_role" class="required">Tên Quyền</label> <input
									class="form-control" type="text" id="name_role"
									name="name_role" required>
							</div>
						</div>
						
					<input type="hidden" id="allow_edit" name="allow_edit" >
					<input type="hidden" id="allow_delete" name="allow_delete" >
					
						<div class="col-lg-6">
							<div class="form-group">
								<label for="order_number" class="required">Thứ tự</label> <input
									class="form-control" type="text" id="order_number"
									name="order_number" onlynumber required>
							</div>
						</div>
					
					

				</form>

			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Đóng</button>
				<button type="button" class="btn btn-primary" id="btnSave">Lưu</button>
			</div>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>

