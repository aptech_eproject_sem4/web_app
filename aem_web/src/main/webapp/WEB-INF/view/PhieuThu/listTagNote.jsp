<%@ page contentType="text/html;charset=UTF-8" language="java"
	isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<c:forEach var="item" items="${listTag}">
    <div class="item-tag" onclick="ChonTag('${item.name_tag}')">
        <label class="item-key">${item.id_tag}</label>
        <span class="item-name">${item.name_tag}</span>
    </div>
</c:forEach>