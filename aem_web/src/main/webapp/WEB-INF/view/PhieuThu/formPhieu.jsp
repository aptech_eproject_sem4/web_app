<%@ page contentType="text/html;charset=UTF-8" language="java" isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ include file="../base/_headerMain.jspf" %>
<style>
    .wrap-choose-nhanvientag {
    position: absolute;
    background: white;
    box-shadow: 1px 2px 3px 0 rgba(90, 89, 90, 0.5);
    overflow-x: hidden;
    overflow-y: auto;
    z-index: 10000;
    height: 200px;
    top: -177px;
    left: 28px;
    width: 300px;
}
.wrap-choose-nhanvientag > .item-tag{
    padding: 5px;
    cursor: pointer;
    border-bottom: 1px solid #ddd;
}
.wrap-choose-nhanvientag > .item-tag:last-child{
    border-bottom: none;
}
.item-key::after{
    content:" ->";
}
</style>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Chi tiết đăng ký
    </h1>
    <div class="wrap-btn-header d-flex">
        <button type="button" class="btn bg-orange margin" id="btnExport" onclick="CapNhatTrangThai('2')" style="display:${check=="3"?"block":"none"}"><i class="fa fa-check"></i>
            Duyệt và Tải phiếu thu</button>
        <button type="button" class="btn bg-purple margin" id="btnGuiDuyet" onclick="CapNhatTrangThai('1')" style="display:${check=="1"?"block":"none"}"><i class="fa fa-paper-plane"></i>
            Gửi duyệt</button>
        <button type="button" class="btn bg-maroon margin" id="btnLuu" onclick="save()" style="display:${check=="1"?"block":"none"}"><i class="fa fa-save"></i>
            Lưu</button>    
        <button type="button" class="btn bg-maroon margin" id="btnLuu" onclick="save()" style="display:${check=="0"?"block":"none"}"><i class="fa fa-save"></i>
            Lưu</button>                    
        <button type="button" class="btn bg-maroon margin" onclick="ReturnIndex()"><i class="fa fa-arrow-left"></i></button>
    </div>
</section>

<!-- Main content -->
<section class="content">
<form id="form-phieu" method="POST" enctype="multipart/form-data">
        <!-- Main row -->
    <input type="hidden" name="id" id="id" value="${formpay.id}">
    <div class="row">
        <div class="col-lg-2">
            <div class="form-group">
                <label for="">Mã sinh viên</label>
                <input type="text" class="form-control" name="student_id" id="student_id" readonly value="${formpay.student_id}" required>
            </div>                        
        </div>
        <div class="col-lg-3">
            <div class="form-group">
                <label for="">Tên sinh viên</label>
                <button type="button" class="btn bg-olive btn-flat margin showTenSV" id="${btn}">${tensv}</button>
            </div>
        </div>
        <div class="col-lg-2">
            <div class="form-control">
                <label for="">Loại phiếu</label>
                <select class="form-control select2" style="width: 100%;"
                id="type_form" name="type_form" required>
                <option value="">Chọn loại</option>
                <c:forEach var="item" items="${listLoai}">
                    <c:choose>
                        <c:when test="${item.id_status == formpay.type_form}">
                            <option selected="selected" value="${item.id_status}">${item.name_status}
                            </option>
                        </c:when>
                        <c:otherwise>
                            <option value="${item.id_status}">${item.name_status}</option>
                        </c:otherwise>
                    </c:choose>
						
				</c:forEach>
                </select>
            </div>
        </div>
        <div class="col-lg-3">
            <div class="form-group clsHideMon">
                <label for="">Môn thi lại</label>
                <select class="form-control select2" style="width: 100%;"
                id="subject_id" name="subject_id">
                <option valu="">Chọn môn</option>
                <c:forEach var="item" items="${listMon}">
                    <c:choose>
                        <c:when test="${item.id_subject == formpay.subject_id}">
                            <option selected="selected" value="${item.id_subject}">${item.sort_name} - ${item.name_subject}
                            </option>
                        </c:when>
                        <c:otherwise>
                            <option value="${item.id_subject}">${item.sort_name} - ${item.name_subject}</option>
                        </c:otherwise>
                    </c:choose>
						
				</c:forEach>
                </select>
            </div>
        </div>
        <div class="col-lg-2">
            <div class="form-group clsHideMon">
                <label for="">Số lần thi lại môn đã chọn</label>
                <input type="text" class="form-control cur-pointer" id="soLan" readonly style="color:red;font-weight: 700;" value="0">
            </div>
        </div>
        <div class="col-lg-3">
            <div class="form-group clsHideLop">
                <label for="">Lớp muốn chuyển</label>
                <select class="form-control select2" style="width: 100%;"
                    id="class_id" name="class_id">
                    <option value="">Chọn lớp</option>
                    <c:forEach var="item" items="${listLop}">
                        <c:choose>
                            <c:when test="${item.id_class == formpay.class_id}">
                                <option selected="selected" value="${item.id_class}">${item.name_class}
                                </option>
                            </c:when>
                            <c:otherwise>
                                <option value="${item.id_class}">${item.name_class}</option>
                            </c:otherwise>
                        </c:choose>
						
				    </c:forEach>
                </select>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-2"> 
            <label for="">Hình thức thanh toán</label>
            <select class="form-control select2" style="width: 100%;"
                id="type_payment" name="type_payment">
                <option value="">Chọn hình thức</option>
                <option value="CK" ${formpay.type_payment=="CK"?"selected":""}>Chuyển khoản</option>
                <option value="TM" ${formpay.type_payment=="TM"?"selected":""}>Tiền mặt</option>
            </select>
        </div>
        <div class="col-lg-3">
            <div class="form-group">
                <label for="">Số tiền</label>
                <input type="text" class="form-control" name="money" id="money" ismoney value="${formpay.money}" required>
            </div>
        </div>
        <div class="col-lg-7">
            <label for="">Số tiền bằng chữ</label>
            <input type="text" class="form-control" id="tienBangChu" readonly/>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="wrap-choose-nhanvientag" id="recommend_member" style="display:none;">
            
            </div>
            <div class="form-group">
                <label for="">Ghi chú</label>
                <textarea class="form-control" name="note" id="note" onkeypress="ShowTag(event,this)">${formpay.note}</textarea>
            </div>
            
        </div>

    </div>

    </form>
    
</section>
<!-- /.content -->
<div class="modal fade" id="modal-chonSV">
        <div class="modal-dialog modal-lg">
          <div class="modal-content">

            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
              <h4 class="modal-title">Chọn Sinh Viên</h4>
            </div>

            <div class="modal-body">
              
            </div>
          </div>
        </div>
        <!--modal large-->
      </div>
<%@ include file="../base/_footer.jspf" %>
<script>
    var inputBL="";
    var chuoiReplace = "";
    var formPhieu=$("#form-phieu");
    var modalChonSV=$("#modal-chonSV")
    if($("#type_form").val()=="ThiLai"){
        $(".clsHideMon").show();
         $(".clsHideLop").hide();
         checkSoLanThi();
    }else if($("#type_form").val()=="ChuyenLop"){
        $(".clsHideLop").show();
        $(".clsHideMon").hide();
    }else{
        $(".clsHideMon").hide();
        $(".clsHideLop").hide();
    }
    if($("#money").val()!=""){
        TienChu();
    }
    $("#type_form").change(function(){
        if($("#type_form").val()=="ThiLai"){
            $(".clsHideMon").show();
            $(".clsHideLop").hide();
            getMoney("ThiLai");
        }else if($("#type_form").val()=="ChuyenLop"){
            $(".clsHideLop").show();
            $(".clsHideMon").hide();
            getMoney("ChuyenLop");
        }else{
            $(".clsHideMon").hide();
            $(".clsHideLop").hide();
            $("#money").val("");
            $("#tienBangChu").val("");
        }
        var note=$("#note").val();
        if(note!="" && note.indexOf("$") != -1){
            var loai=$("#type_form option:selected");
            var tenLoai="";
            if(loai.val()!=""){
                tenLoai=loai.text();
            }
            var id=formPhieu.find("#student_id").val();
            var ten =formPhieu.find(".showTenSV").text();
            if(ten.toLowerCase()=="chọn sinh viên"){
                ten="";
            }
            note=note.replace("$maSV",id).replace("$tenSV",ten).replace("$loai",tenLoai);
            $("#note").val(note);
        }

    })
    $("#btnChonSV").click(function(){
        var url="/PhieuThu/LoadSinhVien";
        modalChonSV.find(".modal-body").load(url,function(){
            InitDataTable();
            modalChonSV.modal("show");
        })
    })
    $("#money").keyup(function(){
        if($("#money").val()!=""){
            TienChu();
        }
    })
    $(document).on("change","#student_id,#subject_id",function(){
        checkSoLanThi();
    })
    function checkSoLanThi(){
        var idsv=$("#student_id").val();
        var mon=$("#subject_id").val();
        if(idsv!="" && mon !="" && $("#type_form").val()=="ThiLai"){
            $.ajax({
			url: "/PhieuThu/CheckTongLanThi",
			data: 
			{ 	"maSinhVien": idsv,
				"monThi":mon,
			},
			type: "post",
			success: function(data){
				console.log(data);
                $("#soLan").val(data);
            }
			})
		}
    }
    function ChonSV(id,ten){
        formPhieu.find("#student_id").val(id);
        formPhieu.find(".showTenSV").text(ten);
        modalChonSV.modal("hide");
        var note=$("#note").val();
        if(note!="" && note.indexOf("$") != -1){
            var loai=$("#type_form option:selected");
            var tenLoai="";
            if(loai.val()!=""){
                tenLoai=loai.text();
            }
            note=note.replace("$maSV",id).replace("$tenSV",ten).replace("$loai",tenLoai);
            $("#note").val(note);
        }
    }
    function ChonTag(content){
        var note=$("#note").val();
        if(content.indexOf("$") != -1){
            var ma=formPhieu.find("#student_id").val();
            var ten =formPhieu.find(".showTenSV").text();
            if(ten.toLowerCase()=="chọn sinh viên"){
                ten="";
            }
            var loai=$("#type_form option:selected");
            var tenLoai="";
            if(loai.val()!=""){
                tenLoai=loai.text();
            }
            content=content.replace("$maSV",ma).replace("$tenSV",ten).replace("$loai",tenLoai);        
        }
        $("#note").val(note.replace(chuoiReplace,"")+" "+content);
        $("#recommend_member").hide();
        chuoiReplace="";
    }
    function ShowTag(event,obj){
        inputBL = $(obj);
        var txtComment = $(obj).val();
        var char = String.fromCharCode(event.which);
        if (char === '@') {
            var urlLoad = "/PhieuThu/GetListTag";
            $("#recommend_member").load(urlLoad, function () {
                chuoiReplace = char;
                $("#recommend_member").show();
            });
        } else if (txtComment.indexOf("@") != -1) {
            var letter = String.fromCharCode(event.which);
            txtComment += letter;
            var tag = txtComment.substr(txtComment.indexOf("@") + 1);
            chuoiReplace = "@" + tag;
            var urlLoad = "/PhieuThu/GetListTag?maTag=" + tag;
            $("#recommend_member").load(urlLoad, function () {
                $("#recommend_member").show();
            });
        }else{
            $("#recommend_member").hide();
        }
    }
	function save(){
		s_Save("/PhieuThu/addOrUpdate",formPhieu,function(data){
            ThongBao_ThanhCong("Lưu thành công");
            var formUrl=window.location.protocol + "//" + window.location.host+"/PhieuThu/FormPhieu/"+data;
		    window.location.href =formUrl;
        })
	}
    function getMoney(maStt){
        $.ajax({
			url: "/PhieuThu/getMoneyLoaiPhieu",
			data: 
			{ 	"maStt": maStt,
			},
			type: "post",
			success: function(data){
                if(data!="0"){
                    $("#money").val(NumberToMoney(data));
                    TienChu();
                }else{

                }
            }
			})
    }
    function TienChu(){
        var money=parseInt(MoneyToNumber($("#money").val()));
        var tienchu=DocTienBangChu(money);
        $("#tienBangChu").val(tienchu);
    }
    function CapNhatTrangThai(trangThai){
        var id=$("#id").val();
        $.ajax({
			url: "/PhieuThu/CapNhatTrangThaiPhieu",
			data: 
			{ 	"maPhieu": id,
				"trangThai":trangThai,
			},
			type: "post",
			success: function(data){
                if(trangThai=="2"){
                    downloadFile();
                }else{
                    var formUrl=window.location.protocol + "//" + window.location.host+"/PhieuThu/FormPhieu/"+id;
			        window.location.href =formUrl;
                }
				
            }
		})
    }
    function ReturnIndex(){
        var index=window.location.protocol + "//" + window.location.host+"/PhieuThu/Index";
		window.location.href =index;
    }
    function downloadFile() {
        var id=$("#id").val();
        var xhr = new XMLHttpRequest();
        xhr.open('POST', "/PhieuThu/exportPhieu/"+id, true);
        xhr.responseType = 'arraybuffer';
        xhr.onload = function () {
        if (this.status === 200) {
        var filename = "";
        var disposition = xhr.getResponseHeader('Content-Disposition');
        if (disposition && disposition.indexOf('attachment') !== -1) {
            var filenameRegex = /filename[^;=\n]*=((['"]).*?\2|[^;\n]*)/;
            var matches = filenameRegex.exec(disposition);
            if (matches != null && matches[1]) {
            filename = matches[1].replace(/['"]/g, '');
            }
        }
        var type = xhr.getResponseHeader('Content-Type');
        var blob = typeof File === 'function'
            ? new File([this.response], filename, { type: type })
            : new Blob([this.response], { type: type });
        if (typeof window.navigator.msSaveBlob !== 'undefined') {
            // IE workaround for "HTML7007: One or more blob URLs were revoked by closing the blob for which they were created. 
            // These URLs will no longer resolve as the data backing the URL has been freed."
            window.navigator.msSaveBlob(blob, filename);
        } else {
            var URL = window.URL || window.webkitURL;
            var downloadUrl = URL.createObjectURL(blob);
        if (filename) {
            // use HTML5 a[download] attribute to specify filename
            var a = document.createElement("a");
            // safari doesn't support this yet
            if (typeof a.download === 'undefined') {
            window.location = downloadUrl;
            } else {
            a.href = downloadUrl;
            a.download = filename;
            document.body.appendChild(a);
            a.click();
            }
            } else {
            window.location = downloadUrl;
            }
            setTimeout(function () { 
                URL.revokeObjectURL(downloadUrl); 
                var formUrl=window.location.protocol + "//" + window.location.host+"/PhieuThu/FormPhieu/"+id;
			    window.location.href =formUrl;
            }, 100); // cleanup
        }
        }
        };
        xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
        xhr.send($.param({
        
        }));
    }
</script>