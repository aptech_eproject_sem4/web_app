<%@ page contentType="text/html;charset=UTF-8" language="java"
	isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
 <table id="table" class="table table-bordered table-striped">
              <thead>
                <tr>
                  <th width="45" class="text-center">STT</th>
                  <th>Mã sinh viên</th>
                  <th>Tên sinh viên</th>
                  <th>Lớp - Khóa</th>
                  <th>Email school</th>
                  <th>Số điện thoại</th>
                  <th></th>
                </tr>
              </thead>
              <tbody class="tblChiTiet">
              <c:forEach var="item" items="${list}">
                <tr>
                  <td class="text-center stt"></td>
                  <td>${item.id_student}</td>
                  <td>${item.full_name}</td>
                  <td>
                      <c:forEach var="itemLop" items="${listLop}">
                        <c:choose>
                          <c:when test="${item.current_class==itemLop.id_class}">
                            ${itemLop.name_class} -
                          </c:when>
                        </c:choose>
                      </c:forEach>
                      <c:forEach var="itemKhoa" items="${listKhoa}">
                        <c:choose>
                          <c:when test="${item.course_family==itemKhoa.id_course}">
                            ${itemKhoa.sort_name}
                          </c:when>
                        </c:choose>
                      </c:forEach>
                    </td>
                  
                  <td>
                    ${item.email_school}
                  </td>
                  <td>
                    ${item.mobile_phone}
                  </td>
                  <td>
                    <button type="button" class="btn-edit" onclick="ChonSV('${item.id_student}','${item.full_name}')">Chọn</button>
                  </td>
                </tr>
                </c:forEach>
              </tbody>
</table>
