<%@ page contentType="text/html;charset=UTF-8" language="java"
	isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
 <table id="tableNM" class="table table-bordered table-striped">
    <thead>
      <tr>
        <th>Học Kì</th>
        <th>Môn</th>
      </tr>
    </thead>
    <tbody class="tblChiTiet">
    <c:forEach var="itemSemester" items="${lsHocKi}">
        <c:forEach var="itemMon" items="${list}">
            <c:choose>
								<c:when test="${itemSemester.id==itemMon.seme_id }">
                    <tr>
                        <td>
                            ${itemSemester.name_seme}
                        </td>
                        <td>
                            ${itemMon.name_subject}
                        </td>
                    </tr>
                </c:when>
                <c:otherwise>

                </c:otherwise>
            </c:choose>
        </c:forEach>
    </c:forEach>
    </tbody>
</table>
