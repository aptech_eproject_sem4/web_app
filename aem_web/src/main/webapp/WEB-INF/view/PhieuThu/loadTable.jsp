<%@ page contentType="text/html;charset=UTF-8" language="java"
	isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
 <table id="tablePT" class="table table-bordered table-striped">
    <thead>
      <tr>
        <th width="45" class="text-center">STT</th>
        <th>Sinh viên</th>
        <th>Lớp - Khóa</th>
        <th>Loại phiếu</th>
        <th>Người lập</th>
        <th>Người duyệt</th>
        <th>Ghi chú</th>
        <th>Thao tác</th>
      </tr>
    </thead>
    <tbody class="tblChiTiet">
    <c:forEach var="item" items="${list}">
      <tr>
        <td class="text-center stt"></td>
        <c:forEach var="itemSV" items="${listSV}">
            <c:choose>
              <c:when test="${item.student_id==itemSV.id_student}">
              <td>
                <span class="label label-primary">${item.student_id}</span>
                <br>
                <span>${itemSV.full_name}</span> <br>
                <span class="label ttd${item.confirmed}">TT </span>
              </td>
              <td>
                  <c:forEach var="itemLop" items="${listLop}">
                    <c:choose>
                      <c:when test="${itemSV.current_class==itemLop.id_class}">
                        ${itemLop.name_class} -
                      </c:when>
                    </c:choose>
                  </c:forEach>
                  <c:forEach var="itemKhoa" items="${listKhoa}">
                    <c:choose>
                      <c:when test="${itemSV.course_family==itemKhoa.id_course}">
                        ${itemKhoa.sort_name}
                      </c:when>
                    </c:choose>
                  </c:forEach>
              </td>
              </c:when>
            </c:choose>
          </c:forEach>
        <c:forEach var="itemLoai" items="${listLoai}">
          <c:choose>
            <c:when test="${item.type_form==itemLoai.id_status}">
              <td>${itemLoai.name_status}</td>
            </c:when>
          </c:choose>
        </c:forEach>
        <td>
          <span>${item.creator}</span><br>
          <span>${item.date_create}</span>
        </td>
        <td>
          <span>${item.creator_confirm}</span><br>
          <span>${item.date_confirm}</span>
        </td>
        <td><i>${item.note}</i></td>
        <td class="d-flex">
          <a href="/PhieuThu/FormPhieu/${item.id}" style="margin-right:10px;"><button type="button" class="btn-edit" ><i class="fa fa-pencil"></i></button></a>
          <button type="button" class="btn-del" onclick="Delete('${item.id}','/PhieuThu/Delete',function(){search()})" style="display:${item.confirmed=="0"?"block":"none"};"><i class="fa fa-trash-o"></i></button>
        </td>
      </tr>
      </c:forEach>
    </tbody>
</table>
