<%@ page contentType="text/html;charset=UTF-8" language="java"
	isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ include file="../base/_headerMain.jspf"%>
<!-- Content Header (Page header) -->
<section class="content-header">
	<h1>
    Phiếu đăng ký - thu
  </h1>
  <div class="wrap-btn-header d-flex">
    <a href="/PhieuThu/FormPhieu/0"><button type="button" class="btn bg-maroon"><i class="fa fa-plus"></i> Thêm</button></a>
  </div>
</section>

<!-- Main content -->
<section class="content">
	<!-- Main row -->
	<div class="row">

		<!--table-->
		<div class="col-lg-12">
			<div class="filter-table">
        <div class="col-lg-2">
          <div class="form-group">
            <select class="form-control select2" style="width: 100%;"
              id="lop" name="lop">
              <option selected="selected" value="">Chọn lớp</option>
              <c:forEach var="item" items="${listLop}">
              <option value="${item.id_class}">${item.name_class}</option>
              </c:forEach>
            </select>
          </div>
        </div>
			  <div class="col-lg-2">
            <div class="form-group">
                <select class="form-control select2" style="width: 100%;" id="loaiPhieu" name="loaiPhieu">
                    <option selected="selected" value="">Chọn loại phiếu</option>
                    <c:forEach var="item" items="${listLoai}">
                    <option value="${item.id_status}">${item.name_status}</option>
                    </c:forEach>
                </select>
            </div>
        </div>
        <div class="col-lg-2">
            <div class="form-group">
                <select class="form-control select2" style="width: 100%;" id="trangThai" name="trangThai">
                    <option selected="selected" value="">Chọn trạng thái</option>
                    <option value="0">Tạo mới</option>
                    <option value="1">Chờ duyệt</option>
                    <option value="2">Đã in phiếu</option>
                    <option value="3">Đã rớt</option>
                    <option value="4">Đã pass</option>
                </select>
            </div>
        </div>
        <div class="col-lg-2">
            <div class="form-group">
                <input class="form-control"type="text" id="qSearch" name="qSearch" placeholder="Tìm kiếm nhanh">
            </div>
        </div>
        <div class="col-lg-1">
          <button type="button" class="btn bg-maroon btnSearch" onclick="search()"><i class="fa fa-search"></i></button>
        </div>
      </div>

			<div class="wrap-table" id="getData">
				<%@ include file="loadTable.jsp" %>
			</div>
		</div>
	</div>
</section>
<!-- /.content -->
<%@ include file="../base/_footer.jspf"%>
  
<script>
  resetSoThuTuRecordTable();
	function search(){
		var qSearch=$("#qSearch").val().replace("/\ /g","+");
		var url="/PhieuThu/Index?ajaxLoad=table&search="+qSearch
				+"&type_form="+$("#loaiPhieu").val()
				+"&id_class="+$("#lop").val()
        +"&confirm="+$('#trangThai').val();
    $("#getData").load(url,function(){
      resetSoThuTuRecordTable();

    })			
		
	}
</script>