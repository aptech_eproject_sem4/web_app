<%@ page contentType="text/html;charset=UTF-8" language="java" isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ include file="../base/_headerMain.jspf" %>
<section class="content-header">
        <h1>
          Quản lý học kỳ
        </h1>
        <div class="m-auto">
		<select class="form-control select2" style="width: 100%;"
			id="listCourse" name="listCourse">
			<c:forEach var="item" items="${listC}">
				<option value="${item.id_course}">${item.sort_name} - ${item.name_course}</option>
			</c:forEach>
		</select>
		</div>
        <div class="wrap-btn-header">
         	  <button type="button" class="btn bg-maroon" id="btnCreate"><i
		  class="fa fa-plus"></i> Thêm</button>
        </div>
</section>
<!-- Main content -->
  <section class="content">
	<!-- Main row -->
	<div class="row">

	  <!--table-->
	  <div class="col-lg-12">
		<div class="filter-table">

		</div>
		<div class="wrap-table" id="getData">
			<%@ include file="loadTable.jsp" %>
		</div>
		
	  </div>
	  <!--table-->
	  <div>

	  </div>
	</div>

  </section>
  <!-- /.content -->
  <%@ include file="modalAction.jsp" %>
  <%@ include file="../base/_footer.jspf" %>
  
<script>
	var modal=$("#modal-semes");
	$("#btnCreate").click(function(){
		$("#title-popup").html("Thêm học kỳ");
		var error = $("label.error");
		$(error).empty();
		$("input.form-control").removeClass("error");
		modal.find('input').val(null);
		modal.modal("show");
	})
	
	$('#listCourse').on('change', function() {
	      search();
	    });
	$("#btnSave").click(function(){
		s_Save('/HocKy/addOrUpdate',$("#form-semes"),function(data){
			if(data=="true"){
				ThongBao_ThanhCong("Lưu thành công");
				modal.modal("hide");
				search();
				
			}else{
				Thong_BaoLoi("Lưu thất bại");
				Thong_BaoLoi(data);//thông báo check trùng date off hiện lỗi return
			}			
		},null)
	})
	function editModal(id,nameseme,courseid,ordernumber,note){
		$("#title-popup").html("Sửa học kỳ");
		modal.find("#id").val(id);
		modal.find("#name_seme").val(nameseme);
		modal.find("#course_id").val(courseid);
		modal.find("#order_number").val(ordernumber);
		modal.find("#note").val(note);
		modal.modal("show");

		var error = $("label.error");
		$(error).empty();
		$("input.form-control").removeClass("error").addClass("valid");

		var parentNode_courseid=$("#course_id").parent();
		var ulitem_courseid=$(parentNode_courseid).find("span.select2-selection__rendered");
		var arr_option_courseid=$("#course_id option");
		$(ulitem_courseid).empty();
		for(i=0;i<arr_option_courseid.length;i++){
			if($(arr_option_courseid[i]).val()==courseid){
				$(arr_option_courseid[i]).prop("selected", true);
				$(ulitem_courseid).prop("title",$(arr_option_courseid[i]).html());
				$(ulitem_courseid).html($(arr_option_courseid[i]).html());
				break;
			}
		}
		
	}
	// function editModal(obj){
	// 	debugger
	// 	var tr=$(obj).closest("tr");
	// 	var id=tr.find("#id").val();
	// 	modal.find("#id").val(id);
	// }
	function search(){
		var e = document.getElementById("listCourse");
		var value = e.options[e.selectedIndex].value;
		var url="/HocKy/Index?ajaxLoad=table&idCourse="+value;
		$("#getData").load(url,function(){
			InitDataTable();
/* 			$("#table").DataTable({
 				ordering: false, 
			}); */
			// console.log("đã load lại table");
		})
	}
  </script>