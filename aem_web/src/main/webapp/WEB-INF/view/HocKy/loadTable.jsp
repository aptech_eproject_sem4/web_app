<%@ page contentType="text/html;charset=UTF-8" language="java"
	isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
 <table id="table" class="table table-bordered table-striped">
              <thead>
                <tr>
                  <th width="45" class="text-center">STT</th>
                  <th>Tên học kỳ</th>
                  <th>Thứ tự</th>
                  <th>Ghi chú</th>
                  <th>Thao tác</th>
                </tr>
              </thead>
              <tbody class="tblChiTiet">
              <c:forEach var="item" items="${lss}">
                <tr>
                  <td class="text-right stt"></td>
                  <td>
                    ${item.name_seme}
                  </td>
                  <td>
                    ${item.order_number}
                  </td>
                  <td><i>${item.note}</i></td>
                  <td>
                    <button type="button" class="btn-edit" onclick="editModal('${item.id}','${item.name_seme}','${item.course_id}','${item.order_number}','${item.note}')"><i class="fa fa-pencil"></i></button>
                    <button type="button" class="btn-del" onclick="Delete('${item.id}','/HocKy/Delete',function(){search()})"><i class="fa fa-trash-o"></i></button>
                  </td>
                </tr>
                </c:forEach>
              </tbody>
              <tfoot>
				<tr>
					<th width="45" class="text-center">STT</th>                
		            <th>Tên học kỳ</th>
		            <th>Thứ tự</th>
		            <th style="max-width: 200px;">Ghi chú</th>
		            <th>Thao tác</th>
		         </tr>
              </tfoot>
            </table>
