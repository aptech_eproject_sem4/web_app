<%@ page contentType="text/html;charset=UTF-8" language="java" isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
      <!--modal default-->
      <div class="modal fade" id="modal-semes">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title" id="title-popup">Thêm học kỳ</h4>
            </div>
            <div class="modal-body">
            <form id="form-semes" action="/HocKy/addOrUpdate" method="POST" enctype="multipart/form-data">
              <div class="row">
                <div class="col-lg-12">
                  <div class="form-group">
                    <label for="" class="required">Tên học kỳ</label>
                    <input class="form-control" type="text" autocomplete="off" id="name_seme" name="name_seme" required>
                    <input type="hidden" id="id" name="id" >
                  </div>
                <div class="form-group">
								<label for="" class="required">Khóa Học</label> <select
									class="form-control select2" style="width: 100%;" id="course_id" name="course_id" class="required">
									<option selected="selected" value="0">Chọn khóa học</option>
									<c:forEach var="item" items="${listC}">
										<option value="${item.id_course}">${item.sort_name} - ${item.name_course}</option>
									</c:forEach>
								</select>
							</div>
                  <div class="form-group">
                    <label for="" class="required">Thứ tự</label>
                    <input class="form-control" autocomplete="off" id="order_number" name="order_number" placeholder="" onlynumber required></textarea>
                  </div>
                  <div class="form-group">
                    <label for="">Ghi chú</label>
                    <textarea class="form-control" autocomplete="off" id="note" name="note" placeholder="" ></textarea>
                  </div>
                </div>
              </div>
                </form>
            </div>
            
        
            <div class="modal-footer">
              <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Đóng</button>
              <button type="button" class="btn btn-primary" id="btnSave">Lưu</button>
            </div>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!--modal default-->