<%@ page contentType="text/html;charset=UTF-8" language="java"
	isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<table id="table" class="table table-bordered table-striped">
	<thead>
		<tr>
			<th width="45" class="text-center">STT</th>
			<th>Mã lớp</th>
			<th>Tên lớp</th>
			<th>Slot/lớp</th>
			<th>Slot/đăng ký</th>
			<th>Ngày tạo</th>
			<th>Thao tác</th>
		</tr>
	</thead>
	<tbody class="tblChiTiet">
		<c:forEach var="item" items="${listClass}">
			<tr>
				<td class="text-center"></td>
				<td><span class="">${item.id_class}</span></td>
				<td>${item.name_class}</td>
				<td>${item.slot_total}</td>
				<td>${item.slot_regis}</td>
				<td>${item.date_create}</td>
				<td>
					<button type="button" class="btn-edit"
						onclick="editModal('${item.id_class}','${item.name_class}','${item.slot_total}','${item.slot_regis}')">
						<i class="fa fa-pencil"></i>
					</button>
					<button type="button" class="btn-del"
						onclick="DeleteClass('${item.id_class}')">
						<i class="fa fa-trash-o"></i>
					</button>
				</td>
			</tr>
		</c:forEach>
	</tbody>
	<tr>
		<th width="45" class="text-center">STT</th>
		<th>Mã lớp</th>
		<th>Tên lớp</th>
		<th>Slot/lớp</th>
		<th>Slot/đăng ký</th>
		<th>Ngày tạo</th>
		<th>Thao tác</th>
	</tr>
</table>
