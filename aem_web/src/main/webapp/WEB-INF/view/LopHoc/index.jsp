<%@ page contentType="text/html;charset=UTF-8" language="java"
	isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ include file="../base/_headerMain.jspf"%>
<!-- Content Header (Page header) -->
<section class="content-header">
	<h1>Quản lý lớp</h1>
	<div class="wrap-btn-header">
		<button type="button" class="btn bg-maroon" id="btnCreate">
			<i class="fa fa-plus"></i> Thêm
		</button>
	</div>
</section>

<!-- Main content -->
<section class="content">
	<!-- Main row -->
	<div class="row">

		<!--table-->
		<div class="col-lg-12">
			<div class="filter-table"></div>
			<div class="wrap-table" id="getData">
				<%@ include file="loadTable.jsp"%>
			</div>

		</div>
		<!--table-->
		<div></div>
	</div>

</section>
<!-- /.content -->
<%@ include file="modalAction.jsp"%>
<%@ include file="../base/_footer.jspf"%>
<script>
	var modal = $("#modal-lopHoc");
	var checkIdStatus;
	$("#btnCreate").click(function() {
		modal.find('input').val(null);
		$("#id_class").prop("disabled", false);
		$("#date_create").prop("disabled", false);
		var error = $("label.error");
		$(error).empty();
		$("input.form-control").removeClass("error");
		$("#title-popup").html("Thêm lớp học");
		modal.modal("show");
	})
	$("#btnSave").click(
			function() {
				if (checkIdStatus == "reject") {
					ThongBao_Loi("Mã đã tồn tại, hãy chọn mã khác!");
				} else if (checkIdStatus == "accept") {
					var total = parseInt($("#slot_total").val());
					var regis = parseInt($("#slot_regis").val());
					if (total < regis) {
						ThongBao_Loi("Total slot phải lớn hơn slot regis!");
					} else {
						s_Save('/LopHoc/addOrUpdate', $("#form-lophoc"),
								function(data) {
									if (data == "true") {
										ThongBao_ThanhCong("Lưu thành công");
										modal.modal("hide");
										search();

									} else {
										Thong_BaoLoi("Lưu thất bại");
										Thong_BaoLoi(data);//thông báo check trùng date off hiện lỗi return
									}
								}, null)
					}
				} else {
					ThongBao_Loi("Có lỗi xảy ra, hãy thử lại!");
				}
			})
	function editModal(idClass, nameClass, slTotal, slRegis) {
		checkIdStatus="accept";
		$("#title-popup").html("Sửa lớp học");
		modal.find("#id_class").val(idClass);
		modal.find("#name_class").val(nameClass);
		modal.find("#slot_total").val(slTotal);
		modal.find("#slot_regis").val(slRegis);
		$("#id_class").prop("disabled", true);
		$("#date_create").prop("disabled", true);
		modal.modal("show");

		var error = $("label.error");
		$(error).empty();
		$("input.form-control").removeClass("error").addClass("valid");
	}
	// function editModal(obj){
	// 	debugger
	// 	var tr=$(obj).closest("tr");
	// 	var id=tr.find("#id").val();
	// 	modal.find("#id").val(id);
	// }
	function DeleteClass(id_class) {
		Delete(id_class, '/LopHoc/Delete', function() {
			modal.remove();
			search();
		})
	}
	function search() {
		var url = "/LopHoc/Index?ajaxLoad=table";
		$("#getData").load(url, function() {
			InitDataTable();
		})
	}
	$('#id_class').change(function() {
		$.ajax({
			url : "/LopHoc/checkIdClass",
			data : {
				"idClass" : $("#id_class").val()
			},
			type : "post",
			success : function(data) {
				if (data == "existed") {
					ThongBao_Loi("Mã đã tồn tại, hãy chọn mã khác!");
					checkIdStatus = "reject";
				} else if (data == "not exist") {
					ThongBao_ThanhCong("Mã chưa tồn tại, có thể tạo mới!");
					checkIdStatus = "accept";
				} else {
					ThongBao_Loi("Có lỗi xảy ra!");
				}
			}
		});
	});

	$('#slot_regis')
			.on(
					'change',
					function() {
						var total = $("#slot_total").val();
						var regis = parseInt($("#slot_regis").val());
						if (!total) {
							ThongBao_Loi("Hãy nhập total slot!");
						} else {
							var totalParse = parseInt(total);
							if (totalParse < regis) {
								ThongBao_Loi("Total slot phải lớn hơn slot regis!");
							} else {
								ThongBao_ThanhCong("Thỏa điều kiện số lượng, hãy tiếp tục công việc!");
							}
						}
					});

	$('#slot_total')
			.on(
					'change',
					function() {
						var regis = $("#slot_regis").val();
						var total = parseInt($("#slot_total").val());
						if (!regis) {
							ThongBao_Loi("Hãy nhập regis slot!");
						} else {
							var regisParse = parseInt(regis);
							if (regisParse > total) {
								ThongBao_Loi("Total slot phải lớn hơn slot regis!");
							} else {
								ThongBao_ThanhCong("Thỏa điều kiện số lượng, hãy tiếp tục công việc!");
							}
						}
					});
</script>