<%@ page contentType="text/html;charset=UTF-8" language="java"
	isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ include file="../base/_headerMain.jspf"%>
<!-- Content Header (Page header) -->
<section class="content-header">
	<h1>Nhóm quyền</h1>
	<div class="m-auto">
		<select class="form-control select2" style="width: 100%;"
			id="listRole" name="listRole">
			<c:forEach var="item" items="${listRole}">
				<option value="${item.id}">${item.name_role}</option>
			</c:forEach>
		</select>
	</div>
	<div class="wrap-btn-header">
		
	</div>
</section>

<!-- Main content -->
<section class="content">
	<!-- Main row -->
	<div class="row">

		<!--table-->
		<div class="col-lg-12">
			<div class="filter-table"></div>

			<div class="wrap-table" id="getData">
				
			</div>
		</div>
	</div>
</section>
<!-- /.content -->
<%@ include file="../base/_footer.jspf"%>
<script>
	search();
	$('#listRole').on('change', function() {
	      search(); // or $(this).val()
	    });
	function search(){
		var value = $("#listRole").val();
		var url='/NhomQuyen/Index?ajaxLoad=table&idRole='+value;
		$("#getData").load(url,function(){
			$("#tablePer tbody tr td:not(.item-per)").remove();
		})
	}

	function updatePer(event,idRole,idController,valueItem){
		var tr=$(event).closest("tr");
		var idPer=tr.attr("data-idper");
		var listAction=tr.attr("data-list");
		var currentRow=$(event).prop("checked");
		if(currentRow==true){
			listAction=listAction+valueItem;
		}else{
			listAction=listAction.replace(valueItem,'');
		}
		tr.attr("data-list",listAction);       
		$.ajax({
			url: "/NhomQuyen/addOrUpdate",
			data: 
			{ 	"idPer": idPer,
				"idRole":idRole,
				"idController":idController,				
				"listAction": listAction,

			},
			type: "post",
			success: function(data){
				tr.attr("data-idper",data);				
				ThongBao_ThanhCong("Cập nhật thành công");
			}
		});
	}
  </script>