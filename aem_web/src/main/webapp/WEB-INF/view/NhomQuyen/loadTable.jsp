<%@ page contentType="text/html;charset=UTF-8" language="java"
	isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<table id="tablePer" class="table table-bordered table-striped">
	<thead>
		<tr>
			<th>Phần quản lý</th>
			<th class="text-center">Quyền Xem</th>
			<th class="text-center">Quyền Tạo mới/Sửa</th>
			<th class="text-center">Quyền Xóa</th>
			<th class="text-center">Quyền Import/Export</th>
		</tr>
	</thead>
	<tbody class="tblChiTiet">
		<c:forEach var="item" items="${listPer}">
			<tr>
				<td class="item-per" style="background-color:#e0e0df; font-weight:bold">${item.menu_name }</td>
				<td colspan="4" class="item-per" style="background-color:#e0e0df;"></td>
				
			</tr>
			<c:forEach var="itemsub" items="${item.list_submenu}">
					<tr data-list="${itemsub.list_action}" data-idper="${itemsub.id_per}">
						<td class="item-per">${itemsub.name_controller}<td>
						<td class="text-center item-per">
							<input type="checkbox" class="checkActive"
										onclick="updatePer(this,'${itemsub.role_id}','${itemsub.id_controller}','Index;')" ${itemsub.per_index=="1"?"checked":""}>
						</td>
						<td class="text-center item-per">
							<input type="checkbox" class="checkActive"
										onclick="updatePer(this,'${itemsub.role_id}','${itemsub.id_controller}','Create;')" ${itemsub.per_create=="1"?"checked":""}>
						</td>
						<td class="text-center item-per">
							<input type="checkbox" class="checkActive"
										onclick="updatePer(this,'${itemsub.role_id}','${itemsub.id_controller}','Delete;')" ${itemsub.per_delete=="1"?"checked":""}>
						</td>
						<td class="text-center item-per">
							<input type="checkbox" class="checkActive"
										onclick="updatePer(this,'${itemsub.role_id}','${itemsub.id_controller}','Export;')" ${itemsub.per_export=="1"?"checked":""}>
						</td>
					</tr>
			</c:forEach>
		</c:forEach>
	</tbody>
</table>
<!--table-->