<%@ page contentType="text/html;charset=UTF-8" language="java"
	isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ include file="../base/_headerMain.jspf"%>
<!-- Content Header (Page header) -->
<section class="content-header">
	<h1>
    Điểm danh
  </h1>
  <div class="wrap-btn-header d-flex">
  </div>
</section>

<!-- Main content -->
<section class="content">
	<!-- Main row -->
	<div class="row">

		<!--table-->
		<div class="col-lg-12">
			<div class="filter-table">
        <div class="col-lg-2">
          <div class="form-group">
            <select class="form-control select2" style="width: 100%;"
              id="lop" name="lop">
              <option selected="selected" value="">Chọn lớp</option>
              <c:forEach var="item" items="${lsLop}">
              <option value="${item.id_class}">${item.name_class}</option>
              </c:forEach>
            </select>
          </div>
        </div>
        <div class="col-lg-2">
            <div class="form-group">
                <select class="form-control select2" style="width: 100%;" id="maCa" name="maCa">
                    <option selected="selected" value="">Chọn ca</option>
                    <c:forEach var="item" items="${lsCa}">
                    <option value="${item.id}">${item.name_shift}</option>
                    </c:forEach>
                </select>
            </div>
        </div>
			  <div class="col-lg-2">
            <div class="form-group">
                <select class="form-control select2" style="width: 100%;" id="chiNhanh" name="chiNhanh">
                    <option selected="selected" value="">Chọn chi nhánh</option>
                    <c:forEach var="item" items="${lsChiNhanh}">
                    <option value="${item.id_status}">${item.name_status}</option>
                    </c:forEach>
                </select>
            </div>
        </div>
        <div class="col-lg-2">
            <div class="form-group">
                <select class="form-control select2" style="width: 100%;" id="maLoai" name="maLoai">
                    <option selected="selected" value="">Chọn loại ngày điểm danh</option>
                    <c:forEach var="item" items="${lsLoai}">
                    <option value="${item.id_status}">${item.name_status}</option>
                    </c:forEach>
                </select>
            </div>
        </div>
        <div class="col-lg-2">
          <button type="button" class="btn bg-maroon btnSearch" onclick="search()"><i class="fa fa-search"></i> Tìm kiếm</button>
        </div>
      </div>

			<div class="wrap-table" id="getData">
				<%@ include file="loadTable.jsp" %>
			</div>
		</div>
	</div>
</section>
<!-- /.content -->
<%@ include file="../base/_footer.jspf"%>
<div class="modal fade" id="modal-listSV">
        <div class="modal-dialog modal-lg">
          <div class="modal-content">

            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
              <h4 class="modal-title">DS sinh viên điểm danh của lớp <span id="showNgay"></span></h4>
            </div>

            <div class="modal-body">
              
            </div>
          </div>
        </div>
        <!--modal large-->
      </div>
<script>
  var modal=$("#modal-listSV");

	function search(){
		var url="/DiemDanh/Index?ajaxLoad=table&maLop="+$("#lop").val()
				+"&chiNhanh="+$("#chiNhanh").val()
				+"&maCa="+$("#maCa").val()
        +"&maLoai="+$('#maLoai').val();
    $("#getData").load(url,function(){
      InitDataTable();

    })			
		
	}
  function showDetail(id,ngay,chuoi){
    var arr=chuoi.split('-');
    var lop=arr[0];
    modal.find("#showNgay").text(lop+"ngày "+ ngay);
    var url="/DiemDanh/DanhSachSinhVien/"+id;
    modal.find(".modal-body").load(url,function(){
      InitDataTable($("#tableSV"));
      modal.modal("show");
    })


  }
</script>