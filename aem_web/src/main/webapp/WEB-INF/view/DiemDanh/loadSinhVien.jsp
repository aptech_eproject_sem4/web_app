<%@ page contentType="text/html;charset=UTF-8" language="java"
	isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<table id="tableSV" class="table table-bordered table-striped">
	<thead>
		<tr>
			<th width="45" class="text-center">STT</th>
			<th>Tên sinh viên</th>
			<th>Thông tin liên hệ</th>
			<th>Trạng thái điểm danh</th>
			<th>Số lần/môn</th>
			<th>Ghi chú</th>
		</tr>
	</thead>
	<tbody class="tblChiTiet">
		<c:forEach var="item" items="${list}">
			<tr>
				<td class="text-center"></td>
				<td>${item.student_id}</td>
				<td>${item.check_in}</td>
				<td>${item.status_id}</td>
				<td>${item.check_out}</td>
				<td>${item.note}</td>
			</tr>
		</c:forEach>
	</tbody>
	<tr>
		<th width="45" class="text-center">STT</th>
		<th>Tên sinh viên</th>
		<th>Thông tin liên hệ</th>
		<th>Trạng thái điểm danh</th>
		<th>Số lần/môn</th>
		<th>Ghi chú</th>
	</tr>
</table>
