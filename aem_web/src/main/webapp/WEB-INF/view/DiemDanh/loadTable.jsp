<%@ page contentType="text/html;charset=UTF-8" language="java"
	isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<table id="table" class="table table-bordered table-striped">
	<thead>
		<tr>
			<th width="45" class="text-center">STT</th>
			<th>Lớp - môn</th>
			<th>Ngày điểm danh</th>
			<th>Ca - chi nhánh</th>
			<th>Loại ngày điểm danh</th>
			<th>Ghi chú</th>
			<th>Thao tác</th>
		</tr>
	</thead>
	<tbody class="tblChiTiet">
		<c:forEach var="item" items="${list}">
			<tr>
				<td class="text-center"></td>
				<td>${item.disable}</td>
				<td>${item.date_attendance}</td>
				<td>${item.shift_id}</td>
				<td>${item.status_id}</td>
				<td>${item.note}</td>
				<td>
					<button type="button" class="btn-edit"
						onclick="showDetail('${item.id}','${item.date_attendance}','${item.disable}')">
						<i class="fa fa-eye"></i>
					</button> 
				</td>
			</tr>
		</c:forEach>
	</tbody>
	<tr>
		<th width="45" class="text-center">STT</th>
		<th>Lớp - môn</th>
		<th>Ngày điểm danh</th>
		<th>Ca - chi nhánh</th>
		<th>Loại ngày điểm danh</th>
		<th>Ghi chú</th>
		<th>Thao tác</th>
	</tr>
</table>
