<%@ page contentType="text/html;charset=UTF-8" language="java"
	isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<table id="tableMH" class="table table-bordered table-striped">
	<thead>
		<tr>
			<th width="45" class="text-center">STT</th>
			<th style="max-width: 200px;">Tên môn học</th>
			<th>Học kỳ</th>
			<th>Tổng giờ - buổi</th>
			<th class="text-right">Giá tiền (VNĐ)</th>
			<th class="text-center">Loại môn học</th>
			<th class="text-center">Thứ tự</th>
			<th>thông tin khác</th>
			<th>ghi chú</th>
			<th>Thao tác</th>
		</tr>
	</thead>
	<tbody class="tblChiTiet">
		<c:forEach var="item" items="${listSubject}">
			<tr>
				<td class="text-center">1</td>
				<td><span class="label label-primary">${item.id_subject }</span>
					<br /> <span>${item.name_subject}-${item.sort_name }</span></td>
				<c:forEach var="itemSem" items="${listSemester}">
					<c:choose>
						<c:when test="${item.seme_id==itemSem.id}">
							<td>${itemSem.name_seme }</td>
						</c:when>
					</c:choose>
				</c:forEach>
				<td>${item.hour_study }-${item.number_session}</td>
				<td class="text-right">${item.money_subject }</td>
				<c:forEach var="itemStat" items="${listStatus}">
					<c:choose>
						<c:when test="${item.type_subject_id==itemStat.id_status}">
							<td class="text-center">${itemStat.name_status}</td>
						</c:when>
					</c:choose>
				</c:forEach>
				<td>${item.order_number }</td>
				<td><span>Thang điểm: ${item.point }</span><br> <span>Hệ
						số: ${item.factor }</span></td>
				<td><i>${item.note}</i></td>
				<td>
					<button type="button" class="btn-edit"
						onclick="editModal('${item.id_subject}','${item.name_subject}','${item.sort_name}','${item.hour_study }','${item.seme_id}','${item.number_session}','${item.money_subject }','${item.sku_id }','${item.type_subject_id }','${item.note }','${item.factor }','${item.point }','${item.order_number }')">
						<i class="fa fa-pencil"></i>
					</button>
					<button type="button" class="btn-del"
						onclick="Delete('${item.id_subject}','/MonHoc/Delete',function(){search()})">
						<i class="fa fa-trash-o"></i>
					</button>
				</td>
			</tr>
		</c:forEach>
	</tbody>
	<tfoot>
		<tr>
			<th width="45" class="text-center">STT</th>
			<th style="max-width: 200px;">Tên môn học</th>
			<th>Học kỳ</th>
			<th>Tổng giờ - buổi</th>
			<th class="text-right">Giá tiền (VNĐ)</th>
			<th class="text-center">Loại môn học</th>
			<th class="text-center">Thứ tự</th>
			<th>thông tin khác</th>
			<th>ghi chú</th>
			<th>Thao tác</th>
		</tr>
	</tfoot>
</table>
<!--table-->