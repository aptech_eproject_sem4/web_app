<%@ page contentType="text/html;charset=UTF-8" language="java"
	isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ include file="../base/_headerMain.jspf"%>
<!-- Content Header (Page header) -->
<section class="content-header">
	<h1>Quản lý môn học</h1>
	<div class="wrap-btn-header">
		<button type="button" class="btn bg-maroon" id="btnCreate">
			<!-- data-toggle="modal" data-target="#modal-default" -->
			<i class="fa fa-plus"></i> Thêm
		</button>
	</div>
</section>

<!-- Main content -->
<section class="content">
	<!-- Main row -->
	<div class="row">

		<!--table-->
		<div class="col-lg-12">
			<div class="filter-table">
				<div class="row">
					<div class="col-lg-2">
						<div class="form-group">
							<select class="form-control select2" style="width: 100%;"
								id="hocky" name="hocky">
								<option selected="selected" value="0">Chọn học kỳ</option>
								<c:forEach var="itemSem" items="${listSemester}">
									<option value="${itemSem.id }">${itemSem.name_seme }</option>
								</c:forEach>
							</select>
						</div>
					</div>
					<div class="col-lg-2">
						<div class="form-group">
							<select class="form-control select2" style="width: 100%;"
								id="loaimonhoc" name="loaimonhoc">
								<option selected="selected" value="0">Chọn loại môn học</option>
								<c:forEach var="itemStat" items="${listStatus}">
									<option value="${itemStat.id_status }">${itemStat.name_status }</option>
								</c:forEach>
							</select>
						</div>
					</div>
					<div class="col-lg-2">
						<div class="form-group">
							<input class="form-control " type="text" id="qSearch"
								name="qSearch" placeholder="Tìm kiếm nhanh">
						</div>
					</div>
					<div class="col-lg-1">
						<button type="button" class="btn bg-maroon btnSearch"
							id="qSearchbtn" onclick="search()">
							<i class="fa fa-search"></i>
						</button>
					</div>
				</div>
			</div>
			<div class="col-lg-12">
				<div class="filter-table"></div>
				<div class="wrap-table" id="getData">
					<%@ include file="loadTable.jsp"%>
				</div>
			</div>
			<div></div>
		</div>
	</div>
</section>
<!-- /.content -->
<%@ include file="modalAction.jsp"%>
<%@ include file="../base/_footer.jspf"%>
<script>
	var modal=$("#modal-monHoc");
	var checkIdStatus;
	$("#btnCreate").click(function(){
		$("#title-popup").html("Thêm môn học");
		modal.find('input').val(null);
		$("#id_subject").prop("disabled", false );
		var error = $("label.error");
		$(error).empty();
		$("input.form-control").removeClass("error");
		modal.modal("show");
	});

	 $('#sku_id').on('change', function() {
		var e1 = document.getElementById("sku_id");
		var value1 = e1.options[e1.selectedIndex].value;
		var value2 = $('#hour_study').val();
		if(value1==null){
			ThongBao_Loi("Hãy chọn danh mục sku để tính số buổi học!");
			}else if(value2==null){
				ThongBao_Loi("Số giờ phải lớn hơn 0!");
				}else{
			$.ajax({
		        url: "/MonHoc/countSession",
		        data: { "idSubject": value1,"hour":$("#hour_study").val() },
		        type: "post",
		        success: function(data){
		           var valSession=parseInt(data);
		           $("#number_session").val(valSession);
		        }
		    });
		}
	}); 


	$('#hour_study').on('change', function() {
		var e1 = document.getElementById("sku_id");
		var value1 = e1.options[e1.selectedIndex].value;
		var value2 = $('#hour_study').val();
		if(value1==null){
			ThongBao_Loi("Hãy chọn danh mục sku để tính số buổi học!");
			}else if(value2==null){
				ThongBao_Loi("Số giờ phải lớn hơn 0!");
				}else{
			$.ajax({
		        url: "/MonHoc/countSession",
		        data: { "idSubject": value1,"hour":$("#hour_study").val() },
		        type: "post",
		        success: function(data){
		           var valSession=parseInt(data);
		           $("#number_session").val(valSession);
		        }
		    });
		}
	});

	$('#number_session').on('change', function() {
		var e1 = document.getElementById("sku_id");
		var value1 = e1.options[e1.selectedIndex].value;
		var value2 = $('#number_session').val();
		if(value1==null){
			ThongBao_Loi("Hãy chọn danh mục sku để tính số giờ!");
			}else if(value2==null){
				ThongBao_Loi("Số buổi phải lớn hơn 0!");
				}else{
			$.ajax({
		        url: "/MonHoc/countHour",
		        data: { "idSubject": value1,"session":$("#number_session").val() },
		        type: "post",
		        success: function(data){
		           var valHour=parseInt(data);
		           $("#hour_study").val(valHour);
		        }
		    });
		}
	});
	
	$("#btnSave").click(function(){
		if(checkIdStatus=="reject"){
			ThongBao_Loi("Mã đã tồn tại, hãy chọn mã khác!");
			}else if(checkIdStatus=="accept"){
			s_Save('/MonHoc/addOrUpdate',$("#form-monhoc"),function(data){
				if(data=="true"){
					ThongBao_ThanhCong("Lưu thành công");
					modal.modal("hide");
					search();
					
				}else{
					Thong_BaoLoi("Lưu thất bại");
					Thong_BaoLoi(data);//thông báo check trùng date off hiện lỗi return
				}			
			},null)
		}else{
ThongBao_Loi("Có lỗi xảy ra, hãy thử lại!");
			}
	});
	
	function editModal(id_subject,name_subject,sort_name,hour_study,seme_id,number_session,money_subject,sku_id,type_subject_id,note,factor,point,order_number){
		checkIdStatus="accept";
		modal.find("#id_subject").val(id_subject);
		modal.find("#name_subject").val(name_subject);
		modal.find("#sort_name").val(sort_name);
		modal.find("#hour_study").val(hour_study);
		modal.find("#seme_id").val(seme_id);
		modal.find("#number_session").val(number_session);
		modal.find("#money_subject").val(money_subject);
		modal.find("#sku_id").val(sku_id);
		modal.find("#type_subject_id").val(type_subject_id);
		modal.find("#note").val(note);
		modal.find("#factor").val(factor);
		modal.find("#point").val(point);
		modal.find("#order_number").val(order_number);
		$("#title-popup").html("Sửa môn học");
		$("#id_subject").prop("disabled", true );
		var error = $("label.error");
		$(error).empty();
		$("input.form-control").removeClass("error").addClass("valid");
		var parentNode_sku_id = $("#sku_id").parent();
		var ulitem_sku_id = $(parentNode_sku_id).find(
				"span.select2-selection__rendered");
		var arr_option_sku_id = $("#sku_id option");
		$(ulitem_sku_id).empty();
		for (i = 0; i < arr_option_sku_id.length; i++) {
			if ($(arr_option_sku_id[i]).val() == sku_id) {
				$(arr_option_sku_id[i]).prop("selected", true);
				$(ulitem_sku_id).prop("title", $(arr_option_sku_id[i]).html());
				$(ulitem_sku_id).html($(arr_option_sku_id[i]).html());
				break;
			}
		}
		var parentNode_seme_id = $("#seme_id").parent();
		var ulitem_seme_id = $(parentNode_seme_id).find(
				"span.select2-selection__rendered");
		var arr_option_seme_id = $("#seme_id option");
		$(ulitem_seme_id).empty();
		for (i = 0; i < arr_option_seme_id.length; i++) {
			if ($(arr_option_seme_id[i]).val() == seme_id) {
				$(arr_option_seme_id[i]).prop("selected", true);
				$(ulitem_seme_id).prop("title", $(arr_option_seme_id[i]).html());
				$(ulitem_seme_id).html($(arr_option_seme_id[i]).html());
				break;
			}
		}
		var parentNode_type_subject_id = $("#type_subject_id").parent();
		var ulitem_type_subject_id = $(parentNode_type_subject_id).find(
				"span.select2-selection__rendered");
		var arr_option_type_subject_id = $("#type_subject_id option");
		$(ulitem_type_subject_id).empty();
		for (i = 0; i < arr_option_type_subject_id.length; i++) {
			if ($(arr_option_type_subject_id[i]).val() == type_subject_id) {
				$(arr_option_type_subject_id[i]).prop("selected", true);
				$(ulitem_type_subject_id).prop("title", $(arr_option_type_subject_id[i]).html());
				$(ulitem_type_subject_id).html($(arr_option_type_subject_id[i]).html());
				break;
			}
		}
		modal.modal("show");
	}

	// function editModal(obj){
	// 	debugger
	// 	var tr=$(obj).closest("tr");
	// 	var id=tr.find("#id").val();
	// 	modal.find("#id").val(id);
	// }
	function InitDataTable(selectorTable){
    selectorTable=selectorTable??$("#tableMH");
    var table=selectorTable.DataTable({
        ordering: false,
        "order": [[ 1, 'asc' ]]
    });
    table.on( 'order.dt search.dt', function () {
        table.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
            cell.innerHTML = i+1;
        } );
    } ).draw();
}
	function search(){
		var e1 = document.getElementById("hocky");
		var value1 = e1.options[e1.selectedIndex].value;
		var e2 = document.getElementById("loaimonhoc");
		var value2 = e2.options[e2.selectedIndex].value;
		var qSearch=$("#qSearch").val().replace("/\ /g","+");
		var url='/MonHoc/Index?ajaxLoad=table&idSemester='+value1+"&idStatus="+value2+"&searchValue="+qSearch;
		$("#getData").load(url,function(){
			//$("#table").DataTable({
				//ordering: false,
			//});
			InitDataTable();
			// console.log("đã load lại table");
		})
	}

	$('#id_subject').change(function(){
	    $.ajax({
	        url: "/MonHoc/checkId",
	        data: { "idSubject": $("#id_subject").val() },
	        type: "post",
	        success: function(data){
	        	if(data=="existed"){
					ThongBao_Loi("Mã đã tồn tại, hãy chọn mã khác!");
					checkIdStatus="reject";
					}else if(data=="not exist"){
						ThongBao_ThanhCong("Mã chưa tồn tại, có thể tạo mới!");
						checkIdStatus="accept";
						}else{
							ThongBao_Loi("Có lỗi xảy ra!");
							}
	        }
	    });
	});
  </script>