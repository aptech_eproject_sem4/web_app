<%@ page contentType="text/html;charset=UTF-8" language="java"
	isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!--modal default-->
<div class="modal fade" id="modal-monHoc">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"
					aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h4 class="modal-title" id="title-popup">Thêm môn học</h4>
			</div>
			<div class="modal-body">
				<form id="form-monhoc" action="/MonHoc/addOrUpdate" method="POST"
					enctype="multipart/form-data">
					<div class="row">
						<div class="col-lg-6">
							<div class="form-group">
								<label for="" class="required">Mã môn học</label> <input
									class="form-control" type="text" id="id_subject"
									name="id_subject" isInputAllowVarchar required>
							</div>
						</div>
						<div class="col-lg-6">
							<div class="form-group">
								<label for="" class="required">Tên viết tắt</label> <input class="form-control"
									type="text" id="sort_name" name="sort_name" isInputAllowVarchar required>
							</div>
						</div>

					</div>
					<div class="row">
						<div class="col-lg-12">
							<label for="" class="required">Tên môn học</label> <input
								class="form-control" type="text" id="name_subject"
								name="name_subject" required>
						</div>

					</div>
					<div class="row">

						<div class="col-lg-6">
							<div class="form-group">
								<label for="" class="required">Tổng số giờ</label> <input class="form-control"
									type="text" id="hour_study" name="hour_study" onlynumber required>
							</div>
						</div>
						<div class="col-lg-6">
							<div class="form-group">
								<label for="" class="required">SKU</label> <select class="form-control select2"
									style="width: 100%;" id="sku_id" name="sku_id" required>
									<c:forEach var="itemSku" items="${listSku}">
										<option value="${itemSku.id }">${itemSku.unit1_value}${itemSku.unit1 }-${itemSku.unit2_value}${itemSku.unit2 }</option>
									</c:forEach>
								</select>
							</div>
						</div>

					</div>
					<div class="row">
						<div class="col-lg-6">
							<div class="form-group">
								<label for="" class="required">Tổng buổi học</label> <input class="form-control"
									type="text" id="number_session" name="number_session"
									onlynumber required>
							</div>
						</div>
						<div class="col-lg-6">
							<div class="form-group">
								<label for="" class="required">Giá tiền</label> <input class="form-control"
									type="text" id="money_subject" name="money_subject" ismoney required>
							</div>
						</div>

					</div>
					<div class="row">
						<div class="col-lg-6">
							<div class="form-group">
								<label for="" class="required">Học kỳ</label> <select
									class="form-control select2" style="width: 100%;" id="seme_id"
									name="seme_id" required>
									<c:forEach var="itemSem" items="${listSemester}">
										<option value="${itemSem.id }">${itemSem.name_seme }</option>
									</c:forEach>
								</select>
							</div>
						</div>

						<div class="col-lg-6">
							<div class="form-group">
								<label for="" class="required">Loại môn học</label> <select
									class="form-control select2" style="width: 100%;"
									id="type_subject_id" name="type_subject_id" required>									
									<c:forEach var="itemStat" items="${listStatus}">
										<option value="${itemStat.id_status }">${itemStat.name_status }</option>
									</c:forEach>
								</select>
							</div>
						</div>

					</div>
					<div class="row">
						<div class="col-lg-6">
							<div class="form-group">
								<label for="" class="required">Hệ số</label> <input class="form-control"
									type="text" id="factor" name="factor" onlynumber required>
							</div>
						</div>
						<div class="col-lg-6">
							<div class="form-group">
								<label for="" class="required">Thang điểm</label> <input class="form-control"
									type="text" id="point" name="point" onlynumber required>
							</div>
						</div>

					</div>
					<div class="row">
						<div class="col-lg-12">
							<div class="form-group">
								<label for="" class="required">Thứ tự</label> <input class="form-control"
									type="text" id="order_number" name="order_number" onlynumber>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-lg-12">
							<div class="form-group">
								<label for="" class="required">Ghi chú</label> <input class="form-control"
									type="text" id="note" name="note">
							</div>
						</div>
					</div>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default pull-left"
					data-dismiss="modal">Đóng</button>
				<button type="button" class="btn btn-primary" id="btnSave">Lưu</button>
			</div>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>