<%@ page contentType="text/html;charset=UTF-8" language="java"
	isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ include file="../base/_headerMain.jspf"%>
<!-- Content Header (Page header) -->
<section class="content-header">
	<h1>Reset Mật Khẩu Và Mac Mobile</h1>
	<!-- 	<div class="wrap-btn-header"> -->
	<!-- 		<button type="button" class="btn bg-maroon" id="btnCreate"> -->
	<!-- 			<i class="fa fa-plus"></i> Reset ở đây -->
	<!-- 		</button> -->
	<!-- 	</div> -->
</section>
<!-- Main content -->
<section class="content">
	<!-- Main row -->
	<div class="row">
		<%@ include file="loadTable.jsp"%>
	</div>
	<div class="row">
		<div class="col-lg-12">
			<a href="/Home"><button type="button" class="btn btn-primary" id="btnBackHome"
				style="float: right; color: white; background-color: #009688">Trở
				về</button></a>
		</div>
	</div>

</section>
<!-- /.content -->
<%@ include file="../base/_footer.jspf"%>
<script>
	var currentID = "1";
	var currentEmail = null;
	$('input[name=radiobtn]').click(function() {
		currentID = $('input[name=radiobtn]:checked').val();
	});

	$("#btnResetPassword").click(function() {
		currentEmail = $("input[id=emailResetPassword]").val();
		if (currentEmail == null||currentEmail == "") {
			ThongBao_Loi("Hãy nhập email!");
		} else {
			$.ajax({
				url : "/ResetMatKhau/resetPassword",
				data : {
					"id_role" : String(currentID),
					"email" : String(currentEmail)
				},
				type : "post",
				success : function(data) {
					if (data == "not found email") {
						ThongBao_Loi("Không tìm thấy email!");
					} else if (data == "choose role") {
						ThongBao_Loi("Hãy chọn 1 trong 3 role!");
					} else if (data == "reset success") {
						ThongBao_ThanhCong("Reset thành công!");
					} else {
						ThongBao_Loi("Có lỗi xảy ra, hãy thử lại!");
					}
					$(".email-reset").val(null);
				},
				error : function(data) {
					if (data == "not found email") {
						ThongBao_Loi("Không tìm thấy email!");
					} else if (data == "choose role") {
						ThongBao_Loi("Hãy chọn 1 trong 3 role!");
					} else if (data == "reset success") {
						ThongBao_ThanhCong("Reset thành công!");
					} else {
						ThongBao_Loi("Có lỗi xảy ra, hãy thử lại!");
					}
					$(".email-reset").val(null);
				}
			});
		}
	})

	$("#btnResetMacMobile").click(function() {
		currentEmail = $("input[id=emailResetMacMobile]").val();
		if (currentEmail == null||currentEmail == "") {
			ThongBao_Loi("Hãy nhập email!");
		} else {
			$.ajax({
				url : "/ResetMatKhau/resetMacMobile",
				data : {
					"email" : String(currentEmail)
				},
				type : "post",
				success : function(data) {
					if (data == "not found email") {
						ThongBao_Loi("Không tìm thấy email!");
					} else if (data == "reset success") {
						ThongBao_ThanhCong("Reset thành công!");
					} else {
						ThongBao_Loi("Có lỗi xảy ra, hãy thử lại!");
					}
					$(".email-reset").val(null);
				},
				error : function(data) {
					if (data == "not found email") {
						ThongBao_Loi("Không tìm thấy email!");
					} else if (data == "reset success") {
						ThongBao_ThanhCong("Reset thành công!");
					} else {
						ThongBao_Loi("Có lỗi xảy ra, hãy thử lại!");
					}
					$(".email-reset").val(null);
				}
			});
		}
	})
</script>