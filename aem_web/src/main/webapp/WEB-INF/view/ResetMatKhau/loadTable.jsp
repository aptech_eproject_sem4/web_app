<%@ page contentType="text/html;charset=UTF-8" language="java"
	isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!--table-->
		<!-- 		<div class="col-lg-12"> -->
		<!-- 			<div class="filter-table"></div> -->
		<!-- 			 <div class="wrap-table" id="getData"> -->
		<%-- 				<%@ include file="loadTable.jsp"%> --%>
		<!-- 			</div>  -->
		<!-- 		</div> -->
		<!--table-->
		<div class="col-lg-5" style="border: 1px solid black; margin: 25px">
			<div style="text-align: center; font-weight: bold; font-size: 18px">Reset
				mật khẩu</div>
			<form id="form-resetmatkhau"
				method="POST" enctype="multipart/form-data">
				<input type="hidden" id="id" name="id">
				<div class="row">
					<div class="col-lg-12">
						<div class="form-group d-flex" style="margin-top: 25px">
							<label for="" style="margin-top: 25px">Đối tượng:</label> <input
								type="hidden" name="even_or_odd" id="rb">
							<div style="margin-left: 25px; margin-top: 25px">
								<label for="even_or_odd"><input type="radio" id=""
									name="radiobtn" value="1" data-val="id_students" checked>
									Sinh viên</label> <label for="even_or_odd" style="margin-left: 20px"><input
									type="radio" id="" name="radiobtn" value="2" data-val="id_fc">
									Giảng viên</label> <label for="even_or_odd" style="margin-left: 20px"><input
									type="radio" id="" name="radiobtn" value="3" data-val="id_emp">
									Nhân viên</label>
							</div>
						</div>
					</div>
					<div class="col-lg-12">
						<div class="form-group">
							<label for="name_shift" class="required">Email đăng nhập:</label>
							<input class="form-control email-reset" type="text" id="emailResetPassword"
								name="emailResetMatKhau" required>
						</div>
					</div>
					<div class="col-lg-12"><span style="color:red">Reset mật khẩu mặc định là: <span style="font-weight: bold">aptech123</span></span></div>
				</div>
			</form>
			<button type="button" class="btn btn-primary" id="btnResetPassword" style="float: right; color: white; background-color: #009688">Cập
					nhật</button>
		</div>
		<div class="col-lg-5"
			style="border: 1px solid black; float: right; margin: 25px">
			<div style="text-align: center; font-weight: bold; font-size: 18px">Reset
				mac mobile</div>
			<form id="form-resetmacmobile"
				method="POST" enctype="multipart/form-data">
				<div class="row">
					<div class="col-lg-12">
						<div class="form-group">
							<label for="name_shift" class="required">Email sinh viên:</label>
							<input class="form-control email-reset" type="text" id="emailResetMacMobile"
								name="emailResetMacMobile" required>
						</div>
					</div>
					<div class="col-lg-12"><span style="color:red;font-style: italic;">Reset thành công, sinh viên đăng nhập lại là được</span></div>
				</div>
			</form>
			<button type="button" class="btn btn-primary" id="btnResetMacMobile" style="float: right; color: white; background-color: #009688">Cập
					nhật</button>
		</div>