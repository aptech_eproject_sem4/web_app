<%@ page contentType="text/html;charset=UTF-8" language="java"
	isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Aptech | Sign in</title>
<link rel="icon" href="favicon.ico" type="image/x-icon">
<meta
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
	name="viewport">
<base href="/">
<!-- Bootstrap 3.3.7 -->
<link rel="stylesheet" type="text/css"
	href="lib/bootstrap/dist/css/bootstrap.min.css">
<!-- Font Awesome -->
<link rel="stylesheet" type="text/css"
	href="lib/font-awesome/css/font-awesome.min.css">
<!-- Theme style -->
<link rel="stylesheet" href="dist/css/AdminLTE.min.css">
<!-- Material Design -->
<link rel="stylesheet"
	href="dist/css/bootstrap-material-design.min.css">
<link rel="stylesheet" href="dist/css/ripples.min.css">
<link rel="stylesheet" href="dist/css/MaterialAdminLTE.min.css">
<link rel="stylesheet" href="dist/css/app/login.css">	
<!-- Google Font -->
<link rel="stylesheet"
	href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">

</head>

<body class="hold-transition login-page" id="wrap-login">
	<div class="row">
		<div class="col-lg-6" id="bg-logo">
			<img src="dist/img/main_login.jpg" alt="">
		</div>
		<div class="col-lg-6">
			<div id="wrap-form">
				<div class="login-logo">
					Aptech Education <i class="fa fa-home"></i><br> <span>Đăng nhập</span>
				</div>
				<div class="login-box-body">
					<form id="form-signIn">
						<div class="form-group has-feedback">
							<input type="text" class="form-control form-login"
								placeholder="Email" name="email" id="email" required> <span
								class="glyphicon glyphicon-envelope form-control-feedback form-login"></span>
						</div>
						<div class="form-group has-feedback">
							<input type="password" class="form-control form-login"
								placeholder="Password" name="password" id="password" required> <span
								class="glyphicon glyphicon-lock form-control-feedback form-login"></span>
						</div>
						<div class="form-group">
							<select class="form-control form-login" style="width: 100%;"
								id="type" name="type" required>
								<option value="">Đăng nhập quyền?</option>
								<option value="1">Sinh Viên</option>
								<option value="2">Giảng Viên</option>
								<option value="3">Nhân Viên</option>
							</select>
						</div>
						<input type="submit" id="btnSignIn" style="display:none;">
					</form>
					<div class="form-group">
						<button class="btn btn-primary btn-raised btn-block btn-flat" onclick="submitSignIn()">Đăng nhập</button>
					</div>
				</div>
			</div>

		</div>
	</div>
	<!-- /.login-box -->

	<!-- jQuery 3 -->
	<script src="lib/jquery/dist/jquery.min.js"></script>
	<!-- Bootstrap 3.3.7 -->
	<script
		src="lib/bootstrap/dist/js/bootstrap.min.js"></script>
	<!-- Material Design -->
	<script src="dist/js/material.min.js"></script>
	<script src="dist/js/ripples.min.js"></script>
	<script>
		$(function () {
			
		});
		function submitSignIn(){
			var form=$("#form-signIn");
			if(form.find("#email").val()=="" || form.find("#password").val()=="" || form.find("#type").val()==""){
				$("#btnSignIn").trigger("click");
				return;
			}
			var formdata = new FormData(form[0]);
			$.ajax({
            type: "POST",
            url: "/SubmitLogin",
            data: formdata,
            processData: false,
            contentType: false,
				success: function (data) {
					if (data=="false") {
						alert("Có lỗi, thử đăng nhập lại!")
					}else{
						var homeUrl=window.location.protocol + "//" + window.location.host;
						window.location.href =homeUrl;
					}
				},
        	});
		}
	</script>
</body>

</html>