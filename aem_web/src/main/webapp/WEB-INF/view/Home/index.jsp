<%@ page contentType="text/html;charset=UTF-8" language="java" isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ include file="../base/_headerMain.jspf" %>
<!-- Content Header (Page header) -->
<section class="content-header"></section>
<section class="">
	<!-- Main row -->
	<div class="row">
		<div class="col-lg-12">
			<div class="wrap-currAttent">
                <!-- PRODUCT LIST -->
                <div class="box box-primary">
                  <div class="box-header with-border">
                    <h3 class="box-title">Lịch biểu ngày hôm nay
                    </h3>

                    <div class="box-tools pull-right">
                      <button type="button" class="btn btn-box-tool"
                        data-widget="collapse"><i class="fa fa-minus"></i>
                      </button>
                    </div>
                  </div>
                  <!-- /.box-header -->
                  <div class="box-body">
                    <ul class="products-list product-list-in-box">
                      <li class="item">
                        <div class="product-img" style="background-color:
                          aquamarine;">

                        </div>
                        <div class="product-info" onclick="">
                          <span class="product-title">GV: Khiêm Bùi - C1808G1</span>
                          <span class="product-description">
                            Môn: Java - 9:00 to 12:00 - Buổi: 21
                          </span>
                        </div>
                      </li>
                      <!-- /.item -->
                      <li class="item">
                        <div class="product-img" style="background-color:
                          aquamarine;">

                        </div>
                        <div class="product-info" onclick="">
                          <span class="product-title">GV: Khiêm Bùi - C1808G1</span>
                          <span class="product-description">
                            Môn: Java - 9:00 to 12:00 - Buổi: 21
                          </span>
                        </div>
                      </li>
                      <!-- /.item -->
                      <li class="item">
                        <div class="product-img" style="background-color:
                          aquamarine;">

                        </div>
                        <div class="product-info" onclick="">
                          <span class="product-title">GV: Khiêm Bùi - C1808G1</span>
                          <span class="product-description">
                            Môn: Java - 9:00 to 12:00 - Buổi: 21
                          </span>
                        </div>
                      </li>
                    </ul>
                  </div>
                  <!-- /.box-body -->
                  <div class="box-footer text-center">
                    <a href="" class="uppercase" style="font-weight: 600;">Xem
                      lịch biểu tháng 9</a>
                  </div>
                </div>
        	</div>
            <%-- <div class="wrap-currAttent">
                <div class="box box-primary">
                  <div class="box-header with-border">
                    <h3 class="box-title">Danh sách môn thi
                    </h3>

                    <div class="box-tools pull-right">
                      <button type="button" class="btn btn-box-tool"
                        data-widget="collapse"><i class="fa fa-minus"></i>
                      </button>
                    </div>
                  </div>
                  <!-- /.box-header -->
                  <div class="box-body">
                    <ul class="products-list product-list-in-box">
                      <li class="item">
                        <div class="product-img" style="background-color:
                          aquamarine;">

                        </div>
                        <div class="product-info" data-toggle="modal" data-target="#modal-large">
                          <span class="product-title">GV: Khiêm Bùi - C1808G1</span>
                          <span class="product-description">
                            Môn: Java - 9:00 to 12:00 - Buổi: 21
                          </span>
                        </div>
                      </li>
                      <!-- /.item -->
                    </ul>
                  </div>
                  <!-- /.box-body -->
                  <div class="box-footer text-center">
                    <span class="uppercase text-danger" style="font-weight:
                      600;">Lưu ý chỉ cập nhật bảng điểm 1 lần duy nhất và không
                      được chỉnh sửa</span><br>
                    <span class="uppercase text-danger" style="font-weight:
                      600;">Cập nhật sai liên hệ với nhân viên đào tạo để reset
                      bảng điểm</span>
                  </div>
                </div>
            </div> --%>
		</div>
	</div>

</section>
<div class="modal fade" id="modal-large">
          <div class="modal-dialog modal-lg">
            <div class="modal-content">

              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"
                  aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="modalLabelLarge">Danh sách sinh viên</h4>
              </div>

              <div class="modal-body">
                <table id="tableTT" class="table table-bordered table-striped">
                  <thead>
                    <tr>
                      <th width="45" class="text-center">STT</th>
                      <th>Sinh viên</th>
                      <th>User ID</th>
                      <th>Mật khẩu</th>
                      <th>Điểm thi</th>
                      <th>File ảnh</th>
                      <th>Ghi chú</th>
                      <th>Thao tác</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td class="text-center">1</td>
                      <td>Tên aaa</td>
                      <td>Student156641</td>
                      <td>dfdf54455</td>
                      <td>10 - 50%</td>
                      <td><a type="button" class="cur-pointer" iswatchfileonline
                          urlfile="dist/img/default-user.png"><i class="fa
                            fa-eye"></i> Xem file</a></td>
                      <td><i>ghi chú nè</i></td>
                      <td></td>
                    </tr>
                    <tr>
                      <td class="text-center">1</td>
                      <td>Tên aaa</td>
                      <td>Student156641</td>
                      <td>dfdf54455</td>
                      <td></td>
                      <td></td>
                      <td></td>
                      <td>
                        <button type="button" class="btn-edit"
                          data-toggle="modal" data-target="#modal-default"><i
                            class="fa fa-pencil"></i></button>
                        <button type="button" class="btn-del"><i class="fa
                            fa-refresh"></i></button>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
          <!--modal large-->
</div>
<!--modal default-->
<div class="modal fade" id="modal-default">
	<div class="modal-dialog">
	<div class="modal-content">
		<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal"
			aria-label="Close">
			<span aria-hidden="true">&times;</span></button>
		<h4 class="modal-title">Cập nhật điểm thi</h4>
		</div>
		<div class="modal-body">
		<div class="row">
			<div class="col-lg-6">
			<div class="form-group">
				<label for="">Điểm số</label>
				<input class="form-control" type="text" id="" name=""
				placeholder="" onlynumber>
			</div>
			</div>
			<div class="col-lg-6">
			<div class="form-group">
				<label for="">Điểm %</label>
				<input class="form-control" type="text" id="" name=""
				placeholder="" onlynumber>
			</div>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-12 d-flex">
			<label for="" style="margin-right: 20px;">File đính kèm</label>
			<input type="file" name="file" id="file">
			</div>
		</div>
		<div class="row">
			<div class="col-lg-12">
			<div class="form-group">
				<label for="">Ghi chú</label>
				<textarea class="form-control" id="" name="" placeholder=""></textarea>
			</div>
			</div>
		</div>
		</div>
		<div class="modal-footer">
		<button type="button" class="btn btn-default pull-left"
			data-dismiss="modal">Close</button>
		<button type="button" class="btn btn-primary">Save changes</button>
		</div>
	</div>
	<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>
<!-- Main content -->
<%@ include file="../base/_footer.jspf" %>
