<%@ page contentType="text/html;charset=UTF-8" language="java"
	isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!--modal default-->
<div class="modal fade" id="modal-update-status">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"
					aria-label="Close" onclick="clearStatusStudentModal()">
					<span aria-hidden="true">&times;</span>
				</button>
				<h4 class="modal-title">Cập nhật trạng thái</h4>
			</div>
			<div class="modal-body">
				<form id="form-status-sinhvien">
					<input type="hidden" id="student_id_status" name="student_id">
					<div class="row">
						<div class="col-lg-12">
							<div class="form-group">
								<label for="">Chọn tình trạng</label> <select
									class="form-control select2" style="width: 100%" id="status_id"
									name="status_id" required>
									<option value="">Chọn tình trạng</option>
									<c:forEach var="status" items="${ getAllStatus }">
										<option value="${ status.id_status }">${ status.name_status }
										</option>
									</c:forEach>
								</select>
							</div>
						</div>
						<div class="col-lg-6">
							<div class="form-group">
								<label for="">Ngày bắt đầu </label>
								<div class="input-group">
									<input type="text" class="form-control datepicker"
										name="start_date" id="start_date" autocomplete="off" isdate
										required onlynumber/>
									<div class="input-group-addon">
										<i class="fa fa-calendar"></i>
									</div>
								</div>
							</div>
						</div>
						<div class="col-lg-6">
							<div class="form-group">
								<label for="">Ngày kết thúc</label>
								<div class="input-group">
									<input type="text" class="form-control datepicker"
										name="end_date" id="end_date" autocomplete="off" isdate
										required onlynumber/>
									<div class="input-group-addon">
										<i class="fa fa-calendar"></i>
									</div>
								</div>
							</div>
						</div>
						<div class="col-lg-12">
							<div class="form-group">
								<label for="">Ghi chú</label> <input class="form-control"
									type="text" id="note" name="note" autocomplete="off" required>
							</div>
						</div>
					</div>
				</form>

			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default pull-left"
					data-dismiss="modal" onclick="clearStatusStudentModal()">Đóng</button>
				<button type="button" class="btn btn-primary" id="btnSaveStudentStatus" onclick="saveStatusStudent()">Lưu</button>
			</div>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>
<!--modal default-->