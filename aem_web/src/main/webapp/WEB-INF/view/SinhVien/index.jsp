<%@ page contentType="text/html;charset=UTF-8" language="java"
	isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ include file="../base/_headerMain.jspf"%>
<!-- Content Header (Page header) -->
<div class="img-container" id="gifLoading"
	style="text-align: center; position: fixed; top: 100px; left: 50%; z-index: 999;display:none;">
	<div style="position: relative;">
		<img src="/static/upload/gif/Loading.gif" />
	</div>
	<div style="background-color: #0d8aee;border:1px solid #0d8aee;border-radius: 50px">
		<span id="textLoading" style="font-weight: bold; color: white"></span>
	</div>
</div>
<div class="formImport_container" id="formImport_container"
	style="text-align: center; position: fixed; top: 100px; left: 50%; z-index: 99;background-color:#0d8aee;display:none;">
	<div style="padding: 10px;color: white;">
	<span>=Nếu chưa có file mẫu, hãy tải xuống file mẫu để nhập đúng form.=</span><br/>
	<button id="downloadSampleFileBtn" style="color:black;">File mẫu</button><br/>
	<span>===Hãy chọn file để import===</span>
			<form id="formImport" method="POST" enctype="multipart/form-data">
					<input id="file-input" type="file" name="file" style="margin:auto;"
						 onchange="importStudents()" />
			</form>
			</div>
</div>
<!-- Main content -->
<section class="content">
	<!-- Main row -->
	<div class="row">
		<h3 class="col-md-3">Quản lí sinh viên</h3>
		<div class="col-md-4 row pull-right">
			<div class="col-md-4">				
				<button class="btn bg-maroon" id="importBtn"
					onclick="clickFileInput()">Import</button>
			</div>
			<div class="col-md-4">
				<button class="btn bg-maroon" id="exportBtn">Export</button>
			</div>
			<div class="col-md-4">
				<a href="/SinhVien/formStudent" class="btn bg-green">Thêm mới</a>
			</div>
		</div>
	</div>
	<div class="row">
		<!--table-->
		<div class="col-lg-12">
			<div class="filter-table">
				<div class="row">
					<div class="col-lg-2">
						<div class="form-group">
							<div class="input-group">
								<button type="button" class="btn btn-default pull-right"
									id="daterange">
									<input type="hidden" id="tuNgay" /> <input type="hidden"
										id="denNgay" /> <span> <i class="fa fa-calendar"></i>
										Tất cả
									</span> <i class="fa fa-caret-down"></i>
								</button>
							</div>
						</div>
					</div>
					<div class="col-lg-2">
						<div class="form-group">
							<select class="form-control select2" style="width: 100%"
								id="course" name="course">
								<!-- <option selected="selected">Course family</option> -->
								<option selected="selected" id="course" value="" data-id="">Chọn
									course</option>
								<c:forEach var="course" items="${courses}" varStatus="loop">
									<option value="${course.id_course }">${course.name_course}
								</c:forEach>
							</select>
						</div>
					</div>

					<div class="col-lg-2">
						<div class="form-group">
							<select class="form-control select2" style="width: 100%"
								id="course_family" name="course_family">
								<option selected="selected" value="">Chọn course family</option>
							</select>
						</div>
					</div>
					<div class="col-lg-2">
						<div class="form-group">
							<select class="form-control select2" style="width: 100%"
								id="class" name="class">
								<option selected="selected" value="">Chọn lớp</option>
								<c:forEach var="itemClass" items="${classes}" varStatus="loop">
									<option value="${itemClass.id_class}">${itemClass.name_class}
									</option>
								</c:forEach>
							</select>
						</div>
					</div>
					<div class="col-lg-2">
						<div class="form-group">
							<select class="form-control select2" style="width: 100%"
								id="tinhTrang" name="tinhTrang">
								<option selected="selected" value="">Chọn tình trạng</option>
								<c:forEach var="status" items="${getAllStatus}">
									<option value="${status.id_status}">${status.name_status}
									</option>
								</c:forEach>
							</select>
						</div>
					</div>
					<div class="col-lg-2 d-flex">
						<div class="form-group">
							<input class="form-control" type="text" id="qSearch"
								name="qSearch" placeholder="Tìm kiếm nhanh" /> <br />
							<button type="button" class="btn bg-maroon btnSearch"
								style="float: right" onclick="setValueLoad()">
								<i class="fa fa-search"></i> Tìm kiếm
							</button>
						</div>
						<div class=""></div>
					</div>
				</div>
			</div>
			<div class="wrap-table" id="getData">
				<%@ include file="./loadTable.jsp"%>
			</div>
		</div>
		<!--table-->
	</div>
</section>
<!-- /.content -->
<%@ include file="./modal-status.jsp"%>
<%@ include file="./modal-student-status.jsp"%>
<!-- /.content -->
<%@ include file="../base/_footer.jspf"%>
<script type="text/javascript">
	var scrollheigthOld = 0;
	var modalStudentStatustList = $("#modal-student-status");
	var page = 2;
	var count;
	var course_family_load;
	var course_id_load;
	var current_class_load;
	var searchValue_load;
	var from_date_load;
	var to_date_load;
	var status_load;

	$("#course").change(
			function() {
				var courseId = $("#course").val();
				var course_family = $("#course_family");

				var row = "<option value= ''>Chọn course family</option>";
				//     var optionCourseFamily = $("<option></option>").text("Chọn course family");
				course_family.empty().append(row);

				$.ajax({
					url : "/KhoaHoc/courseByRootId",
					dataType : "json",
					data : {
						courseRoot : courseId
					},
					type : "GET",
					success : function(data) {
						var listData = data;
						for (var i = 0; i < listData.length; i++) {
							course_family.append(
									'<option value= "'+listData[i].id_course +'">'
											+ listData[i].name_course + '</option>')
						}
						//         data.forEach((data) =>
						//           course_family
						//             .empty()
						//             .append(
						//               '<option value= "'+data.id_course +'">'+data.name_course+'</option>'
						//             )
						//         );
					},
				});
			});

	//   function InitDataTable(selectorTable) {
	//     selectorTable = selectorTable ?? $("#tableSV");
	//     var table = selectorTable.DataTable({
	//       ordering: false,
	//       order: [[1, "asc"]],
	//     });
	//     table
	//       .on("order.dt search.dt", function () {
	//         table
	//           .column(0, { search: "applied", order: "applied" })
	//           .nodes()
	//           .each(function (cell, i) {
	//             cell.innerHTML = i + 1;
	//           });
	//       })
	//       .draw();
	//   }

	function setValueLoad() {	
		var e1 = document.getElementById("tinhTrang");
		status_load = e1.options[e1.selectedIndex].value;
		if (status_load == undefined) {
			status_load = "";
		} else {
		}
		var e2 = document.getElementById("course");
		course_id_load = e2.options[e2.selectedIndex].value;
		if (course_id_load == undefined) {
			course_id_load = "";
		} else {
		}
		var e3 = document.getElementById("course_family");
		course_family_load = e3.options[e3.selectedIndex].value;
		if (course_family_load == undefined) {
			course_family_load = "";
		} else {
		}
		var e4 = document.getElementById("class");
		current_class_load = e4.options[e4.selectedIndex].value;
		if (current_class_load == undefined) {
			current_class_load = "";
		} else {
		}
		from_date_load = $("#tuNgay").val();
		if (from_date_load == undefined) {
			from_date_load = "";
		} else {
		}
		to_date_load = $("#denNgay").val();
		if (to_date_load == undefined) {
			to_date_load = "";
		} else {
		}
		searchValue_load = $("#qSearch").val().replace("/\ /g", "+");
		if (searchValue_load == undefined) {
			searchValue_load = "";
		} else {
		}
		search();
	}

	function search() {
		scrollheigthOld=0;
		page=2;
		if (status_load == undefined) {
			status_load = "";
		} else {
		}
		if (course_id_load == undefined) {
			course_id_load = "";
		} else {
		}
		if (course_family_load == undefined) {
			course_family_load = "";
		} else {
		}
		if (current_class_load == undefined) {
			current_class_load = "";
		} else {
		}
		if (from_date_load == undefined) {
			from_date_load = "";
		} else {
		}
		if (to_date_load == undefined) {
			to_date_load = "";
		} else {
		}
		if (searchValue_load == undefined) {
			searchValue_load = "";
		} else {
		}
		var url = "/SinhVien/Index?ajaxLoad=table&course_family="
				+ course_family_load + "&course_id=" + course_id_load
				+ "&current_class=" + current_class_load + "&searchValue="
				+ searchValue_load + "&from_date=" + from_date_load
				+ "&to_date=" + to_date_load + "&page=" + 1 + "&status_id="
				+ status_load;
		$("#getData").load(url, function() {

		});
	}

	// function ScrollLoadData($("#tableSV tbody"),"/SinhVien/Index",{
	//   "course_family":$("#course_family").val(),
	//    "course_id" : $("#course").val(),
	//   "form_date" : $("#tuNgay").val(),
	//   "to_date" : $("#denNgay").val(),
	//   "searchValue" :$("#qSearch").val().replace("/\ /g", "+") ,
	//   "status" :$("#tinhTrang").val(),
	//   "ajaxLoad":"table"
	//   });

	function changeActiveAccount(event, id_student) {
		var currentRow = $(event).closest("tr");
		var id_student = currentRow.find("td:eq(2) #getIdStudent").text();
		$.ajax({
			url : "/SinhVien/changeActiveAccount/" + id_student,
			type : "post",
			success : function(data) {
				ThongBao_ThanhCong(data);
				search();
			},
		});
	}

	function openStatusStudent(event) {
		let idStudent = $(event).attr("id-student");
		let idStatus = $(event).attr("id-status");
		$.ajax({
			url : "/SinhVien/getStudentById?id=" + idStudent,
			type : "get",
			dataType : 'json',
			success : function(data) {
				var parentNode_status = $("#status_id").parent();
				var ulitem_status = $(parentNode_status).find(
						"span.select2-selection__rendered");
				var arr_option_status = $("#status_id option");
				for (i = 0; i < arr_option_status.length; i++) {
					if ($(arr_option_status[i]).val() == data.status_id) {
						$(arr_option_status[i]).prop("selected", true);
						$(ulitem_status).prop("title",
								$(arr_option_status[i]).html());
						$(ulitem_status).html($(arr_option_status[i]).html());
						break;
					}
				}
				;
				//$("#modal-update-status").modal("show");
				$("#student_id_status").val(data.id_student);
			},
		});
		$
				.ajax({
					url : "/SinhVien/getLatestStatusStudentByStudentIdAndStatusId?student_id="
							+ idStudent + "&status_id=" + idStatus,
					type : "get",
					dataType : 'json',
					success : function(data) {
						$("#start_date").val(reformatDate(data.start_date));
						$("#end_date").val(reformatDate(data.end_date));
						$("#note").val(data.note);
					},
				});
		$("#modal-update-status").modal("show");
	}

	document.addEventListener('scroll', function(e) {
		var doc = document.documentElement;
		var scrollheigthNew = (window.pageYOffset || doc.scrollTop)
				- (doc.clientTop || 0);
		if (scrollheigthNew - scrollheigthOld > 1000) {
			scrollToLoadData();
			scrollheigthOld = scrollheigthNew;
		}

	});
	function scrollToLoadData() {
		var url = "/SinhVien/scrollToGetStudent";
		count=parseInt(parseInt($('#tableSV tbody tr:last').find('td').eq(0).text())+1);
		$
				.ajax({
					url : url,
					type : "get",
					dataType : 'json',
					data : {
						course_family : course_family_load,
						course_id : course_id_load,
						current_class : current_class_load,
						searchValue : searchValue_load,
						from_date : from_date_load,
						to_date : to_date_load,
						page : page,
						status : status_load
					},
					success : function(data) {
						var test = data;
						var tableSV = $("#tableSV tbody");
						for (var i = 0; i < data.length; i++) {
							var testi = data[i];
							var imgStd = data[i].image_student;
							var row = '<tr><td class="stt">' + count + '</td>'
									+ '<td>';
							if (imgStd == undefined || imgStd == null
									|| imgStd == "") {
								row += '<img src="/static/upload/avatar/dealftUser.png" '+
				 		 'alt="" class="img-sv-tbl">';
							} else {
								row += '<img src="/static/upload/avatar/'+data[i].image_student+'" '+
									'alt="" class="img-sv-tbl">';
							}

							row += '<td><span class="label label-primary cur-pointer" '+
						'id="getIdStudent">'
									+ data[i].id_student
									+ '</span> <br> <span>'
									+ data[i].full_name
									+ '</span>'
									+ '</td><td><span>Lớp đầu tiên: '
									+ data[i].first_class
									+ '</span><br>'
									+ '<span>Lớp hiện tại: '
									+ data[i].current_class
									+ '</span></td>'
									+ '<td><span>Course: '
									+ data[i].course_id
									+ '</span><br>'
									+ '<span>Course Family: '
									+ data[i].course_family
									+ '</span></td>'
									+ '<td><span>SDT: '
									+ data[i].mobile_phone
									+ '</span><br> <span>Email:'
									+ data[i].email_student
									+ '</span><br> <span>Email-school:'
									+ data[i].email_school
									+ '</span></td>'
									+ '<td>'
									+ data[i].date_of_doing
									+ '</td>'
									+ '<td><span class="label label-primary btn-update-student-status" '
									+ 'id-student="'
									+ data[i].id_student
									+ '" '
									+ 'id-status="'
									+ data[i].status_id
									+ '" onclick="openStatusStudent(this)"'
									+ 'style="cursor: pointer;">Cập nhật</span> <br>';
							if (data[i].statusStudentDTO == undefined
									|| data[i].statusStudentDTO == null
									|| data[i].statusStudentDTO == "") {
								row += '<span class="label" style="cursor: pointer;" onclick="ThongBao_Loi('
										+ 'Chưa có danh sách trạng thái của sinh viên này!'
										+ ')">Chưa có trạng thái</span>';
							} else {
								row += '<span class="label" '
										+ 'style="background-color: '
										+ data[i].statusStudentDTO.statusDTO.color
										+ '; cursor: pointer;" '
										+ 'id="showListStatus" id-student="'
										+ data[i].id_student
										+ '" '
										+ 'onclick="openListStatusStudent(this)">'
										+ data[i].statusStudentDTO.statusDTO.name_status
										+ '</span>';
							}

							row += '<td>';
							if ((data[i].active_account) == true) {
								row += '<button type="button" class="btn-warning" '
										+ 'onclick="changeActiveAccount(this)">'
										+ '<i class="fa fa-lock"></i>'
										+ '</button>';
							} else {
								row += '<button type="button" class="btn-warning" '
										+ 'onclick="changeActiveAccount(this)">'
										+ '<i class="fa fa-unlock"></i></button>';
							}
							row += '<button type="button" data-id="'
									+ data[i].id_student
									+ '" '
									+ 'class="btn-student-edit btn-edit" onclick="editStudent(this)">'
									+ '<i class="fa fa-pencil"></i>'
									+ '</button>'
									+ '<button type="button" class="btn-del" '
									+ 'onclick="Delete(' + data[i].id_student
									+ ',' + '/SinhVien/Delete'
									+ ',function(){search()})">'
									+ '<i class="fa fa-trash-o"></i>'
									+ '</button></td>' + '</tr>';
							tableSV[0].innerHTML += row;
							count++;
						}
						page++;
					},
				});
	}

	function openListStatusStudent(event) {
		$("#modal-student-status tbody").empty();
		let idStudent = $(event).attr("id-student");
		$.ajax({
			url : "/SinhVien/getListStudentStatusByStudentId?id_student="
					+ idStudent,
			type : "get",
			dataType : 'json',
			success : function(data) {
				var table = $("#modal-student-status tbody");
				var listData = data;
				var listStatusStudent = (${jsonGetAllStatusStudent});
				var listStatus = (${jsonGetAllStatus});
				for (var i = 0; i < listData.length; i++) {
					var name_status;
					var start_date = listData[i].start_date;
					var end_date = listData[i].end_date;
					var note = listData[i].note;
					for (var j = 0; j < listStatus.length; j++) {
						if (listData[i].status_id == listStatus[j].id_status) {
							name_status = listStatus[j].name_status;
						}
					}
					row = '<tr><td>' + (i + 1) + '</td><td>' + name_status
							+ '</td><td>' + start_date + '</td><td>' + end_date
							+ '</td><td>' + note + '</td></tr>';
					table.append(row);
				}
			},
		});
		$("#modal-student-status").modal("show");
	}

	function saveStatusStudent() {
		var check = compare();
		if (check == false) {
			ThongBao_Loi("Ngày kết thúc phải lớn hơn ngày bắt đầu!");
		} else {
			let statusStudentData = {
				status_id : $('#status_id').val(),
				student_id : $('#student_id_status').val(),
				start_date : $('#start_date').val(),
				end_date : $('#end_date').val(),
				note : $('#note').val()
			};
			console.log(statusStudentData);
			if($('#status_id').val()==""||$('#status_id').val()==null){
				ThongBao_Loi("Hãy chọn tình trạng!");
				}else{
			$.ajax({
				url : "/SinhVien/updateStatusStudent/",
				type : "post",
				data : statusStudentData,
				success : function(data) {
					let res = data.split('%');
					res[0] === 'error' ? ThongBao_Loi(res[1])
							: ThongBao_ThanhCong(res[1]);
					$("#form-status-sinhvien").find("input").val(null);
					var parentNode_status = $("#status_id").parent();
					var ulitem_status = $(parentNode_status).find(
							"span.select2-selection__rendered");
					$("#status_id").find('option:selected').removeAttr(
							"selected");
					var name = $("#status_id option:nth-child(1)")
							.prop("label");
					$(ulitem_status).prop("title", name);
					$(ulitem_status).html(name);
					$("#modal-update-status").modal("hide");
					search();
				},
			});}
		}
	}

	function clearStatusStudentModal() {
		$("#form-status-sinhvien").find("input").val(null);
		var parentNode_status = $("#status_id").parent();
		var ulitem_status = $(parentNode_status).find(
				"span.select2-selection__rendered");
		$("#status_id").find('option:selected').removeAttr("selected");
		document.getElementById('status_id').selectedIndex = -1;
		var name = $("#status_id option:nth-child(1)").prop("label");
		$(ulitem_status).prop("title", name);
		$(ulitem_status).html(name);
	}

	function compare() {
		var end_time = $("#end_date").val();
		var start_time = $("#start_date").val();
		var momentA = moment(start_time, "DD/MM/YYYY");
		var momentB = moment(end_time, "DD/MM/YYYY");
		return momentB.isSameOrAfter(momentA);
	}

	//format yyyy-MM-dd to dd/MM/yyyy
	function reformatDate(dateStr) {
		dArr = dateStr.split("-"); // ex input "2010-01-18"
		return dArr[2] + "/" + dArr[1] + "/" + dArr[0]; //ex out: "18/01/10"
	}

	function editStudent(event) {
		window.location.href = '/SinhVien/formStudent?studentId='
				+ $(event).attr("data-id");
	}

	function clickFileInput() {
// 	    $('#file-input').trigger('click');
		$('#formImport_container').show();
	};

	function importStudents() {
	    importExcel();
	  };

	function importExcel(){
		var form = $('#formImport')[0];
	    var data = new FormData(form);
	    $("#textLoading").text("Importing...");
	    $("#gifLoading").show();
        $.ajax({
            type: "POST",
            enctype: 'multipart/form-data',
            url: "/SinhVien/importFromExcel",
            data: data,
            processData: false,
            contentType: false,
            cache: false,
            timeout: 600000,
            success: function (data) {
            	console.log("RESPONSE : ",data);
            	if(data=="success"){
            		$("#gifLoading").hide();
            		$("#formImport_container").hide();
					ThongBao_ThanhCong("Successfully imported data from excel into the database!");
					search();
            	}else if(data!="success" && (data!=null||data!="")){
            		$("#gifLoading").hide();
					ThongBao_Loi(data);
               	}else{
               		$("#gifLoading").hide();
					ThongBao_Loi("Error has been occured, please try again!\nPlease be sure you have entered data correctly!");
               	}
               	$("#file-input").val("");	               
            },
            error: function (e) {
            	$("#gifLoading").hide();
                console.log("ERROR : ", e);
				ThongBao_Loi(e);
            }
        });
	}

	

	function exportExcel(){
		window.location.href='/SinhVien/exportToExcel';
	}
	
//SUPPORT COOKIE
	function setCookie(name, value, expiresInSeconds) {
	    var exdate = new Date();
	    exdate.setTime(exdate.getTime() + expiresInSeconds * 1000);
	    var c_value = escape(value) + ((expiresInSeconds == null) ? "" : "; expires=" + exdate.toUTCString());
	    document.cookie = name + "=" + c_value + '; path=/';
	};
	function getCookie(name) {
	    var parts = document.cookie.split(name + "=");
	    if (parts.length == 2) return parts.pop().split(";").shift();
	}
	function expireCookie(name) {
	    document.cookie = encodeURIComponent(name) + "=; path=/; expires=" + new Date(0).toUTCString();
	}
//-----END-----

	var downloadTimer;  // reference to timer object

	function startDownloadChecker(buttonId, imageId, timeout) {
	    var cookieName = "DownloadCompleteChecker";
	    var downloadTimerAttempts = timeout;    // seconds
	    var cookieValue = getCookie(cookieName);
	    setCookie(cookieName, 0, downloadTimerAttempts);

	    // set timer to check for cookie every milisecond
	    downloadTimer = window.setInterval(function () {
	        var cookie = getCookie(cookieName);
	        // if cookie doesn't exist, or attempts have expired, re-enable form
	        if ((cookie === 'undefined') || (downloadTimerAttempts == 0)) {
	            $("#" + buttonId).removeAttr("disabled");
	            $("#" + imageId).hide();
	            $("#formImport_container").hide();	            
	            window.clearInterval(downloadTimer);
	            expireCookie(cookieName);
	        }

	        downloadTimerAttempts--;
	    }, 100);
	    
	}

	// form submit event
	$("#exportBtn").click(function () {  // disable form submit button
		$("#textLoading").text("Exporting and downloading...");
	    $("#gifLoading").show();  // show loading animation
	    window.location.href="/SinhVien/exportToExcel";
	    startDownloadChecker("exportBtn", "gifLoading", 120);
	});

	$("#downloadSampleFileBtn").click(function () {  // disable form submit button
		$("#textLoading").text("Downloading sample file...");
	    $("#gifLoading").show();  // show loading animation
	    window.location.href="/SinhVien/downloadSampleFile";
	    startDownloadChecker("downloadSampleFileBtn", "gifLoading", 120);
	});

	//   $('.btn-student-edit').click(function(){
// 	    window.location.href = '/SinhVien/formStudent?studentId='+ $(this).attr("data-id");
	// 	});
</script>
