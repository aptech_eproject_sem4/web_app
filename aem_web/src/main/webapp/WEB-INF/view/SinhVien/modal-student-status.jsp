<%@ page contentType="text/html;charset=UTF-8" language="java"
	isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!--modal default-->
<div class="modal fade" id="modal-student-status">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"
					aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h4 class="modal-title">Danh sách trạng thái</h4>
			</div>
			<div class="modal-body">
				<table id="tableStatusStudent"
					class="table table-bordered table-striped">
					<thead>
						<tr>
							<th>STT</th>
							<th>Tình trạng</th>
							<th>Ngày BD</th>
							<th>Ngày KT</th>
							<th>Ghi chú</th>
						</tr>
					</thead>
					<tbody class="tblChiTiet">
						<tr>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
						</tr>
					</tbody>
					<tfoot>
						<tr>
							<th>STT</th>
							<th>Tình trạng</th>
							<th>Ngày BD</th>
							<th>Ngày KT</th>
							<th>Ghi chú</th>
						</tr>
					</tfoot>
				</table>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default pull-left"
					data-dismiss="modal">Đóng</button>
			</div>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>
<!--modal default-->