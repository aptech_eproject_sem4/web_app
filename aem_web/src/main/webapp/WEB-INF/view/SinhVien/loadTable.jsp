<%@ page contentType="text/html;charset=UTF-8" language="java"
	isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>


<table id="tableSV" class="table table-bordered table-striped"
	onscroll="scrollToLoadData()">
	<thead>
		<tr>
			<th width="45">STT</th>
			<th style="max-width: 85px;">Ảnh</th>
			<th style="max-width: 200px;">Tên sinh viên</th>
			<th>Lớp</th>
			<th>Chương trình học</th>
			<th>Liên hệ</th>
			<th>Ngày bắt đầu học</th>
			<th>Tình trạng</th>
			<th>Thao tác</th>
		</tr>
	</thead>
	<tbody class="tblChiTiet" style="overflow: auto; height: 50vh;">
		<c:forEach var="student" items="${students}" varStatus="loop">
			<tr>
				<td class="stt">${loop.index + 1}</td>
				<td>
				<c:set var="path" value="/static/upload/avatar/"/><c:choose>
						<c:when test="${empty student.image_student}">
							<img
								src="${path}dealftUser.png"
								alt="" class="img-sv-tbl">
						</c:when>
						<c:otherwise>
							<img
								src="${path}${student.image_student}"
								alt="" class="img-sv-tbl">
						</c:otherwise>
					</c:choose></td>
				<td><span class="label label-primary cur-pointer"
					id="getIdStudent">${ student.id_student }</span> <br> <span>${ student.full_name }</span>
				</td>
				<td><span>Lớp đầu tiên: ${student.first_class}</span><br>
					<span>Lớp hiện tại: ${ student.current_class }</span></td>
				<td><span>Course: ${student.course_id}</span><br> <span>Course
						Family: ${student.course_family}</span></td>
				<td><span>SDT: ${student.mobile_phone }</span><br> <span>Email:
						${student.email_student }</span><br> <span>Email-school:
						${student.email_school }</span></td>
				<td>${student.date_of_doing }</td>

				<td><span class="label label-primary btn-update-student-status"
					id-student="${student.id_student}"
					id-status="${student.status_id }" onclick="openStatusStudent(this)"
					style="cursor: pointer;">Cập nhật</span> <br> <c:choose>
						<c:when
							test='${student.statusStudentDTO == null||student.statusStudentDTO == ""}'>
							<span class="label" style="cursor: pointer;"
								onclick="ThongBao_Loi('Chưa có danh sách trạng thái của sinh viên này!')">Chưa
								có trạng thái</span>
						</c:when>
						<c:otherwise>
							<span class="label"
								style="background-color: ${student.statusStudentDTO.statusDTO.color}; cursor: pointer;"
								id="showListStatus" id-student="${student.id_student}"
								onclick="openListStatusStudent(this)">${student.statusStudentDTO.statusDTO.name_status}</span>
						</c:otherwise>
					</c:choose> <br>
				<td><c:choose>
						<c:when test="${student.active_account==true}">
							<button type="button" class="btn-warning"
								onclick="changeActiveAccount(this)">
								<i class="fa fa-lock"></i>
							</button>
						</c:when>
						<c:otherwise>
							<button type="button" class="btn-warning"
								onclick="changeActiveAccount(this)">
								<i class="fa fa-unlock"></i>
							</button>
						</c:otherwise>

					</c:choose>
					<button type="button" data-id="${ student.id_student }"
						class="btn-student-edit btn-edit " onclick="editStudent(this)">
						<i class="fa fa-pencil"></i>
					</button>
					<button type="button" class="btn-del"
						onclick="Delete('${student.id_student}','/SinhVien/Delete',function(){search()})">
						<i class="fa fa-trash-o"></i>
					</button></td>
			</tr>
		</c:forEach>
	</tbody>
	<tfoot>
		<tr>
			<th width="45">STT</th>
			<th style="max-width: 85px;">Ảnh</th>
			<th style="max-width: 200px;">Tên sinh viên</th>
			<th>Lớp</th>
			<th>Chương trình học</th>
			<th>Liên hệ</th>
			<th>Ngày bắt đầu học</th>
			<th>Tình trạng</th>
			<th>Thao tác</th>
		</tr>
	</tfoot>
</table>






