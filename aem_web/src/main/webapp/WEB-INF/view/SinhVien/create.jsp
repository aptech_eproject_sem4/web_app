<%@ page contentType="text/html;charset=UTF-8" language="java"
	isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ include file="../base/_headerMain.jspf"%>
<!-- Content Header (Page header) -->
<link rel="stylesheet" href="${pageContext.request.contextPath}/dist/css/app/sinhvien.css">
<!-- Main content -->
<section class="content-header">
	<h3 class="col-md-3 ml-3">Thông tin chi tiết</h3>
	<div class="wrap-btn-header">
		<button type="button" class="btn bg-maroon" id="btnSave" onclick="saveStudent()">
			<i class="fa fa-save"></i> Lưu
		</button>
		<button type="button" class="btn bg-maroon" id="btnClear" onclick="addNewStudent()">
			<i class="fa fa-plus"></i> THÊM MỚI
		</button>

		<c:if test="${ student != null }">
			<button type="button" class="btn bg-purple"
				id-student="${student.id_student}"
					id-status="${student.status_id }" id="btnUpdate" onclick="openStatusStudent(this)">
				<i class="fa fa-save"></i> CẬP NHẬT THÔNG TIN
			</button>
		</c:if>

		<a href="/SinhVien/Index" class="btn bg-maroon"> <i
			class="fa fa-arrow-left "></i>
		</a>
	</div>
</section>



<section class="" id="wrap-sv">
	<!-- Main row -->
	<div class="row">
		<div class="col-lg-3">
			<div class="box box-primary">
				<div class="box-body box-profile">
					<div class="form-group">
						<select class="form-control select2" style="width: 100%;" id="lop"
							name="lop">
							<option selected="selected" value="">Chọn lớp</option>
							<c:forEach var="itemClass" items="${ classes }">
								<option value="${ itemClass.id_class }">${ itemClass.name_class }
								</option>
							</c:forEach>
						</select>
					</div>
					<div class="form-group d-flex">
						<input class="form-control " type="text" id="qSearch"
							name="qSearch" placeholder="Tìm kiếm nhanh">
						<button type="button" class="btn bg-maroon btnSearch" onclick="setValueLoad()">
							<i class="fa fa-search"></i>
						</button>
					</div>
					<div class="wrap-chitiet" id="miniListAllStudents">
						<div id="results" style="max-height: inherit;">
						<c:set var="path" value="/static/upload/avatar/"/>
							<c:forEach var="student" items="${ students }">
								<c:choose>
									<c:when test="${ empty fn:trim(student.image_student)}">
										<div class="itemSV" data-id=${ student.id_student } onclick="loadInfoStudent(this)">
											<img
												src="${path}dealftUser.png"
												alt=""> <span>${student.id_student} - ${ student.full_name }
												- ${ student.statusStudentDTO.statusDTO.name_status }</span>
										</div>
									</c:when>
									<c:otherwise>
										<div class="itemSV" data-id=${ student.id_student } onclick="loadInfoStudent(this)">
											<img src="${path}${student.image_student}" alt=""> <span>${student.id_student}
												- ${ student.full_name } - ${ student.statusStudentDTO.statusDTO.name_status }</span>
										</div>
									</c:otherwise>
								</c:choose>
							</c:forEach>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-lg-9">
			<c:choose>
				<c:when test="${ student != null }">
					<form action="/SinhVien/addOrUpdate" id="form-student"
						enctype="multipart/form-data">
						<input type="hidden" value="" id="hiddenStudentId"
							name="hiddenStudentId">
						<section class="content">
							<div class="row">
							<c:set var="path" value="/static/upload/avatar/"/>
								<c:choose>
									<c:when test="${ empty fn:trim(student.image_student)}">
										<div class="col-lg-3" id="wrap-img-sv">
											<img id="image_student_photo" class="profile-photo"
												accept="image/*"
												src="${path}dealftUser.png"
												style="cursor: auto; width: 200px; height: 200px;"> <input
												type="file" name="fileData" id="image_student"
												class="inputImgSV" data-max-size="50" accept="image/*">
										</div>
									</c:when>
									<c:otherwise>
										<div class="col-lg-3" id="wrap-img-sv">
											<img id="image_student_photo" class="profile-photo"
												accept="image/*"
												src="${path}${student.image_student}"
												style="cursor: auto; width: 200px; height: 200px;"> <input
												type="file" name="fileData" id="image_student"
												class="inputImgSV" data-max-size="50" accept="image/*">
										</div>
									</c:otherwise>

								</c:choose>

								<div class="col-lg-9">
									<div class="wrap-form-sv">
										<div class="row">
											<div class="col-lg-4">
												<div class="form-group">
													<input class="form-control" type="text" id="id_student"
														name="id_student" value="${student.id_student}"
														placeholder="Mã sinh viên" isInputAllowVarchar disabled
														required>
												</div>
											</div>
											<div class="col-lg-4">
												<div class="form-group">
													<input class="form-control" type="text" id="last_name"
														name="last_name" placeholder="Họ sinh viên"
														value="${student.last_name}" required>
												</div>
											</div>
											<div class="col-lg-4">
												<div class="form-group">
													<input class="form-control" type="text" id="first_name"
														name="first_name" placeholder="Tên sinh viên"
														value="${student.first_name}" required>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-lg-4">
												<div class="form-group">
													<input class="form-control" type="text" id="full_name"
														name="full_name" placeholder="Họ tên đầy đủ"
														value="${student.full_name}" required>
												</div>
											</div>
											<div class="col-lg-4">
												<div class="form-group">
													<select class="form-control select2" style="width: 100%;"
														id="course" name="course_id" onchange="changeCourseId()">
														<c:forEach var="course" items="${ courses }"
															varStatus="loop">
															<c:choose>
																<c:when
																	test="${ course.id_course  == student.course_id }">
																	<option selected="selected"
																		value="${ course.id_course }">${ course.name_course }
																	</option>
																</c:when>
																<c:otherwise>
																	<option value="${ course.id_course }">${ course.name_course }
																</c:otherwise>
															</c:choose>
														</c:forEach>
													</select>
												</div>
											</div>
											<div class="col-lg-4">
												<div class="form-group">
													<select class="form-control select2" style="width: 100%;"
														data-placeholder="Chọn course" name="course_family"
														id="course_family">
														<c:forEach var="course" items="${ allCourse }">
															<c:if
																test="${ course.id_course == student.course_family }">
																<option selected="selected"
																	value="${ course.id_course }">${ course.name_course }
																</option>
															</c:if>
														</c:forEach>
													</select>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-lg-4">
												<div class="form-group">
													<select class="form-control select2" style="width: 100%;"
														id="current_class" name="current_class" required>
														<c:forEach var="itemClass" items="${ classes }"
															varStatus="loop">
															<c:choose>
																<c:when
																	test="${itemClass.id_class == student.current_class}">
																	<option selected="selected" value="${itemClass.id_class }">${itemClass.name_class }
																	</option>
																</c:when>
																<c:otherwise>
																	<option value="${itemClass.id_class }">${itemClass.name_class }
																	</option>
																</c:otherwise>
															</c:choose>
														</c:forEach>
													</select> <span style="color: red;" id="class-notification"></span>
												</div>
											</div>
											<div class="col-lg-4">
												<div class="form-group">
													<input class="form-control" type="text" id="first_class"
														name="first_class" placeholder="Lớp đầu tiên"
														value="${student.first_class}" readonly>
												</div>
											</div>
											<div class="col-lg-4">
												<div class="form-group">
													<div class="input-group">
														<!-- data-inputmask="'alias': 'dd/mm/yyyy'" data-mask -->
														<input type="text" class="form-control datepicker"
															id="application_date" name="application_date"
															placeholder="Ngày đăng ký"
															value="${student.application_date}" required>
														<div class="input-group-addon">
															<i class="fa fa-calendar"></i>
														</div>
													</div>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-lg-4">
												<div class="form-group">
													<div class="input-group">
														<input type="text" class="form-control datepicker"
															id="date_of_doing" name="date_of_doing"
															placeholder="Ngày bắt đầu học"
															value="${student.date_of_doing}" required>
														<div class="input-group-addon">
															<i class="fa fa-calendar"></i>
														</div>
													</div>
												</div>
											</div>
											<!-- <div class="col-lg-4">
										<select class="form-control select2" style="width: 100%;"
											id="status_id" name="status_id" required>
											<option value="" selected="selected">Chọn trạng thái</option>
											<c:forEach var="status" items="${ getAllStatus }">
												<c:choose>
													<c:when test="${st}"></c:when>
												</c:choose>
											</c:forEach>
										</select>
									</div> -->
											<div class="col-lg-4">
												<div class="form-group">
													<div class="input-group">
														<input type="text" class="form-control datepicker"
															id="dob" name="dob" placeholder="Ngày sinh" required
															value="${student.dob}">
														<div class="input-group-addon">
															<i class="fa fa-calendar"></i>
														</div>
													</div>
												</div>
											</div>
											<div class="col-lg-4">
												<select class="form-control select2" style="width: 100%;"
													id="sex" name="sex" required>
													<option value="" selected="selected">Chọn giới
														tính</option>
													<c:choose>
														<c:when test="${student.sex}">
															<option selected="selected" value="true">Nam</option>
															<option value="false">Nữ</option>
														</c:when>
														<c:otherwise>
															<option value="true">Nam</option>
															<option selected="selected" value="false">Nữ</option>
														</c:otherwise>
													</c:choose>
												</select>
											</div>
										</div>
										<div class="row">
											<div class="col-lg-4">
												<div class="form-group">
													<input class="form-control" type="text" id="email_student"
														name="email_student" placeholder="Email"
														value="${ student.email_student }">
												</div>
											</div>
											<div class="col-lg-4">
												<div class="form-group">
													<input class="form-control" type="text" id="high_school"
														name="high_school" placeholder="Trường cấp 3"
														value="${ student.high_school }">
												</div>
											</div>
											<div class="col-lg-4">
												<div class="form-group">
													<input class="form-control" type="text" id="university"
														name="university" placeholder="Đại học"
														value="${ student.university }">
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-lg-8">
												<div class="form-group">
													<input class="form-control" type="text" id="email_school"
														name="email_school" placeholder="Email trường"
														value="${ student.email_school }">
												</div>
											</div>
											<div class="col-lg-4">
												<select class="form-control select2" style="width: 100%;"
													id="active_account" name="active_account" required>
													<option value="">Trạng thái
														account</option>
													<c:choose>
														<c:when test="${ student.active_account }">
															<option selected="selected" value="true">Mở</option>
															<option value="false">Đóng</option>
														</c:when>
														<c:otherwise>
															<option value="true">Mở</option>
															<option selected="selected" value="false">Đóng</option>
														</c:otherwise>
													</c:choose>
												</select>
											</div>
										</div>
										<div class="row">
											<div class="col-lg-4">
												<div class="form-group">
													<input class="form-control" type="text" id="mobile_phone"
														name="mobile_phone" placeholder="Di động" onlynumber
														value="${ student.mobile_phone }">
												</div>
											</div>
											<div class="col-lg-8">
												<div class="form-group">
													<input class="form-control" type="text" id="address"
														name="address" placeholder="Địa chỉ"
														value="${ student.address }">
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-lg-4">
												<div class="form-group">
													<input class="form-control" type="text" id="contact_phone"
														name="contact_phone" placeholder="Di động liên hệ"
														onlynumber required value="${ student.contact_phone }">
												</div>
											</div>
											<div class="col-lg-8">
												<div class="form-group">
													<input class="form-control" type="text"
														id="contact_address" name="contact_address"
														placeholder="Địa chỉ liên hệ"
														value="${ student.contact_address }">
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-lg-4">
												<div class="form-group">
													<input class="form-control" type="text" id="home_phone"
														name="home_phone" placeholder="Điện thoại nhà"
														value="${ student.home_phone }" onlynumber>
												</div>
											</div>
											<div class="col-lg-4">
												<div class="form-group">
													<input class="form-control" type="text" id="district"
														name="district" placeholder="Quận/huyện"
														value="${ student.district }">
												</div>
											</div>
											<div class="col-lg-4">
												<div class="form-group">
													<input class="form-control" type="text" id="city"
														name="city" placeholder="Thành phố/Tỉnh"
														value="${ student.city }">
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</section>
					</form>
					<section>
						<div class="row" style="margin-top: 20px;">
							<div class="col-lg-12">
								<div class="nav-tabs-custom">
									<ul class="nav nav-tabs">
										<li class="active"><a href="#quanHe" data-toggle="tab">Quan
												hệ</a></li>
										<%-- <li><a href="#diemThi" data-toggle="tab">Điểm thi</a></li> --%>
										<li><a href="#noMon" data-toggle="tab">Nợ môn</a></li>
									</ul>
									<div class="tab-content">
										<div class="active tab-pane" id="quanHe">
											<table id="tableQH"
												class="table table-bordered table-striped">
												<thead>
													<tr>
														<th style="min-width: 120px;">Mối quan hệ</th>
														<th style="max-width: 150px;">Tên người thân</th>
														<th style="max-width: 100px;">Nghề nghiệp</th>
														<th style="max-width: 100px;">SDT</th>
														<th style="min-width: 300px;">Địa chỉ</th>
														<th class="text-center" style="min-width: 60px;">Mặc
															định</th>
														<th class="text-center">
															<button type="button" class="btn bg-maroon btnAdd"
																id="addRowQuanhe" onclick="addRowQuanhe()">
																<i class="fa fa-plus"></i>
															</button>
														</th>
													</tr>
												</thead>
												<tbody class="tblChiTiet">
													<c:choose>
														<c:when test="${empty jsonHoso }">
															<tr>
																<td><select class="form-control"
																	style="width: 100%;" class="idQuanhe" name="idQuanhe"
																	required>
																		<option selected="selected" value="">Chọn
																			quan hệ</option>
																		<c:forEach var="dmqh" items="${ danhMucQuanHe }"
																			varStatus="loop">
																			<option value="${ dmqh.id_status }">${ dmqh.name_status }
																		</c:forEach>
																</select></td>
																<td><input class="form-control" type="text"
																	class="nameQuanhe" name="nameQuanhe" placeholder=""
																	required="required"></td>
																<td><input class="form-control" type="text"
																	class="nghenghiepQuanhe" name="nghenghiepQuanhe"
																	placeholder="" required="required"></td>
																<td><input class="form-control" type="text"
																	class="sdtQuanhe" name="sdtQuanhe" placeholder=""
																	onlynumber required="required"></td>
																<td><input class="form-control" type="text"
																	class=diachiQuanhe name="diachiQuanhe" placeholder=""
																	required="required"></td>
																<td class="text-center"><input type="checkbox"
																	class="macdinhQuanhe" name="macdinhQuanhe"></td>
																<td class="text-center">
																	<button type="button" class="btn bg-purple"
																		class="saveQuanhe" name="saveQuanhe"
																		onclick="addOrUpdateQuanhe(this)" value="">
																		<i class="fa fa-save"></i>
																	</button>
																	<button type="button" class="btn bg-orange btnRemove"
																		class="removeQuanhe" name="removeQuanhe"
																		onclick="addOrUpdateQuanhe(this)" value="">
																		<i class="fa fa-minus"></i>
																	</button>
																</td>
															</tr>
														</c:when>
														<c:otherwise>
															<c:forEach var="jsonItem" items="${jsonHoso}" varStatus="loop">
<%-- 															<c:forEach begin="0" end="${jsonHoso.length()-1}" var="index">--%>														
																<tr>
																	<td><select class="form-control"
																		style="width: 100%;" class="idQuanhe" name="idQuanhe"
																		required="required">
																			<c:forEach var="dmqh" items="${ danhMucQuanHe }">
																				<c:choose>
																					<c:when
																						test="${jsonItem.idQuanhe==dmqh.id_status}">
																						<option value="${ dmqh.id_status }" selected>${ dmqh.name_status }
																					</c:when>
																					<c:otherwise>
																						<option value="${ dmqh.id_status }">${ dmqh.name_status }
																					</c:otherwise>
																				</c:choose>
																			</c:forEach>
																	</select></td>
																	<td><input class="form-control" type="text"
																		class="nameQuanhe" name="nameQuanhe" placeholder=""
																		value="${jsonItem.nameQuanhe}"
																		required="required"></td>
																	<td><input class="form-control" type="text"
																		class="nghenghiepQuanhe" name="nghenghiepQuanhe"
																		placeholder=""
																		value="${jsonItem.nghenghiepQuanhe}"
																		required="required"></td>
																	<td><input class="form-control" type="text"
																		class="sdtQuanhe" name="sdtQuanhe" placeholder=""
																		value="${jsonItem.sdtQuanhe}"
																		onlynumber required="required"></td>
																	<td><input class="form-control" type="text"
																		class=diachiQuanhe name="diachiQuanhe" placeholder=""
																		value="${jsonItem.diachiQuanhe}"
																		required="required"></td>
																	<c:choose>
																		<c:when
																			test="${jsonItem.macdinhQuanhe==true}">
																			<td class="text-center"><input type="checkbox"
																				class="macdinhQuanhe" name="macdinhQuanhe"
																				checked="checked"
																				onclick="changeMacdinhQuanhe(this)"></td>
																		</c:when>
																		<c:otherwise>
																			<td class="text-center"><input type="checkbox"
																				class="macdinhQuanhe" name="macdinhQuanhe"
																				onclick="changeMacdinhQuanhe(this)"></td>
																		</c:otherwise>
																	</c:choose>
																	<td class="text-center">
																		<button type="button" class="btn bg-purple"
																			id="saveQuanhe" name="saveQuanhe"
																			value="${jsonItem.idHoso}"
																			onclick="addOrUpdateQuanhe(this)">
																			<i class="fa fa-save"></i>
																		</button>
																		<button type="button" class="btn bg-orange btnRemove"
																			id="removeQuanhe" name="removeQuanhe"
																			value="${jsonItem.idHoso}"
																			onclick="deleteQuanhe(this)">
																			<i class="fa fa-minus"></i>
																		</button>
																	</td>
																</tr>
															</c:forEach>

														</c:otherwise>
													</c:choose>
												</tbody>
											</table>
										</div>
										<%-- <div class=" tab-pane" id="diemThi">
											<div class="tableSV">
												<div class="headTitle">
													<div class="colHK">Học kì</div>
													<div class="colMH">Môn học</div>
													<div class="colLT">Loại thi</div>
													<div class="text-right colDT">Điểm thi</div>
													<div class="colPass">Pass</div>
												</div>
												<div class="bodyTable">
													<div class="itemRow">
														<div class="colHK itemHK">HK1</div>
														<div class="wrap-MH">
															<div class="itemMH">
																<div class="colMH titleMH">EAD</div>
																<div class="itemLT">
																	<div class="colLT">E</div>
																	<div class="colLT">R</div>
																</div>
																<div class="itemDT">
																	<div class="colDT">20</div>
																	<div class="colDT">4</div>
																</div>
																<div class="itemPass">
																	<div class="colPass">
																		<span class="label label-success text-uppercase">pass</span>
																	</div>
																	<div class="colPass">
																		<span class="label label-danger text-uppercase">not</span>
																	</div>
																</div>
															</div>
															<div class="itemMH">
																<div class="colMH titleMH">EAD</div>
																<div class="itemLT">
																	<div class="colLT">E</div>
																	<div class="colLT">R</div>
																</div>
																<div class="itemDT">
																	<div class="colDT">20</div>
																	<div class="colDT">4</div>
																</div>
																<div class="itemPass">
																	<div class="colPass">
																		<span class="label label-success text-uppercase">pass</span>
																	</div>
																	<div class="colPass">
																		<span class="label label-danger text-uppercase">not</span>
																	</div>
																</div>
															</div>
														</div>

													</div>
													<div class="itemRow">
														<div class="colHK itemHK">HK1</div>
														<div class="wrap-MH">
															<div class="itemMH">
																<div class="colMH titleMH">EAD</div>
																<div class="itemLT">
																	<div class="colLT">E</div>
																	<div class="colLT">R</div>
																</div>
																<div class="itemDT">
																	<div class="colDT">20</div>
																	<div class="colDT">4</div>
																</div>
																<div class="itemPass">
																	<div class="colPass">
																		<span class="label label-success text-uppercase">pass</span>
																	</div>
																	<div class="colPass">
																		<span class="label label-danger text-uppercase">not</span>
																	</div>
																</div>
															</div>
															<div class="itemMH">
																<div class="colMH titleMH">EAD</div>
																<div class="itemLT">
																	<div class="colLT">E</div>
																	<div class="colLT">R</div>
																</div>
																<div class="itemDT">
																	<div class="colDT">20</div>
																	<div class="colDT">4</div>
																</div>
																<div class="itemPass">
																	<div class="colPass">
																		<span class="label label-success text-uppercase">pass</span>
																	</div>
																	<div class="colPass">
																		<span class="label label-danger text-uppercase">not</span>
																	</div>
																</div>
															</div>
															<div class="itemMH">
																<div class="colMH titleMH">EAD</div>
																<div class="itemLT">
																	<div class="colLT">E</div>
																	<div class="colLT">R</div>
																</div>
																<div class="itemDT">
																	<div class="colDT">20</div>
																	<div class="colDT">4</div>
																</div>
																<div class="itemPass">
																	<div class="colPass">
																		<span class="label label-success text-uppercase">pass</span>
																	</div>
																	<div class="colPass">
																		<span class="label label-danger text-uppercase">not</span>
																	</div>
																</div>
															</div>
														</div>

													</div>
												</div>
											</div>
										</div> --%>
										<div class="tab-pane" id="noMon">
											
										</div>
									</div>
								</div>
							</div>
						</div>
					</section>
				</c:when>
				<c:otherwise>
					<form action="/SinhVien/addOrUpdate" id="form-student"
						enctype="multipart/form-data">
						<section class="content">
							<div class="row">
								<div class="col-lg-3" id="wrap-img-sv">
								<c:set var="path" value="/static/upload/avatar/"/>
									<img id="image_student_photo" class="profile-photo"
										accept="image/*" src="${path}dealftUser.png"
										style="cursor: auto; width: 200px; height: 200px;"> <input
										type="file" name="fileData" id="image_student"
										class="inputImgSV" data-max-size="50" accept="image/*">
								</div>
								<div class="col-lg-9">
									<div class="wrap-form-sv">
										<div class="row">
											<div class="col-lg-4">
												<div class="form-group">
													<input class="form-control" type="text" id="id_student"
														name="id_student" placeholder="Mã sinh viên"
														isInputAllowVarchar required>
												</div>
											</div>
											<div class="col-lg-4">
												<div class="form-group">
													<input class="form-control" type="text" id="last_name"
														name="last_name" placeholder="Họ sinh viên" required>
												</div>
											</div>
											<div class="col-lg-4">
												<div class="form-group">
													<input class="form-control" type="text" id="first_name"
														name="first_name" placeholder="Tên sinh viên" required>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-lg-4">
												<div class="form-group">
													<input class="form-control" type="text" id="full_name"
														name="full_name" placeholder="Họ tên đầy đủ" required>
												</div>
											</div>
											<div class="col-lg-4">
												<div class="form-group">
													<select class="form-control select2" style="width: 100%;"
														id="course" name="course_id" onchange="changeCourseId()">
														<option selected="selected" value="">Chọn course</option>
														<c:forEach var="course" items="${ courses }"
															varStatus="loop">
															<option value="${ course.id_course }">${ course.name_course }
															</option>
														</c:forEach>
													</select>
												</div>

											</div>
											<div class="col-lg-4">
												<div class="form-group">
													<select class="form-control select2" style="width: 100%;"
														data-placeholder="Chọn course" name="course_family"
														id="course_family">
														<option value="" selected="selected">Chọn Course
															family</option>
													</select>
												</div>

											</div>

										</div>
										<div class="row">
											<div class="col-lg-4">
												<div class="form-group">
													<select class="form-control select2" style="width: 100%;"
														id="current_class" name="current_class" required>
														<option value="" selected="selected">Chọn lớp</option>
														<c:forEach var="itemClass" items="${ classes }"
															varStatus="loop">
															<option value="${itemClass.id_class }">${itemClass.name_class }
															</option>
														</c:forEach>
													</select><span style="color: red;" id="class-notification"></span>
												</div>
											</div>
											<div class="col-lg-4">
												<div class="form-group">
													<input class="form-control" type="text" id="first_class"
														name="first_class" placeholder="Lớp đầu tiên" readonly>
												</div>
											</div>
											<div class="col-lg-4">
												<div class="form-group">
													<div class="input-group">
														<input type="text" class="form-control datepicker"
															id="application_date" name="application_date"
															placeholder="Ngày đăng ký" required>
														<div class="input-group-addon">
															<i class="fa fa-calendar"></i>
														</div>
													</div>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-lg-4">
												<div class="form-group">
													<div class="input-group">
														<input type="text" class="form-control datepicker"
															id="date_of_doing" name="date_of_doing"
															placeholder="Ngày bắt đầu học" required>
														<div class="input-group-addon">
															<i class="fa fa-calendar"></i>
														</div>
													</div>
												</div>
											</div>
											<!-- <div class="col-lg-4">
										<select class="form-control select2" style="width: 100%;"
											id="status_id" name="status_id" required>
											<option value="" selected="selected">Chọn trạng thái</option>
											<c:forEach var="status" items="${ getAllStatus }">
												<c:choose>
													<c:when test="${st}"></c:when>
												</c:choose>
											</c:forEach>
										</select>
									</div> -->
											<div class="col-lg-4">
												<div class="form-group">
													<div class="input-group">
														<input type="text" class="form-control datepicker"
															id="dob" name="dob" placeholder="Ngày sinh" required>
														<div class="input-group-addon">
															<i class="fa fa-calendar"></i>
														</div>
													</div>
												</div>
											</div>
											<div class="col-lg-4">
												<select class="form-control select2" style="width: 100%;"
													id="sex" name="sex" required>
													<option value="" selected="selected">Chọn giới
														tính</option>
													<option value="true">Nam</option>
													<option value="false">Nữ</option>
												</select>
											</div>
										</div>
										<div class="row">
											<div class="col-lg-4">
												<div class="form-group">
													<input class="form-control" type="text" id="email_student"
														name="email_student" placeholder="Email">
												</div>
											</div>
											<div class="col-lg-4">
												<div class="form-group">
													<input class="form-control" type="text" id="high_school"
														name="high_school" placeholder="Trường cấp 3">
												</div>
											</div>
											<div class="col-lg-4">
												<div class="form-group">
													<input class="form-control" type="text" id="university"
														name="university" placeholder="Đại học">
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-lg-8">
												<div class="form-group">
													<input class="form-control" type="text" id="email_school"
														name="email_school" placeholder="Email trường">
												</div>
											</div>
											<div class="col-lg-4">
												<select class="form-control select2" style="width: 100%;"
													id="active_account" name="active_account" required>
													<option value="" selected="selected">Trạng thái
														account</option>
													<option value="true">Mở</option>
													<option value="false">Đóng</option>
												</select>
											</div>
										</div>
										<div class="row">
											<div class="col-lg-4">
												<div class="form-group">
													<input class="form-control" type="text" id="mobile_phone"
														name="mobile_phone" placeholder="Di động" onlynumber>
												</div>
											</div>
											<div class="col-lg-8">
												<div class="form-group">
													<input class="form-control" type="text" id="address"
														name="address" placeholder="Địa chỉ">
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-lg-4">
												<div class="form-group">
													<input class="form-control" type="text" id="contact_phone"
														name="contact_phone" placeholder="Di động liên hệ"
														onlynumber required>
												</div>
											</div>
											<div class="col-lg-8">
												<div class="form-group">
													<input class="form-control" type="text"
														id="contact_address" name="contact_address"
														placeholder="Địa chỉ liên hệ">
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-lg-4">
												<div class="form-group">
													<input class="form-control" type="text" id="home_phone"
														name="home_phone" placeholder="Điện thoại nhà" onlynumber>
												</div>
											</div>
											<div class="col-lg-4">
												<div class="form-group">
													<input class="form-control" type="text" id="district"
														name="district" placeholder="Quận/huyện">
												</div>
											</div>
											<div class="col-lg-4">
												<div class="form-group">
													<input class="form-control" type="text" id="city"
														name="city" placeholder="Thành phố/Tỉnh">
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</section>
					</form>

				</c:otherwise>
			</c:choose>
		</div>
	</div>
</section>

<!-- /.content -->


<!-- /.content -->
<%@ include file="../base/_footer.jspf"%>
<%@ include file="./modal-status.jsp"%>
<script>
var currentId_student=$("#id_student").val();
var current_class_load;
var searchValue_load;
var page = 2;
var lastScrollTop = 0;
var checkInvalidValue=0;
var height=$("#miniListAllStudents").height();
// $(".wrap-chitiet").scroll(function() {
//     var $this = $(this);
//     var $results = $("#results");
//     var resultHeight=parseFloat($results.height());
//     var thisHeight=parseFloat($this.height());
//     var thisScrollTop=parseFloat($this.scrollTop());    
// 	console.log("thisScrollTop:"+thisScrollTop);
//     console.log("thisHeight:"+thisHeight);
//     console.log("resultHeight:"+resultHeight);
//     console.log("tổng thisScrolltop+thisHeight:"+parseFloat(thisScrollTop+thisHeight));
//     console.log(parseFloat(resultHeight-(thisScrollTop + thisHeight)));
//         if (resultHeight-(thisScrollTop + thisHeight) < 10) {
//             scrollToLoadData();    		
//         }
// });


$("#miniListAllStudents").scroll(function() {
	var st = $(this).scrollTop();
	console.log("lastScrollTop: "+lastScrollTop);
	console.log("scrollTop: "+st);
	console.log("height :"+height);

	if (parseFloat(height-st)<30){
	       // downscroll code
	       height=parseFloat(height+st);
	       console.log("new height: "+height);
	       scrollToLoadData();
	   } else {
	      // upscroll code
	   }
	   
});

function scrollToLoadData(){
	var url = "/SinhVien/scrollToGetStudent";
	var insertResult=$("#results");
	$
			.ajax({
				url : url,
				type : "get",
				dataType : 'json',
				data : {
					current_class : current_class_load,
					searchValue : searchValue_load,
					page : page,
				},
				success : function(data) {
					var test=data;
					for (var i = 0; i < data.length; i++) {
							var row='<div class="itemSV" data-id='+ data[i].id_student +' onclick="loadInfoStudent(this)">';
							var imgStd = data[i].image_student;
							if (imgStd == undefined || imgStd == null || imgStd == "") {
									row += '<img src="/static/upload/avatar/dealftUser.png" alt="">';
								} else {
									row += '<img src="/static/upload/avatar/'+data[i].image_student+'" '+
									'alt="">';
								};
							
							var check='statusStudentDTO' in data[i];
							if (!data[i].statusStudentDTO
									||data[i].statusStudentDTO == undefined
									|| data[i].statusStudentDTO == null
									|| data[i].statusStudentDTO == ""){
									row+='<span>'+data[i].id_student +' - '+  data[i].full_name + '</span>';
								}else{
									row+='<span>'+data[i].id_student +' - '+  data[i].full_name + ' - '+data[i].statusStudentDTO.statusDTO.name_status + '</span>';
								}						
							row+='</div>';
							insertResult.append(row);
						};
					page++;
				},
			});
}

function setValueLoad() {
	page = 2;
	height=$("#miniListAllStudents").height();
	scrollheigthOld=0;
	var e4 = document.getElementById("lop");
	current_class_load = e4.options[e4.selectedIndex].value;
	if (current_class_load == undefined) {
		current_class_load = "";
	} else {
	}
	searchValue_load = $("#qSearch").val().replace("/\ /g", "+");
	if (searchValue_load == undefined) {
		searchValue_load = "";
	} else {
	}
	search();
}

function search() {
	
	if (current_class_load == undefined) {
		current_class_load = "";
	} else {
	}
	if (searchValue_load == undefined) {
		searchValue_load = "";
	} else {
	}
	$.ajax({
		url: "/SinhVien/ajaxFilterStudent",
		type : "get",
		dataType : 'json',
		data : {
			current_class : current_class_load,
			searchValue : searchValue_load,
			page : 1,
		},success : function(data) {
			var res=$("#results").empty();
			for(var i=0;i<data.length;i++){
				var row='<div class="itemSV" data-id='+ data[i].id_student +' onclick="loadInfoStudent(this)">';
				var imgStd = data[i].image_student;
				if (imgStd == undefined || imgStd == null || imgStd == "") {
						row += '<img src="/upload/avatar/dealftUser.png" alt="">';
					} else {
						row += '<img src="'+data[i].image_student+'" '+
						'alt="">';
					};
				
				var check='statusStudentDTO' in data[i];
				if (!data[i].statusStudentDTO
						||data[i].statusStudentDTO == undefined
						|| data[i].statusStudentDTO == null
						|| data[i].statusStudentDTO == ""){
						row+='<span>'+data[i].id_student +' - '+  data[i].full_name + '</span>';
					}else{
						row+='<span>'+data[i].id_student +' - '+  data[i].full_name + ' - '+data[i].statusStudentDTO.statusDTO.name_status + '</span>';
					}						
				row+='</div>';
				res.append(row);
			}
		}
	});
}

    function changeCourseId() {
        var courseId = $("#course").val();
        var course_family = $("#course_family");
  
        var optionCourseFamily = "<option value= ''>Chọn course family</option>";
        course_family.empty().append(optionCourseFamily);
  
        $.ajax({
          url: "/KhoaHoc/courseByRootId",
          dataType: "json",
          data: { courseRoot: courseId },
          type: "GET",
          success: function (data) {
            data.forEach((data) =>
              course_family                
                .append(
                  "<option value= '" +
                    data.id_course +
                    "'> " +
                    data.name_course +
                    "</option>"
                )
            );
          },
        });
      };

//=========== photo 

      function readURL(input) {
        if (input.files && input.files[0]) {
          var reader = new FileReader();
          
          reader.onload = function(e) {
            $('#image_student_photo').attr('src', e.target.result);
          }
          
          reader.readAsDataURL(input.files[0]); // convert to base64 string
        }
      }

      $("#image_student").on('change',function (){
            readURL(this)
      });

//============

    

    function addNewStudent(){
        window.location.href="/SinhVien/formStudent";

    };
    

	function saveStudent(){

	        let studentForm = {
            id_student : $('#id_student').val(),
            last_name : $('#last_name').val(),
            first_name : $('#first_name').val(),
            full_name : $('#full_name').val(),
            first_class : $('#first_class').val(),
            current_class : $('#current_class').val(),
            application_date : $('#application_date').val(),
            date_of_doing : $('#date_of_doing').val(),
            status_id : $('#status_id').val(),
            dob : $('#dob').val(),
            sex : $('#sex').val(),
            email_student : $('#email_student').val(),
            mobile_phone : $('#mobile_phone').val(),
            address : $('#address').val(),
            contact_phone : $('#contact_phone').val(),
            contact_address : $('#contact_address').val(),
            home_phone : $('#home_phone').val(),
            email_school : $('#email_school').val(),
            high_school : $('#high_school').val(),
            course : $('#course').val(),
            course_family : $('#course_family').val(),
            university : $('#university').val(),
            district : $('#district').val(),
            city : $('#city').val(),
            active_account : $('#active_account').val()
        }
        //
        s_Save('/SinhVien/addOrUpdate',$("#form-student"),function(data){
			if(data=="true"){
				ThongBao_ThanhCong("Lưu thành công");
				window.location.href = "/SinhVien/formStudent?studentId="+$('#id_student').val();
				
			}else{
				ThongBao_Loi("Lưu thất bại");
				ThongBao_Loi(data);//thông báo check trùng date off hiện lỗi return
			}			
		},null)
	}


		$('#current_class').on('change',function(){
			var currentClass = $('#current_class').val();
			$("#class-notification").empty();
			$.ajax({
				url : "/SinhVien/checkClassValid?class_id="+currentClass,
				type : "GET", 
                success : function(data) {
				   let res = data.split('%');
				   if(res[0]==='error'){
					   checkInvalidValue+=1;
					ThongBao_Loi(res[1])
				   }else if(res[0]==='errorfull'){
					   checkInvalidValue+=1;
					$("#class-notification").html(res[1]);
				   }else if(res[0]==='success'){
					$("#class-notification").html(res[1]);
					}
                }
			})
		});

	function addOrUpdateQuanhe(event){
		var id_student = $("#id_student").val();
		var parent = $(event).parents("tr");
		var idHoso=$(event).val();
		if(idHoso==null||idHoso==""||idHoso==undefined){
			idHoso= new Date().getTime()+id_student.toString();
		}else{

		}
		var idQuanheTag=parent.find("td select").eq(0);
		var idQuanhe=$(idQuanheTag).children("option").filter(":selected").val();
		var nameQuanhe=parent.find("td input").eq(0).val();
		var nghenghiepQuanhe=parent.find("td input").eq(1).val();
		var sdtQuanhe=parent.find("td input").eq(2).val();
		var diachiQuanhe=parent.find("td input").eq(3).val();
		var macdinhQuanhe=parent.find("td input").eq(4).is(":checked");
		$.ajax({
			url : "/SinhVien/addOrUpdateQuanhe",
			type : "POST",
			dataType: "json",
			data: {
				idStudent:id_student,
				idHoso:idHoso,
				idQuanhe:idQuanhe,
				nameQuanhe:nameQuanhe,
				nghenghiepQuanhe:nghenghiepQuanhe,
				sdtQuanhe:sdtQuanhe,
				diachiQuanhe:diachiQuanhe,
				macdinhQuanhe:macdinhQuanhe
				},
            success : function(data) {
			   if(data=="success")
				   {ThongBao_ThanhCong("Cập nhật thành công!");
				   window.location.href = "/SinhVien/formStudent?studentId="+$('#id_student').val();}
			   else if(data=="error")
				   {ThongBao_Loi("Cập nhật thất bại!");}
			   else if(data=="not found id")
				   {ThongBao_Loi("Không tìm thấy quan hệ này!");}
			   else{ThongBao_Loi("Có lỗi xảy ra, hãy thử lại!");}
            }
		})
	}

	function deleteQuanhe(event){
		var id_student = $("#id_student").val();
		var idHoso=$(event).val();
		if(idHoso==null||idHoso==""||idHoso==undefined){
			ThongBao_Loi("Không tìm thấy id quan hệ để xóa!");
		}else{
			$.ajax({
				url : "/SinhVien/deleteQuanhe",
				type : "POST",
				dataType: "json",
				data: {
					idStudent:id_student,
					idHoso:idHoso,
					},
	            success : function(data) {
				   if(data=="nothing to delete"){
					   ThongBao_Loi("Không có mối quan hệ để xóa!");
					   }
				   else if(data=="delete success"){
					   window.location.href = "/SinhVien/formStudent?studentId="+$('#id_student').val();
					   ThongBao_ThanhCong("Xóa thành công!");
				   }
	            }
			})
		}	
	}

	function changeMacdinhQuanhe(event){
		var id_student = $("#id_student").val();
		var macdinhQuanhe=$(event).is(":checked");
		var parent = $(event).parents("tr");
		var idHoso=parent.find("td button").eq(0).val();
		var idQuanheTag=parent.find("td select").eq(0);
		var textIdQuanhe=$(idQuanheTag).children("option").filter(":selected").text();
		var nameQuanhe=parent.find("td input").eq(0).val();
		if(idHoso==null||idHoso==""||idHoso==undefined){
			ThongBao_Loi("Không thể cập nhật mặc định cho dòng chưa có dữ liệu!");
		}else{
			$.ajax({
				url : "/SinhVien/checkMacdinhQuanhe",
				type : "POST",
				dataType: "json",
				data: {
					idStudent:id_student,
					idHoso:idHoso,
					macdinhQuanhe:macdinhQuanhe
					},
	            success : function(data) {
				   if(data=="nothing to set"){
					   ThongBao_Loi("Không có mối quan hệ để đặt mặc định!");
					   }
				   else if(data=="setting default success"){
					   ThongBao_ThanhCong("Đã thiết lập "+textIdQuanhe+": "+nameQuanhe+" là mối quan hệ mặc định!");
					   window.location.href = "/SinhVien/formStudent?studentId="+$('#id_student').val();
					}else if(data=="cancel setting default success"){
						ThongBao_ThanhCong("Đã hủy thiết lập mặc định của mối quan hệ "+textIdQuanhe+":"+nameQuanhe);
						window.location.href = "/SinhVien/formStudent?studentId="+$('#id_student').val();
					}
	            }
			})
		}	
	}

	function addRowQuanhe(){
		var table=$("#tableQH tbody");
		var test=${jsonDanhMucQuanHe};
		var row='<tr>'+
			'<td><select class="form-control"'+
				'style="width: 100%;" class="idQuanhe" name="idQuanhe"'+
				'required>'+
					'<option selected="selected" value="">Chọn quan hệ</option>';
						for(var i=0;i<test.length;i++){
							row+='<option value="'+test[i].id_status+'">'+test[i].name_status+'</option>';
						};
			row+='</select></td>'+
			'<td><input class="form-control" type="text" '+
				'class="nameQuanhe" name="nameQuanhe" placeholder="" '+
				'required></td>'+
			'<td><input class="form-control" type="text" '+
				'class="nghenghiepQuanhe" name="nghenghiepQuanhe" '+
				'placeholder="" required></td>'+
			'<td><input class="form-control" type="text" '+
				'class="sdtQuanhe" name="sdtQuanhe" placeholder="" '+
				'onlynumber required></td>'+
			'<td><input class="form-control" type="text" '+
				'class=diachiQuanhe name="diachiQuanhe" placeholder="" '+
				'required></td>'+
			'<td class="text-center"><input type="checkbox" '+
				'class="macdinhQuanhe" name="macdinhQuanhe"></td>'+
			'<td class="text-center">'+
				'<button type="button" class="btn bg-purple" '+
					'class="saveQuanhe" name="saveQuanhe" '+
					'onclick="addOrUpdateQuanhe(this)" value="">'+
					'<i class="fa fa-save"></i>'+
				'</button>'+
				'<button type="button" class="btn bg-orange btnRemove" '+
					'class="removeQuanhe" name="removeQuanhe" '+
					'onclick="addOrUpdateQuanhe(this)" value="">'+
					'<i class="fa fa-minus"></i>'+
				'</button>'+
			'</td>'+
		'</tr>';
			table.append(row);
		}
	
//======= validate student

	$('#id_student').focusout(function() {
        var id_student =$("#id_student").val(); 
        if(id_student === "") ThongBao_Loi("Nhập mã học sinh");
        else{
            $.ajax({
                url : "/SinhVien/checkId",
                data : {
                    "id_student" : id_student
                },
                type : "post",
                success : function(data) {
                    if(data=="not exist"){
							ThongBao_ThanhCong("Mã sinh viên chưa tồn tại, có thể tạo mới!");
                        }else if(data=="existed"){
								ThongBao_Loi("Mã sinh viên đã tồn tại, hãy chọn mã khác!");
                            }else{
									ThongBao_Loi("Có lỗi xảy ra, hãy thử lại!");
                                }
                }
            });
        }
	});

    $('#email_school').focusout(function() {
        var email_school =$("#email_school").val(); 
        if(email_school === "") ThongBao_Loi("nhập email");
        else{
            $.ajax({
                url : "/SinhVien/checkEmail",
                data : {
                    "email_school" : email_school
                },
                type : "post",
                success : function(data) {
                	if(data=="not exist"){
						ThongBao_ThanhCong("Email chưa tồn tại, có thể tạo mới!");
                    }else if(data=="existed"){
							ThongBao_Loi("Email đã tồn tại, hãy chọn email khác!");
                        }else{
								ThongBao_Loi("Có lỗi xảy ra, hãy thử lại!");
                            }
                }
            });
        }
	});
   
    function loadInfoStudent(event){
		let student_id = $(event).attr("data-id");
		window.location.href = "/SinhVien/formStudent?studentId="+student_id;
        };


    function reformatDate(dateStr)
    {
      dArr = dateStr.split("-");  // ex input "2010-01-18"
      return dArr[2]+ "/" +dArr[1]+ "/" +dArr[0]; //ex out: "18/01/10"
    }

</script>

<c:if test="${ student == null }">
	<script>
		$('#current_class').on('change',function(){
			var currentClass = $('#current_class').find('option:selected').text().replace(/\s+/g, '');
			$('#first_class').val(currentClass); 
		})
	</script>
</c:if>

<c:if test="${student !=null}">
	<script>
		var urlLoadNoMon="/PhieuThu/getListNoMon/"+$('#id_student').val();
		$("#noMon").load(urlLoadNoMon);
		function openStatusStudent(event) {
			let idStudent = $(event).attr("id-student");
			let idStatus = $(event).attr("id-status");
			$.ajax({
				url : "/SinhVien/getStudentById?id=" + idStudent,
				type : "get",
				dataType : 'json',
				success : function(data) {
					var parentNode_status = $("#status_id").parent();
					var ulitem_status = $(parentNode_status).find(
							"span.select2-selection__rendered");
					var arr_option_status = $("#status_id option");
					for (i = 0; i < arr_option_status.length; i++) {
						if ($(arr_option_status[i]).val() == data.status_id) {
							$(arr_option_status[i]).prop("selected", true);
							$(ulitem_status).prop("title",
									$(arr_option_status[i]).html());
							$(ulitem_status).html($(arr_option_status[i]).html());
							break;
						}
					}
					;
					//$("#modal-update-status").modal("show");
					$("#student_id_status").val(data.id_student);
				},
			});
			$
					.ajax({
						url : "/SinhVien/getLatestStatusStudentByStudentIdAndStatusId?student_id="
								+ idStudent + "&status_id=" + idStatus,
						type : "get",
						dataType : 'json',
						success : function(data) {
							$("#start_date").val(reformatDate(data.start_date));
							$("#end_date").val(reformatDate(data.end_date));
							$("#note").val(data.note);
						},
					});
			$("#modal-update-status").modal("show");
		}

		function saveStatusStudent() {
			var check = compare();
			if (check == false) {
				ThongBao_Loi("Ngày kết thúc phải lớn hơn ngày bắt đầu!");
			} else {
				let statusStudentData = {
					status_id : $('#status_id').val(),
					student_id : $('#student_id_status').val(),
					start_date : $('#start_date').val(),
					end_date : $('#end_date').val(),
					note : $('#note').val()
				};
				console.log(statusStudentData);
				if($('#status_id').val()==""||$('#status_id').val()==null){
					ThongBao_Loi("Hãy chọn tình trạng!");
					}else{
				$.ajax({
					url : "/SinhVien/updateStatusStudent/",
					type : "post",
					data : statusStudentData,
					success : function(data) {
						let res = data.split('%');
						res[0] === 'error' ? ThongBao_Loi(res[1])
								: ThongBao_ThanhCong(res[1]);
						$("#form-status-sinhvien").find("input").val(null);
						var parentNode_status = $("#status_id").parent();
						var ulitem_status = $(parentNode_status).find(
								"span.select2-selection__rendered");
						$("#status_id").find('option:selected').removeAttr(
								"selected");
						var name = $("#status_id option:nth-child(1)")
								.prop("label");
						$(ulitem_status).prop("title", name);
						$(ulitem_status).html(name);
						$("#modal-update-status").modal("hide");
						window.location.href = "/SinhVien/formStudent?studentId="+$('#id_student').val();
					},
				});}
			}
		}

		function clearStatusStudentModal() {
			$("#form-status-sinhvien").find("input").val(null);
			var parentNode_status = $("#status_id").parent();
			var ulitem_status = $(parentNode_status).find(
					"span.select2-selection__rendered");
			$("#status_id").find('option:selected').removeAttr("selected");
			document.getElementById('status_id').selectedIndex = -1;
			var name = $("#status_id option:nth-child(1)").prop("label");
			$(ulitem_status).prop("title", name);
			$(ulitem_status).html(name);
		}

		function compare() {
			var end_time = $("#end_date").val();
			var start_time = $("#start_date").val();
			var momentA = moment(start_time, "DD/MM/YYYY");
			var momentB = moment(end_time, "DD/MM/YYYY");
			return momentB.isSameOrAfter(momentA);
		}
	</script>
</c:if>