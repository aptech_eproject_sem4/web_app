<%@ page contentType="text/html;charset=UTF-8" language="java" isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>"
<!--modal default-->
        <div class="modal fade" id="modal-course">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"
                  aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="title-popup">Thêm khóa học</h4>
              </div>
              <div class="modal-body">
              <form id="form-course" action="/KhoaHoc/addOrUpdate" method="POST" enctype="multipart/form-data">
                <div class="row">
                  <div class="col-lg-12">
                    <div class="row">
                      <div class="col-lg-6">
                        <div class="form-group">
                          <label for="" class="required">Mã khóa học</label>
                          <input class="form-control" type="text" autocomplete="off" id="id_course" name="id_course"
                            isInputAllowVarchar required>
                        </div>
                      </div>
                      <div class="col-lg-6">
                        <div class="form-group">
                          <label for="" class="required">Tên viết tắt</label>
                          <input class="form-control" type="text" autocomplete="off" id="sort_name" name="sort_name"
                            isInputAllowVarchar required>
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="" class="required">Tên khóa học</label>
                      <input class="form-control" type="text" autocomplete="off" id="name_course" name="name_course" required>
                    </div>
                    <div class="row">
                      <div class="col-lg-6">
                        <div class="form-group">
                          <label for="">Khóa học </label>
                          <select class="form-control select2" style="width:
                            100%;" id="course_root" name="course_root">
                            <option selected="selected" value="">Chọn khóa học</option>
                            <c:forEach var="item" items="${lscr}">
										<option value="${item.id_course}">${item.sort_name} - ${item.name_course}</option>
							</c:forEach>
                          </select>
                        </div>
                      </div>
                      <div class="col-lg-6">
                        <div class="form-group">
                          <label for="">Giá tiền</label>
                          <input class="form-control" autocomplete="off" type="text" id="price_course" name="price_course"
                            ismoney>
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="">Ghi chú</label>
                      <textarea class="form-control" id="note" name="note" autocomplete="off"
                        placeholder=""></textarea>
                    </div>
                  </div>
                </div>
                </form>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left"
                  data-dismiss="modal">Đóng</button>
                <button type="button" class="btn btn-primary" id="btnSave">Lưu</button>
              </div>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
        <!--modal default-->