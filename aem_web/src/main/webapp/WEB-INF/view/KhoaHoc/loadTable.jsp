
<%@ page contentType="text/html;charset=UTF-8" language="java"
	isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
 <!--table-->
<table id="table" class="table table-bordered table-striped">
                <thead>
                  <tr>
                    <th width="45" class="text-center stt">STT</th>
                    <th>Khóa học</th>
                    <th>Tên viết tắt</th>
                    <th>Giá tiền</th>
                    <th>Ngày tạo</th>
                    <th style="max-width: 200px;">Ghi chú</th>
                    <th>Thao tác</th>
                  </tr>
                </thead>
       <tbody class="tblChiTiet">
                <c:forEach var="item" items="${lsc}">
                  <tr>
                    <td class="stt"></td>
                    <td>
                      <span class="label label-primary"> ${item.id_course}</span> <br>
                      <span> ${item.name_course}</span>
                    </td>
                    <td>
                       ${item.sort_name}
                    </td>
                    <td>
                      <i class="fa fa-money-bill"></i> ${item.price_course}
                    </td>
                  
                 	  <td>
                       ${item.date_create}
                    </td>
                    <td><i> ${item.note}</i></td>
                    <td>
                      <button type="button" class="btn-edit" onclick="editModal('${item.id_course}','${item.name_course}','${item.sort_name}','${item.course_root}','${item.price_course}','${item.note}')"><i class="fa
                          fa-pencil"></i></button>
                      <button type="button" class="btn-del" onclick="Delete('${item.id_course}','/KhoaHoc/Delete',function(){search()})"><i class="fa
                          fa-trash-o"></i></button>
                    </td>
                  </tr>
                  </c:forEach>
                </tbody>
                <tfoot>
				<tr>
					<th width="45" class="text-center">STT</th>
                    <th>Khóa học</th>
                    <th>Tên viết tắt</th>
                    <th>Giá tiền</th>
                     <th>Ngày tạo</th>
                    <th style="max-width: 200px;">Ghi chú</th>
                    <th>Thao tác</th>
		         </tr>
              </tfoot>
              </table>
            </div>
            <!--table-->