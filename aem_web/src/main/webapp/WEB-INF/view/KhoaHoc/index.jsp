<%@ page contentType="text/html;charset=UTF-8" language="java"
	isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ include file="../base/_headerMain.jspf"%>
<!-- Content Wrapper. Contains page content -->
<!-- Content Header (Page header) -->
<section class="content-header">
	<h1>Quản lý Khóa học</h1>
	<div class="m-auto">
		<select class="form-control select2" style="width: 100%;" id="lsc"
			name="lsc">
			<c:forEach var="item" items="${lscr}">
				<option value="${item.id_course}">${item.sort_name}-
					${item.name_course}</option>
			</c:forEach>
		</select>
	</div>
	<div class="wrap-btn-header">
		<button id="btnCreate" type="button" class="btn bg-maroon">
			<i class="fa fa-plus"></i> Thêm
		</button>
	</div>
</section>
<!-- Main content -->
<section class="content">
	<!-- Main row -->
	<div class="row">

		<!--table-->
		<div class="col-lg-12">
			<div class="filter-table"></div>
			<div class="wrap-table" id="getData">
				<%@ include file="loadTable.jsp"%>
			</div>

		</div>
		<!--table-->
		<div></div>
	</div>

</section>
<!-- /.content -->
<%@ include file="modalAction.jsp"%>
<%@ include file="../base/_footer.jspf"%>

<script>
	var modal = $("#modal-course");
	var checkIdStatus;
	$("#btnCreate").click(function() {
		var error = $("label.error");
		$(error).empty();
		$("input.form-control").removeClass("error");
		$("textarea.form-control").removeClass("error");
		modal.find('input').val(null);
		modal.find('textarea').val(null);
		$("#title-popup").html("Thêm khóa học");
		modal.modal("show");
	})
	$('#id_course').change(function() {
		$.ajax({
			url : "/KhoaHoc/checkIdCourse",
			data : {
				"idCourse" : $("#id_course").val()
			},
			type : "post",
			success : function(data) {
				if (data == "existed") {
					ThongBao_Loi("Mã đã tồn tại, hãy chọn mã khác!");
					checkIdStatus = "reject";
				} else if (data == "not exist") {
					ThongBao_ThanhCong("Mã chưa tồn tại, có thể tạo mới!");
					checkIdStatus = "accept";
				} else {
					ThongBao_Loi("Có lỗi xảy ra!");
				}
			}
		});
	});
	$('#lsc').on('change', function() {
		search();
	});
	$("#btnSave").click(function() {
		if (checkIdStatus == "reject") {
			ThongBao_Loi("Mã đã tồn tại, hãy chọn mã khác!");
		} else if (checkIdStatus == "accept") {
			s_Save('/KhoaHoc/addOrUpdate', $("#form-course"), function(data) {
				if (data == "true") {
					ThongBao_ThanhCong("Lưu thành công");
					modal.modal("hide");
					search();

				} else {
					Thong_BaoLoi("Lưu thất bại");
					Thong_BaoLoi(data);//thông báo check trùng date off hiện lỗi return
				}
			}, null)
		} else {
			ThongBao_Loi("Có lỗi xảy ra, hãy thử lại!");
		}
	})
	function editModal(id_course, name_course, sort_name, course_root,
			price_course, note) {
		checkIdStatus="accept";
		$("#title-popup").html("Sửa khóa học");
		modal.find("#id_course").val(id_course);
		modal.find("#name_course").val(name_course);
		modal.find("#sort_name").val(sort_name);
		modal.find("#course_root").val(course_root);
		modal.find("#price_course").val(price_course);
		modal.find("#note").val(note);
		modal.modal("show");

		var error = $("label.error");
		$(error).empty();
		$("input.form-control").removeClass("error").addClass("valid");
		$("textarea.form-control").removeClass("error").addClass("valid");

		var parentNode_courseroot = $("#course_root").parent();
		var ulitem_courseroot = $(parentNode_courseroot).find(
				"span.select2-selection__rendered");
		var arr_option_courseroot = $("#course_root option");
		$(ulitem_courseroot).empty();
		for (i = 0; i < arr_option_courseroot.length; i++) {
			if ($(arr_option_courseroot[i]).val() == course_root) {
				$(arr_option_courseroot[i]).prop("selected", true);
				$(ulitem_courseroot).prop("title",
						$(arr_option_courseroot[i]).html());
				$(ulitem_courseroot).html($(arr_option_courseroot[i]).html());
				break;
			}
		}

	}

	function search() {
		var e = document.getElementById("lsc");
		var value = e.options[e.selectedIndex].value;
		var url = "/KhoaHoc/Index?ajaxLoad=table&idCourse=" + value;
		$("#getData").load(url, function() {
			InitDataTable();
		})
	}
</script>