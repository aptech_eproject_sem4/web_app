<%@ page contentType="text/html;charset=UTF-8" language="java"
	isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<table id="tableFC" class="table table-bordered table-striped">
	<thead>
		<tr>
			<th width="45">STT</th>
			<th style="max-width: 200px;">Tên FC</th>
			<th>Liên hệ</th>
			<th>Tình trạng</th>
			<th>Quyền</th>
			<th>Thao tác</th>
		</tr>
	</thead>
	<tbody class="tblChiTiet">
		<c:forEach var="item" items="${listFc}">
			<tr>
				<td class="text-center"></td>
				<td><span class="label label-primary" id="getIdFc">${item.id_fc }</span>
					<br> <span>${item.name_fc }</span></td>
				<td><span>SDT: ${item.phone_fc }</span><br> <span>Email:
						${item.email_fc }</span><br> <span>Email school:
						${item.email_school }</span></td>
				<td><span class="label label-primary" data-toggle="modal" id="addFcStatus"
					onclick="editModalFcStatus('${item.id_fc}','${item.status_id}','${item.start_date}','${item.end_date}','${item.note_status}')">Cập
nhật</span>
				<!-- data-target="#modal-fc-status" -->
					<br> <c:choose>
						<c:when test="${item.status_id==null}">

						</c:when>
						<c:otherwise>
							<c:forEach var="itemStatus" items="${listStatus}">
								<c:choose>
									<c:when test="${item.status_id==itemStatus.id_status }">
										<span>Tình trạng: ${itemStatus.name_status }</span>
									</c:when>
									<c:otherwise>
									</c:otherwise>
								</c:choose>
							</c:forEach>
							<br>
							<span>Thời gian: ${item.start_date } - ${item.end_date } </span>
							<br>
							<span>Ghi chú: <i>${item.note_status }</i>
							</span>
						</c:otherwise>
					</c:choose></td>
				<td><c:set var="rolePart"
						value="${fn:split(item.list_role, ',')}" /> <c:forEach
						var="itemRolePart" items="${rolePart}">
						<c:forEach var="itemRole" items="${listRole}">
							<c:choose>
								<c:when test="${itemRolePart==itemRole.id}">
									<span>${itemRole.name_role},</span>
								</c:when>
								<c:otherwise>
								</c:otherwise>
							</c:choose>
						</c:forEach>
					</c:forEach></td>

				<td><c:choose>
						<c:when test="${item.active_account==true}">
							<button type="button" class="btn-warning"
								onclick="changeActiveAccount(this)">
								<i class="fa fa-lock"></i>
							</button>
						</c:when>
						<c:otherwise>
							<button type="button" class="btn-warning"
								onclick="changeActiveAccount(this)">
								<i class="fa fa-unlock"></i>
							</button>
						</c:otherwise>
					</c:choose>
					<button type="button" data-id="${item.id_fc}" class="btn-edit btn-edit-fc" onclick="editFC(this)">
						<i class="fa fa-pencil"></i>
					</button>
					<button type="button" class="btn-del"
						onclick="Delete('${item.id_fc}','/FC/Delete',function(){search()})">
						<i class="fa fa-trash-o"></i>
					</button></td>
			</tr>
		</c:forEach>
	</tbody>
	<tfoot>
		<tr>
			<th>STT</th>
			<th>Tên FC</th>
			<th>Liên hệ</th>
			<th>Tình trạng</th>
			<th>Quyền</th>
			<th>Thao tác</th>
		</tr>
	</tfoot>
</table>





