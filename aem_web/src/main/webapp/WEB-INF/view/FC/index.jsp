<%@page import="com.fasterxml.jackson.annotation.JsonInclude.Include"%>
<%@ page contentType="text/html;charset=UTF-8" language="java"
	isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ include file="../base/_headerMain.jspf"%>


<!-- Content Header (Page header) -->
<section class="content-header">
	<h1>Quản lý Giảng viên</h1>
	<div class="wrap-btn-header">
		<!-- <button type="button" class="btn bg-maroon"><i class="fa fa-download"></i></button>
          <button type="button" class="btn bg-maroon"><i class="fa fa-upload"></i></button> -->
		<button type="button" id="btnAddNew" class="btn bg-maroon">
			<i class="fa fa-plus"></i> Thêm
		</button>
	</div>
</section>

<!-- Main content -->
<section class="content">
	<!-- Main row -->
	<div class="row">

		<!--table-->
		<div class="col-lg-12">
			<div class="filter-table">
				<div class="row">
					<div class="col-lg-2">
						<div class="form-group">
							<select class="form-control select2" style="width: 100%;"
								id="listStatus" name="listStatus">
								<option selected="selected" value="">Chọn trạng thái</option>
								<c:forEach var="item" items="${listStatus}">
									<option value="${item.id_status }">${item.name_status }</option>
								</c:forEach>
							</select>
						</div>
					</div>
					<div class="col-lg-2">
						<div class="form-group">
							<input class="form-control " type="text" id="qSearch"
								name="qSearch" placeholder="Tìm kiếm nhanh">
						</div>
					</div>
					<div class="col-lg-1">
						<button type="button" class="btn bg-maroon btnSearch"
							id=qSearchbtn onclick="search()">
							<i class="fa fa-search"></i>
						</button>
					</div>
				</div>
			</div>
			<div class="wrap-table" id="getData">
				<%@ include file="loadTable.jsp"%>
			</div>

		</div>
		<!--table-->
		<div></div>
	</div>

</section>
<!-- /.content -->
<%@ include file="modal-fc-status.jsp"%>
<%@ include file="../base/_footer.jspf"%>
<script>
	var modal_status = $("#modal-fc-status");
	
	function editModalFcStatus(id_fc,status_id,start_date,end_date,note_status) {
	    //clear all input
	    modal_status.find("input").val(null);
	    modal_status.find("#idFcUpdateStatus").val(id_fc);
	    modal_status.find("#status_id").val(status_id);
	    if(start_date==""||start_date==null){
	    	modal_status.find("#start_date").val(null);
			}else{
				modal_status.find("#start_date").val(reformatDate(start_date));
				}
	    if(end_date==""||end_date==null){
	    	modal_status.find("#end_date").val(null);
			}else{
				modal_status.find("#end_date").val(reformatDate(end_date));
				}    
	    modal_status.find("#note_status").val(note_status);
	    $("#idFcUpdateStatus").prop("disabled", true);
	    var error = $("label.error");
	    $(error).empty();
	    $("input.form-control").removeClass("error").addClass("valid");
	    var parentNode_status = $("#status_id").parent();
	    var ulitem_status = $(parentNode_status).find(
	      "span.select2-selection__rendered"
	    );
	    $("#statud_id").find('option:selected').removeAttr("selected");
		document.getElementById('status_id').selectedIndex = -1;
		var name = $("#status_id option:nth-child(1)")
				.prop("label");
		$(ulitem_status).prop("title", name);
		$(ulitem_status).html(name);
	    var arr_option_status = $("#status_id option");
	    for (i = 0; i < arr_option_status.length; i++) {
	      if ($(arr_option_status[i]).val() == status_id) {
	        $(arr_option_status[i]).prop("selected", true);
	        $(ulitem_status).prop("title", $(arr_option_status[i]).html());
	        $(ulitem_status).html($(arr_option_status[i]).html());
	        break;
	      }
	    }
	    modal_status.modal("show");
	}
	  
	$("#btnAddStatus").click(function () {
		var check=compare();
		if(check==false){ThongBao_Loi("Ngày kết thúc phải lớn hơn ngày bắt đầu!");}else{
	      s_Save(
	        "/FC/addOrUpdateFcStatus",
	        $("#form-fc-status"),
	        function (data) {
	          if (data == "true") {
	            ThongBao_ThanhCong("Lưu thành công");
	            modal_status.modal("hide");			
	            search();
	          } else if(data=="not found id"){
	        	  ThongBao_Loi("Không tìm thấy id giảng viên!");
		      } else{
	            ThongBao_Loi("Lưu thất bại");
	            ThongBao_Loi(data);
	          }
	        },
	        null
	      );}
	  });

	  function changeActiveAccount(event, idfc) {
		    var currentRow = $(event).closest("tr");
		    var idfc = currentRow.find("td:eq(1) #getIdFc").text();
		    $.ajax({
		      url: "/FC/changeActiveAccount",
		      data: {
		        id_fc: idfc,
		      },
		      type: "post",
		      success: function (data) {
		        ThongBao_ThanhCong(data);
		        search();
		      },
		    });
		  }
	  
	function InitDataTable(selectorTable) {
		selectorTable = selectorTable ?? $("#tableFC");
		var table = selectorTable.DataTable({
		  ordering: false,
		  order: [[1, "asc"]],
		});
		table
		  .on("order.dt search	.dt", function () {
			table
			  .column(0, { search: "applied", order: "applied" })
			  .nodes()
			  .each(function (cell, i) {
				cell.innerHTML = i + 1;
			  });
		  })
		  .draw();
	  }
	  function search() {
		var e1 = document.getElementById("listStatus");
		var value1 = e1.options[e1.selectedIndex].value;
		var qSearch = $("#qSearch").val().replace("/\ /g", "+");
		var url =
		  "/FC/Index?ajaxLoad=table&status=" +
		  value1 +
		  "&searchValue=" +
		  qSearch;
		$("#getData").load(url, function () {
		  InitDataTable();
		});
	  }
	
	  function compare() {
			var end_time=$("#end_date").val();
			var start_time=$("#start_date").val();
		    var momentA = moment(start_time,"DD/MM/YYYY");
		    var momentB = moment(end_time,"DD/MM/YYYY");
		    return momentB.isSameOrAfter(momentA);
		}

	  function reformatDate(dateStr)
	  {
	    dArr = dateStr.split("-");  // ex input "2010-01-18"
	    return dArr[2]+ "/" +dArr[1]+ "/" +dArr[0]; //ex out: "18/01/10"
	  }
	  $("#btnAddNew").click(function(){
		  window.location.href="/FC/formFC"
	  });
	 function editFC(event){
		  window.location.href = "/FC/formFC?idFc="+ $(event).attr("data-id");
	  };

</script>