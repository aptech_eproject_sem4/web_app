<%@ page contentType="text/html;charset=UTF-8" language="java"
	isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ include file="../base/_headerMain.jspf"%>
<!-- Content Header (Page header) -->
<!-- Main content -->
<section class="content-header">
	<h3 class="col-md-3 ml-3">Thông tin chi tiết</h3>
	<div class="wrap-btn-header">
		<button type="button" class="btn bg-maroon" id="btnSave">
			<i class="fa fa-save"></i> Lưu
		</button>
		<button type="button" class="btn bg-maroon" id="btnClear">
			<i class="fa fa-plus"></i> THÊM MỚI
		</button>
		<a href="/FC/Index" class="btn bg-maroon"> <i
			class="fa fa-arrow-left "></i>
		</a>
	</div>
</section>
<section class="">
	<!-- Main row -->
	<div class="row">
		<div class="col-lg-3">
			<div class="box box-primary">
				<div class="box-body box-profile">
					<div class="form-group">
						<select class="form-control select2" style="width: 100%;"
							id="listStatus" name="listStatus">
							<option selected="selected" value="">Chọn trạng thái</option>
							<c:forEach var="item" items="${listStatus}">
								<option value="${item.id_status }">${item.name_status }</option>
							</c:forEach>
						</select>
					</div>
					<div class="form-group d-flex">
						<input class="form-control " type="text" id="qSearch"
							name="qSearch" placeholder="Tìm kiếm nhanh">
						<button type="button" class="btn bg-maroon btnSearch"
							onclick="setValueLoad()">
							<i class="fa fa-search"></i>
						</button>
					</div>
					<div class="wrap-chitiet" id="results">
						<c:forEach var="itemfc" items="${ fcs }">
							<c:choose>
								<c:when test="${itemfc.status_id != null}">
									<c:forEach var="listStatus" items="${ listStatus }">
										<c:choose>
											<c:when test="${itemfc.status_id==listStatus.id_status }">
												<div class="itemSV" data-id=${ itemfc.id_fc }
													onclick="loadInfoFC(this)">
													<span>${ itemfc.id_fc } - ${ itemfc.name_fc} -
														${listStatus.name_status}</span>
												</div>
											</c:when>
											<c:otherwise>
											</c:otherwise>
										</c:choose>
									</c:forEach>
								</c:when>
								<c:otherwise>
									<div class="itemSV" data-id=${ itemfc.id_fc }
										onclick="loadInfoFC(this)">
										<span>${ itemfc.id_fc } - ${ itemfc.name_fc} </span>
									</div>
								</c:otherwise>
							</c:choose>
						</c:forEach>


					</div>

				</div>
			</div>
		</div>
		<div class="col-lg-9">
			<p></p>
			<c:choose>
				<c:when test="${fc != null }">
					<form action="/FC/addOrUpdateFC" id="fc-form"
						enctype="multipart/form-data">
						<input id="fc-id" type="hidden" value="">
						<section class="content">
							<div class="row">
								<div class="col-lg-12">
									<div class="col-lg-4">
										<div class="form-group">
											<input class="form-control" type="text" id="id_fc"
												name="id_fc" placeholder="Mã FC" value="${fc.id_fc}"
												isInputAllowVarchar required disabled>
										</div>
									</div>
									<div class="col-lg-8">
										<div class="form-group">
											<input class="form-control" type="text" id="name_fc"
												name="name_fc" placeholder="Họ tên FC" required
												value="${fc.name_fc}">
										</div>
									</div>
									<div class="col-lg-4">
										<div class="form-group">
											<input class="form-control" type="text" id="phone_fc"
												name="phone_fc" placeholder="Di động" onlynumber
												value="${fc.phone_fc}">
										</div>
									</div>
									<div class="col-lg-4">
										<div class="form-group">
											<input class="form-control" type="text" id="email_fc"
												name="email_fc" placeholder="Email" value="${fc.email_fc}">
										</div>
									</div>
									<div class="col-lg-4">
										<div class="form-group">
											<input class="form-control" type="text" id="email_school"
												name="email_school" placeholder="Email trường"
												value="${fc.email_school}">
										</div>
									</div>
									<div class="col-lg-4">
										<div class="form-group">
											<div class="input-group">
												<input type="text" class="form-control my-colorpicker1"
													id="color_css" name="color_css" placeholder="Chọn màu FC"
													required value="${fc.color_css}">
												<div class="input-group-addon">
													<i></i>
												</div>
											</div>
										</div>
									</div>
									<div class="col-lg-4">
										<div class="form-group">
											<select class="form-control select2" id="list_role"
												name="list_role" multiple style="width: 100%;"
												data-placeholder="Chọn quyền">
												 
												<c:forEach var="itemRole" items="${listRole}">
													<c:forEach var="itemRolePart" items="${rolePart}">
														<c:if test="${itemRole.id==itemRolePart }">
															<option value="${itemRole.id}" selected>${itemRole.name_role}</option>
														</c:if>
														<option value="${itemRole.id}">${itemRole.name_role}</option>
													</c:forEach>
												</c:forEach>
											</select>
										</div>

									</div>
									<div class="col-lg-4">
										<div class="form-group">
											<select class="form-control select2" style="width: 100%;"
												id="active_account" name="active_account" required>
												<option value="">Trạng thái account</option>
												<c:choose>
													<c:when test="${ fc.active_account }">
														<option selected="selected" value="true">Mở</option>
														<option value="false">Đóng</option>
													</c:when>
													<c:otherwise>
														<option value="true">Mở</option>
														<option selected="selected" value="false">Đóng</option>
													</c:otherwise>
												</c:choose>
											</select>
										</div>
									</div>

								</div>
							</div>

						</section>


					</form>
					<section>
						<div class="row" style="margin-top: 20px;">
							<div class="col-lg-12">
								<div class="nav-tabs-custom">
									<ul class="nav nav-tabs">
										<li class="active"><a href="#bacLuong" data-toggle="tab">Bậc
												lương</a></li>
										<li><a href="#luongTheoGio" data-toggle="tab">Lương
												theo giờ</a></li>
										<li><a href="#diemThi" data-toggle="tab">Điểm thi
												portal</a></li>
									</ul>
									<div class="tab-content">
										<div class="active tab-pane" id="bacLuong">
											<form id="form-gradeSalaryFC"
												action="/FC/addOrUpdateGradeSalaryFc" method="POST"
												enctype="multipart/form-data">
												<table id="tableBL"
													class="table table-bordered table-striped">
													<thead>
														<tr>
															<th width="45">STT</th>
															<th>Ngày bắt đầu</th>
															<th>Số tiền/giờ</th>
															<th class="text-center">
																<button type="button" class="btn bg-maroon" id="btnAdd">
																	<i class="fa fa-plus"></i>
																</button>
															</th>
														</tr>
													</thead>
													<tbody class="tblChiTiet">

														<c:set var="index" value="1" />
														<c:choose>
															<c:when test="${empty gsfc}">
																<tr>
																	<td class="thuTu">${index}</td>
																	<td>
																		<div class="input-group">
																			<input type="text" class="form-control datepicker"
																				id="start_date" name="start_date" required>
																			<input type="hidden" id="id" name="id"> <input
																				type="hidden" id="fc_id" name="fc_id"
																				value="${fc.id_fc} ">
																			<div class="input-group-addon">
																				<i class="fa fa-calendar"></i>
																			</div>
																		</div>
																	</td>
																	<td><input class="form-control" type="text"
																		id="hour_salary" name="hour_salary" ismoney></td>
																	<td class="text-center">
																		<button type="button" class="btn bg-purple "
																			onclick="AddOrUpdateGradeFC(this)">
																			<i class="fa fa-save"></i>
																		</button>
																		<button type="button" class="btn bg-orange"
																			onclick="DeleteRow(this)">
																			<i class="fa fa-minus"></i>
																		</button>
																	</td>
																	<c:set var="index" value="${index+1}" />
																</tr>
															</c:when>
															<c:otherwise>
																<c:forEach var="itemGSFC" items="${gsfc}">
																	<tr>
																		<td class="thuTu">${index}</td>
																		<td>
																			<div class="input-group">
																				<input type="text" class="form-control datepicker"
																					id="start_date " name="start_date"
																					value="${itemGSFC.start_date}" required> <input
																					type="hidden" id="id" name="id"
																					value="${itemGSFC.id}"> <input
																					type="hidden" id="fc_id" name="fc_id"
																					value="${fc.id_fc}">
																				<div class="input-group-addon">
																					<i class="fa fa-calendar"></i>
																				</div>
																			</div>
																		</td>
																		<td><input class="form-control" type="text"
																			id="hour_salary" value="${itemGSFC.hour_salary}"
																			name="hour_salary" ismoney></td>
																		<td class="text-center">
																			<button type="button" class="btn bg-purple "
																				onclick="AddOrUpdateGradeFC(this)">
																				<i class="fa fa-save"></i>
																			</button>
																			<button type="button" class="btn bg-orange"
																				onclick="DeleteRow(this)">
																				<i class="fa fa-minus"></i>
																			</button>
																		</td>
																		<c:set var="index" value="${index+1}" />
																	</tr>
																</c:forEach>
															</c:otherwise>
														</c:choose>
													</tbody>

												</table>
											</form>
										</div>
										<div class=" tab-pane" id="luongTheoGio">
											<section class="content-header">
												<h1>Bảng lương chi tiết của FC</h1>
												<div class="wrap-btn-header d-flex">
													<button type="button" class="btn btn-primary">
														<i class="fa fa-caret-left"><i></i></i>
													</button>
													<input class="form-control text-center" id="dateSearch"
														value="10/2020" readonly>
													<button type="button" class="btn btn-primary">
														<i class="fa fa-caret-right"><i></i></i>
													</button>
												</div>
											</section>
											<section class="content">
												<table id="tableLuong"
													class="table table-bordered table-striped">
													<thead>
														<th>Môn học/Hoạt động</th>
														<th>Lớp</th>
														<th>Thời gian</th>
														<th class="text-right">Số giờ</th>
														<th class="text-right">Hệ số</th>
														<th class="text-right">Quy đổi</th>
													</thead>
													<tbody>
														<tr>
															<td>CSW</td>
															<td>c1808g1</td>
															<td class="relate" style="width: 150px"><span
																class="from">Fr:</span> 23/11/2020 <br> <span
																class="to">To:</span> 23/11/2020</td>
															<td class="text-right">30</td>
															<td class="text-right">1</td>
															<td class="text-right">30</td>
														</tr>
														<tr>
															<td>C#</td>
															<td>c1808g1</td>
															<td class="relate" style="width: 150px"><span
																class="from">Fr:</span> 23/11/2020 <br> <span
																class="to">To:</span> 23/11/2020</td>
															<td class="text-right">30</td>
															<td class="text-right">0.5</td>
															<td class="text-right">15</td>
														</tr>
														<tr>
															<td colspan="5" class="text-right">Tổng số giờ</td>
															<td class="text-right sum">45</td>
														</tr>
														<tr>
															<td colspan="5" class="text-right">Lương/giờ (VND)</td>
															<td class="text-right sum">100.000</td>
														</tr>
														<tr>
															<td colspan="5" class="text-right">Tổng lương (VND)</td>
															<td class="text-right sum">4.500.000</td>
														</tr>
													</tbody>

												</table>
											</section>
										</div>
										<div class="tab-pane" id="diemThi">
											<table id="tableDT"
												class="table table-bordered table-striped">
												<thead>
													<tr>
														<th width="45">STT</th>
														<th>Môn thi</th>
														<th>Số điểm</th>
													</tr>
												</thead>
												<tbody class="tblChiTiet">
													<tr id="form-create-salary-rank">
														<c:set var="index" value="1" />
														<c:forEach var="itemsfc" items="${sfc}" varStatus="loop">
															<c:choose>
																<c:when test="${fc.id_fc==itemsfc.fc_id}">
																	<c:forEach var="sbj" items="${sbj}">
																		<c:choose>
																			<c:when test="${itemsfc.subject_id==sbj.id_subject}">
																				<tr>
																					<td class="thuTu">${index}</td>
																					<td><span>${sbj.sort_name} -
																							${sbj.name_subject}</span></td>
																					<td><span>Điểm
																							số:${itemsfc.score_number}</span> <br> <span>Điểm
																							% : ${itemsfc.score_percent}</span></td>
																				</tr>
																				<c:set var="index" value="${index+1}" />
																			</c:when>
																			<c:otherwise>
																			</c:otherwise>
																		</c:choose>
																	</c:forEach>
																</c:when>
																<c:otherwise>
																</c:otherwise>
															</c:choose>
														</c:forEach>
													</tr>
												</tbody>

											</table>
										</div>
									</div>
								</div>
							</div>
						</div>
					</section>
				</c:when>
				<c:otherwise>
					<form action="/FC/addOrUpdateFC" id="fc-form"
						enctype="multipart/form-data">
						<input id="fc-id" type="hidden" value="">
						<section class="content">
							<div class="row">
								<div class="col-lg-12">
									<div class="col-lg-4">
										<div class="form-group">
											<input class="form-control" type="text" id="id_fc"
												name="id_fc" placeholder="Mã FC" isInputAllowVarchar
												required>
										</div>
									</div>
									<div class="col-lg-8">
										<div class="form-group">
											<input class="form-control" type="text" id="name_fc"
												name="name_fc" placeholder="Họ tên FC" required>
										</div>
									</div>
									<div class="col-lg-4">
										<div class="form-group">
											<input class="form-control" type="text" id="phone_fc"
												name="phone_fc" placeholder="Di động" onlynumber>
										</div>
									</div>
									<div class="col-lg-4">
										<div class="form-group">
											<input class="form-control" type="text" id="email_fc"
												name="email_fc" placeholder="Email">
										</div>
									</div>
									<div class="col-lg-4">
										<div class="form-group">
											<input class="form-control" type="text" id="email_school"
												name="email_school" placeholder="Email trường">
										</div>
									</div>
									<div class="col-lg-4">
										<div class="form-group">
											<div class="input-group">
												<input type="text" class="form-control my-colorpicker1"
													id="color_css" name="color_css" placeholder="Chọn màu FC"
													required>

												<div class="input-group-addon">
													<i></i>
												</div>
											</div>
											<!-- <input type="text" class="form-control my-colorpicker1"
																		placeholder="Chọn màu FC"> -->
										</div>
									</div>
									<div class="col-lg-4">
										<div class="form-group">
											<select class="form-control select2" id="list_role"
												name="list_role" multiple style="width: 100%;"
												data-placeholder="Chọn quyền">
												<c:forEach var="itemRole" items="${listRole}">
													<option value="${itemRole.id }">${itemRole.name_role }</option>
												</c:forEach>
											</select>
										</div>

									</div>
									<div class="col-lg-4">
										<div class="form-group">
											<select class="form-control select2" style="width: 100%;"
												id="active_account" name="active_account" required>
												<option value="">Trạng thái account</option>
												<option value="true">Mở</option>
												<option value="false">Đóng</option>
											</select>
										</div>
									</div>

								</div>
							</div>

						</section>
					</form>
				</c:otherwise>
			</c:choose>
		</div>
	</div>
</section>







<!-- /.content -->
<%@ include file="../base/_footer.jspf"%>


<script>
	var status_load;
	var searchValue_load;

	$("#btnSave").click(function() {
		s_Save('/FC/addOrUpdateFc', $("#fc-form"), function(data) {
			console.log(data);
			if (data == "true") {
				ThongBao_ThanhCong("Lưu thành công");
				window.location.href = "/FC/formFC?idFc="+$('#id_fc').val();
			} else {
				ThongBao_Loi("Lưu thất bại");
				ThongBao_Loi(data);//thông báo check trùng date off hiện lỗi return
			}
		}, null);

	});

	function AddOrUpdateGradeFC(event) {
		var parent = $(event).parents("tr");
		let gradeFC = {
			date_create : "",
			fc_id : parent.find("td input").eq(2).val(),
			hour_salary : parent.find("td input").eq(3).val(),
			id : parent.find("td input").eq(1).val(),
			start_date : parent.find("td div input").eq(0).val(),

		};
		console.log(gradeFC);
		$.ajax({
			url : "/FC/addOrUpdateGradeSalaryFc/",
			type : "post",
			data : gradeFC,
			success : function(data) {
				ThongBao_ThanhCong("Lưu thành công");
				window.location.href = "/FC/formFC?idFc="+$('#id_fc').val();
			},
			error : function(data) {
				ThongBao_Loi("Lưu thất bại");
			}
		});
	}

	function DeleteRow(event) {

		var parent = $(event).parents("tr");
		let gradeFC = {
			id : parent.find("td input").eq(1).val(),
		}
		console.log(gradeFC);
		$.ajax({
			url : "/FC/DeleteGSFC/" + parent.find("td input").eq(1).val(),
			type : "post",
			success : function(data) {
				if(data=="success"){
				ThongBao_ThanhCong("Xóa thành công");
				window.location.href = "/FC/formFC?idFc="+$('#id_fc').val();
				}else if(data=="Not found id!"){
				ThongBao_Loi("Không tìm thấy id!");
				}else{
				ThongBao_Loi("Có lỗi xảy ra, xóa thất bại!");
				}
			}

		});
	}

	$('#id_fc').focusout(function() {
		var id_fc = $("#id_fc").val();
		if (id_fc == "")
			ThongBao_Loi("Nhập mã FC");
		else {
			$.ajax({
				url : "/FC/checkId",
				data : {
					"id_fc" : id_fc
				},
				type : "post",
				success : function(data) {
					ThongBao_ThanhCong(data);
				}
			});
		}
	});

	$('#email_school').focusout(function() {
		var email_school = $("#email_school").val();
		if (email_school == "")
			ThongBao_Loi("nhập email");
		else {
			$.ajax({
				url : "/FC/checkEmail",
				data : {
					"email_school" : email_school
				},
				type : "post",
				success : function(data) {
					var arr = data.split('.');
					if (arr[1] === '1')
						ThongBao_ThanhCong(arr[0]);
					else
						ThongBao_Loi(arr[0]);
				}
			});
		}
	});

	function loadInfoFC(event) {
		let idFC = $(event).attr("data-id");
		window.location.href = "/FC/formFC?idFc=" + idFC;
	};

	function loadNameRole(event) {
		//list_role
	    var parentNode_list_role = $("#list_role").parent();
	    var ulitem_list_role = $(parentNode_list_role).find(
	      "ul.select2-selection__rendered"
	    );
	    var arr_option_list_role = $("#list_role option");
	    let list_role=$(event).attr("data-list-role");
	    //clear list role
	    $(ulitem_list_role).empty();
	    for (i = 0; i < arr_option_list_role.length; i++) {
	      $(arr_option_list_role[i]).prop("selected", false);
	    }
	    for (i = 0; i < arr_option_list_role.length; i++) {
	        for (j = 0; j < listrole.length; j++) {
	          if ($(arr_option_list_role[i]).val() == listrole[j]) {
	            $(arr_option_list_role[i]).prop("selected", true);
	            var item_li = document.createElement("li");
	            $(item_li).addClass("select2-selection__choice");
	            $(item_li).prop("title", $(arr_option_list_role[i]).html());
	            $(item_li).html(
	              '<span class="select2-selection__choice__remove" role="presentation">×</span>' +
	                $(arr_option_list_role[i]).html()
	            );
	            $(ulitem_list_role).append($(item_li));
	            break;
	          }
	        }
      	}
	};

	$('#btnClear').on('click', function() {
		window.location.href = "/FC/formFC";

	});

	$('#btnAdd')
			.on(
					'click',
					function() {
						$('#tableBL > tbody')
								.append(
										'<tr>'
												+ '<td class="thuTu">${index}</td>'
												+ '<td>'
												+ '<div class="input-group">'
												+ '<input type="text" class="form-control datepicker"'+
													'id="start_date" name="start_date" required>'
												+ '<input type="hidden" id="id" name="id">'
												+ '<input type="hidden" id="fc_id" name="fc_id" value="${fc.id_fc} ">'
												+ '<div class="input-group-addon">'
												+ '<i class="fa fa-calendar"></i>'
												+ '</div>'
												+ '</div>'
												+ '</td>'
												+ '<td><input class="form-control" type="text"'+
											'id="hour_salary" name="hour_salary" ismoney></td>'
												+ '<td class="text-center">'
												+ '<button type="button" class="btn bg-purple "'
												+ 'onclick="AddOrUpdateGradeFC(this)">'
												+ '<i class="fa fa-save"></i>'
												+ '</button>'
												+ '<button type="button" class="btn bg-orange" onclick="DeleteRow(this)">'
												+ '<i class="fa fa-minus"></i>'
												+ '</button>'
												+ '</td>'
												+ '<c:set var="index" value="${index+1}" />'
												+ '</tr>');
						$('.datepicker').datepicker({
							dateFormat : 'dd/MM/yyyy'
						});
					});
	// 	function reloadPage() {
	// 		windows.loacation.reload();
	// 	}
	//format yyyy-MM-dd to dd/MM/yyyy
	function reformatDate(dateStr) {
		dArr = dateStr.split("-"); // ex input "2010-01-18"
		return dArr[2] + "/" + dArr[1] + "/" + dArr[0]; //ex out: "18/01/10"
	}

	function setValueLoad() {
		var e4 = document.getElementById("listStatus");
		status_load = e4.options[e4.selectedIndex].value;
		if (status_load == undefined) {
			status_load = "";
		} else {
		}
		searchValue_load = $("#qSearch").val().replace("/\ /g", "+");
		if (searchValue_load == undefined) {
			searchValue_load = "";
		} else {
		}
		search();
	}

	function search() {
		if (status_load == undefined) {
			status_load = "";
		} else {
		}
		if (searchValue_load == undefined) {
			searchValue_load = "";
		} else {
		}
		$.ajax({
			url : "/FC/ajaxFilterFC",
			type : "get",
			dataType : 'json',
			data : {
				status : status_load,
				searchValue : searchValue_load,
			},
			success : function(data) {
				var jsonStatus = (${jsonListStatus});
				var res = $("#results").empty();
				for (var i = 0; i < data.length; i++) {
					var row = '<div class="itemSV" data-id="'+data[i].id_fc +'" onclick="loadInfoFC(this)">'+
							'<span>' + data[i].id_fc + ' - '+
							data[i].name_fc;
					for (var j = 0; j < jsonStatus.length; j++) {
						if (jsonStatus[j].id_status == data[i].status_id) {
							row += ' - ' + jsonStatus[j].name_status;
						}
					}
					row += '</span>' + '</div>';
					res.append(row);
				}
			}
		});
	}
</script>

