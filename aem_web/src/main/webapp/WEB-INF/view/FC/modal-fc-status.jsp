<%@ page contentType="text/html;charset=UTF-8" language="java"
	isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<div class="modal fade" id="modal-fc-status">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"
					aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h4 class="modal-title">Cập nhật tình trạng FC</h4>
			</div>
			<div class="modal-body">
				<form id="form-fc-status" action="/FC/addOrUpdateFcStatus"
					method="POST" enctype="multipart/form-data">
					<input type="hidden" id="idFcUpdateStatus" name="id_fc" value="">
					<div class="row">
						<div class="col-lg-12">
							<div class="form-group">
								<label for="">Tình trạng</label> <select
									class="form-control select2" style="width: 100%;"
									id="status_id" name="status_id" required>
									<option value="">Chọn tình trạng</option>
									<c:forEach var="item" items="${listStatus}">
										<option value="${item.id_status }">${item.name_status }</option>
									</c:forEach>
								</select>
							</div>
							<div class="row">
								<div class="col-lg-6">
									<div class="form-group">
										<label for="">Ngày bắt đầu</label>
										<div class="input-group">
											<input type="text" class="form-control datepicker" name="start_date" id="start_date" autocomplete="off" isdate
												required>
											<div class="input-group-addon">
												<i class="fa fa-calendar"></i>
											</div>
										</div>
									</div>

								</div>
								<div class="col-lg-6">
									<div class="form-group">
										<label for="">Ngày kết thúc</label>
										<div class="input-group">
											<input type="text" class="form-control datepicker" name="end_date" id="end_date" autocomplete="off" isdate
												required>
											<div class="input-group-addon">
												<i class="fa fa-calendar"></i>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="form-group">
								<label for="">Ghi chú</label>
								<textarea class="form-control" id="note_status" name="note_status" placeholder=""></textarea>
							</div>
						</div>
					</div>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default pull-left"
					data-dismiss="modal">Close</button>
				<button type="button" class="btn btn-primary" id="btnAddStatus">Save changes</button>
			</div>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>