<%@ page contentType="text/html;charset=UTF-8" language="java"
	isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<table id="tableSchedule" class="table table-bordered table-striped">
	<thead>
		<tr>
			<th width="45">STT</th>
			<th style="max-width: 200px;">Môn học</th>
			<th>Lớp</th>
			<th>Giảng viên</th>
			<th>Thời gian</th>
			<th>Thông tin</th>
			<th>Số buổi</th>
			<th>Thao tác</th>
		</tr>
	</thead>
	<tbody class="tblChiTiet">
		<c:forEach var="item" items="${schedules}">
			<tr>
				<td class="text-center"></td>
				<td><c:forEach var="itemSubject" items="${listAllSubject}">
						<c:choose>
							<c:when test="${itemSubject.id_subject==item.subject_id}">
								${itemSubject.name_subject}
							</c:when>
							<c:otherwise>
							</c:otherwise>
						</c:choose>
					</c:forEach></td>
				<td><c:forEach var="itemClass" items="${listAllClass}">
						<c:if test="${itemClass.id_class==item.class_id }">
							${itemClass.name_class }
						</c:if>
					</c:forEach></td>
				<td><c:set var="listFCid"
						value="${fn:split(item.list_fc, ',')}" /> <c:forEach var="fc_id"
						items="${listFCid}">
						<c:forEach var="itemFC" items="${listAllFC}">
							<c:choose>
								<c:when test="${fc_id==itemFC.id_fc}">
									<span>${itemFC.name_fc},</span>
								</c:when>
								<c:otherwise>
								</c:otherwise>
							</c:choose>
						</c:forEach>
					</c:forEach></td>
				<td>${item.start_date } - ${item.end_date }</td>
				<td>Ca: ${item.shift_id }<br />Chi nhánh: ${item.brand_id }
				</td>
				<td>Đã học: ${item.current_session }<br />Đăng ký tối đa:
					${item.max_session }
				</td>
				<td><button type="button" data-id="${item.id}" class="btn-edit"
						onclick="editSchedule(this)">
						<i class="fa fa-pencil"></i>
					</button> <c:if test="${item.current_session==0 }">
						<button type="button" class="btn-del"
							onclick="Delete('${item.id}','/LichHoc/Delete',function(){search()})">
							<i class="fa fa-trash-o"></i>
						</button>
					</c:if></td>
			</tr>
		</c:forEach>
	</tbody>
	<tfoot>
		<tr>
			<th width="45">STT</th>
			<th style="max-width: 200px;">Môn học</th>
			<th>Lớp</th>
			<th>Giảng viên</th>
			<th>Thời gian</th>
			<th>Thông tin</th>
			<th>Số buổi</th>
			<th>Thao tác</th>
		</tr>
	</tfoot>
</table>