<%@ page contentType="text/html;charset=UTF-8" language="java"
	isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ include file="../base/_headerMain.jspf"%>

<!-- Content Header (Page header) -->
<section class="content-header">
	<h1>Quản lý Lịch học</h1>
	<div class="wrap-btn-header">
		<!-- <button type="button" class="btn bg-maroon"><i class="fa fa-download"></i></button>
          <button type="button" class="btn bg-maroon"><i class="fa fa-upload"></i></button> -->
		<button type="button" id="btnAddNew" class="btn bg-maroon" data-id="" onclick="editSchedule(this)">
			<i class="fa fa-plus"></i> Thêm
		</button>
	</div>
</section>
<!-- Main content -->
<section class="content">
	<!-- Main row -->
	<div class="row">

		<!--table-->
		<div class="col-lg-12">
			<div class="filter-table">
				<div class="row">
					<div class="col-lg-2">
						<div class="form-group">
							<select class="form-control select2" style="width: 100%;"
								id="listClass" name="listClass">
								<option selected="selected" value="">Chọn lớp</option>
								<c:forEach var="itemClass" items="${listAllClass}">
									<option value="${itemClass.id_class }">${itemClass.name_class }</option>
								</c:forEach>
							</select>
						</div>
					</div>
					<div class="col-lg-2">
						<div class="form-group">
							<select class="form-control select2" style="width: 100%;"
								id="listFC" name="listFC">
								<option selected="selected" value="">Chọn giảng viên</option>
								<c:forEach var="itemFC" items="${listAllFC}">
									<option value="${itemFC.id_fc }">${itemFC.name_fc }</option>
								</c:forEach>
							</select>
						</div>
					</div>
					<div class="col-lg-1">
						<button type="button" class="btn bg-maroon btnSearch"
							id=qSearchbtn onclick="search()">
							<i class="fa fa-search"></i>
						</button>
					</div>
				</div>
			</div>
			<div class="wrap-table" id="getData">
				<%@ include file="loadTable.jsp"%>
			</div>

		</div>
		<!--table-->
		<div></div>
	</div>

</section>
<!-- /.content -->
<%@ include file="../base/_footer.jspf"%>
<script type="text/javascript">
function InitDataTable(selectorTable) {
	selectorTable = selectorTable ?? $("#tableSchedule");
	var table = selectorTable.DataTable({
	  ordering: false,
	  order: [[1, "asc"]],
	});
	table
	  .on("order.dt search	.dt", function () {
		table
		  .column(0, { search: "applied", order: "applied" })
		  .nodes()
		  .each(function (cell, i) {
			cell.innerHTML = i + 1;
		  });
	  })
	  .draw();
  }
  function search() {
	var e1 = document.getElementById("listClass");
	var value1 = e1.options[e1.selectedIndex].value;
	var e2 = document.getElementById("listFC");
	var value2 = e2.options[e2.selectedIndex].value;
// 	var qSearch = $("#qSearch").val().replace("/\ /g", "+");
	var url =
	  "/LichHoc/Index?ajaxLoad=table&filterClass=" +
	  value1 +
	  "&filterFc=" +
	  value2;
// 	  "&searchValue=" +
// 	  qSearch;
	$("#getData").load(url, function () {
	  InitDataTable();
	});
  }

  function editSchedule(event){
	  window.location.href = "/LichHoc/formSchedule?id="+ $(event).attr("data-id");
  };
</script>