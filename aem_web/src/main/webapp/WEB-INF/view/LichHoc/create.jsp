<%@ page contentType="text/html;charset=UTF-8" language="java"
	isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ include file="../base/_headerMain.jspf"%>
<section class="content-header">
	<h3 class="col-md-3 ml-3">Thông tin lịch học</h3>
	<div class="wrap-btn-header">
		<button type="button" class="btn bg-maroon" id="btnSave">
			<i class="fa fa-save"></i> Lưu
		</button>
		<a href="/LichHoc/Index" class="btn bg-maroon"> <i
			class="fa fa-arrow-left "></i>
		</a>
	</div>
</section>
<section>
	<div class="col-lg-12">
		<p></p>
		<c:choose>
			<c:when test="${schedule != null }">
				<form action="/LichHoc/addOrUpdateSchedule" id="schedule-form"
					enctype="multipart/form-data">
					<input id="schedule-id" type="hidden" value="${schedule.id}">
					<input id="current_session" type="hidden"
						value="${schedule.current_session}">
					<section class="content">
						<div class="row">
							<div class="col-lg-12">
								<div class="col-lg-4">
								<span>Lớp học</span>
									<div class="form-group">
										<select class="form-control select2" style="width: 100%;"
											id="class_id" name="class_id" required disabled="disabled"
											onchange="loadStudentByClassId(this)">
											<option value="">Chọn lớp</option>
											<c:forEach var="itemClass" items="${listAllClass}">
												<c:if test="${schedule.class_id==itemClass.id_class }">
													<option value="${itemClass.id_class }" selected="selected">${itemClass.name_class }</option>
												</c:if>
												<option value="${itemClass.id_class }">${itemClass.name_class }</option>
											</c:forEach>
										</select>
									</div>
								</div>
								<div class="col-lg-4">
								<span>Môn học</span>
									<div class="form-group">
										<select class="form-control select2" style="width: 100%;"
											id="subject_id" name="subject_id" required
											disabled="disabled" onchange="setSessionBySubjectId(this)">
											<option value="">Chọn môn học</option>
											<c:forEach var="itemSubject" items="${listAllSubject}">
												<c:if test="${schedule.subject_id==itemSubject.id_subject }">
													<option value="${itemSubject.id_subject }"
														selected="selected">${itemSubject.name_subject }</option>
												</c:if>
												<option value="${itemSubject.id_subject }">${itemSubject.name_subject }</option>
											</c:forEach>
										</select>
									</div>
								</div>
								<div class="col-lg-4">
								<span>Giảng viên</span>
									<div class="form-group">
										<select class="form-control select2" id="list_fc"
											name="list_fc" multiple style="width: 100%;"
											data-placeholder="Chọn giảng viên" required>
											<c:set var="listFC" value="${fn:split(schedule.list_fc,',')}" />
											<c:forEach var="itemFC" items="${listAllFC}">
												<c:forEach var="fc" items="${listFC}">
													<c:if test="${itemFC.id_fc==fc }">
														<option value="${itemFC.id_fc}" selected="selected">${itemFC.name_fc}</option>
													</c:if>
												</c:forEach>
												<option value="${itemFC.id_fc}">${itemFC.name_fc}</option>
											</c:forEach>
										</select>
									</div>
								</div>
								<div class="col-lg-4">
								<span>Ca học</span>
									<div class="form-group">
										<select class="form-control select2" style="width: 100%;"
											id="shift_id" name="shift_id" required>
											<option value="">Chọn ca học</option>
											<c:forEach var="itemShift" items="${listAllShift}">
												<c:if test="${schedule.shift_id==itemShift.id }">
													<option value="${itemShift.id }" selected="selected">${itemShift.name_shift }</option>
												</c:if>
												<option value="${itemShift.id }">${itemShift.name_shift }</option>
											</c:forEach>
										</select>
									</div>
								</div>
								<div class="col-lg-4">
								<span>Chi nhánh</span>
									<div class="form-group">
										<select class="form-control select2" style="width: 100%;"
											id="brand_id" name="brand_id" required>
											<option value="">Chọn nhánh</option>
											<c:forEach var="itemBrand" items="${listAllBrand}">
												<c:if test="${schedule.brand_id==itemBrand.id_status }">
													<option value="${itemBrand.id_status }" selected="selected">${itemBrand.name_status }</option>
												</c:if>
												<option value="${itemBrand.id_status }">${itemBrand.name_status }</option>
											</c:forEach>
										</select>
									</div>
								</div>
								<div class="col-lg-4">
								<span>Số buổi học</span>
									<div class="form-group">
										<div class="input-group" style="width: 100%">
											<input type="text" class="form-control" id="number_session"
												name="number_session" placeholder="Số buổi học" required
												value="${schedule.number_session }" onlynumber
												disabled="disabled">
										</div>
									</div>
								</div>
								<div class="col-lg-4">
								<span>Ngày bắt đầu</span>
									<div class="form-group">
										<input type="text" class="form-control datepicker"
											id="start_date" name="start_date" placeholder="Ngày bắt đầu"
											value="${schedule.start_date }" required>
									</div>
								</div>
								<div class="col-lg-4">
								<span>Ngày kết thúc</span>
									<div class="form-group">
										<input type="text" class="form-control datepicker"
											id="end_date" name="end_date" placeholder="Ngày kết thúc"
											value="${schedule.end_date }" required>
									</div>
								</div>
								<div class="col-lg-4">
								<span>Số buổi học tối đa</span>
									<div class="form-group">
										<div class="input-group" style="width: 100%">
											<input type="text" class="form-control" id="max_session"
												name="max_session" placeholder="Số buổi tối đa" required
												value="${schedule.max_session }" readonly="readonly"
												onlynumber disabled="disabled">
										</div>
									</div>
								</div>
								<div class="col-lg-4">
								<span>Hệ số lương giảng viên</span>
									<div class="form-group">
										<div class="input-group" style="width: 100%">
											<input type="text" class="form-control" id="coef_salary"
												name="coef_salary" placeholder="Hệ số lương FC" required
												value="${schedule.coef_salary }" onlynumber>
										</div>
									</div>
								</div>
								<div class="col-lg-8">
								<span>Ghi chú</span>
									<div class="form-group">
										<div class="input-group" style="width: 100%">
											<input type="text" class="form-control" id="note" name="note"
												placeholder="Ghi chú" value="${schedule.note }">
										</div>
									</div>
								</div>
							</div>
						</div>
					</section>
				</form>
				<p>Chọn sinh viên</p>
				<table id="addStudentTbl"
					class="table table-bordered table-striped addStudentTbl">
					<thead>
						<tr>
							<th style="max-width: 200px;">Mã sinh viên</th>
							<th style="max-width: 200px;">Tên sinh viên</th>
							<th>Lớp</th>
							<th>Email trường</th>
							<th>SDT</th>
							<th></th>
						</tr>
					</thead>
					<tbody class="tblChiTiet">
						<c:set var="listStudentId"
							value="${fn:split(schedule.list_student,',')}" />
						<c:forEach var="itemStudent" items="${listAllStudent}">
							<c:forEach var="idstudent" items="${listStudentId}">
								<c:if test="${itemStudent.id_student==idstudent}">
									<tr>
										<td>${itemStudent.id_student }</td>
										<td>${itemStudent.full_name }</td>
										<td><c:forEach var="itemClass" items="${listAllClass}">
												<c:if
													test="${itemStudent.current_class==itemClass.id_class }">
										${itemClass.name_class }
									</c:if>
											</c:forEach></td>
										<td>${itemStudent.email_school }</td>
										<td>${itemStudent.mobile_phone }</td>
										<td><button type="button" class="btn bg-orange"
												onclick="DeleteRow(this)">
												<i class="fa fa-minus"></i>
											</button></td>
									</tr>
								</c:if>
							</c:forEach>
						</c:forEach>
					</tbody>
					<tfoot>
						<tr>
							<th style="max-width: 200px;">Mã sinh viên</th>
							<th style="max-width: 200px;">Tên sinh viên</th>
							<th>Lớp</th>
							<th>Email trường</th>
							<th>SDT</th>
							<th></th>
						</tr>
					</tfoot>
				</table>
				<br />
				<p>Danh sách buổi học</p>
				<br />
				<table id="addAttendanceTbl"
					class="table table-bordered table-striped addStudentTbl">
					<thead>
						<tr>
							<th style="max-width: 200px;">Ca học</th>
							<th style="max-width: 200px;">Chi nhánh</th>
							<th>Ngày học</th>
							<th>Loại học</th>
							<th>Nghỉ</th>
							<th><button type="button" class="btn bg-maroon"
									id="btnAddRowAttendance" onclick="addRowAttendance()">
									<i class="fa fa-plus"></i>
								</button></th>
						</tr>
					</thead>
					<tbody class="tblChiTiet">
						<c:forEach var="itemAttendance" items="${listAllAttendance }">
							<c:if test="${itemAttendance.schedule_id==schedule.id }">
								<tr>
									<td><select class="form-control select2 shift_id"
										style="width: 100%;" name="shift_id" required>
											<c:forEach var="itemShift" items="${listAllShift}">
												<c:if test="${itemShift.id==itemAttendance.shift_id }">
													<option value="${itemShift.id }" selected="selected">${itemShift.name_shift }</option>
												</c:if>
												<option value="${itemShift.id }">${itemShift.name_shift }</option>
											</c:forEach>
									</select></td>
									<td><select class="form-control select2 brand_id"
										style="width: 100%;" name="brand_id" required>
											<c:forEach var="itemBrand" items="${listAllBrand}">
												<c:if
													test="${itemBrand.id_status==itemAttendance.brand_id }">
													<option value="${itemBrand.id_status }" selected="selected">${itemBrand.name_status }</option>
												</c:if>
												<option value="${itemBrand.id_status }">${itemBrand.name_status }</option>
											</c:forEach>
									</select></td>
									<td><input type="text"
										class="form-control datepicker date_attendance"
										name="date_attendance" placeholder="Ngày điểm danh"
										value="${itemAttendance.date_attendance}" required></td>
									<td><select class="form-control select2 status_id"
										style="width: 100%;" name="status_id" required>
											<c:forEach var="itemSubjectType"
												items="${ listAllSubjectType}">
												<c:if
													test="${itemSubjectType.id_status==itemAttendance.brand_id }">
													<option value="${itemSubjectType.id_status }"
														selected="selected">${itemSubjectType.name_status }</option>
												</c:if>
												<option value="${itemSubjectType.id_status }">${itemSubjectType.name_status }</option>
											</c:forEach>
									</select></td>
									<td><c:choose>
											<c:when test="${itemAttendance.disable==true }">
												<input type="checkbox" class="disable" name="disable"
													checked="checked">
											</c:when>
											<c:otherwise>
												<input type="checkbox" class="disable" name="disable">
											</c:otherwise>
										</c:choose></td>
									<td><button type="button"
											class="btn bg-purple btnSaveRowAttendance"
											data-id="${itemAttendance.id }"
											onclick="arrOrUpdateAttendance(this)">
											<i class="fa fa-save"></i>
										</button>
										<button type="button"
											class="btn bg-orange btnDeleteRowAttendance"
											data-id="${itemAttendance.id }" onclick="DeleteRowAttendance(this)">
											<i class="fa fa-minus"></i>
										</button></td>
								</tr>
							</c:if>
						</c:forEach>
					</tbody>
					<tfoot>
						<tr>
							<th style="max-width: 200px;">Ca học</th>
							<th style="max-width: 200px;">Chi nhánh</th>
							<th>Ngày học</th>
							<th>Loại học</th>
							<th>Nghỉ</th>
							<th></th>
						</tr>
					</tfoot>
				</table>
			</c:when>
			<c:otherwise>
				<form action="/LichHoc/addOrUpdateSchedule" id="schedule-form"
					enctype="multipart/form-data">
					<input id="schedule-id" type="hidden" value=""> <input
						id="current_session" type="hidden" value="0">
					<section class="content">
						<div class="row">
							<div class="col-lg-12">
								<div class="col-lg-4">
								<span>Lớp học</span>
									<div class="form-group">
										<select class="form-control select2" style="width: 100%;"
											id="class_id" name="class_id" required
											onchange="loadStudentByClassId(this)">
											<option value="" selected="selected">Chọn lớp</option>
											<c:forEach var="itemClass" items="${listAllClass}">
												<option value="${itemClass.id_class }">${itemClass.name_class }</option>
											</c:forEach>
										</select>
									</div>
								</div>
								<div class="col-lg-4">
								<span>Môn học</span>
									<div class="form-group">
										<select class="form-control select2" style="width: 100%;"
											id="subject_id" name="subject_id" required
											onchange="setSessionBySubjectId(this)">
											<option value="" selected="selected">Chọn môn học</option>
											<c:forEach var="itemSubject" items="${listAllSubject}">
												<option value="${itemSubject.id_subject }">${itemSubject.name_subject }</option>
											</c:forEach>
										</select>
									</div>
								</div>
								<div class="col-lg-4">
								<span>Giảng viên</span>
									<div class="form-group">
										<select class="form-control select2" id="list_fc"
											name="list_fc" multiple style="width: 100%;"
											data-placeholder="Chọn giảng viên">
											<c:forEach var="itemFC" items="${listAllFC}">
												<option value="${itemFC.id_fc }">${itemFC.name_fc }</option>
											</c:forEach>
										</select>
									</div>
								</div>
								<div class="col-lg-4">
								<span>Ca học</span>
									<div class="form-group">
										<select class="form-control select2" style="width: 100%;"
											id="shift_id" name="shift_id" required>
											<option value="" selected="selected">Chọn ca học</option>
											<c:forEach var="itemShift" items="${listAllShift}">
												<option value="${itemShift.id }">${itemShift.name_shift }</option>
											</c:forEach>
										</select>
									</div>
								</div>
								<div class="col-lg-4">
								<span>Chi nhánh</span>
									<div class="form-group">
										<select class="form-control select2" style="width: 100%;"
											id="brand_id" name="brand_id" required>
											<option value="" selected="selected">Chọn nhánh</option>
											<c:forEach var="itemBrand" items="${listAllBrand}">
												<option value="${itemBrand.id_status }">${itemBrand.name_status }</option>
											</c:forEach>
										</select>
									</div>
								</div>
								<div class="col-lg-4">
								<span>Số buổi học</span>
									<div class="form-group">
										<div class="input-group" style="width: 100%">
											<input type="text" class="form-control" id="number_session"
												name="number_session" placeholder="Số buổi học" required
												value="" onlynumber disabled="disabled">
										</div>
									</div>
								</div>
								<div class="col-lg-4">
								<span>Ngày bắt đầu</span>
									<div class="form-group">
										<input type="text" class="form-control datepicker"
											id="start_date" name="start_date" placeholder="Ngày bắt đầu">
									</div>
								</div>
								<div class="col-lg-4">
								<span>Ngày kết thúc</span>
									<div class="form-group">
										<input type="text" class="form-control datepicker"
											id="end_date" name="end_date" placeholder="Ngày kết thúc">
									</div>
								</div>
								<div class="col-lg-4">
								<span>Số buổi học tối đa</span>
									<div class="form-group">
										<div class="input-group" style="width: 100%">
											<input type="text" class="form-control" id="max_session"
												name="max_session" placeholder="Số buổi tối đa" required
												value="" readonly="readonly" onlynumber disabled="disabled">
										</div>
									</div>
								</div>
								<div class="col-lg-4">
								<span>Hệ số lương giảng viên</span>
									<div class="form-group">
										<div class="input-group" style="width: 100%">
											<input type="text" class="form-control" id="coef_salary"
												name="coef_salary" placeholder="Hệ số lương FC" required
												value="" onlynumber>
										</div>
									</div>
								</div>
								<div class="col-lg-8">
								<span>Ghi chú</span>
									<div class="form-group">
										<div class="input-group" style="width: 100%">
											<input type="text" class="form-control" id="note" name="note"
												placeholder="Ghi chú" required value="">
										</div>
									</div>
								</div>
							</div>
						</div>
					</section>
				</form>
				<p>Chọn sinh viên</p>
				<br />
				<table id="addStudentTbl"
					class="table table-bordered table-striped addStudentTbl">
					<thead>
						<tr>
							<th style="max-width: 200px;">Mã sinh viên</th>
							<th style="max-width: 200px;">Tên sinh viên</th>
							<th>Lớp</th>
							<th>Email trường</th>
							<th>SDT</th>
							<th></th>
						</tr>
					</thead>
					<tbody class="tblChiTiet">

					</tbody>
					<tfoot>
						<tr>
							<th style="max-width: 200px;">Mã sinh viên</th>
							<th style="max-width: 200px;">Tên sinh viên</th>
							<th>Lớp</th>
							<th>Email trường</th>
							<th>SDT</th>
							<th></th>
						</tr>
					</tfoot>
				</table>
			</c:otherwise>
		</c:choose>
	</div>
</section>
<%@ include file="../base/_footer.jspf"%>
<script>
	$("#btnSave")
			.click(
					function() {
						var check = compare();
						if (check == false) {
							ThongBao_Loi("Ngày kết thúc phải lớn hơn ngày bắt đầu!");
						} else {
							var table = document
									.getElementById('addStudentTbl');
							var id_student_cells = table
									.querySelectorAll('td:first-child');
							var list_id_student = [];
							id_student_cells.forEach(function(singleCell) {
								list_id_student.push(singleCell.innerText);
							});
							console.log(list_id_student);
							var dataSchedule = {
								id : $("#schedule-id").val().toString(),
								class_id : $("#class_id").val().toString(),
								subject_id : $("#subject_id").val().toString(),
								shift_id : $("#shift_id").val().toString(),
								brand_id : $("#brand_id").val().toString(),
								start_date : $("#start_date").val().toString(),
								end_date : $("#end_date").val().toString(),
								note : $("#note").val().toString(),
								list_student : list_id_student.toString(),
								current_session : $("#current_session").val()
										.toString(),
								max_session : $("#max_session").val()
										.toString(),
								list_fc : $("#list_fc").val().toString(),
								coef_salary : $("#coef_salary").val()
										.toString(),
								number_session : $("#number_session").val()
										.toString()
							};
							$
									.ajax({
										url : "/LichHoc/addOrUpdateSchedule",
										type : "POST",
										contentType : "application/json",
										dataType : "json",
										data : JSON.stringify(dataSchedule),
										success : function(data) {
											if (data == "no data") {
												ThongBao_Loi("No data to insert, please try again!");
											} else if (data.includes("success")) {
												var id_schedule = data
														.split('?id_schedule=')[1];
												ThongBao_ThanhCong("Success!");
												window.location.href = "/LichHoc/formSchedule?id="
														+ id_schedule;
											} else {
												ThongBao_Loi("Error has been occured, please try again!");
											}
										}
									});
						}
					});

	function loadStudentByClassId(event) {
		var class_id = $(event).val();
		$
				.ajax({
					url : "/LichHoc/getListStudentByClassId",
					type : "get",
					dataType : 'json',
					data : {
						current_class : class_id
					},
					success : function(data) {
						$('#addStudentTbl > tbody').empty();
						for (var i = 0; i < data.length; i++) {
							$('#addStudentTbl > tbody')
									.append(
											'<tr>'
													+ '<td>'
													+ data[i].id_student
													+ '</td>'
													+ '<td>'
													+ data[i].full_name
													+ '</td>'
													+ '<td>'
													+ data[i].current_class
													+ '</td>'
													+ '<td>'
													+ data[i].email_school
													+ '</td>'
													+ '<td>'
													+ data[i].mobile_phone
													+ '</td>'
													+ '<td><button type="button" class="btn bg-orange"'
													+ 'onclick="DeleteRow(this)">'
													+ '<i class="fa fa-minus"></i>'
													+ '</button></td>'
													+ '</tr>');
						}
					}
				});
	};
	function setSessionBySubjectId(event) {
		var subject_id = $(event).val();
		$
				.ajax({
					url : "/LichHoc/getSubjectBySubjectId",
					type : "get",
					dataType : 'json',
					data : {
						id_subject : subject_id
					},
					success : function(data) {
						$("#number_session").val(data.number_session);
						$("#max_session")
								.val(
										parseFloat(
												parseFloat(data.number_session)
														+ parseFloat(data.number_session * 25 / 100))
												.toFixed(1));
					}
				});
	};
	function DeleteRow(event) {
		$(event).closest('tr').remove();
	}
	function compare() {
		var end_time = $("#end_date").val();
		var start_time = $("#start_date").val();
		var momentA = moment(start_time, "DD/MM/YYYY");
		var momentB = moment(end_time, "DD/MM/YYYY");
		return momentB.isSameOrAfter(momentA);
	}
	function compareWithToday(date) {
		var day = date;
		var today = new Date();
		var momentA = moment(day, "DD/MM/YYYY");
		var momentB = moment(today, "DD/MM/YYYY");
		return momentB.isSameOrAfter(momentA);
	}
	//convert yyyy-MM-dd to dd-MM-yyyy
	function reformatDate(dateStr) {
		dArr = dateStr.split("-"); // ex input "2010-01-18"
		return dArr[2] + "/" + dArr[1] + "/" + dArr[0]; //ex out: "18/01/2010"
	}
	function addRowAttendance() {
		var listAllShift = ${jsonListAllShift};
		var listAllBrand = ${jsonListAllBrand};
		var listAllSubjectType = ${jsonListAllSubjectType};
		var row = '<tr>' + '<td><select class="form-control select2 shift_id"'
				+ 'style="width: 100%;" name="shift_id" required>'
				+ '<option selected="selected" value="">Chọn ca học</option>';
		for (var i = 0; i < listAllShift.length; i++) {
			row += '<option value="'+listAllShift[i].id+'">'
					+ listAllShift[i].name_shift + '</option>';
		}
		;
		row += '</select></td>'
				+ '<td><select class="form-control select2 brand_id"'
				+ 'style="width: 100%;" name="brand_id" required>'
				+ '<option value="">Chọn chi nhánh</option>';
		for (var i = 0; i < listAllBrand.length; i++) {
			row += '<option value="'+listAllBrand[i].id_status+'">'
					+ listAllBrand[i].name_status + '</option>';
		}
		;
		row += '</select></td>'
				+ '<td><input type="text" class="form-control datepicker date_attendance"'+
							'name="date_attendance"'+
							'placeholder="Ngày điểm danh"'+
							'required></td>'
				+ '<td><select class="form-control select2 status_id"'
				+ 'style="width: 100%;" name="status_id" required>'
				+ '<option value="">Chọn loại môn học</option>';
		for (var i = 0; i < listAllSubjectType.length; i++) {
			row += '<option value="'+listAllSubjectType[i].id_status+'">'
					+ listAllSubjectType[i].name_status + '</option>';
		}
		;
		row += '</select></td>'
				+ '<td><input type="checkbox" class="disable" name="disable"'+
					'></td>'
				+ '<td><button type="button" class="btn bg-purple btnSaveRowAttendance"'
				+ 'data-id="" onclick="arrOrUpdateAttendance(this)">'
				+ '<i class="fa fa-save"></i>'
				+ '</button>'
				+ '<button type="button" class="btn bg-orange btnDeleteRowAttendance"'+
			'data-id="" onclick="DeleteRowAttendance(this)">'
				+ '<i class="fa fa-minus"></i>' + '</button></td>' + '</tr>';
				var checkMiddleDay1 = compareWithToday($("#start_date").val());
				var checkMiddleDay2 = compareWithToday($("#end_date").val());
				if ((checkMiddleDay1 == true && checkMiddleDay2 == false)
						|| (checkMiddleDay1 == false)) {			
		$('#addAttendanceTbl > tbody').append(row);
		$(".datepicker").datepicker({format: 'dd/mm/yyyy'});
		}else{
			ThongBao_Loi("Schedule end date expired, cannot add row attendance!");
			}
	}

	function arrOrUpdateAttendance(event) {
		var check = true;
		var row = $(event).closest('tr');
		if ($(row).find('.shift_id').val() == null
				|| $(row).find('.shift_id').val() == "") {
			check = false;
		} else {
		}
		if ($(row).find('.brand_id').val() == null
				|| $(row).find('.brand_id').val() == "") {
			check = false;
		} else {
		}
		if ($(row).find('.date_attendance').val() == null
				|| $(row).find('.date_attendance').val() == "") {
			check = false;
		} else {
		}
		if ($(row).find('.status_id').val() == null
				|| $(row).find('.status_id').val() == "") {
			check = false;
		} else {
		}
		if (check == false) {
			ThongBao_Loi("Please enter full field data! ");
		} else {
			var dataAttendance = {
				id : $(event).attr("data-id").toString(),
				schedule_id : $("#schedule-id").val().toString(),
				shift_id : $(row).find('.shift_id').val().toString(),
				brand_id : $(row).find('.brand_id').val().toString(),
				date_attendance : $(row).find('.date_attendance').val()
						.toString(),
				status_id : $(row).find('.status_id').val().toString(),
				disable : $(row).find('.disable').is(":checked"),
			};
			$
					.ajax({
						url : "/LichHoc/addOrUpdateAttendance",
						type : "POST",
						contentType : "application/json",
						dataType : "json",
						data : JSON.stringify(dataAttendance),
						success : function(data) {
							if (data == "no data") {
								ThongBao_Loi("No data to insert, please try again!");
							} else if (data == "success") {
								ThongBao_ThanhCong("Success!");
								window.location.href = "/LichHoc/formSchedule?id="
										+ $("#schedule-id").val();
							} else {
								ThongBao_Loi("Error has been occured, please try again!");
							}
						}
					});
		}

	};

	function DeleteRowAttendance(event) {
		var row = $(event).closest('tr');
		//compare if today same or after insert date
		var check = compareWithToday($(row).find('.date_attendance').val());
		var checkMiddleDay1 = compareWithToday($("#start_date").val());
		var checkMiddleDay2 = compareWithToday($("#end_date").val());
		if ((checkMiddleDay1 == true && checkMiddleDay2 == false)
				|| (checkMiddleDay1 == false)) {
			if (check == true) {
				ThongBao_Loi("Cannot delete attendance data in the past!");
			} else {
				var id_attendance=$(event).attr("data-id");
				if(id_attendance.toString()==""||id_attendance.toString()==null){
ThongBao_Loi("Not found id attendance!");
					}else{
						$.ajax({
							url : "/LichHoc/deleteAttendance",
							type : "DELETE",
							dataType : "json",
							data : {id:id_attendance},
							success : function(data) {
								if (data == "no data") {
									ThongBao_Loi("No data to insert, please try again!");
								} else if (data == "success") {
									$(event).closest('tr').remove();
									ThongBao_ThanhCong("Success!");
									window.location.href = "/LichHoc/formSchedule?id="
											+ $("#schedule-id").val();
								} else {
									ThongBao_Loi("Error has been occured, please try again!");
								}
							}
						});
					}
				
			}
		} else {
			ThongBao_Loi("Schedule end date expired, cannot delete attendance of this schedule!");
		}

	}
</script>