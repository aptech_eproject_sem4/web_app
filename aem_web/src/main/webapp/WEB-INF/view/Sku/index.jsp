<%@ page contentType="text/html;charset=UTF-8" language="java"
	isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ include file="../base/_headerMain.jspf"%>
<!-- Content Wrapper. Contains page content -->
<!-- Content Header (Page header) -->
<section class="content-header">
	<h1>Quản lý SKU</h1>
	<div class="wrap-btn-header">
		<button type="button" class="btn bg-maroon" id="btnCreate">
			<i class="fa fa-plus"></i> Thêm
		</button>
	</div>
</section>

<!-- Main content -->
<section class="content">
	<!-- Main row -->
	<div class="row">

		<!--table-->
		<div class="col-lg-12">
			<div class="filter-table"></div>
			<div class="wrap-table" id="getData">
				<%@ include file="loadTable.jsp"%>
			</div>

		</div>
		<!--table-->
		<div></div>
	</div>

</section>
<!-- /.content -->
<%@ include file="modalAction.jsp"%>
<%@ include file="../base/_footer.jspf"%>
<script>
	var modal = $("#modal-sku");
	$("#btnCreate").click(function() {

		$("#title-popup").html("Thêm Sku")
		var error = $("label.error");
		$(error).empty();
		$("input.form-control").removeClass("error");
		$("textarea.form-control").removeClass("error");
		modal.find('input').val(null);
		modal.find('textarea').val(null);
		var parentNode = $("#test").parent();
		var item1 = $(parentNode).find("input#r1");
		var item2 = $(parentNode).find("input#r2");
		$(item1).prop("checked", true);
		$(item2).prop("checked", false);
		//init default
		modal.modal("show");
		a=$("#r1").attr("data-val");		
		$("#test").val(a);
	})

	$("#btnSave").click(function() {	
// 	var a=$("#r1").attr("data-val");
//  		if(a==false){
// 			a=$("#r2").attr("data-val");
//  		}		
//  		$("#test").val(a)  
		s_Save('/Sku/addOrUpdate', $("#form-sku"), function(data) {
			if (data == "true") {
				ThongBao_ThanhCong("Lưu thành công");
				modal.modal("hide");
				search();

			} else {
				Thong_BaoLoi("Lưu thất bại");
				Thong_BaoLoi(data);
			}
		}, null)
	})

	function editModal(id, unit1, unit1_value, unit2, unit2_value, type, note) {
		$("#title-popup").html("Sửa Sku")
		
		modal.find("#id").val(id);
		modal.find("#unit1").val(unit1);
		modal.find("#unit1_value").val(unit1_value);
		modal.find("#unit2").val(unit2);
		modal.find("#unit2_value").val(unit2_value);
		modal.find("#test").val(type);
		modal.find("#note").val(note);

		//clear error
		var error = $("label.error");
		$(error).empty();
		$("input.form-control").removeClass("error").addClass("valid");
		$("textarea.form-control").removeClass("error").addClass("valid");
		
		var parentNode = $("#test").parent();
		var item1 = $(parentNode).find("input#r1");
		var item2 = $(parentNode).find("input#r2");
		if($("#test").val()=="true"){
			$(item1).prop("checked", true);
		}else{
			$(item2).prop("checked", true);
		}
		
		
// 		if ($(item1).val() == type) {
// 			$(item1).prop("checked", true);
// 			//break;
// 		} else if ($(item2).val() == type) {
// 			$(item2).prop("checked", true);
// // 			break;
// 		}
	
		modal.modal("show");
	}
	function changeRadio(event) {
		var target = event.target;
		var a = target.getAttribute("data-val");
		var test_item = document.getElementById("test");
		test_item.value = a;
	}
	// function editModal(obj){
	// 	debugger
	// 	var tr=$(obj).closest("tr");
	// 	var id=tr.find("#id").val();
	// 	modal.find("#id").val(id);
	// }
	function search() {
		var url = "/Sku/Index?ajaxLoad=table";
		$("#getData").load(url, function() {
			InitDataTable();

		})
	}
</script>