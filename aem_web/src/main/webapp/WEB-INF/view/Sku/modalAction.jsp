<%@ page contentType="text/html;charset=UTF-8" language="java"
	isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!--modal default-->
<div class="modal fade" id="modal-sku">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"
					aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h4 class="modal-title" id="title-popup">Thêm SKU</h4>
			</div>
			<div class="modal-body">
				<form id="form-sku" action="/Sku/addOrUpdate" method="POST"
					enctype="multipart/form-data">
					<div class="row">
						<div class="col-lg-12">
							<div class="row">
								<div class="col-lg-6">
									<div class="form-group">
										<label for="" class="required">Unit 1</label> <input
											class="form-control" type="text" autocomplete="off"
											id="unit1" name="unit1" required> <input
											type="hidden" id="id" name="id">
									</div>
								</div>
								<div class="col-lg-6">
									<div class="form-group">
										<label for="" class="required">Value 1</label> <input
											class="form-control" type="text" autocomplete="off"
											id="unit1_value" name="unit1_value" onlynumber required>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-lg-6">
									<div class="form-group">
										<label for="" class="required">Unit 2</label> <input
											class="form-control" type="text" autocomplete="off"
											id="unit2" name="unit2" required>
									</div>
								</div>
								<div class="col-lg-6">
									<div class="form-group">
										<label for="" class="required">Value 2</label> <input
											class="form-control" type="text" autocomplete="off"
											id="unit2_value" name="unit2_value" onlynumber required>
									</div>
								</div>
							</div>

							<div class="form-group ">
								<label for="">Loại</label> <input type="hidden" name="type"
									id="test">
								<div style="margin-left: 10px;">
									<label><input type="radio" id="r1" name="rb"
										data-val="true" value="true" onclick="changeRadio(event);">
										Tiền </label> <label style="margin-left: 30px;"><input
										type="radio" id="r2" name="rb" data-val="false" value="false"
										onclick="changeRadio(event);"> Thời gian</label>
								</div>
							</div>

							<div class="form-group">
								<label for="">Ghi chú</label>
								<textarea class="form-control" autocomplete="off" id="note"
									name="note" placeholder=""></textarea>
							</div>
						</div>
					</div>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default pull-left"
					data-dismiss="modal">Đóng</button>
				<button type="button" class="btn btn-primary" id="btnSave">Lưu</button>
			</div>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>
<!--modal default-->