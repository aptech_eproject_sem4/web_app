<%@ page contentType="text/html;charset=UTF-8" language="java"
	isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<table id="table" class="table table-bordered table-striped">
	<thead>
		<tr>
			<th width="45" class="text-center ">STT</th>                
            <th>Unit - Value 1</th>
            <th>Unit - Value 2</th>
             <th>Loại</th>
            <th style="max-width: 200px;">Ghi chú</th>
            <th>Thao tác</th>
         </tr>
    </thead>
              <tbody class="tblChiTiet">
              <c:forEach var="item" items="${lss}">
                <tr>
                  <td class="text-center stt "></td>
                  <td>
                    ${item.unit1} - ${item.unit1_value}
                  </td>
                  <td>
                    ${item.unit2} -${item.unit2_value}
                  </td>
                   <td><c:choose><c:when test="${item.type==false}">
							<i>Thời Gian</i>
						
						</c:when>
						<c:when test="${item.type==true}">
							<i>Tiền</i>
							<br />
						</c:when></c:choose></td>
                  <td><i>${item.note}</i></td>
                  <td>
                    <button type="button" class="btn-edit" onclick="editModal('${item.id}','${item.unit1}','${item.unit1_value}','${item.unit2}','${item.unit2_value}','${item.type}','${item.note}')"><i class="fa fa-pencil"></i></button>
                    <button type="button" class="btn-del" onclick="Delete('${item.id}','/Sku/Delete',function(){search()})"><i class="fa fa-trash-o"></i>
					</button>
                  </td>
                </tr>
                </c:forEach>
              </tbody>
              <tfoot>
				<tr>
					<th width="45" class="text-center">STT</th>                
		            <th>Unit - Value 1</th>
		            <th>Unit - Value 2</th>
		            <th>Loại</th>
		            <th style="max-width: 200px;">Ghi chú</th>
		            <th>Thao tác</th>
		         </tr>
              </tfoot>
            </table>
          <!--table-->
