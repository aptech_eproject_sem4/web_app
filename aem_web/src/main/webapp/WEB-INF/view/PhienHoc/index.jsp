<%@ page contentType="text/html;charset=UTF-8" language="java"
	isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ include file="../base/_headerMain.jspf"%>
<!-- Content Header (Page header) -->
<section class="content-header">
	<h1>Quản lý Phiên Học</h1>
	<div class="wrap-btn-header">
		<button type="button" class="btn bg-maroon" id="btnCreate">
			<i class="fa fa-plus"></i> Thêm
		</button>
	</div>
</section>
<!-- Main content -->
<section class="content">
	<!-- Main row -->
	<div class="row">

		<!--table-->
		<div class="col-lg-12">
			<div class="filter-table"></div>
			<div class="wrap-table" id="getData">
				<%@ include file="loadTable.jsp"%>
			</div>
		</div>
		<!--table-->
		<div></div>
	</div>

</section>
<!-- /.content -->
<%@ include file="modalAction.jsp"%>
<%@ include file="../base/_footer.jspf"%>
<script>
// 	var modal=$("#modal-phienHoc");
// 	$("#btnCreate").click(function(){
// 		modal.find('input').val(null);
// 		$("#title-popup").html("Thêm phiên học");
// 		modal.modal("show");
// 	});

	var modal = $("#modal-phienHoc");
	$("#btnCreate").click(function() {
		$("#title-popup").html("Thêm Phiên Học")
		var error = $("label.error");
		$(error).empty();
		$("input.form-control").removeClass("error");
		$("textarea.form-control").removeClass("error");
		modal.find('input').val(null);
		modal.find('textarea').val(null);
		var parentNode = $("#test").parent();
		var item1 = $(parentNode).find("input#r1");
		var item2 = $(parentNode).find("input#r2");
		$(item1).prop("checked", true);
		$(item2).prop("checked", false);
		//init default
		modal.modal("show");
		a=$("#r1").attr("data-val");		
		$("#test").val(a);
	})

	$("#btnSave").click(function(){
		var check=compare();
		if(check==false){ThongBao_Loi("Giờ kết thúc phải lớn hơn giờ bắt đầu!");}else{
		s_Save('/PhienHoc/addOrUpdate',$("#form-phienhoc"),function(data){
			if(data=="true"){
				ThongBao_ThanhCong("Lưu thành công");
				modal.modal("hide");
				search();
				
			}else{
				Thong_BaoLoi("Lưu thất bại");
				Thong_BaoLoi(data);//thông báo check trùng date off hiện lỗi return
			}			
		},null)}
	})
	
	function editModal(idSC,nameshift,starttime,endtime,allowlate,allowleaveearly,evenorodd,noteSC){
		$("#title-popup").html("Sửa phiên học");
		modal.find("#id").val(idSC);
		modal.find("#name_shift").val(nameshift);
		modal.find("#start_time").val(starttime);
		modal.find("#end_time").val(endtime);
		modal.find("#allow_late").val(allowlate);
		modal.find("#allow_leave_early").val(allowleaveearly);
		modal.find("#test").val(evenorodd);
		modal.find("#note").val(noteSC);

		var error = $("label.error");
		$(error).empty();
		$("input.form-control").removeClass("error").addClass("valid");
		$("textarea.form-control").removeClass("error").addClass("valid");
		var parentNode = $("#test").parent();
		var item1 = $(parentNode).find("input#r1");
		var item2 = $(parentNode).find("input#r2");
		if($("#test").val()=="true"){
			$(item1).prop("checked", true);
		}else{
			$(item2).prop("checked", true);
		}
		modal.modal("show");
	}

	function changeRadio(event){
		var target=event.target;
		var a=target.getAttribute("data-val");
		var test_item=document.getElementById("test");
		test_item.value=a;
	}

	function compare() {
		var end_time=$("#end_time").val();
		var start_time=$("#start_time").val();
	    var momentA = moment(start_time,"HH:mm");
	    var momentB = moment(end_time,"HH:mm");
	    return momentB.isSameOrAfter(momentA);
	}

	function search(){
		var url="/PhienHoc/Index?ajaxLoad=table";
		$("#getData").load(url,function(){
		InitDataTable();
		})
	}

	
  </script>