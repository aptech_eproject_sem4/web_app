<%@ page contentType="text/html;charset=UTF-8" language="java"
	isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<table id="table" class="table table-bordered table-striped">
	<thead>
		<tr>
			<th class="text-center" width="45">STT</th>
			<th class="text-center" width="45">Tên phiên học</th>
			<th class="text-center" width="45">Thời gian</th>
			<th class="text-center" width="45">Ngày chẵn / lẻ</th>
			<th class="text-center" width="45">Số phút cho phép</th>
			<th class="text-center" width="45">Ghi chú</th>
			<th class="text-center" width="45">Thao Tác</th>
		</tr>
	</thead>
	<tbody class="tblChiTiet">
		<c:forEach var="item" items="${listSS}">
			<tr class="parent">
				<td class="text-center" width="45">${item.id}</td>
				<td class="text-center" width="45">${item.name_shift}</td>
				<td class="text-center" width="45">${item.start_time}-
					${item.end_time}</td>
				<td class="text-center"><c:choose>
						<c:when test="${item.even_or_odd==false}">
							<i>Lẻ</i>
						</c:when>
						<c:when test="${item.even_or_odd==true}">
							<i>Chẵn</i>
							<br />
						</c:when>
					</c:choose></td>
				<td class="text-center" width="45">Đi trễ : ${item.allow_late}<br>Về
					sớm : ${item.allow_leave_early}
				</td>
				<td class="text-center" width="45">${item.note}</td>
				<td class="text-center" width="45">
					<button type="button" class="btn-edit"
						onclick="editModal('${item.id}','${item.name_shift}','${item.start_time}','${item.end_time}','${item.allow_late}','${item.allow_leave_early }','${item.even_or_odd}','${item.note}')">
						<i class="fa fa-pencil"></i>
					</button>
					<button type="button" class="btn-del"
						onclick="Delete('${item.id}','/PhienHoc/Delete',function(){search()})">
						<i class="fa fa-trash-o"></i>
					</button>
				</td>
			</tr>
		</c:forEach>
	</tbody>

	<tfoot>
		<tr>
			<th class="text-center" width="45">STT</th>
			<th class="text-center" width="45">Tên phiên học</th>
			<th class="text-center" width="45">Thời gian</th>
			<th class="text-center" width="45">Ngày chẵn / lẻ</th>
			<th class="text-center" width="45">Số phút cho phép</th>
			<th class="text-center" width="45">Ghi chú</th>
			<th class="text-center" width="45">Thao Tác</th>
		</tr>
	</tfoot>
</table>