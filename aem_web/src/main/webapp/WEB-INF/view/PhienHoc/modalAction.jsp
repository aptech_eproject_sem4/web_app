<%@ page contentType="text/html;charset=UTF-8" language="java"
	isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<div class="modal fade" id="modal-phienHoc">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"
					aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h4 class="modal-title" id="title-popup">Thêm lịch học</h4>
			</div>
			<div class="modal-body">
				<form id="form-phienhoc" action="/PhienHoc/addOrUpdate"
					method="POST" enctype="multipart/form-data">
					<input type="hidden" id="id" name="id">
					<div class="row">
						<div class="col-lg-12">
							<div class="form-group">
								<label for="name_shift" class="required">Tên phiên học</label> <input
									class="form-control" type="text" id="name_shift"
									name="name_shift" required>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-lg-6">
							<div class="form-group">
								<label for="start_time">Giờ bắt đầu</label>
								<div class="input-group">
									<input type="text" class="form-control" id="start_time"
										name="start_time" istime required>
									<div class="input-group-addon">
										<i class="fa fa-clock-o"></i>
									</div>
								</div>
							</div>
						</div>

						<div class="col-lg-6">
							<div class="form-group">
								<label for="start_time">Giờ kết thúc</label>
								<div class="input-group">
									<input type="text" class="form-control" id="end_time"
										name="end_time" istime required>
									<div class="input-group-addon">
										<i class="fa fa-clock-o"></i>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-lg-6">
							<div class="allow_late">
								<label for="allow_late" class="required">Số phút cho
									phép đi trễ</label> <input class="form-control" type="text"
									id="allow_late" name="allow_late" onlynumber required>
							</div>
						</div>

						<div class="col-lg-6">
							<div class="allow_leave_early">
								<label for="end_time" class="required">Số phút cho phép
									về sớm</label> <input class="form-control" type="text"
									id="allow_leave_early" name="allow_leave_early" onlynumber
									required>
							</div>
						</div>
					</div>
					<div class="form-group ">
						<label for="">Ngày dạy</label> <input type="hidden"
							name="even_or_odd" id="test">
						<div style="margin-left: 10px;">
							<label><input type="radio" id="r1" name="rb"
								data-val="true" value="true" onclick="changeRadio(event);">Chẵn</label>
							<label style="margin-left: 30px;"><input type="radio"
								id="r2" name="rb" data-val="false" value="false"
								onclick="changeRadio(event);"> Lẻ</label>
						</div>
					</div>


					<div class="row">
						<div class="col-lg-12">
							<div class="note">
								<label for="end_time" class="required">Ghi chú</label> <input
									class="form-control" type="text" id="note" name="note" isModel>
							</div>
						</div>
					</div>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default pull-left"
					data-dismiss="modal">Đóng</button>
				<button type="button" class="btn btn-primary" id="btnSave">Lưu</button>
			</div>
		</div>
	</div>
	<!-- /.modal-content -->
</div>
<!-- /.modal-dialog -->
</div>