<%@ page contentType="text/html;charset=UTF-8" language="java"
	isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<table id="table" class="table table-bordered table-striped">
	<thead>
		<tr>
			<th width="45" class="text-center">STT</th>
			<th>Mã menu</th>
			<th>Tên menu</th>
			<th>Thứ tự</th>
			<th>Ẩn</th>
			<th>Thao tác</th>
		</tr>
	</thead>
	<tbody class="tblChiTiet">
		<c:forEach var="item" items="${listController}">
			<tr>
				<td class="stt"></td>
				<td><span class="label label-primary">${item.idcontroller }</span></td>
				<td><span>${item.namecontroller }</span></td>
				<td><span>${item.ordernumber }</span></td>
				<td class="text-center"><c:choose>
						<c:when test="${item.active==false}">
							<input type="checkbox" class="checkActive"
								onclick="updateActive(this)" checked>
							<br />
						</c:when>
						<c:when test="${item.active==true}">
							<input type="checkbox" class="checkActive"
								onclick="updateActive(this)">
							<br />
						</c:when>
					</c:choose>
<%-- 					<input type="hidden" id="menu_root" value="${item.menuroot }"/>	 --%>
				</td>					
				<td>
					<button type="button" class="btn-edit"
						onclick="editModal('${item.idcontroller}','${item.namecontroller}','${item.active}','${item.ordernumber}','${item.menuroot}')">
						<i class="fa fa-pencil"></i>
					</button>
					<button type="button" class="btn-del"
						onclick="Delete('${item.idcontroller}','/Menu/Delete',function(){search()})">
						<i class="fa fa-trash-o"></i>
					</button>
				</td>
			</tr>
		</c:forEach>
	</tbody>
	<tfoot>
		<tr>
			<th width="45" class="text-center">STT</th>
			<th>Mã menu</th>
			<th>Tên menu</th>
			<th>Thứ tự</th>
			<th>Ẩn</th>
			<th>Thao tác</th>
		</tr>
	</tfoot>
</table>
<!--table-->