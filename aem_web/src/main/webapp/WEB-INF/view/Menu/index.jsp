<%@ page contentType="text/html;charset=UTF-8" language="java"
	isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ include file="../base/_headerMain.jspf"%>
<!-- Content Header (Page header) -->
<section class="content-header">
	<h1>Quản lý menu</h1>
	<div class="m-auto">
		<select class="form-control select2" style="width: 100%;"
			id="listParent" name="listParent">
			<c:forEach var="item" items="${listParent}">
				<c:choose>
					<c:when test="${item.value=='QLCTHOC'}">
						<option value="${item.key}" selected="selected">${item.value}</option>
					</c:when>
					<c:otherwise>
						<option value="${item.key}">${item.value}</option>
					</c:otherwise>
				</c:choose>
			</c:forEach>
		</select>
	</div>
	<div class="wrap-btn-header">
		<button type="button" class="btn bg-maroon" id="btnCreate">
			<!-- data-toggle="modal" data-target="#modal-default" -->
			<i class="fa fa-plus"></i> Thêm
		</button>
	</div>
</section>

<!-- Main content -->
<section class="content">
	<!-- Main row -->
	<div class="row">

		<!--table-->
		<div class="col-lg-12">
			<div class="filter-table"></div>

			<div class="wrap-table" id="getData">
				<%@ include file="loadTable.jsp"%>
			</div>
		</div>
	</div>
</section>
<!-- /.content -->
<%@ include file="modalAction.jsp"%>
<%@ include file="../base/_footer.jspf"%>
<script>
	var modal=$("#modal-Menu");
	var checkIdStatus;
	$("#btnCreate").click(function(){
		$("#title-popup").html("Thêm Controller");
		var error = $("label.error");
		$(error).empty();
		$("input.form-control").removeClass("error");
		modal.find('input').val(null);
		$("#id_controller").prop("disabled", false );
		modal.modal("show");
	});

	$('#listParent').on('change', function() {
	      search(); // or $(this).val()
	    });
	
	$("#btnSave").click(function(){
		if(checkIdStatus=="reject"){
			ThongBao_Loi("Mã đã tồn tại, hãy chọn mã khác!");
			}else if(checkIdStatus=="accept"){
		var e = document.getElementById("menuroot");
		var value = e.options[e.selectedIndex].value;
		if(value<=0){
			ThongBao_Loi("Chọn danh mục!");
		}else{
			s_Save('/Menu/addOrUpdate',$("#form-menu"),function(data){
				if(data=="true"){
					ThongBao_ThanhCong("Lưu thành công");
					modal.modal("hide");
					search();
					
				}else{
					Thong_BaoLoi("Lưu thất bại");
					Thong_BaoLoi(data);//thông báo check trùng date off hiện lỗi return
				}			
			},null)
		}}else{
ThongBao_Loi("Có lỗi xảy ra, hãy thử lại!");
			}
	});
	
	function editModal(id_controller,name_controller,active,order_number,menu_root){
		checkIdStatus="accept";
		$("#title-popup").html("Sửa Controller");
		modal.find("#id_controller").val(id_controller);
		modal.find("#name_controller").val(name_controller);
		modal.find("#active").val(active);		
		modal.find("#order_number").val(order_number);
		modal.find("#menu_root").val(menu_root);
		$("#id_controller").prop("disabled", true );
		var error = $("label.error");
		$(error).empty();
		$("input.form-control").removeClass("error").addClass("valid");
		var parentNode_menuroot = $("#menuroot").parent();
		var ulitem_menuroot = $(parentNode_menuroot).find(
				"span.select2-selection__rendered");
		var arr_option_menuroot = $("#menuroot option");
		$(ulitem_menuroot).empty();
		for (i = 0; i < arr_option_menuroot.length; i++) {
			if ($(arr_option_menuroot[i]).val() == menu_root) {
				$(arr_option_menuroot[i]).prop("selected", true);
				$(ulitem_menuroot).prop("title", $(arr_option_menuroot[i]).html());
				$(ulitem_menuroot).html($(arr_option_menuroot[i]).html());
				break;
			}
		}
		modal.modal("show");
	}
	// function editModal(obj){
	// 	debugger
	// 	var tr=$(obj).closest("tr");
	// 	var id=tr.find("#id").val();
	// 	modal.find("#id").val(id);
	// }
	function InitDataTable(selectorTable){
    selectorTable=selectorTable??$("#table");
    var table=selectorTable.DataTable({
        ordering: false,
        "order": [[ 1, 'asc' ]]
    });
    table.on( 'order.dt search.dt', function () {
        table.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
            cell.innerHTML = i+1;
        } );
    } ).draw();
}
	function search(){
		var e = document.getElementById("listParent");
		var value = e.options[e.selectedIndex].value;
		var url='/Menu/Index?ajaxLoad=table&idParent='+value;
		$("#getData").load(url,function(){
			//$("#table").DataTable({
				//ordering: false,
			//});
			InitDataTable();
			// console.log("đã load lại table");
		})
	}

	   function updateActive(event){
		   var currentRow=$(event).closest("tr");         
	        var idParent=currentRow.find("td:eq(1)").text(); 
	        $.ajax({
		        url: "/Menu/updateActive",
		        data: { "idParent": idParent },
		        type: "post",
		        success: function(data){
		           ThongBao_ThanhCong(data);
		           search();
		        }
		    });
		   }
	
	$('#id_controller').change(function(){
	    $.ajax({
	        url: "/Menu/checkId",
	        data: { "idParent": $("#id_controller").val() },
	        type: "post",
	        success: function(data){
	        	if(data=="existed"){
					ThongBao_Loi("Mã đã tồn tại, hãy chọn mã khác!");
					checkIdStatus="reject";
					}else if(data=="not exist"){
						//ThongBao_ThanhCong("Mã chưa tồn tại, có thể tạo mới!");
						checkIdStatus="accept";
						}else{
							ThongBao_Loi("Có lỗi xảy ra!");
							}
	        }
	    });
	});

  </script>