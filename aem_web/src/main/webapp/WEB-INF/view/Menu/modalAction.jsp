<%@ page contentType="text/html;charset=UTF-8" language="java"
	isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!--modal default-->
<div class="modal fade" id="modal-Menu">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"
					aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h4 class="modal-title" id="title-popup">Thêm controller</h4>
			</div>
			<div class="modal-body">
				<form id="form-menu" action="/Menu/addOrUpdate" method="POST"
					enctype="multipart/form-data">
					<div class="row">
						<div class="col-lg-6">
							<div class="form-group">
								<label for="" class="required">Mã menu</label> <input class="form-control"
									type="text" id="id_controller" name="idcontroller" isInputAllowVarchar required>
							</div>
						</div>
						<div class="col-lg-6">
							<div class="form-group">
								<label for="" class="required">Tên menu</label> <input class="form-control"
									type="text" id="name_controller" name="namecontroller" required>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-lg-6">
							<div class="form-group">
								<label for="" class="required">Thứ tự</label> <input class="form-control"
									type="text" id="order_number" name="ordernumber" onlynumber required>
							</div>
						</div>
						<div class="col-lg-6">
							<div class="form-group">
								<label for="" class="required">Menu cha</label> <select
									class="form-control select2" style="width: 100%;" id="menuroot" name="menuroot" required>
									<option></option>
									<c:forEach var="item" items="${listParent}">
										<option value="${item.key}">${item.value}</option>
									</c:forEach>
								</select>
							</div>
						</div>
					</div>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default pull-left"
					data-dismiss="modal">Đóng</button>
				<button type="button" class="btn btn-primary" id="btnSave">Lưu</button>
			</div>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>
<!--modal default-->