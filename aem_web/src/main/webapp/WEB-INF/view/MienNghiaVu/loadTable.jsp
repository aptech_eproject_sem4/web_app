<%@ page contentType="text/html;charset=UTF-8" language="java"
	isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<table id="table" class="table table-bordered table-striped">
	<thead>
		<tr>
			<th width="45" class="text-center">STT</th>
			<th>Sinh viên</th>
			<th>Lớp - Khóa</th>
			<th>Thông tin</th>
			<th>Người lập</th>
        	<th>Người duyệt</th>
			<th>Ghi chú</th>
			<th>Thao tác</th>
		</tr>
	</thead>
	<tbody class="tblChiTiet">
		<c:forEach var="item" items="${listExemptionMs}">
			<tr>
				<td class="stt">1</td>
				<c:forEach var="itemlss" items="${lss}">
					<c:choose>
						<c:when test="${item.studentid==itemlss.id_student }">
							<td><span class="label label-primary cur-pointer">${itemlss.id_student}</span>
								<br> <span>${itemlss.full_name}</span> <br> <span
								class="label ttd${item.confirmed}">TT </span>
							<td>
								<c:forEach var="itemlsco" items="${lsco}">
								<c:choose>
								<c:when test="${itemlss.course_id==itemlsco.id_course}">
								<span>
								<c:forEach var="itemlsc" items="${lsc}">
									<c:choose>
								<c:when test="${itemlss.current_class==itemlsc.id_class}">
									${itemlsc.name_class}- ${itemlsco.name_course}
									</c:when>
									<c:otherwise></c:otherwise>
									</c:choose>
									</c:forEach>
									</span>
								</c:when>
								<c:otherwise></c:otherwise>
								</c:choose>
							</c:forEach>
							
							</td>
						</c:when>
						<c:otherwise>
						</c:otherwise>
					</c:choose>
				</c:forEach>

				<td><span>Khóa năm: ${item.yearstart} - ${item.yearend}</span><br> 
				<span>Hạn ngày cấp: ${item.expirationdate}</span></td>
				 <td>
					<span>${item.creator}</span><br>
					<span>${item.datecreate}</span>
				</td>
				<td>
					<span>${item.creatorconfirm}</span><br>
					<span>${item.dateconfirm}</span>
				</td>
				<td><i>${item.note}</i></td>
				<td>
					<button type="button" data-id="${item.id}" class="btn-edit btn-edit-fc" onclick="editExMS(this)">
						<i class="fa fa-pencil"></i></button>
					<button type="button" class="btn-del" onclick="Delete('${item.id}','/MienNghiaVu/Delete',function(){search()})">
						<i class="fa fa-trash-o"></i></button>
				</td>
			</tr>
		</c:forEach>
	</tbody>
</table>