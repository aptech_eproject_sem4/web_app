<%@page import="com.fasterxml.jackson.annotation.JsonInclude.Include"%>
<%@ page contentType="text/html;charset=UTF-8" language="java"
	isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ include file="../base/_headerMain.jspf"%>

        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Đăng ký hoãn nghĩa vụ quân sự
          </h1>
          <div class="wrap-btn-header d-flex">
            <button type="button" class="btn bg-maroon" id="btnAddNew"><i
                class="fa fa-plus"></i> Thêm</button>
          </div>
        </section>

<section class="content">
	<!-- Main row -->
	<div class="row">

		<!--table-->
		<div class="col-lg-12">
			<div class="filter-table">
				
			</div>
			<div class="wrap-table" id="getData">
				<%@ include file="loadTable.jsp"%>
			</div>

		</div>
		<!--table-->
		<div></div>
	</div>

</section>
<%@ include file="../base/_footer.jspf"%>
<script>
function search() {
	var url = "/MienNghiaVu/Index?ajaxLoad=table";
	$("#getData").load(url, function() {
		InitDataTable();
	})
}
$("#btnAddNew").click(function(){
	  window.location.href="/MienNghiaVu/formExMs/0"
});
function editExMS(event){
	  window.location.href = "/MienNghiaVu/formExMs/"+ $(event).attr("data-id");
};
</script>
