<%@ page contentType="text/html;charset=UTF-8" language="java"
	isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ include file="../base/_headerMain.jspf"%>
<!-- Content Header (Page header) -->
<!-- Content Header (Page header) -->
<section class="content-header">
	<h1>Chi tiết đăng ký</h1>
	<div class="wrap-btn-header d-flex">
		<button type="button" class="btn bg-orange margin" id="btnExport" onclick="CapNhatTrangThai('2')" style="display:${check=="3"?"block":"none"}"><i class="fa fa-check"></i>
            Duyệt và Tải phiếu</button>
        <button type="button" class="btn bg-purple margin" id="btnGuiDuyet" onclick="CapNhatTrangThai('1')" style="display:${check=="1"?"block":"none"}"><i class="fa fa-paper-plane"></i>
            Gửi duyệt</button>
        <button type="button" class="btn bg-maroon margin" id="btnLuu" onclick="save()" style="display:${check=="1"?"block":"none"}"><i class="fa fa-save"></i>
            Lưu</button>    
        <button type="button" class="btn bg-maroon margin" id="btnLuu" onclick="save()" style="display:${check=="0"?"block":"none"}"><i class="fa fa-save"></i>
            Lưu</button>
		<button type="button" class="btn bg-maroon margin" onclick="ReturnIndex()"><i class="fa fa-arrow-left"></i></button>
	</div>
</section>


<!-- Main content -->
<section class="content">
	<form action="/MienNghiaVu/addOrUpdate" id="exms-form"
				enctype="multipart/form-data">
				<input id="id" type="hidden" name="id" value="${exms.id}">
				<!-- Main row -->
				<div class="row">
					<div class="col-lg-3">
						<div class="form-group">
							<label for="">Mã sinh viên</label> <input type="text"
								id="studentid" name="studentid" value="${exms.studentid}"
								class="form-control" readonly required>
						</div>
					</div>
					<div class="col-lg-3">
						<div class="form-group">
							<label for="">Tên sinh viên</label>
               				 <button type="button" class="btn bg-olive btn-flat margin showTenSV" id="${btn}">${tensv}</button>
						</div>
					</div>
					<div class="col-lg-3">
						<div class="form-group">
							<label for="">Sinh viên năm thứ</label> <input type="text"
								class="form-control" name="monthstart"
								value="${exms.monthstart}" onlynumber required min="1" max="10">
						</div>
					</div>

				</div>
				<div class="row">
					<div class="col-lg-3">
						<div class="form-group">
							<label for="">Tháng/năm bắt đầu khóa</label> <input type="text"
								class="form-control" id="yearstart" name="yearstart"
								value="${exms.yearstart}" required ismonthyear>
						</div>
					</div>
					<div class="col-lg-3">
						<div class="form-group">
							<label for="">Tháng/năm kết thúc khóa</label> <input type="text"
								class="form-control" id="yearend" name="yearend"
								value="${exms.yearend}" required ismonthyear>
						</div>
					</div>
					<div class="col-lg-3">
						<div class="form-group">
							<label for="">Hiệu lực đến</label>
							<div class="input-group">
								<input type="text" class="form-control datepicker"
									id="expirationdate" name="expirationdate"
									value="${exms.expirationdate}" placeholder="" required>
								<div class="input-group-addon">
									<i class="fa fa-calendar"></i>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-9">
						<div class="form-group">
							<label for="">Ghi chú</label>
							<textarea class="form-control" id="note" name="note"
								autocomplete="off" placeholder=""> ${exms.note}</textarea>
						</div>

					</div>
				</div>
			</form>
</section>
<div class="modal fade" id="modal-chonSV">
        <div class="modal-dialog modal-lg">
          <div class="modal-content">

            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
              <h4 class="modal-title">Chọn Sinh Viên</h4>
            </div>

            <div class="modal-body">
              
            </div>
          </div>
        </div>
        <!--modal large-->
      </div>

<%@ include file="../base/_footer.jspf"%>

<script>		
	var formPhieu=$("#exms-form");
	var modalChonSV=$("#modal-chonSV")
	$("#btnChonSV").click(function(){
        var url="/PhieuThu/LoadSinhVien";
        modalChonSV.find(".modal-body").load(url,function(){
            InitDataTable();
            modalChonSV.modal("show");
        })
    })
	function ReturnIndex(){
        var index=window.location.protocol + "//" + window.location.host+"/MienNghiaVu/Index";
		window.location.href =index;
    }
	function ChonSV(id,ten){
        formPhieu.find("#studentid").val(id);
        formPhieu.find(".showTenSV").text(ten);
        modalChonSV.modal("hide");        
    }
	function save(){
		s_Save('/MienNghiaVu/addOrUpdate', $("#exms-form"), function(data) {
			ThongBao_ThanhCong("Lưu thành công");
            var formUrl=window.location.protocol + "//" + window.location.host+"/MienNghiaVu/formExMs/"+data;
		    window.location.href =formUrl;
		}, null)
	}
	function CapNhatTrangThai(trangThai){
        var id=$("#id").val();
        $.ajax({
			url: "/MienNghiaVu/CapNhatTrangThaiPhieu",
			data: 
			{ 	"maPhieu": id,
				"trangThai":trangThai,
			},
			type: "post",
			success: function(data){
                if(trangThai=="2"){
                    downloadFile();
                }else{
                    var formUrl=window.location.protocol + "//" + window.location.host+"/MienNghiaVu/formExMs/"+id;
			        window.location.href =formUrl;
                }
				
            }
		})
    }
	function downloadFile() {
        var id=$("#id").val();
        var xhr = new XMLHttpRequest();
        xhr.open('POST', "/MienNghiaVu/exportPhieu/"+id, true);
        xhr.responseType = 'arraybuffer';
        xhr.onload = function () {
        if (this.status === 200) {
        var filename = "";
        var disposition = xhr.getResponseHeader('Content-Disposition');
        if (disposition && disposition.indexOf('attachment') !== -1) {
            var filenameRegex = /filename[^;=\n]*=((['"]).*?\2|[^;\n]*)/;
            var matches = filenameRegex.exec(disposition);
            if (matches != null && matches[1]) {
            filename = matches[1].replace(/['"]/g, '');
            }
        }
        var type = xhr.getResponseHeader('Content-Type');
        var blob = typeof File === 'function'
            ? new File([this.response], filename, { type: type })
            : new Blob([this.response], { type: type });
        if (typeof window.navigator.msSaveBlob !== 'undefined') {
            // IE workaround for "HTML7007: One or more blob URLs were revoked by closing the blob for which they were created. 
            // These URLs will no longer resolve as the data backing the URL has been freed."
            window.navigator.msSaveBlob(blob, filename);
        } else {
            var URL = window.URL || window.webkitURL;
            var downloadUrl = URL.createObjectURL(blob);
        if (filename) {
            // use HTML5 a[download] attribute to specify filename
            var a = document.createElement("a");
            // safari doesn't support this yet
            if (typeof a.download === 'undefined') {
            window.location = downloadUrl;
            } else {
            a.href = downloadUrl;
            a.download = filename;
            document.body.appendChild(a);
            a.click();
            }
            } else {
            window.location = downloadUrl;
            }
            setTimeout(function () { 
                URL.revokeObjectURL(downloadUrl); 
                var formUrl=window.location.protocol + "//" + window.location.host+"/MienNghiaVu/formExMs/"+id;
			    window.location.href =formUrl;
            }, 100); // cleanup
        }
        }
        };
        xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
        xhr.send($.param({
        
        }));
    }
</script>