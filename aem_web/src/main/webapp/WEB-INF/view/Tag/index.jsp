<%@ page contentType="text/html;charset=UTF-8" language="java"
	isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ include file="../base/_headerMain.jspf"%>
<!-- Content Header (Page header) -->
<section class="content-header">
	<h1>Quản lý Tag Content</h1>
	<div class="wrap-btn-header">
		<button type="button" class="btn bg-maroon" id="btnCreate">
			<i class="fa fa-plus"></i> Thêm
		</button>
	</div>
</section>

<!-- Main content -->
<section class="content">
	<!-- Main row -->
	<div class="row">

		<!--table-->
		<div class="col-lg-12">
			<div class="filter-table"></div>
			<div class="wrap-table" id="getData">
				<%@ include file="loadTable.jsp"%>
			</div>

		</div>
		<!--table-->
		<div></div>
	</div>

</section>
<!-- /.content -->
<%@ include file="modalAction.jsp"%>
<%@ include file="../base/_footer.jspf"%>
<script>
	var modal=$("#modal-tag");
	var checkIdStatus;
	$("#btnCreate").click(function(){
		$("#title-popup").html("Thêm tag");
		modal.find('input').val(null);
		$("#id_tag").prop("disabled",false);
		modal.modal("show");

		var error = $("label.error");
		$(error).empty();
		$("input.form-control").removeClass("error").addClass("valid");
	})
	$("#btnSave").click(function(){
		if(checkIdStatus=="reject"){
			ThongBao_Loi("Mã đã tồn tại, hãy chọn mã khác!");
			}else if(checkIdStatus=="accept"){
		s_Save('/Tag/addOrUpdate',$("#form-tag"),function(data){
			if(data=="true"){
				ThongBao_ThanhCong("Lưu thành công");
				modal.modal("hide");
				search();
				
			}else{
				Thong_BaoLoi("Lưu thất bại");
				Thong_BaoLoi(data);//thông báo check trùng date off hiện lỗi return
			}			
		},null)}else{
ThongBao_Loi("Có lỗi xảy ra, hãy thử lại!");
			}
	})
	function editModal(idTag,nameTag){
		checkIdStatus="accept";
		$("#title-popup").html("Sửa tag");
		modal.find("#id_tag").val(idTag);
		modal.find("#name_tag").val(nameTag);
		$("#id_tag").prop("disabled",true);
		modal.modal("show");

		var error = $("label.error");
		$(error).empty();
		$("input.form-control").removeClass("error").addClass("valid");
	}
	// function editModal(obj){
	// 	debugger
	// 	var tr=$(obj).closest("tr");
	// 	var id=tr.find("#id").val();
	// 	modal.find("#id").val(id);
	// }
	function DeleteTag(id_tag){
		Delete(id_tag,'/Tag/Delete',function(){
			modal.remove();
			search();	
		})
	}
	function search(){
		var url="/Tag/Index?ajaxLoad=table";
		$("#getData").load(url,function(){
			InitDataTable();
			// console.log("đã load lại table");
		})
	}
	$('#id_tag').change(function(){
	    $.ajax({
	        url: "/Tag/checkId",
	        data: { "idTag": $("#id_tag").val() },
	        type: "post",
	        success: function(data){
	        	if(data=="existed"){
					ThongBao_Loi("Mã đã tồn tại, hãy chọn mã khác!");
					checkIdStatus="reject";
					}else if(data=="not exist"){
						ThongBao_ThanhCong("Mã chưa tồn tại, có thể tạo mới!");
						checkIdStatus="accept";
						}else{
							ThongBao_Loi("Có lỗi xảy ra!");
							}
	        }
	    });
	});
  </script>