<%@ page contentType="text/html;charset=UTF-8" language="java"
	isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!--modal default-->
<div class="modal fade" id="modal-tag">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"
					aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h4 class="modal-title" id="title-popup">Thêm tag</h4>
			</div>
			<div class="modal-body">
				<form id="form-tag" action="/Tag/addOrUpdate"
					method="POST" enctype="multipart/form-data">
					<div class="row">
						<div class="col-lg-6">
							<div class="form-group">
								<label for="" class="required">Mã tag</label> <input class="form-control"
									type="text" id="id_tag" name="id_tag" ismodel isidmodel
									isInputAllowVarchar required>
							</div>
						</div>
						<div class="col-lg-6">
							<div class="form-group">
								<label for="" class="required">Tên tag</label> <input class="form-control"
									type="text" id="name_tag" name="name_tag" ismodel required>
							</div>
						</div>
					</div>
				</form>

			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default pull-left"
					data-dismiss="modal">Đóng</button>
				<button type="button" class="btn btn-primary" id="btnSave">Lưu</button>
			</div>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>
<!--modal default-->