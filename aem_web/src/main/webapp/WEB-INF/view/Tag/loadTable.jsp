<%@ page contentType="text/html;charset=UTF-8" language="java"
	isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<table id="table" class="table table-bordered table-striped">
	<thead>
		<tr>
			<th width="45" class="text-center">STT</th>
			<th>Mã tag</th>
			<th>Tên tag</th>
			<th>Thao Tác</th>

		</tr>
	</thead>
	<tbody class="tblChiTiet">
		<c:forEach var="item" items="${listTag}">
		<tr>
				<td class="text-center"></td>
				<td><span class="">${item.id_tag}</span></td>
				<td>${item.name_tag}</td>
				<td>
					<%-- <input type="hidden" id="id" value="1"/> --%>
					<button type="button" class="btn-edit"
						onclick="editModal('${item.id_tag}','${item.name_tag}')">
						<i class="fa fa-pencil"></i>
					</button> <%-- <button type="button" class="btn-edit" onclick="editModal(this)"><i class="fa fa-pencil"></i></button> --%>
					<button type="button" class="btn-del"
						onclick="DeleteTag('${item.id_tag}')">
						<i class="fa fa-trash-o"></i>
					</button>
				</td>
			</tr>
		</c:forEach>
	</tbody>
	<tr>
		<th width="45" class="text-center">STT</th>
		<th>Mã tag</th>
		<th>Tên tag</th>
		<th>Thao Tác</th>
	</tr>
</table>
