<%@ page contentType="text/html;charset=UTF-8" language="java"
	isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<style>
    .wrap-tag{
        position: absolute;
        bottom: 55px;
        left: 30px;
        background: white;
        box-shadow: 1px 2px 3px 0 rgba(90, 89, 90, 0.5);
        z-index: 1000;
        max-height: calc(100vh / 3 * 2);
        overflow-x: hidden;
        overflow-y: auto;
    }
    .form-group-tag < .wrap-tag{
        position:relative;
    }
    .wrap-tag>.item-tag{
        border-bottom: 1px solid #e0e1e3;
        cursor:pointer;
    }
    .item-tag>.item-key{
        font-weight:700;
        margin-right:10px;
    }
</style>
<section class="content-header">
	<h1>Test tag</h1>
	
</section>
<!-- Main content -->
<section class="content">
	<!-- Main row -->
	<div class="row">

		<!--table-->
		<div class="col-lg-12">
			
            <div class="form-group">
                <label for="">Ghi chú</label>
                <textarea class="form-control" name="note" id="note" onkeypress="LoadTag(event,this)" data-init="0"></textarea>
            </div>

		</div>
		<!--table-->
		<div></div>
	</div>

</section>
<script>
    $(document).ready(function(){
        $("[istagnote]").closest(".form-group").addClass("form-group-tag");
    })
    function loadList(key,load){
        var urlTag="/Test/Index?ajaxLoad="+load+"&key="+key;
        $(".form-group-tag").load(urlTag,function(){

            
        })
    }
</script>