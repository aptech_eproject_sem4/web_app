<%@ page contentType="text/html;charset=UTF-8" language="java"
	isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ include file="../base/_headerMain.jspf"%>
<section class="content-header">
	<h1>Thông tin cá nhân Giảng Viên</h1>
</section>
<section class="content">
	<div class="container-fluid">
		<form id="form-userGiangVien" action="" method="GET"
			enctype="multipart/form-data">
			<div class="row">
				<div class="col-lg-12">
					<div class="row ">
						<div class="col-lg-4 ">
							<div class="row " style="margin-top: 20px;">

								<div class="nav-tabs-custom col-lg-12">
									<ul class="nav nav-tabs">
										<li><a href="" data-toggle="tab" class="fa fa-id-card" style="font-size:16px"> Thông
												tin chính</a></li>

									</ul>
									<div class="col-lg-12">
										<div class="form-group">
											<h3 class="text-center">${fc.name_fc }</h3>
										</div>
										<div class="form-group">
											<h4 class="text-center" style="color: red">${fc.id_fc }</h4>
										</div>
										<div class="form-group">
											<c:forEach var="itemStatus" items="${listStatus}">
												<c:choose>
													<c:when test="${fc.status_id==itemStatus.id_status }">
														<div style="text-align: center">
															<input style="color: white;background-color: orange" type="submit" value="${itemStatus.name_status }" disabled/>
														</div>
													</c:when>
													<c:otherwise>

													</c:otherwise>
												</c:choose>
											</c:forEach>
										</div>
										<div class="form-group">
											<label for="">Email</label> <input class="form-control"
												type="text" id="email_fc" name="email_fc" disabled
												value="${fc.email_fc }">
										</div>
										<div class="form-group">
											<label for="">Email Trường(Email Login)</label> <input
												class="form-control" type="text" id="email_school"
												name="email_school" disabled value="${fc.email_school }">
										</div>
										<div class="form-group">
											<label for="">Di động</label> <input class="form-control"
												type="text" id="phone_fc" name="phone_fc" disabled
												value="${fc.phone_fc }">
										</div>
										<div class="form-group">
											<label for="">Màu</label> <input class="form-control"
												type="text" id="color_css" name="color_css"
												style="background-color:${fc.color_css}!important" disabled>
										</div>
										<div class="form-group">
											<label for="">Quyền truy cập</label>
											<c:set var="rolePart" value="${fn:split(emp.list_role, ',')}" />
											<input class="form-control" type="text" id="list_role"
												name="list_role" disabled value="${listRole}">
										</div>

									</div>
								</div>
							</div>
						</div>
						<div class="col-lg-8">
							<section>
								<div class="row" style="margin-top: 20px;">
									<div class="col-lg-12">
										<div class="nav-tabs-custom">
											<ul class="nav nav-tabs">
												<li class="active"><a href="#bacLuong"
													data-toggle="tab">Bậc lương</a></li>
												<li><a href="#luongTheoGio" data-toggle="tab">Lương
														theo giờ</a></li>
												<li><a href="#diemThi" data-toggle="tab">Điểm thi
														portal</a></li>
											</ul>
											<div class="tab-content">
												<div class="active tab-pane" id="bacLuong">
													<table id="tableBL"
														class="table table-bordered table-striped">
														<thead>
															<tr>
																<th width="45">STT</th>
																<th>Ngày bắt đầu</th>
																<th>Số tiền/giờ</th>

															</tr>
														</thead>
														<tbody class="tblChiTiet">
															<c:set var="index" value="1" />
															<c:forEach var="itemgsf" items="${lsgsf}"
																varStatus="loop">
																<c:choose>
																	<c:when test="${fc.id_fc==itemgsf.fc_id }">
																		<tr>
																			<td class="thuTu">${index}</td>
																			<td><span>${itemgsf.start_date }</span></td>
																			<td><span>${itemgsf.hour_salary }</span></td>
																		</tr>
																		<c:set var="index" value="${index+1}" />
																	</c:when>
																	<c:otherwise>
																	</c:otherwise>
																</c:choose>
															</c:forEach>
														</tbody>
													</table>
												</div>
												<div class=" tab-pane" id="luongTheoGio">
													<section class="content-header">
														<h1>Bảng lương chi tiết của FC</h1>
														<div class="wrap-btn-header d-flex">
															<button type="button" class="btn btn-primary">
																<i class="fa fa-caret-left"><i></i></i>
															</button>
															<input class="form-control text-center" id="dateSearch"
																value="10/2020" readonly>
															<button type="button" class="btn btn-primary">
																<i class="fa fa-caret-right"><i></i></i>
															</button>
														</div>
													</section>
													<section class="content">
														<table id="tableLuong"
															class="table table-bordered table-striped">
															<thead>
																<th>Môn học/Hoạt động</th>
																<th>Lớp</th>
																<th>Thời gian</th>
																<th class="text-right">Số giờ</th>
																<th class="text-right">Hệ số</th>
																<th class="text-right">Quy đổi</th>
															</thead>
															<tbody>
																<tr>
																	<td>CSW</td>
																	<td>c1808g1</td>
																	<td class="relate" style="width: 150px"><span
																		class="from">Fr:</span> 23/11/2020 <br> <span
																		class="to">To:</span> 23/11/2020</td>
																	<td class="text-right">30</td>
																	<td class="text-right">1</td>
																	<td class="text-right">30</td>
																</tr>
																<tr>
																	<td>C#</td>
																	<td>c1808g1</td>
																	<td class="relate" style="width: 150px"><span
																		class="from">Fr:</span> 23/11/2020 <br> <span
																		class="to">To:</span> 23/11/2020</td>
																	<td class="text-right">30</td>
																	<td class="text-right">0.5</td>
																	<td class="text-right">15</td>
																</tr>
																<tr>
																	<td colspan="5" class="text-right">Tổng số giờ</td>
																	<td class="text-right sum">45</td>
																</tr>
																<tr>
																	<td colspan="5" class="text-right">Lương/giờ (VND)</td>
																	<td class="text-right sum">100.000</td>
																</tr>
																<tr>
																	<td colspan="5" class="text-right">Tổng lương
																		(VND)</td>
																	<td class="text-right sum">4.500.000</td>
																</tr>
															</tbody>

														</table>
													</section>
												</div>
												<div class="tab-pane" id="diemThi">
													<table id="tableDT"
														class="table table-bordered table-striped">
														<thead>
															<tr>
																<th width="45">STT</th>
																<th>Môn thi</th>
																<th>Số điểm</th>
															</tr>
														</thead>
														<tbody class="tblChiTiet">
															<c:set var="index" value="1" />
															<c:forEach var="itemsfc" items="${sfc}" varStatus="loop">
																<c:choose>
																	<c:when test="${fc.id_fc==itemsfc.fc_id}">
																		<c:forEach var="sbj" items="${sbj}">
																			<c:choose>
																				<c:when test="${itemsfc.subject_id==sbj.id_subject}">
																					<tr>
																						<td class="thuTu">${index}</td>
																						<td><span>${sbj.sort_name} -
																								${sbj.name_subject}</span></td>
																						<td><span>Điểm
																								số:${itemsfc.score_number}</span> <br> <span>Điểm
																								% : ${itemsfc.score_percent}</span></td>
																					</tr>
																					<c:set var="index" value="${index+1}" />
																				</c:when>
																				<c:otherwise>
																				</c:otherwise>
																			</c:choose>
																		</c:forEach>
																	</c:when>
																	<c:otherwise>
																	</c:otherwise>
																</c:choose>
															</c:forEach>
														</tbody>

													</table>
												</div>
											</div>
										</div>
									</div>
								</div>
							</section>
						</div>
					</div>
				</div>
			</div>
		</form>
	</div>
</section>
<%@ include file="../base/_footer.jspf"%>