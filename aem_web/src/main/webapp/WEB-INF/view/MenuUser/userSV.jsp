<%@ page contentType="text/html;charset=UTF-8" language="java"
	isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ include file="../base/_headerMain.jspf"%>

<style>
ul, #myUL {
	list-style-type: none;
}

#myUL {
	margin: 0;
	padding: 0;
}

#myUL .caretx {
	cursor: pointer;
	-webkit-user-select: none; /* Safari 3.1+ */
	-moz-user-select: none; /* Firefox 2+ */
	-ms-user-select: none; /* IE 10+ */
	user-select: none;
	font-weight: bold;
}

#myUL .caretx::before {
	content: "\25B6";
	color: black;
	display: inline-block;
	margin-right: 6px;
}

#myUL .caret-down::before {
	-ms-transform: rotate(90deg); /* IE 9 */
	-webkit-transform: rotate(90deg); /* Safari */ '
	transform: rotate(90deg);
}

#myUL .nested {
	display: none;
}

#myUL .active {
	display: block;
}
</style>

<section class="content-header">
	<h1>Thông tin cá nhân Giảng Viên</h1>
</section>
<section class="content">
	<div class="container-fluid">
		<div class="row">
			<div class="col-lg-12">
				<div class="col-lg-3">
					<div class="nav-tabs-custom col-lg-12" id="thongTinChinh">
						<ul class="nav nav-tabs">
							<li><a href="" data-toggle="tab" class="fa fa-id-card"
								style="font-size: 16px"> Thông tin chính</a></li>
						</ul>
						<div class="col-lg-12">
							<form>
								<div class="row">
									<div class="col-lg-4">
										<c:choose>
											<c:when test="${ empty fn:trim(student.image_student)}">
												<img id="image_student_photo" class="profile-photo"
													accept="image/*" src="/upload/avatar/dealftUser.png"
													style="cursor: auto; width: 100px; height: 100px;">
											</c:when>
											<c:otherwise>
												<img id="image_student_photo" class="profile-photo"
													accept="image/*" src="${student.image_student}"
													style="cursor: auto; width: 100px; height: 100px;">
											</c:otherwise>
										</c:choose>
									</div>
									<div class="col-lg-8">
										<div class="form-group">
											<h3 class="text-center">${student.full_name}</h3>
										</div>
										<div class="form-group">
											<h4 class="text-center" style="color: red">${student.id_student}</h4>
										</div>
										<div class="form-group">
											<div style="text-align: center">
												<c:forEach var="itemStatus" items="${listStatus}">
													<c:choose>
														<c:when test="${itemStatus.id_status==student.status_id }">
															<input style="color: white; background-color: orange"
																type="submit" value="${itemStatus.name_status }"
																disabled />
														</c:when>
														<c:otherwise>
														</c:otherwise>
													</c:choose>
												</c:forEach>
											</div>
										</div>
									</div>
								</div>
								<div class="form-group">
									<label for="">Giới tính</label>
									<c:choose>
										<c:when test="${student.sex==true }">
											<input class="form-control" type="text" value="Nam" disabled>
										</c:when>
										<c:otherwise>
											<input class="form-control" type="text" value="Nữ" disabled>
										</c:otherwise>
									</c:choose>
								</div>
								<div class="form-group">
									<label for="">Ngày sinh</label> <input class="form-control"
										value="${student.dob }" type="text" disabled>
								</div>
								<div class="form-group">
									<label for="">Email</label> <input class="form-control"
										value="${student.email_student }" type="text" disabled>
								</div>
								<div class="form-group">
									<label for="">Di động</label> <input class="form-control"
										value="${student.mobile_phone }" type="text" disabled>
								</div>
								<div class="form-group">
									<label for="">Di động liên hệ</label> <input
										class="form-control" type="text"
										value="${student.contact_phone }" disabled>
								</div>
								<div class="form-group">
									<label for="">Điện thoại nhà</label> <input
										class="form-control" type="text"
										value="${student.home_phone }" disabled>
								</div>
								<div class="form-group">
									<label for="">Địa chỉ</label> <input class="form-control"
										type="text" value="${student.address }" disabled>
								</div>
								<div class="form-group">
									<label for="">Địa chỉ liên hệ</label> <input
										class="form-control" type="text"
										value="${student.contact_address}" disabled>
								</div>
								<div class="form-group">
									<label for="">Quận/huyện</label> <input class="form-control"
										type="text" value="${student.district }" disabled>
								</div>
								<div class="form-group">
									<label for="">Thành phố tỉnh</label> <input
										class="form-control" type="text" value="${student.city}"
										disabled>
								</div>
								<div class="form-group">
									<label for="">Trường cấp 3</label> <input class="form-control"
										type="text" value="${student.high_school }" disabled>
								</div>
								<div class="form-group">
									<label for="">Đại học</label> <input class="form-control"
										type="text" value="${student.university }" disabled>
								</div>
							</form>
						</div>
					</div>
				</div>
				<div class="col-lg-9">
					<div id="thongTinNguoiThan">
						<ul class="nav nav-tabs">
							<li><a href="" data-toggle="tab" class="fa fa-id-card"
								style="font-size: 16px"> Thông tin người thân</a></li>
						</ul>
						<table id="tableSV" class="table table-bordered table-striped"
							onscroll="scrollToLoadData()">
							<thead>
								<tr>
									<th width="45">STT</th>
									<th>Tên người thân</th>
									<th>Quan hệ</th>
									<th>SDT</th>
									<th>Địa chỉ</th>
									<th>Nghề nghiệp</th>
								</tr>
							</thead>
							<tbody class="tblChiTiet" style="overflow: auto; height: 50vh;">
								<c:choose>
									<c:when test="${empty jsonHoso }">
										<tr>
											<td class="stt"></td>
											<td class="nameQuanhe"></td>
											<td class="quanhe"></td>
											<td class="sdtQuanhe"></td>
											<td class="diachiQuanhe"></td>
											<td class="nghenghiepQuanhe"></td>
										</tr>
									</c:when>
									<c:otherwise>
										<c:forEach var="jsonItem" items="${jsonHoso}" varStatus="loop">										
<%-- 										<c:forEach begin="0" end="${jsonHoso.length()-1}" var="index"> --%>
											<tr>
												<td class="stt">${loop.index+1 }</td>
<%-- 												<td class="nameQuanhe">${jsonHoso.getJSONObject(index).getString('nameQuanhe')}</td> --%>
												<td class="nameQuanhe">${jsonItem.nameQuanhe}</td>
												<td class="quanhe"><c:forEach var="dmqh"
														items="${ listStatus }">
														<c:choose>
															<c:when
																test="${jsonItem.idQuanhe==dmqh.id_status}">
																	${ dmqh.name_status }
															</c:when>
															<c:otherwise>

															</c:otherwise>
														</c:choose>
													</c:forEach></td>
												<td class="sdtQuanhe">${jsonItem.sdtQuanhe}</td>
												<td class="diachiQuanhe">${jsonItem.diachiQuanhe}</td>
												<td class="nghenghiepQuanhe">${jsonItem.nghenghiepQuanhe}</td>
											</tr>
										</c:forEach>
									</c:otherwise>
								</c:choose>
							</tbody>
							<tfoot>
								<tr>
									<th width="45">STT</th>
									<th>Tên người thân</th>
									<th>Quan hệ</th>
									<th>SDT</th>
									<th>Địa chỉ</th>
									<th>Nghề nghiệp</th>
								</tr>
							</tfoot>
						</table>
					</div>
					<div id="thongTinKhoaHoc">
						<ul class="nav nav-tabs">
							<li><a href="" data-toggle="tab" class="fa fa-book"
								style="font-size: 16px"> Thông tin khóa học</a></li>
						</ul>
						<div>
							<div class="col-lg-12">
								<div class="col-lg-4">
									<label for="">Lớp đầu tiên</label>
									<c:set var="falseCount" value="0" scope="page"/>
									<c:forEach var="itemClass" items="${listClass}">
										<c:choose>
											<c:when test="${itemClass.id_class==student.first_class }">
												<input class="form-control" type="text"
													value="${itemClass.name_class}" disabled>
											</c:when>
											<c:otherwise>
											<c:set var="falseCount" value="${falseCount + 1}" scope="page"/>
											</c:otherwise>
										</c:choose>
									</c:forEach>
									<c:if test="${falseCount==listClass.size() }">
										<input class="form-control" type="text" value="" disabled>
									</c:if>
								</div>
								<div class="col-lg-4">
									<label for="">Lớp hiện tại</label>
									<c:set var="falseCount" value="0" scope="page"/>
									<c:forEach var="itemClass" items="${listClass}">
										<c:choose>
											<c:when test="${itemClass.id_class==student.current_class }">
												<input class="form-control" type="text"
													value="${itemClass.name_class}" disabled>
											</c:when>
											<c:otherwise>
											<c:set var="falseCount" value="${falseCount + 1}" scope="page"/>
											</c:otherwise>
										</c:choose>
									</c:forEach>
									<c:if test="${falseCount==listClass.size() }">
										<input class="form-control" type="text" value="" disabled>
									</c:if>
								</div>
								<div class="col-lg-4">
									<label for="">Ngày bắt đầu học</label> <input
										class="form-control" type="text"
										value="${student.date_of_doing }" disabled>
								</div>
							</div>
							<div class="col-lg-12">
								<div class="col-lg-4">
									<label for="">Course</label>
									<c:set var="falseCount" value="0" />
									<c:forEach var="itemCourse" items="${listCourse}">
										<c:choose>
											<c:when test="${itemCourse.id_course==student.course_id }">
												<input class="form-control" type="text"
													value="${itemCourse.name_course}" disabled>
											</c:when>
											<c:otherwise>
												<c:set var="falseCount" value="${falseCount + 1}"
													scope="page" />
											</c:otherwise>
										</c:choose>
									</c:forEach>
									<c:if test="${falseCount==listCourse.size() }">
										<input class="form-control" type="text" value="" disabled>
									</c:if>
								</div>
								<div class="col-lg-4">
									<label for="">Course family</label>
									<c:set var="falseCount" value="0" />
									<c:forEach var="itemCourse" items="${listCourse}">
										<c:choose>
											<c:when
												test="${itemCourse.id_course==student.course_family }">
												<input class="form-control" type="text"
													value="${itemCourse.name_course}" disabled>
											</c:when>
											<c:otherwise>
												<c:set var="falseCount" value="${falseCount + 1}"
													scope="page" />
											</c:otherwise>
										</c:choose>
									</c:forEach>
									<c:if test="${falseCount==listCourse.size() }">
										<input class="form-control" type="text" value="" disabled>
									</c:if>
								</div>
								<div class="col-lg-4">
									<label for="">Email trường(email login)</label> <input
										class="form-control" type="text"
										value="${student.email_school }" disabled>
								</div>
							</div>
						</div>
					</div>
					<div id="thongTinHoc">
						<section>
							<div class="row" style="margin-top: 20px;">
								<div class="col-lg-12">
									<div class="nav-tabs-custom">
										<ul class="nav nav-tabs">
											<li class="active"><a href="#khoaHoc" data-toggle="tab">Khóa
													học</a></li>
											<%-- <li><a href="#diemThi" data-toggle="tab">Điểm thi</a></li> --%>
											<li><a href="#noMon" data-toggle="tab">Nợ môn</a></li>
										</ul>
										<div class="tab-content">
											<div class="active tab-pane" id="khoaHoc">
												<c:forEach var="itemSemester" items="${listSemester}" varStatus="index">
													<c:choose>
														<c:when test="${itemSemester.course_id==student.course_id }">
															<ul id="myUL">
																<li><span class="caretx" style="cursor: pointer;">${itemSemester.name_seme }</span>
																	<ul class="nested">
																		<c:forEach var="itemSubject" items="${listSubject}">
																			<c:choose>
																				<c:when
																					test="${itemSubject.seme_id==itemSemester.id }">
																					<li>${itemSubject.name_subject }</li>
																				</c:when>
																				<c:otherwise>
																				</c:otherwise>
																			</c:choose>
																		</c:forEach>
																	</ul></li>
															</ul>
														</c:when>
														<c:otherwise>

														</c:otherwise>
													</c:choose>
												</c:forEach>
											</div>
											<%-- <div class=" tab-pane" id="diemThi">
												<section class="content">
													<table id="tableDiemThi"
														class="table table-bordered table-striped">
														<thead>
															<tr>
																<th>Học kỳ</th>
																<th>Môn học</th>
																<th>Loại thi</th>
																<th class="text-right">Điểm thi</th>
																<th class="text-center">Pass</th>
															</tr>
														</thead>
														<tbody>
															<tr>
																<td rowspan="4">HK1</td>
																<td rowspan="2">EAD</td>
																<td>E</td>
																<td class="text-right">20</td>
																<td class="text-center"><div
																		style="text-align: center">
																		<input style="color: white; background-color: green"
																			type="submit" value="Pass" disabled />
																	</div></td>
															</tr>
															<tr>
																<td>R</td>
																<td class="text-right">4</td>
																<td class="text-center"><div
																		style="text-align: center">
																		<input style="color: white; background-color: red"
																			type="submit" value="Not" disabled />
																	</div></td>
															</tr>
															<tr>
																<td rowspan="2">EAD</td>
																<td>E</td>
																<td class="text-right">20</td>
																<td class="text-center"><div
																		style="text-align: center">
																		<input style="color: white; background-color: green"
																			type="submit" value="Pass" disabled />
																	</div></td>
															</tr>
															<tr>
																<td>R</td>
																<td class="text-right">4</td>
																<td class="text-center"><div
																		style="text-align: center">
																		<input style="color: white; background-color: red"
																			type="submit" value="Not" disabled />
																	</div></td>
															</tr>
														</tbody>

													</table>
												</section>
											</div> --%>
											<div class="tab-pane" id="noMon">
												
											</div>
										</div>
									</div>
								</div>
							</div>
						</section>

					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<%@ include file="../base/_footer.jspf"%>
<script>
	var toggler = document.getElementsByClassName("caretx");
	var i;
	for (i = 0; i < toggler.length; i++) {
		toggler[i].addEventListener("click", function() {
			this.parentElement.querySelector(".nested").classList
					.toggle("active");
			this.classList.toggle("caretx-down");
		});
	}
	var urlLoadNoMon="/PhieuThu/getListNoMon/${student.id_student}";
	$("#noMon").load(urlLoadNoMon);

</script>