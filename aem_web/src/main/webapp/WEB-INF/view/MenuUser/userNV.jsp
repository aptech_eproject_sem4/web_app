<%@ page contentType="text/html;charset=UTF-8" language="java"
	isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ include file="../base/_headerMain.jspf"%>
<section class="content-header">
	<h1>Thông tin cá nhân</h1>
</section>
<section class="content">
	<form id="form-userNhanvien" action="" method="GET"
		enctype="multipart/form-data">
		<div class="row">
			<div class="col-lg-12">
				<div class="row">
					<div class="col-lg-4">
						<div class="form-group">
							<label for="">Mã nhân viên</label> <input class="form-control"
								type="text" id="id_emp" name="id_emp" disabled
								value="${emp.id_emp }">
						</div>
					</div>
					<div class="col-lg-4">
						<div class="form-group">
							<label for="">Tên nhân viên</label> <input class="form-control"
								type="text" id="id_emp" name="id_emp" disabled
								value="${emp.name_emp }">
						</div>
					</div>
					<div class="col-lg-4">
						<div class="form-group">
							<label for="">Di động</label> <input class="form-control"
								type="text" id="phone_emp" name="phone_emp" onlynumber disabled
								value="${emp.phone_emp }">
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-4">
						<div class="form-group">
							<label for="">Email login</label> <input class="form-control"
								type="text" id="email_emp" name="email_emp" disabled
								value="${emp.email_emp }">
						</div>
					</div>
					<div class="col-lg-8">
						<div class="form-group">
							<label for="">Quyền truy cập</label>
							<c:set var="rolePart" value="${fn:split(emp.list_role, ',')}" />
							<input class="form-control" type="text" id="list_role"
								name="list_role" disabled
								value="${listRole}">	
						</div>
					</div>
				</div>
			</div>
		</div>
	</form>
</section>
<%@ include file="../base/_footer.jspf"%>