<%@ page contentType="text/html;charset=UTF-8" language="java"
	isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ include file="../base/_headerMain.jspf"%>
<!-- Content Header (Page header) -->
<section class="content-header">
	<h1>Quản lý nhân viên</h1>
	<div class="wrap-btn-header">
		<button type="button" class="btn bg-maroon" id="btnCreate">
			<!-- data-toggle="modal" data-target="#modal-default" -->
			<i class="fa fa-plus"></i> Thêm
		</button>
	</div>
</section>

<!-- Main content -->
<section class="content">
	<script type="text/javascript">
    var listrole = null;
  </script>
	<!-- Main row -->
	<div class="row">
		<!--table-->
		<div class="col-lg-12">
			<div class="filter-table">
				<div class="row">
					<div class="col-lg-2">
						<div class="form-group">
							<select class="form-control select2" style="width: 100%"
								id="listStatus" name="listStatus">
								<option selected="selected" value="">Chọn trạng thái</option>
								<option value="1">Đang làm</option>
								<option value="0">Đã nghỉ việc</option>
							</select>
						</div>
					</div>
					<div class="col-lg-2">
						<div class="form-group">
							<input class="form-control" type="text" id="qSearch"
								name="qSearch" placeholder="Tìm kiếm" />
						</div>
					</div>
					<div class="col-lg-1">
						<button type="button" class="btn bg-maroon btnSearch"
							id="qSearchbtn" onclick="search()">
							<i class="fa fa-search"></i>
						</button>
					</div>
				</div>
			</div>
			<div class="wrap-table" id="getData">
				<%@ include file="loadTable.jsp"%>
			</div>
		</div>
		<!--table-->
		<div></div>
	</div>
</section>
<!-- /.content -->
<%@ include file="modalAction.jsp"%>
<%@ include file="../base/_footer.jspf"%>
<script>
  var modal = $("#modal-nhanVien");
  var checkIdStatus;
  $("#btnCreate").click(function () {
    modal.find("input").val(null);
    $("#title-popup").html("Thêm nhân viên");
    var parentNode_list_role = $("#list_role").parent();
    var ulitem_list_role = $(parentNode_list_role).find(
      "ul.select2-selection__rendered"
    );
    $(ulitem_list_role).empty();
    var arr_option_list_role = $("#list_role option");
    for (i = 0; i < arr_option_list_role.length; i++) {
      $(arr_option_list_role[i]).prop("selected", false);
    }
    var error = $("label.error");
    $(error).empty();
    $("input.form-control").removeClass("error");
    $("#id_emp").prop("disabled", false);
    $("#list_role").prop("required", true);
    modal.modal("show");
  });

  $("#btnSave").click(function () {
    if (checkIdStatus == "reject") {
      ThongBao_Loi("Mã đã tồn tại, hãy chọn mã khác!");
    } else if (checkIdStatus == "accept") {
      s_Save(
        "/NhanVien/addOrUpdate",
        $("#form-nhanvien"),
        function (data) {
          if (data == "true") {
            ThongBao_ThanhCong("Lưu thành công");
            modal.modal("hide");
            search();
          } else {
            ThongBao_Loi("Lưu thất bại");
            ThongBao_Loi(data); //thông báo check trùng date off hiện lỗi return
          }
        },
        null
      );
    } else {
      ThongBao_Loi("Có lỗi xảy ra, hãy thử lại!");
    }
  });

  function editModal(
    id_emp,
    name_emp,
    email_emp,
    phone_emp,
    active_account,
    list_role,
    status
  ) {
    checkIdStatus = "accept";
    //clear all input
    modal.find("input").val(null);
    $("#title-popup").html("Sửa nhân viên");
    modal.find("#id_emp").val(id_emp);
    modal.find("#name_emp").val(name_emp);
    modal.find("#email_emp").val(email_emp);
    modal.find("#phone_emp").val(phone_emp);
    modal.find("#active_account").val(active_account);
    modal.find("#list_role").val(list_role);
    modal.find("#status").val(status);
    listrole = list_role.split(",");
    $("#id_emp").prop("disabled", true);
    //clear error label
    var error = $("label.error");
    $(error).empty();
    $("input.form-control").removeClass("error").addClass("valid");
    //list_role
    var parentNode_list_role = $("#list_role").parent();
    var ulitem_list_role = $(parentNode_list_role).find(
      "ul.select2-selection__rendered"
    );
    var arr_option_list_role = $("#list_role option");
    //clear list role
    $(ulitem_list_role).empty();
    for (i = 0; i < arr_option_list_role.length; i++) {
      $(arr_option_list_role[i]).prop("selected", false);
    }
    for (i = 0; i < arr_option_list_role.length; i++) {
      for (j = 0; j < listrole.length; j++) {
        if ($(arr_option_list_role[i]).val() == listrole[j]) {
          $(arr_option_list_role[i]).prop("selected", true);
          var item_li = document.createElement("li");
          $(item_li).addClass("select2-selection__choice");
          $(item_li).prop("title", $(arr_option_list_role[i]).html());
          $(item_li).html(
            '<span class="select2-selection__choice__remove" role="presentation">×</span>' +
              $(arr_option_list_role[i]).html()
          );
          $(ulitem_list_role).append($(item_li));
          break;
        }
      }
    }
    //status
    var parentNode_status = $("#status").parent();
    var ulitem_status = $(parentNode_status).find(
      "span.select2-selection__rendered"
    );
    var arr_option_status = $("#status option");
    for (i = 0; i < arr_option_status.length; i++) {
      if ($(arr_option_status[i]).val() == status) {
        $(arr_option_status[i]).prop("selected", true);
        $(ulitem_status).prop("title", $(arr_option_status[i]).html());
        $(ulitem_status).html($(arr_option_status[i]).html());
        break;
      }
    }
    //active_account
    var parentNode_active_account = $("#active_account").parent();
    var ulitem_active_account = $(parentNode_active_account).find(
      "span.select2-selection__rendered"
    );
    var arr_option_active_account = $("#active_account option");
    for (i = 0; i < arr_option_active_account.length; i++) {
      if ($(arr_option_active_account[i]).val() == active_account) {
        $(arr_option_active_account[i]).prop("selected", true);
        $(ulitem_active_account).prop(
          "title",
          $(arr_option_active_account[i]).html()
        );
        $(ulitem_active_account).html($(arr_option_active_account[i]).html());
        break;
      }
    }
    modal.modal("show");
  }

  // function editModal(obj){
  // 	debugger
  // 	var tr=$(obj).closest("tr");
  // 	var id=tr.find("#id").val();
  // 	modal.find("#id").val(id);
  // }
  function InitDataTable(selectorTable) {
    selectorTable = selectorTable ?? $("#tableNV");
    var table = selectorTable.DataTable({
      ordering: false,
      order: [[1, "asc"]],
    });
    table
      .on("order.dt search.dt", function () {
        table
          .column(0, { search: "applied", order: "applied" })
          .nodes()
          .each(function (cell, i) {
            cell.innerHTML = i + 1;
          });
      })
      .draw();
  }
  function search() {
    var e1 = document.getElementById("listStatus");
    var value1 = e1.options[e1.selectedIndex].value;
    var qSearch = $("#qSearch").val().replace("/\ /g", "+");
    var url =
      "/NhanVien/Index?ajaxLoad=table&status=" +
      value1 +
      "&searchValue=" +
      qSearch;
    $("#getData").load(url, function () {
      //$("#table").DataTable({
      //ordering: false,
      //});
      InitDataTable();
      // console.log("đã load lại table");
    });
  }

  function changeActiveAccount(event, idemp) {
    var currentRow = $(event).closest("tr");
    var idemp = currentRow.find("td:eq(1) #getIdEmp").text();
    $.ajax({
      url: "/NhanVien/changeActiveAccount",
      data: {
        id_emp: idemp,
      },
      type: "post",
      success: function (data) {
        ThongBao_ThanhCong(data);
        search();
      },
    });
  }

  $("#id_emp").change(function () {
    $.ajax({
      url: "/NhanVien/checkId",
      data: {
        id_emp: $("#id_emp").val(),
      },
      type: "post",
      success: function (data) {
        if (data == "existed") {
          ThongBao_Loi("Mã đã tồn tại, hãy chọn mã khác!");
          checkIdStatus = "reject";
        } else if (data == "not exist") {
          ThongBao_ThanhCong("Mã chưa tồn tại, có thể tạo mới!");
          checkIdStatus = "accept";
        } else {
          ThongBao_Loi("Có lỗi xảy ra!");
        }
      },
    });
  });



  
</script>
