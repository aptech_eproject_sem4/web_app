<%@ page contentType="text/html;charset=UTF-8" language="java"
	isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<table id="tableNV" class="table table-bordered table-striped">
	<thead>
		<tr>
			<th width="45">STT</th>
			<th style="max-width: 200px;">Tên NV</th>
			<th>Liên hệ</th>
			<th>Quyền</th>
			<th>Tình trạng</th>
			<th>Thao tác</th>
		</tr>
	</thead>
	<tbody class="tblChiTiet">
		<c:forEach var="item" items="${listEmployee}">
			<tr>
				<td class="text-center"></td>
				<td><span class="label label-primary" id="getIdEmp">${item.id_emp }</span>
					<br> <span>${item.name_emp }</span></td>
				<td><span>${item.phone_emp }</span><br> <span>${item.email_emp }</span><br></td>
				<!-- <span>${item.list_role}</span> -->
				<td><c:set var="rolePart"
						value="${fn:split(item.list_role, ',')}" /> <c:forEach
						var="itemRolePart" items="${rolePart}">
						<c:forEach var="itemRole" items="${listRole}">
							<c:choose>
								<c:when test="${itemRolePart==itemRole.id}">
									<span>${itemRole.name_role},</span>
								</c:when>
								<c:otherwise>
								</c:otherwise>
							</c:choose>
						</c:forEach>
					</c:forEach></td>
				<td><c:choose>
						<c:when test="${item.status==true}">
							<span class="label label-success">Đang làm</span>
						</c:when>
						<c:otherwise>
							<span class="label label-danger">Đã nghỉ việc</span>
						</c:otherwise>
					</c:choose><br></td>
				<td><c:choose>
						<c:when test="${item.active_account==true}">
							<button type="button" class="btn-warning"
								onclick="changeActiveAccount(this)">
								<i class="fa fa-lock"></i>
							</button>
						</c:when>
						<c:otherwise>
							<button type="button" class="btn-warning"
								onclick="changeActiveAccount(this)">
								<i class="fa fa-unlock"></i>
							</button>
						</c:otherwise>
					</c:choose>
					<button type="button" class="btn-edit"
						onclick="editModal('${item.id_emp}','${item.name_emp}','${item.email_emp }','${item.phone_emp }','${item.active_account }','${item.list_role }','${item.status }')">
						<i class="fa fa-pencil"></i>
					</button>
					<button type="button" class="btn-del"
						onclick="Delete('${item.id_emp}','/NhanVien/Delete',function(){search()})">
						<i class="fa fa-trash-o"></i>
					</button></td>
			</tr>
		</c:forEach>
	</tbody>
	<tfoot>
		<tr>
			<th>STT</th>
			<th>Tên NV</th>
			<th>Liên hệ</th>
			<th>Quyền</th>
			<th>Tình trạng</th>
			<th>Thao tác</th>
		</tr>
	</tfoot>
</table>
