<%@ page contentType="text/html;charset=UTF-8" language="java"
	isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!--modal default-->
<div class="modal fade" id="modal-nhanVien">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"
					aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h4 class="modal-title" id="title-popup">Thêm nhân viên</h4>
			</div>
			<div class="modal-body">
				<form id="form-nhanvien" action="/NhanVien/addOrUpdate" method="POST"
					enctype="multipart/form-data">
					<div class="row">
						<div class="col-lg-12">
							<div class="row">
								<div class="col-lg-6">
									<div class="form-group">
										<label for="" class="required">Mã nhân viên</label> <input class="form-control"
											type="text" id="id_emp" name="id_emp" isInputAllowVarchar required>
									</div>
								</div>
								<div class="col-lg-6">
									<div class="form-group">
										<label for="">Di động</label> <input class="form-control"
											type="text" id="phone_emp" name="phone_emp" onlynumber>
									</div>
								</div>
							</div>
							<div class="form-group">
								<label for="" class="required">Tên nhân viên</label> <input class="form-control"
									type="text" id="name_emp" name="name_emp" required>
							</div>
							<div class="form-group">
								<label for="" class="required">Email</label> <input class="form-control"
									type="text" id="email_emp" name="email_emp" required>
							</div>
							<div class="form-group">
								<label for="" class="required">Chọn quyền</label>
								<select
									class="form-control select2" id="list_role" name="list_role" multiple style="width: 100%;"
									data-placeholder="Chọn quyền">
									<c:forEach var="itemRole" items="${listRole}">
										<option value="${itemRole.id }">${itemRole.name_role }</option>
									</c:forEach>
								</select>
							</div>
							<div class="row">
								<div class="col-lg-6">
									<div class="form-group">
										<label for="" class="required">Tình trạng</label><br/> <select
											class="form-control select2" style="width: 100%;" id="status"
											name="status" required>
											<option></option>
											<option value="true">Đang làm việc</option>
											<option value="false">Đã nghỉ việc</option>
										</select>
									</div>
								</div>
								<div class="col-lg-6">
									<div class="form-group">
										<label for="" class="required">Tình trạng login</label><br/> <select
											class="form-control select2" style="width: 100%;" id="active_account"
											name="active_account" required>
											<option></option>
											<option value="true">Mở</option>
											<option value="false">Đóng</option>
										</select>
									</div>
								</div>
							</div>
						</div>
					</div>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default pull-left"
					data-dismiss="modal">Đóng</button>
				<button type="button" class="btn btn-primary" id="btnSave">Lưu</button>
			</div>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>
<!--modal default-->