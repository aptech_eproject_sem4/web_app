<%@ page contentType="text/html;charset=UTF-8" language="java"
	isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ include file="../base/_headerMain.jspf"%>
<!-- Content Header (Page header) -->
<section class="content-header">
	<h1>Quản lý danh mục</h1>
	<div class="m-auto">
		<select class="form-control select2" style="width: 100%;"
			id="listGroup" name="listGroup">
			<c:forEach var="item" items="${listGroup}">
				<option value="${item.id}">${item.name_group}</option>
			</c:forEach>
		</select>
	</div>
	<div class="wrap-btn-header">
		<button type="button" class="btn bg-maroon" id="btnCreate">
			<i class="fa fa-plus"></i> Thêm
		</button>
	</div>
</section>
<!-- Main content -->
<section class="content">
	<!-- Main row -->
	<div class="row">

		<!--table-->
		<div class="col-lg-12">
			<div class="filter-table"></div>
			<div class="wrap-table" id="getData">
				<%@ include file="loadTable.jsp"%>
			</div>
		</div>
		<!--table-->
		<div></div>
	</div>

</section>
<!-- /.content -->
<%@ include file="modalAction.jsp"%>
<%@ include file="../base/_footer.jspf"%>
<script>
	var modal = $("#modal-danhMucTinhTrang");
	var checkIdStatus;
	$("#btnCreate").click(function() {
		$("#title-popup").html("Thêm danh mục");
		modal.find('input').val(null);
		$("#id_status").prop("disabled", false);
		var error = $("label.error");
		$(error).empty();
		$("input.form-control").removeClass("error");
		modal.modal("show");
	});

	$('#listGroup').on('change', function() {
		search(); // or $(this).val()
	});

	$("#btnSave").click(function() {
		if(checkIdStatus=="reject"){
			ThongBao_Loi("Mã đã tồn tại, hãy chọn mã khác!");
			}else if(checkIdStatus=="accept"){
				var e = document.getElementById("group_type");
				var value = e.options[e.selectedIndex].value;
				if (value <= 0) {
					ThongBao_Loi("Chọn danh mục!");
				} else {
					s_Save('/DanhMucTinhTrang/addOrUpdate',
							$("#form-danhmuctinhtrang"), function(data) {
								if (data == "true") {
									ThongBao_ThanhCong("Lưu thành công");
									modal.modal("hide");
									search();

								} else {
									Thong_BaoLoi("Lưu thất bại");
									Thong_BaoLoi(data);//thông báo check trùng date off hiện lỗi return
								}
							}, null)
				}}else{
ThongBao_Loi("Có lỗi xảy ra, hãy thử lại!");
					}
			});

	function editModal(id_status, name_status, group_type, order_number, color,note) {
		checkIdStatus="accept";
		$("#title-popup").html("Sửa danh mục");
		modal.find("#id_status").val(id_status);
		modal.find("#name_status").val(name_status);
		modal.find("#group_type").val(group_type);
		modal.find("#order_number").val(order_number);
		modal.find("#color").val(color);
		modal.find("#note").val(note);
		$("#id_status").prop("disabled", true);
		var error = $("label.error");
		$(error).empty();
		$("input.form-control").removeClass("error").addClass("valid");
		var parentNode_group_type = $("#group_type").parent();
		var ulitem_group_type = $(parentNode_group_type).find(
				"span.select2-selection__rendered");
		var arr_option_group_type = $("#group_type option");
		$(ulitem_group_type).empty();
		for (i = 0; i < arr_option_group_type.length; i++) {
			if ($(arr_option_group_type[i]).val() == group_type) {
				$(arr_option_group_type[i]).prop("selected", true);
				$(ulitem_group_type).prop("title", $(arr_option_group_type[i]).html());
				$(ulitem_group_type).html($(arr_option_group_type[i]).html());
				break;
			}
		}
		modal.modal("show");
	}
	// function editModal(obj){
	// 	debugger
	// 	var tr=$(obj).closest("tr");
	// 	var id=tr.find("#id").val();
	// 	modal.find("#id").val(id);
	// }

	function search() {
		var e = document.getElementById("listGroup");
		var value = e.options[e.selectedIndex].value;
		var url = '/DanhMucTinhTrang/Index?ajaxLoad=table&idGroup=' + value;
		$("#getData").load(url, function() {
			//$("#table").DataTable({
			//ordering: false,
			//});
			InitDataTable();
			// console.log("đã load lại table");
		})
	}

	function updateActive(event) {
		var currentRow = $(event).closest("tr");
		var idGroup = currentRow.find("td:eq(1)").text();
		$.ajax({
			url : "/DanhMucTinhTrang/updateActive",
			data : {
				"idGroup" : idGroup
			},
			type : "post",
			success : function(data) {
				ThongBao_ThanhCong(data);
				search();
			}
		});
	}

	$('#id_status').change(function() {
		$.ajax({
			url : "/DanhMucTinhTrang/checkId",
			data : {
				"idGroup" : $("#id_status").val()
			},
			type : "post",
			success : function(data) {
				if(data=="existed"){
					ThongBao_Loi("Mã đã tồn tại, hãy chọn mã khác!");
					checkIdStatus="reject";
					}else if(data=="not exist"){
						ThongBao_ThanhCong("Mã chưa tồn tại, có thể tạo mới!");
						checkIdStatus="accept";
						}else{
							ThongBao_Loi("Có lỗi xảy ra!");
							}
			}
		});
	});

	$("#listGroup option:first").attr('selected', 'selected');
</script>