<%@ page contentType="text/html;charset=UTF-8" language="java"
	isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<div class="modal fade" id="modal-danhMucTinhTrang">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"
					aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h4 class="modal-title" id="title-popup">Thêm danh mục</h4>
			</div>
			<div class="modal-body">
				<form id="form-danhmuctinhtrang"
					action="/DanhMucTinhTrang/addOrUpdate" method="POST"
					enctype="multipart/form-data">
					<div class="row">
						<div class="col-lg-6">
							<div class="form-group">
								<label for="" class="required">Mã danh mục</label> <input
									class="form-control" type="text" id="id_status"
									name="id_status" isInputAllowVarchar required>
							</div>
						</div>
						<div class="col-lg-6">
							<div class="form-group">
								<label for="" class="required">Màu danh mục</label>
								<div class="input-group">
									<input type="text" id="color" name="color"
										class="form-control my-colorpicker1"
										placeholder="Chọn màu danh mục" required>
									<div class="input-group-addon">
										<i></i>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-lg-6">
							<div class="form-group">
								<label for="" class="required">Tên danh mục</label> <input
									class="form-control" type="text" id="name_status"
									name="name_status" required>
							</div>
						</div>
						<div class="col-lg-6">
							<div class="form-group">
								<label for="" class="required">Thứ tự</label> <input
									class="form-control" type="text" id="order_number"
									name="order_number" onlynumber required>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-lg-12">
							<div class="form-group">
								<label for="" class="required">Loại danh mục</label> <select
									class="form-control select2" style="width: 100%;" id="group_type" name="group_type">
									<option></option>
									<c:forEach var="item" items="${listGroup}">
										<option value="${item.id}">${item.name_group}</option>
									</c:forEach>
								</select>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-lg-12">
							<div class="form-group">
								<label for="">Ghi chú</label> 
								<input class="form-control" type="text" id="note" name="note">
							</div>
						</div>
					</div>

				</form>

			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default pull-left"
					data-dismiss="modal">Đóng</button>
				<button type="button" class="btn btn-primary" id="btnSave">Lưu</button>
			</div>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>