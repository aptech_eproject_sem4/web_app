<%@ page contentType="text/html;charset=UTF-8" language="java"
	isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<table id="table" class="table table-bordered table-striped">
	<thead>
		<tr>
			<th width="45" class="text-center">STT</th>
			<th>Mã danh mục</th>
			<th>Tên danh mục</th>
			<th class="text-right">Thứ tự</th>
			<th>Ghi chú</th>
			<th class="text-center">Ẩn</th>
			<th>Thao tác</th>
		</tr>
	</thead>
	<tbody class="tblChiTiet">
		<c:forEach var="item" items="${listStatus}">
			<tr>
				<td class="stt" style="background-color:${item.color}"></td>
				<td>${item.id_status}</td>
				<td>${item.name_status}</td>
				<td class="text-right">${item.order_number}</td>
				<td><i>${item.note}</i></td>
				<td class="text-center"><c:choose>
						<c:when test="${item.active==false}">
							<input type="checkbox" class="checkActive" onclick="updateActive(this)" checked>
							<br />
						</c:when>
						<c:when test="${item.active==true}">
							<input type="checkbox" class="checkActive" onclick="updateActive(this)">
							<br />
						</c:when>
					</c:choose></td>
				<td>
					<button type="button" class="btn-edit" onclick="editModal('${item.id_status}','${item.name_status}','${item.group_type}','${item.order_number}','${item.color}','${item.note}')">
						<i class="fa fa-pencil"></i>
					</button>
					<button type="button" class="btn-del" onclick="Delete('${item.id_status}','/DanhMucTinhTrang/Delete',function(){search()})">
						<i class="fa fa-trash-o"></i>
					</button>
				</td>
			</tr>
		</c:forEach>
	</tbody>
	<tfoot>
		<tr>
			<th width="45" class="text-center">STT</th>
			<th>Mã danh mục</th>
			<th>Tên danh mục</th>
			<th class="text-right">Thứ tự</th>
			<th>Ghi chú</th>
			<th class="text-center">Ẩn</th>
			<th>Thao tác</th>
		</tr>
	</tfoot>
</table>