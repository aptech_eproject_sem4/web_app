<%@ page contentType="text/html;charset=UTF-8" language="java"
	isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ include file="../base/_headerMain.jspf"%>
<!-- Content Header (Page header) -->
<section class="content-header">
	<h1>Quản lý ngày nghỉ</h1>
	<div class="wrap-btn-header">
		<button type="button" class="btn bg-maroon" id="btnCreate">
			<i class="fa fa-plus"></i> Thêm
		</button>
	</div>
</section>

<!-- Main content -->
<section class="content">
	<!-- Main row -->
	<div class="row">

		<!--table-->
		<div class="col-lg-12">
			<div class="filter-table"></div>
			<div class="wrap-table" id="getData">
				<%@ include file="loadTable.jsp"%>
			</div>
		</div>
		<!--table-->
		<div></div>
	</div>

</section>
<!-- /.content -->
<%@ include file="modalAction.jsp"%>
<%@ include file="../base/_footer.jspf"%>
<script>
	var modal = $("#modal-ngayNghi");
	var checkDateoffStatus;
	$("#btnCreate").click(function() {
		$("#title-popup").html("Thêm ngày nghỉ");
		modal.find('input').val(null);
		$("#id").prop("disabled", false);
		modal.modal("show");

		var error = $("label.error");
		$(error).empty();
		$("input.form-control").removeClass("error");
	})
	$("#btnSave").click(
			function() {
				if (checkDateoffStatus == "reject") {
					ThongBao_Loi("Ngày nghỉ đã tồn tại!");
				} else if (checkDateoffStatus == "accept") {
					s_Save('/NgayNghi/addOrUpdate', $("#form-ngaynghi"),
							function(data) {
								if (data == "true") {
									ThongBao_ThanhCong("Lưu thành công");
									modal.modal("hide");
									search();

								} else {
									Thong_BaoLoi("Lưu thất bại");
									Thong_BaoLoi(data);//thông báo check trùng date off hiện lỗi return
								}
							}, null)
				} else {
					ThongBao_Loi("Có lỗi xảy ra, hãy thử lại!");
				}
			})
	function editModal(id, nameHoli, dateOff) {
		checkDateoffStatus = "accept";
		$("#title-popup").html("Sửa ngày nghỉ");
		modal.find("#id").val(id);
		modal.find("#name_holiday").val(nameHoli);
		modal.find("#date_off").val(dateOff);
		$("#id").prop("disabled", true);
		modal.modal("show");

		var error = $("label.error");
		$(error).empty();
		$("input.form-control").removeClass("error").addClass("valid");
	}
	// function editModal(obj){
	// 	debugger
	// 	var tr=$(obj).closest("tr");
	// 	var id=tr.find("#id").val();
	// 	modal.find("#id").val(id);
	// }
	function DeleteHoli(id) {
		Delete(id, '/NgayNghi/Delete', function() {
			modal.remove();
			search();
		})
	}
	function search() {
		var url = "/NgayNghi/Index?ajaxLoad=table";
		$("#getData").load(url, function() {
			/* $("#table").DataTable({
				ordering: false,
			}); */
			// console.log("đã load lại table");
			InitDataTable();
		})
	}
	$('#date_off').change(function() {
		var dateoff = $("#date_off").val();
		var res = dateoff.split("/");
		var day = res[0];
		var month = res[1];
		var year = res[2];
		var parseDateoff = day + "-" + month + "-" + year;
		var value = parseDateoff.toString();
		$.ajax({
			url : "/NgayNghi/checkDateoff",
			data : {
				"dateoff" : value
			},
			type : "post",
			success : function(data) {
				if (data == "existed") {
					ThongBao_Loi("Ngày nghỉ đã tồn tại!");
					checkDateoffStatus = "reject";
				} else if (data == "not exist") {
					ThongBao_ThanhCong("Ngày nghỉ chưa tồn tại!");
					checkDateoffStatus = "accept";
				} else {
					ThongBao_Loi("Có lỗi xảy ra!");
				}
			}
		});
	});
</script>