<%@ page contentType="text/html;charset=UTF-8" language="java"
	isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<table id="table" class="table table-bordered table-striped">
	<thead>
		<tr>
			<th width="45" class="text-center">STT</th>
			<th>Tên ngày nghỉ</th>
			<th>Ngày nghỉ</th>
			<th>Thao tác</th>
		</tr>
	</thead>
	<tbody class="tblChiTiet">
		<c:forEach var="item" items="${listHoliday}">
			<tr>
				<td class="text-center"></td>
				<td>${item.name_holiday}</td>
				<td>${item.date_off}</td>
				<td>
					<%-- <input type="hidden" id="id" value="1"/> --%>
					<button type="button" class="btn-edit"
						onclick="editModal('${item.id}','${item.name_holiday}','${item.date_off}')">
						<i class="fa fa-pencil"></i>
					</button> 
					<button id="btnDelete" type="button" class="btn-del"
						onclick="DeleteHoli('${item.id}')">
						<i class="fa fa-trash-o"></i>
					</button>
				</td>
			</tr>
		</c:forEach>
	</tbody>
	<tr>
		<th width="45" class="text-center">STT</th>
		<th>Tên ngày nghỉ</th>
		<th>Ngày nghỉ</th>
		<th>Thao tác</th>
	</tr>
</table>
