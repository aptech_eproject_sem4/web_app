<%@ page contentType="text/html;charset=UTF-8" language="java"
	isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!--modal default-->
      <div class="modal fade" id="modal-ngayNghi">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title" id="title-popup">Thêm ngày nghỉ</h4>
            </div>
            <div class="modal-body">
            <form id="form-ngaynghi" action="/NgayNghi/addOrUpdate" method="POST" enctype="multipart/form-data">
            <div class="row">
            <div class="col-lg-6">
                      <div class="form-group">
                        <label for="" class="required">Tên ngày nghỉ</label>
                        <input class="form-control" type="text" id="name_holiday" name="name_holiday" autocomplete="off" required>
                        <input type="hidden" id="id" name="id" >
                      </div>
                    </div>
                    <div class="col-lg-6">
                      <div class="form-group">
                        <label for="">Ngày nghỉ</label>
                        <div class="input-group">
                            <input type="text" class="form-control datepicker"
                                name="date_off" id="date_off" autocomplete="off" required/>
                            <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </div>
                        </div>
                      </div>
                    </div>
              </div>
            </form>
              
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Đóng</button>
              <button type="button" class="btn btn-primary" id="btnSave">Lưu</button>
            </div>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!--modal default-->