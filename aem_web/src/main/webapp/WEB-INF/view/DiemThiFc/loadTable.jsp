<%@ page contentType="text/html;charset=UTF-8" language="java"
	isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<table id="tableScoreFc" class="table table-bordered table-striped">
	<thead>
		<tr>
			<th width="45" class="text-center">STT</th>
			<th>FC</th>
			<th>Môn thi</th>
			<th>Điểm thi</th>
			<th>Thao tác</th>
		</tr>
	</thead>
	<tbody class="tblChiTiet">
		<c:forEach var="item" items="${listSFc}">
			<tr>
			<input type="hidden" value="${item.id }"/>
				<td class="text-right stt"></td>
				<c:forEach var="itemFC" items="${listFC}">
						<c:choose>
							<c:when test="${item.fc_id==itemFC.id_fc}">
								<td><span class="label label-primary">${itemFC.id_fc }</span><br/>
								${itemFC.name_fc }</td>
							</c:when>
						</c:choose>
					</c:forEach>
				<c:forEach var="itemSubject" items="${listSubject}">
						<c:choose>
							<c:when test="${item.subject_id==itemSubject.id_subject}">
								<td>${itemSubject.id_subject }<br/>
								${itemSubject.sort_name }</td>
							</c:when>
						</c:choose>
					</c:forEach>
				<td><i>Điểm thi: ${item.score_number}</i><br> <i> Điểm
						%: ${item.score_percent}</i></td>
				<td>
					<button type="button" class="btn-edit"
						onclick="editModal('${item.id}','${item.subject_id}','${item.fc_id}','${item.score_number}','${item.score_percent}','${item.date_create}')">
						<i class="fa fa-pencil"></i>
					</button>
					<button type="button" class="btn-del"
						onclick="Delete('${item.id}','/DiemThiFc/Delete',function(){search()})">
						<i class="fa fa-trash-o"></i>
					</button>
				</td>
			</tr>
		</c:forEach>
	</tbody>
	<tfoot>
		<tr>
			<th width="45" class="text-center">STT</th>
			<th>FC</th>
			<th>Môn thi</th>
			<th>Điểm thi</th>
			<th>Thao tác</th>
		</tr>
	</tfoot>
</table>
