<%@ page contentType="text/html;charset=UTF-8" language="java"
	isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ include file="../base/_headerMain.jspf"%>
<!-- Content Header (Page header) -->
<section class="content-header">
	<h1>Danh sách điểm thi FC</h1>
	<div class="wrap-btn-header">
		<button type="button" class="btn bg-maroon" id="btnCreate">
			<!-- data-toggle="modal" data-target="#modal-default" -->
			<i class="fa fa-plus"></i> Thêm
		</button>
	</div>
</section>

<!-- Main content -->
<section class="content">
	<!-- Main row -->
	<div class="row">

		<!--table-->
		<div class="col-lg-12">
			<div class="filter-table">
				<div class="row">
					<div class="col-lg-2">
						<div class="form-group">
							<select class="form-control select2" style="width: 100%;"
								id="listFC" name="listFC">
								<option selected="selected" value="0">Chọn giảng viên</option>
								<c:forEach var="itemFC" items="${listFC}">
									<option value="${itemFC.id_fc }">${itemFC.id_fc }-${itemFC.name_fc }</option>
								</c:forEach>
							</select>
						</div>
					</div>
					<div class="col-lg-2">
						<div class="form-group">
							<select class="form-control select2" style="width: 100%;"
								id="listSubject" name="listSubject">
								<option selected="selected" value="0">Chọn môn học</option>
								<c:forEach var="itemSubject" items="${listSubject}">
									<option value="${itemSubject.id_subject }">${itemSubject.sort_name }-${itemSubject.name_subject }</option>
								</c:forEach>
							</select>
						</div>
					</div>
					<div class="col-lg-2">
						<div class="form-group">
							<input class="form-control " type="text" id="qSearch"
								name="qSearch" placeholder="Tìm kiếm nhanh">
						</div>
					</div>
					<div class="col-lg-1">
						<button type="button" class="btn bg-maroon btnSearch" id="qSearchbtn" onclick="search()">
							<i class="fa fa-search"></i>
						</button>
					</div>
				</div>
			</div>
			<div class="wrap-table" id="getData">
				<%@ include file="loadTable.jsp"%>
			</div>

		</div>
		<!--table-->
		<div></div>
	</div>

</section>
<!-- /.content -->
<%@ include file="modalAction.jsp"%>
<%@ include file="../base/_footer.jspf"%>
<script>
	var modal=$("#modal-scoreFc");
	$("#btnCreate").click(function(){
		$("#title-popup").html("Thêm điểm môn cho giảng viên")
		var error = $("label.error");
		$(error).empty();
		$("input.form-control").removeClass("error");
		modal.find('input').val(null);

		modal.modal("show");
	});
	
	$("#btnSave").click(function(){
			s_Save('/DiemThiFc/addOrUpdate',$("#form-scoreFc"),function(data){
				if(data=="true"){
					ThongBao_ThanhCong("Lưu thành công");
					modal.modal("hide");
					search();
					
				}else{
					Thong_BaoLoi("Lưu thất bại");
					Thong_BaoLoi(data);//thông báo check trùng date off hiện lỗi return
				}			
			},null)
	});
	
	function editModal(id,subject_id,fc_id,score_percent,score_number,date_create){
		$("#title-popup").html("Sửa điểm môn cho giảng viên")
		
		modal.find("#id").val(id);
		modal.find("#subject_id").val(subject_id);
		modal.find("#fc_id").val(fc_id);
		modal.find("#score_percent").val(score_percent);
		modal.find("#score_number").val(score_number);
		modal.find("#date_create").val(date_create);

		var error = $("label.error");
		$(error).empty();
		$("input.form-control").removeClass("error").addClass("valid");
		var parentNode_subject_id = $("#subject_id").parent();
		var ulitem_subject_id = $(parentNode_subject_id).find(
				"span.select2-selection__rendered");
		var arr_option_subject_id = $("#subject_id option");
		for (i = 0; i < arr_option_subject_id.length; i++) {
			if ($(arr_option_subject_id[i]).val() == subject_id) {
				$(arr_option_subject_id[i]).prop("selected", true);
				$(ulitem_subject_id).prop("title", $(arr_option_subject_id[i]).html());
				$(ulitem_subject_id).html($(arr_option_subject_id[i]).html());
				break;
			}
		}
		var parentNode_fc_id = $("#fc_id").parent();
		var ulitem_fc_id = $(parentNode_fc_id).find(
				"span.select2-selection__rendered");
		var arr_option_fc_id = $("#fc_id option");
		for (i = 0; i < arr_option_fc_id.length; i++) {
			if ($(arr_option_fc_id[i]).val() == fc_id) {
				$(arr_option_fc_id[i]).prop("selected", true);
				$(ulitem_fc_id).prop("title", $(arr_option_fc_id[i]).html());
				$(ulitem_fc_id).html($(arr_option_fc_id[i]).html());
				break;
			}
		}
		modal.modal("show");
	}

	// function editModal(obj){
	// 	debugger
	// 	var tr=$(obj).closest("tr");
	// 	var id=tr.find("#id").val();
	// 	modal.find("#id").val(id);
	// }
	function InitDataTable(selectorTable){
    selectorTable=selectorTable??$("#tableScoreFc");
    var table=selectorTable.DataTable({
        ordering: false,
        "order": [[ 1, 'asc' ]]
    });
    table.on( 'order.dt search.dt', function () {
        table.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
            cell.innerHTML = i+1;
        } );
    } ).draw();
}
	function search(){
		var e1 = document.getElementById("listFC");
		var value1 = e1.options[e1.selectedIndex].value;
		var e2 = document.getElementById("listSubject");
		var value2 = e2.options[e2.selectedIndex].value;
		var qSearch=$("#qSearch").val().replace("/\ /g","+");
		var url='/DiemThiFc/Index?ajaxLoad=table&subject_id='+value2+'&fc_id='+value1+'&searchValue='+qSearch;
		$("#getData").load(url,function(){			
			InitDataTable();	
		})
	}


  </script>