<%@ page contentType="text/html;charset=UTF-8" language="java"
	isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!--modal default-->
<div class="modal fade" id="modal-scoreFc">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"
					aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h4 class="modal-title" id="title-popup">Thêm điểm môn cho
					giảng viên</h4>
			</div>
			<div class="modal-body">
				<form id="form-scoreFc" action="/DiemThiFc/addOrUpdate"
					method="POST" enctype="multipart/form-data">
					<div class="row">
						<div class="col-lg-12">
							<div class="row">
								<div class="col-lg-6">
									<div class="form-group">
										<label for="" class="required">Môn thi</label><br /> <select
											class="form-control select2" style="width: 100%;"
											id="subject_id" name="subject_id" required>
											<option></option>
											<c:forEach var="itemSubject" items="${listSubject}">
												<option value="${itemSubject.id_subject }">${itemSubject.sort_name }-${itemSubject.name_subject }</option>
											</c:forEach>				
										</select> <input type="hidden" id="id" name="id">
									</div>
								</div>
								<div class="col-lg-6">
									<div class="form-group">
										<label for="" class="required">Giảng viên</label><br /> <select
											class="form-control select2" style="width: 100%;" id="fc_id"
											name="fc_id" required>
											<option></option>
											<c:forEach var="itemFC" items="${listFC}">
												<option value="${itemFC.id_fc }">${itemFC.id_fc }-${itemFC.name_fc }</option>
											</c:forEach>
										</select>
									</div>
								</div>
							</div>

							<div class="row">
								<div class="col-lg-6">
									<div class="form-group">
										<label for="" class="required">Điểm số</label> <input
											class="form-control" type="text" id="score_number"
											name="score_number" onlynumber required>
									</div>
								</div>
								<div class="col-lg-6">
									<div class="form-group">
										<label for="" class="required">Điểm %</label> <input
											class="form-control" type="text" id="score_percent"
											name="score_percent" onlynumber required> <input
											type="hidden" id="date_create" name="date_create">
									</div>
								</div>
							</div>
						</div>
					</div>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default pull-left"
					data-dismiss="modal">Đóng</button>
				<button type="button" class="btn btn-primary" id="btnSave">Lưu</button>
			</div>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>
<!--modal default-->