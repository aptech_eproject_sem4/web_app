package c1808g1.aem_web;

import java.io.IOException;
import java.util.NoSuchElementException;
import java.util.concurrent.ExecutionException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import c1808g1.Models.HeThong.PermissionRoleDTO;
import c1808g1.aem_web.DAO.HeThong.MenuDAO;
import c1808g1.aem_web.DAO.HeThong.PermissionRoleDao;
import c1808g1.aem_web.common.danhMucDungChung;

@Component
public class MyCustomInterceptor implements HandlerInterceptor {

    // unimplemented methods comes here. Define the following method so that it
    // will handle the request before it is passed to the controller.

    @Override
    @SuppressWarnings("squid:S2696")
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
            throws IOException, ServletException, InterruptedException, ExecutionException,NoSuchElementException {
        System.out.println("\n-------- MyCustomInterceptor.preHandle --- ");
        var result=true;
        var url=request.getRequestURI().equals("/")?"/Home":request.getRequestURI();
        String[] arr=url.split("/");
        var controllerName=arr[1];
        var action="";
        var listController=MenuDAO.getAllController();
        var modelController=listController.stream().filter(p->p.getIdcontroller().equals(controllerName)).findFirst().orElse(null);
        if(modelController!=null && arr.length>2){
            action=arr[2].toLowerCase();
            if(action.equals("") || action.contains("index") || action.contains("form")){
                action="Index";
            }else if(action.contains("addorupdate")){
                action="Create";
            }else if(action.contains("delete")){
                action="Delete";
            }else if(action.contains("import") || action.contains("export")){
                action="Export";
            }
            System.out.println("\n"+action);
        }
        String userlogin=danhMucDungChung.extractCookie(request);
        String listRole="";
        if(userlogin.equals("")){
            if(!controllerName.equals("Home") && !controllerName.equals("login") && !controllerName.equals("SubmitLogin") && !controllerName.equals("LogOut")){
                request.getRequestDispatcher("/WEB-INF/view/Error/_errorExpired.jsp").forward(request, response);
                return false;
            }
        }else{
            var arruser=userlogin.split("\\|");
            listRole=arruser[5];
            danhMucDungChung.id_userlogin=arruser[0];
            danhMucDungChung.email_userlogin=arruser[1];
            danhMucDungChung.name_userlogin=arruser[2];
            danhMucDungChung.creator_login=arruser[3];
            danhMucDungChung.id_rolelogin=arruser[4];
            danhMucDungChung.list_rolelogin=listRole;
        }
        if( !controllerName.equals("Home") && (action.equals("Index") || action.equals("Create") || action.equals("Delete") || action.equals("Export"))){
            
            if(listRole.isEmpty()){
                result=false;
                request.getRequestDispatcher("/WEB-INF/view/Error/_error403.jsp").forward(request, response);
            }else{
                var check=0;
                var arrRole=listRole.split(",");
                for (String itemRole : arrRole) {
                    var listPer=PermissionRoleDao.getPermissionByRoleid(Integer.parseInt(itemRole));
                    PermissionRoleDTO checkPer=new PermissionRoleDTO();
                    if(listPer==null){
                        checkPer=null;
                    }else{
                        var actionCheck=action;
                        checkPer=listPer.stream().filter(p->p.getController_id().equals(controllerName) && p.getList_action().contains(actionCheck)).findFirst().orElse(null);                                
                    }
                    if(checkPer!=null){
                        check=1;
                    }
                }
                if(check==0){
                    result=false;
                    request.getRequestDispatcher("/WEB-INF/view/Error/_error403.jsp").forward(request, response);
                }
            }
        }
        return result;
    }
    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
 
        // This code will never be run.
        System.out.println("\n-------- MyCustomInterceptor.postHandle --- ");
    }
 
    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, //
            Object handler, Exception ex) throws Exception {
         
        // This code will never be run.
        System.out.println("\n-------- MyCustomInterceptor.afterCompletion --- ");
    }
}
    
