package c1808g1.aem_web;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import c1808g1.Models.CauHinh.TagDTO;
import c1808g1.aem_web.DAO.CauHinh.TagDAO;

@Controller
@RequestMapping("/Test/")
public class TestController {
    String pathView = "Test/";
    @RequestMapping({ "", "/Index" })
	public String index(Model model, @RequestParam(required = false) String ajaxLoad,
			@RequestParam(required = false) String key) {
		
        // List<TagDTO> listTag = new ArrayList<TagDTO>();
        // listTag = TagDAO.findByIdContaining(key);
        // model.addAttribute("listTag", listTag);
		if (ajaxLoad != null && ajaxLoad.equals("table")) {
			return pathView + "loadTable";
		}
		return pathView + "index";
	}
}
