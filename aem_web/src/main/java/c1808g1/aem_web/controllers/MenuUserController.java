package c1808g1.aem_web.controllers;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.ExecutionException;


import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.reflect.TypeToken;

import c1808g1.Models.CauHinh.StatusDTO;
import c1808g1.Models.HeThong.RoleDTO;
import c1808g1.Models.QuanLiHoSo.EmployeeDTO;
import c1808g1.Models.QuanLiHoSo.FCDTO;
import c1808g1.Models.QuanLiHoSo.GradeSalaryFcDTO;
import c1808g1.Models.QuanLiHoSo.HosoStudentDTO;
import c1808g1.Models.QuanLiHoSo.ScoreFCDTO;
import c1808g1.Models.QuanLiHoSo.StudentDTO;
import c1808g1.Models.QuanLyChuongTrinhHoc.SubjectDTO;
import c1808g1.aem_web.DAO.CauHinh.DanhMucTinhTrangDAO;
import c1808g1.aem_web.DAO.HeThong.QuyenDAO;
import c1808g1.aem_web.DAO.QuanLyChuongTrinhHoc.HocKyDao;
import c1808g1.aem_web.DAO.QuanLyChuongTrinhHoc.KhoaHocDao;
import c1808g1.aem_web.DAO.QuanLyChuongTrinhHoc.LopDao;
import c1808g1.aem_web.DAO.QuanLyChuongTrinhHoc.MonHocDao;
import c1808g1.aem_web.DAO.QuanLyHoSo.GiangVienDao;
import c1808g1.aem_web.DAO.QuanLyHoSo.GradeSalaryFcDao;
import c1808g1.aem_web.DAO.QuanLyHoSo.NhanVienDao;
import c1808g1.aem_web.DAO.QuanLyHoSo.SinhVienDao;
import c1808g1.aem_web.common.danhMucDungChung;

@Controller
@RequestMapping("/MenuUser/")
public class MenuUserController {
	String pathView = "MenuUser/";

	// Thong tin ca nhan Employee
	@RequestMapping(value = "/ThongTinCaNhanEmployee")
	public String indexEmployee(Model model, @RequestParam(required = false) String ajaxLoad,
			@RequestParam(required = false) Integer page)
			throws InterruptedException, ExecutionException, IOException {
		EmployeeDTO employee = new EmployeeDTO();
//		if (idEmp == null || idEmp.equals("")) {
		employee = NhanVienDao.getEmployeeById("test");
//		} else {
//			employee = NhanVienDao.getEmployeeById(idEmp);
//		}
		List<RoleDTO> listRole = QuyenDAO.getAllOrderbyASC();
		String[] arrOfStr = employee.getList_role().split(",");
		String role = "";
		for (String a : arrOfStr) {
			for (int i = 0; i < listRole.size(); i++) {
				if (a.equals(listRole.get(i).getId())) {
					role += listRole.get(i).getName_role() + ",";
				} else {

				}
			}
		}
		model.addAttribute("emp", employee);
		model.addAttribute("listRole", role);
		if (ajaxLoad != null && ajaxLoad.equals("table")) {
			return pathView + "userNV";
		}
		return pathView + "userNV";
	}

	// Thong tin ca nhan FC
	@RequestMapping(value = "/ThongTinCaNhanFC")
	public String indexFC(Model model, @RequestParam(required = false) String ajaxLoad,
			@RequestParam(required = false) Integer page) throws InterruptedException, ExecutionException, IOException {
		FCDTO fc = new FCDTO();
		fc = GiangVienDao.getFCById("test123456");
		List<RoleDTO> listRole = QuyenDAO.getAllOrderbyASC();
		String[] arrOfStr = fc.getList_role().split(",");
		String role = "";
		for (String a : arrOfStr) {
			for (int i = 0; i < listRole.size(); i++) {
				if (a.equals(listRole.get(i).getId())) {
					role += listRole.get(i).getName_role() + ",";
				} else {

				}
			}
		}
		List<FCDTO> fcs = GiangVienDao.getAllFC();
		List<ScoreFCDTO> sfc = GiangVienDao.getAllScoreFC();
		List<SubjectDTO> sbj = MonHocDao.getAllSubject();
		List<StatusDTO> listStatus = danhMucDungChung.TrangThaiFC();
		List<GradeSalaryFcDTO> lsgsf = GradeSalaryFcDao.getAllGradeSalaryFc();
		model.addAttribute("listStatus", listStatus);
		model.addAttribute("lsgsf", lsgsf);
		model.addAttribute("fcs", fcs);
		model.addAttribute("sbj", sbj);
		model.addAttribute("sfc", sfc);
		model.addAttribute("fc", fc);
		model.addAttribute("listRole", role);
		if (ajaxLoad != null && ajaxLoad.equals("table")) {
			return pathView + "userFC";
		}
		return pathView + "userFC";
	}

	// Thong tin ca nhan SV
	@RequestMapping(value = "/ThongTinCaNhanSV")
	public String indexSV(Model model, @RequestParam(required = false) String ajaxLoad,
			@RequestParam(required = false) Integer page)
			throws InterruptedException, ExecutionException, IOException{
		StudentDTO student = new StudentDTO();
		student = SinhVienDao.getStudentById("stuasd123");
		model.addAttribute("student", student);
		model.addAttribute("listCourse", KhoaHocDao.getAllCourse());
		model.addAttribute("listSemester", HocKyDao.getAllSemester());
		model.addAttribute("listSubject", MonHocDao.getAllSubject());
		model.addAttribute("listClass", LopDao.getAllClass());
		model.addAttribute("listStatus", DanhMucTinhTrangDAO.getAllStatus());

		if (student.getHo_so() == null || student.getHo_so().equals(null) || student.getHo_so().equals("")
				|| student.getHo_so().equals("[]")) {
			model.addAttribute("jsonHoso", null);
		} else {
			JsonArray jsonHoso=new JsonArray();
			jsonHoso=new Gson().fromJson(student.getHo_so(), JsonArray.class);
			Type hosoListType = new TypeToken<ArrayList<HosoStudentDTO>>(){}.getType();	 
			ArrayList<HosoStudentDTO> hosoArray = new Gson().fromJson(jsonHoso, hosoListType);
			model.addAttribute("jsonHoso", hosoArray);
		}

		if (ajaxLoad != null && ajaxLoad.equals("table")) {
			return pathView + "userSV";
		}
		return pathView + "userSV";
	}

}
