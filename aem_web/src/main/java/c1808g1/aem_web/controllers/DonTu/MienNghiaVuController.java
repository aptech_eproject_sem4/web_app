package c1808g1.aem_web.controllers.DonTu;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URLConnection;
import java.nio.file.Files;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletResponse;

import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFRun;
import org.apache.xmlbeans.XmlException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.exc.InvalidFormatException;
import com.google.gson.Gson;

import c1808g1.Models.CauHinh.StatusDTO;
import c1808g1.Models.DonTu.ExemptionMsDTO;
import c1808g1.Models.QuanLiHoSo.FCDTO;
import c1808g1.Models.QuanLiHoSo.GradeSalaryFcDTO;
import c1808g1.Models.QuanLiHoSo.ScoreFCDTO;
import c1808g1.Models.QuanLiHoSo.StudentDTO;
import c1808g1.Models.QuanLyChuongTrinhHoc.ClassDTO;
import c1808g1.Models.QuanLyChuongTrinhHoc.CourseDTO;
import c1808g1.Models.QuanLyChuongTrinhHoc.SubjectDTO;
import c1808g1.aem_web.DAO.DonTu.MienNghiaVuDAO;
import c1808g1.aem_web.DAO.QuanLyChuongTrinhHoc.KhoaHocDao;
import c1808g1.aem_web.DAO.QuanLyChuongTrinhHoc.LopDao;
import c1808g1.aem_web.DAO.QuanLyHoSo.SinhVienDao;
import c1808g1.aem_web.common.FileDinhKem;
import c1808g1.aem_web.common.danhMucDungChung;


@Controller
@RequestMapping("/MienNghiaVu")
public class MienNghiaVuController{
	String pathView = "MienNghiaVu/";
	private static final String SOURCE_FILE = FileDinhKem.GetPathContainFile() + FileDinhKem.PATHFILE_HETHONG
            + "mau_hoan_nghia_vu.docx";//file mẫu có chứa từ khóa replace
    private static final String NEW_FILE = FileDinhKem.GetPathContainFile() + FileDinhKem.PATHFILE_HETHONG
            + "phieu_hoan_nghia_vu.docx";//file trống
	// Index
	@RequestMapping({ "", "/Index" })
	public String index(Model model, @RequestParam(required = false) String ajaxLoad,
			@RequestParam(required = false) Integer page, @RequestParam(required = false) String searchValue) throws JsonParseException, JsonMappingException, InterruptedException, ExecutionException, IOException {
		// get list data
		List<ExemptionMsDTO> listExemptionMs = MienNghiaVuDAO.getAllExemptionMs();
		if(listExemptionMs!=null){
			if (danhMucDungChung.id_rolelogin.equals("1")) {
                listExemptionMs = listExemptionMs.stream().filter(p -> p.getStudentid().equals(danhMucDungChung.id_userlogin)).collect(Collectors.toList());
            }
			Collections.sort(listExemptionMs, Comparator.comparing(ExemptionMsDTO::getConfirmed));
		}
		List<StudentDTO> lss = SinhVienDao.getAllStudent();
		List<CourseDTO> lsco = KhoaHocDao.getAllCourse();
		List<ClassDTO> lsc = LopDao.getAllClass();
		model.addAttribute("lss", lss);
		model.addAttribute("lsco", lsco);
		model.addAttribute("lsc", lsc);
		model.addAttribute("listExemptionMs", listExemptionMs);
		if (ajaxLoad != null && ajaxLoad.equals("table")) {
			return pathView + "loadTable";
		}
		return pathView + "index";
	}
	
	@RequestMapping(value = "/formExMs/{id}", method = RequestMethod.GET)
	public String CreateForm(Model model, @PathVariable("id") String id) {

		try {				
			List<ExemptionMsDTO> lsexms = new ArrayList<ExemptionMsDTO>();
			if (!id.equals("0")) {
				// search
				ExemptionMsDTO exms = MienNghiaVuDAO.getExemptionMsById(id);
				if (exms == null)
					return "redirect:/MienghiaVu/formExMs";
				model.addAttribute("exms", exms);
				model.addAttribute("lsexms", lsexms);
				model.addAttribute("tensv", SinhVienDao.getStudentById(exms.getStudentid()).getFull_name());
				if (exms.getConfirmed().equals("0")) {// tạo mới
					model.addAttribute("check", "1");// chỉ hiển thị nút lưu và chờ duyệt
				}
				if (exms.getConfirmed().equals("1")) {// chờ duyệt
					if (danhMucDungChung.id_rolelogin.equals("1")) {
						model.addAttribute("check", "2");// không hiện nút
					} else {
						model.addAttribute("check", "3");// hiện nút tải
					}
				}
				if (exms.getConfirmed().equals("2")) {
					model.addAttribute("check", "4");// không hiện nút
				}
				java.util.Date utilExD = new SimpleDateFormat("yyyy-MM-dd").parse(exms.getExpirationdate());
				SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
				exms.setExpirationdate(String.valueOf(formatter.format(utilExD)));
			}else{
				var exms=new ExemptionMsDTO();
				exms.setId("0");
				if (danhMucDungChung.id_rolelogin.equals("1")) {
					exms.setStudentid(danhMucDungChung.name_userlogin);
					model.addAttribute("tensv", danhMucDungChung.name_userlogin);
				} else {
					exms.setStudentid("");
					model.addAttribute("tensv", "Chọn sinh viên");

				}
				model.addAttribute("check", "0");// chỉ hiển thị nút thêm mới và lưu
				model.addAttribute("exms", exms);
			}
			if (danhMucDungChung.id_rolelogin.equals("1")) {
				model.addAttribute("btn", "btnSV");
			} else {
				model.addAttribute("btn", "btnChonSV");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return this.pathView + "create";
	}

	@RequestMapping(value = "/addOrUpdate")
	public @ResponseBody String addOrUpdate(ExemptionMsDTO entity) throws ParseException {
		// check trùng thì return "Đã có trong bảng"
		// ok thì return true
		if (entity == null) {
			return "Error, please try again!";
		}
		try {
			entity.setConfirmed("0");
			ExemptionMsDTO check = null;
			ExemptionMsDTO result = null;
			java.util.Date utilExD = new SimpleDateFormat("dd/MM/yyyy").parse(entity.getExpirationdate());
			SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
			entity.setExpirationdate(String.valueOf(formatter.format(utilExD)));

			if (entity.getId().equals("0")) {
				Timestamp timestamp = new Timestamp(System.currentTimeMillis());
				entity.setDatecreate(String.valueOf(timestamp));
				entity.setCreator(danhMucDungChung.creator_login);
				result = MienNghiaVuDAO.create(entity);
				var list=MienNghiaVuDAO.getAllExemptionMs();
				var model=list.get(list.size() - 1);
				entity.setId(model.getId());
			}else{
				var model=MienNghiaVuDAO.getExemptionMsById(entity.getId());
                entity.setDatecreate(model.getDatecreate());
                entity.setCreator(model.getCreator());
				result = MienNghiaVuDAO.update(entity);
			}

		} catch (InterruptedException | ExecutionException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return e.getMessage();
		}
		return entity.getId();
	}



	@RequestMapping(value = "/Delete/{id}")
	public @ResponseBody String Delete(@PathVariable("id") String id) {
		try {
			if (id == null || id == "") {
				return "Not found id!";
			} else {
				ExemptionMsDTO result = MienNghiaVuDAO.delete(id);
			}
		} catch (InterruptedException | ExecutionException | IOException e) {
			e.printStackTrace();
			return e.getMessage();
		}
		return "true";
	}
	
	@RequestMapping(value = "/CapNhatTrangThaiPhieu", method = RequestMethod.POST)
	public @ResponseBody String capNhat(String maPhieu, String trangThai)
			throws JsonParseException, JsonMappingException, InterruptedException, ExecutionException, IOException {
		var model=MienNghiaVuDAO.getExemptionMsById(maPhieu);
		if(model!=null){
			model.setConfirmed(trangThai);
			if(trangThai.equals("2")){
				Timestamp timestamp = new Timestamp(System.currentTimeMillis());
				model.setDateconfirm(String.valueOf(timestamp));
				model.setCreatorconfirm(danhMucDungChung.creator_login);
			}            
		}
		MienNghiaVuDAO.update(model);
		return "true";
	}
	@RequestMapping(value = "/exportPhieu/{id}")
    public void export(@PathVariable("id") String id,HttpServletResponse response) throws InterruptedException, ExecutionException, IOException, XmlException, InvalidFormatException,
			ParseException {
        var model=MienNghiaVuDAO.getExemptionMsById(id);
        var modelsv=SinhVienDao.getStudentById(model.getStudentid());
		SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
		String ngayHetHan="";
		String ngaySinh="";
		String thangBatDau="";
		String namBatDau="";
		String thangKetThuc="";
		String namKetThuc="";
		String lop="";
		if(!((StudentDTO) modelsv).getDob().equals("")) {
			java.util.Date utilExD = new SimpleDateFormat("yyyy-MM-dd").parse(modelsv.getDob());
			ngaySinh = String.valueOf(formatter.format(utilExD));
		}
		if(!((ExemptionMsDTO) model).getExpirationdate().equals("")) {
			java.util.Date utilExD = new SimpleDateFormat("yyyy-MM-dd").parse(model.getExpirationdate());			
			ngayHetHan = String.valueOf(formatter.format(utilExD));
		}
		if(!model.getYearstart().equals("")){
			var arr=model.getYearstart().split("/");
			thangBatDau=arr[0];
			namBatDau=arr[1];
		}
		if(!model.getYearend().equals("")){
			var arr=model.getYearend().split("/");
			thangKetThuc=arr[0];
			namKetThuc=arr[1];
		}
		if(!modelsv.getCurrent_class().equals("")){
			var modelLop=LopDao.getClassById(modelsv.getCurrent_class());
			if(modelLop!=null){
				lop=modelLop.getName_class();
			}
		}
		Date today = new Date(); // Fri Jun 17 14:54:28 PDT 2016 
		Calendar cal = Calendar.getInstance(); 
		cal.setTime(today);
		int year = cal.get(Calendar.YEAR);
		int yearNext=year+1;
        try {
            File file = new File(SOURCE_FILE);
            FileInputStream fis = new FileInputStream(file.getAbsolutePath());
            XWPFDocument document = new XWPFDocument(fis);
            List<XWPFParagraph> xwpfParagraphList = document.getParagraphs();
            for (XWPFParagraph xwpfParagraph : xwpfParagraphList) {
                for (XWPFRun xwpfRun : xwpfParagraph.getRuns()) {
                    String docText = xwpfRun.getText(0);
                    if(docText!=null){
                         //replacement and setting position                    
                        docText = docText.replace("tk_NgaySinh", ngaySinh).replace("tk_HoTen", modelsv.getFull_name()).replace("tk_DiaChi", modelsv.getAddress())
						.replace("tk_SinhVienNamThu", model.getMonthstart()).replace("tk_MaSinhVien",model.getStudentid()).replace("tk_Lop", lop).replace("tk_HienTai",year +" - "+yearNext)
						.replace("tk_ThoiGianKhoaHoc", namBatDau + " - "+ namKetThuc).replace("tk_TNamBD",model.getYearstart()).replace("tk_TNamKT",model.getYearend()).replace("tk_NgayHetHan",ngayHetHan);
                        xwpfRun.setText(docText, 0);
                    }
                   
                }
            }
            try (FileOutputStream out = new FileOutputStream(NEW_FILE)) {
                document.write(out);
            }
            fis.close();                    
        } catch (Exception e) {
            e.printStackTrace();
        }
        //get and download
        File file = new File(NEW_FILE);
        FileInputStream in = new FileInputStream(NEW_FILE);
        response.setContentType(URLConnection.guessContentTypeFromStream(in));
        response.setContentLength(Files.readAllBytes(file.toPath()).length);
        response.setHeader("Content-Disposition","attachment; filename=\"" + "phieu_hoan_nghia_vu.docx" +"\"");
        
        FileCopyUtils.copy(in, response.getOutputStream());
        in.close();
    }
}
