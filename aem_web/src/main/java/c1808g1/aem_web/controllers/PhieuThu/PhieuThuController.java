package c1808g1.aem_web.controllers.PhieuThu;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.function.Predicate;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.servlet.http.HttpServletResponse;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFRun;
import org.apache.xmlbeans.XmlException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import c1808g1.Models.DonTu.FormPayDTO;
import c1808g1.Models.QuanLiHoSo.StudentDTO;
import c1808g1.Models.QuanLyChuongTrinhHoc.SubjectDTO;
import c1808g1.aem_web.DAO.CauHinh.DanhMucTinhTrangDAO;
import c1808g1.aem_web.DAO.CauHinh.TagDAO;
import c1808g1.aem_web.DAO.DonTu.HocLaiMonDAO;
import c1808g1.aem_web.DAO.QuanLyChuongTrinhHoc.HocKyDao;
import c1808g1.aem_web.DAO.QuanLyChuongTrinhHoc.KhoaHocDao;
import c1808g1.aem_web.DAO.QuanLyChuongTrinhHoc.LopDao;
import c1808g1.aem_web.DAO.QuanLyChuongTrinhHoc.MonHocDao;
import c1808g1.aem_web.DAO.QuanLyHoSo.SinhVienDao;
import c1808g1.aem_web.common.ChuyenTienThanhChu;
import c1808g1.aem_web.common.FileDinhKem;
import c1808g1.aem_web.common.danhMucDungChung;

@Controller
@RequestMapping("/PhieuThu")
public class PhieuThuController {
    String pathView = "PhieuThu/";
    private static final String SOURCE_FILE = FileDinhKem.GetPathContainFile() + FileDinhKem.PATHFILE_HETHONG
            + "mau_phieu_thu.docx";//file mẫu có chứa từ khóa replace
    private static final String NEW_FILE = FileDinhKem.GetPathContainFile() + FileDinhKem.PATHFILE_HETHONG
            + "phieu_thu.docx";//file trống

    @RequestMapping({ "", "/Index" })
    public String index(Model model, @RequestParam(required = false) String type_form,
            @RequestParam(required = false) String id_class, @RequestParam(required = false) String confirm,
            @RequestParam(required = false) Integer page, @RequestParam(required = false) String search,
            @RequestParam(required = false) String ajaxLoad)
            throws InterruptedException, ExecutionException, IOException {
        var list = HocLaiMonDAO.getAllFormPay();
        if (list != null) {
            if (danhMucDungChung.id_rolelogin.equals("1")) {
                list = list.stream().filter(p -> p.getStudent_id().equals(danhMucDungChung.id_userlogin)).collect(Collectors.toList());
            }
            if (type_form != null && !type_form.equals("")) {
                list = list.stream().filter(p -> p.getType_form().equals(type_form)).collect(Collectors.toList());
            }
            if (id_class != null && !id_class.equals("")) {
                var listNew = new ArrayList<FormPayDTO>();
                for (FormPayDTO item : list) {
                    var modelSV = SinhVienDao.getStudentById(item.getStudent_id()) == null ? new StudentDTO()
                            : SinhVienDao.getStudentById(item.getStudent_id());
                    if (modelSV != null && modelSV.getCurrent_class().equals(id_class)) {
                        listNew.add(item);
                    }
                }
                list = listNew;
            }
            if (confirm != null && !confirm.equals("")) {
                list = list.stream().filter(f->f.getConfirmed().equals(confirm.toString())).collect(Collectors.toList());
            }
            if (search != null && !search.equals("")) {
                list = list.stream().filter(p -> p.getCreator().contains(search)
                || p.getCreator_confirm().contains(search) || p.getNote().contains(search)
                || p.getStudent_id().contains(search)).collect(Collectors.toList());
            }
            Collections.sort(list, Comparator.comparing(FormPayDTO::getConfirmed));
        }

        model.addAttribute("list", list);
        model.addAttribute("listLop", LopDao.getAllClass());
        model.addAttribute("listKhoa", KhoaHocDao.getAllCourse());
        model.addAttribute("listSV", SinhVienDao.getAllStudent());
        model.addAttribute("listLoai", danhMucDungChung.LoaiPhieuThu());
        if (ajaxLoad != null && ajaxLoad.equals("table")) {
            return pathView + "loadTable";
        }
        return pathView + "index";
    }

    @RequestMapping(value = "/FormPhieu/{id}")
    public String form(Model model, @PathVariable("id") String maPhieu)
            throws InterruptedException, ExecutionException, IOException {
        FormPayDTO modelForm = new FormPayDTO();
        if (maPhieu.equals("0")) {
            modelForm.setId("0");
            if (danhMucDungChung.id_rolelogin.equals("1")) {
                modelForm.setStudent_id(danhMucDungChung.name_userlogin);
                model.addAttribute("tensv", danhMucDungChung.name_userlogin);
            } else {
                modelForm.setStudent_id("");
                model.addAttribute("tensv", "Chọn sinh viên");

            }
            model.addAttribute("check", "0");// chỉ hiển thị nút thêm mới và lưu
        } else {
            modelForm = HocLaiMonDAO.getFormPayById(maPhieu);
            model.addAttribute("tensv", SinhVienDao.getStudentById(modelForm.getStudent_id()).getFull_name());
            if (modelForm.getConfirmed().equals("0")) {// tạo mới
                model.addAttribute("check", "1");// chỉ hiển thị nút lưu và chờ duyệt
            }
            if (modelForm.getConfirmed().equals("1")) {// chờ duyệt
                if (danhMucDungChung.id_rolelogin.equals("1")) {
                    model.addAttribute("check", "2");// không hiện nút
                } else {
                    model.addAttribute("check", "3");// hiện nút tải
                }
            }
            if (modelForm.getConfirmed().equals("2")) {
                model.addAttribute("check", "4");// không hiện nút
            }
            modelForm.setMoney(modelForm.getMoney().replace(".0", ""));
        }
        if (danhMucDungChung.id_rolelogin.equals("1")) {
            model.addAttribute("btn", "btnSV");
        } else {
            model.addAttribute("btn", "btnChonSV");
        }
        model.addAttribute("formpay", modelForm);
        model.addAttribute("listLoai", danhMucDungChung.LoaiPhieuThu());
        model.addAttribute("listLop", LopDao.getAllClass());
        model.addAttribute("listMon", MonHocDao.getAllSubject());
        return pathView + "formPhieu";
    }
    @RequestMapping(value = "/addOrUpdate")
	public @ResponseBody String addOrUpdate(FormPayDTO entity) {
		// check trùng thì return "Đã có trong bảng"
		// ok thì return true
		if (entity == null) {
			return "Error, please try again!";
		}
		try {
            entity.setConfirmed("0");
			if (entity.getId().equals("0")) {
				Timestamp timestamp = new Timestamp(System.currentTimeMillis());
				entity.setDate_create(String.valueOf(timestamp));
                entity.setCreator(danhMucDungChung.creator_login);
                HocLaiMonDAO.create(entity);                
                var list=HocLaiMonDAO.getAllFormPay();
                var model=list.get(list.size() - 1);
                entity.setId(model.getId());
			} else {
                var model=HocLaiMonDAO.getFormPayById(entity.getId());
                entity.setDate_create(model.getDate_create());
                entity.setCreator(model.getCreator());
				HocLaiMonDAO.update(entity);
			}

		} catch (InterruptedException | ExecutionException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return e.getMessage();
		}
		return entity.getId();
	}
    @RequestMapping(value = "/Delete/{id}")
    public @ResponseBody String delete(@PathVariable("id") String id) {
        try {
            if (id == null || id == "") {
                return "Not found id!";
            } else {
                FormPayDTO result = HocLaiMonDAO.delete(id);
            }
        } catch (InterruptedException | ExecutionException | IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return e.getMessage();
        }
        return "true";
    }

    @RequestMapping(value = "/LoadSinhVien")
    public String loadsv(Model model) throws InterruptedException, ExecutionException, IOException {
        model.addAttribute("list", SinhVienDao.getAllStudent());
        model.addAttribute("listLop", LopDao.getAllClass());
        model.addAttribute("listKhoa", KhoaHocDao.getAllCourse());
        return pathView + "loadTableChonSinhVien";
    }
    @RequestMapping(value = "/getListNoMon/{id}")
    public String listSVNoMon(Model model,@PathVariable("id") String id)
            throws JsonParseException, JsonMappingException, InterruptedException, ExecutionException, IOException {
        model.addAttribute("lsHocKi", HocKyDao.getAllSemeByOrderNumber());
        List<SubjectDTO> lsSubject=new ArrayList<SubjectDTO>();
        List<FormPayDTO> list=HocLaiMonDAO.getAllFormPay();
        if(list!=null){
            list=list.stream().filter(f->f.getStudent_id().equals(id) && f.getType_form().equals("ThiLai") && f.getConfirmed().equals("2") && !f.getSubject_id().equals("")).collect(Collectors.toList());
            Collections.sort(list, Comparator.comparing(FormPayDTO::getConfirmed).thenComparing(FormPayDTO::getDate_confirm));
            if(list!=null){
                for (FormPayDTO item : list) {
                    var modelSubject=MonHocDao.getSubjectById(item.getSubject_id());
                    if(modelSubject!=null){
                        lsSubject.add(modelSubject);
                    }
                }
            }
        }
        model.addAttribute("list", lsSubject);
        return pathView+"getListSVNoMon";
    }
    @RequestMapping(value = "/CheckTongLanThi", method = RequestMethod.POST)
    public @ResponseBody String checkSolanthi(String maSinhVien, String monThi)
            throws JsonParseException, JsonMappingException, InterruptedException, ExecutionException, IOException {
		var list=HocLaiMonDAO.getAllFormPay();
        if(list!=null){
            Predicate<FormPayDTO> byConfirm = p -> p.getStudent_id().equals(maSinhVien) && p.getSubject_id().equals(monThi) && p.getType_form().equals("ThiLai") && p.getConfirmed().equals("3");
            list=list.stream().filter(byConfirm).collect(Collectors.toList());
            return list.size()+"";
        }
		return "0";
	}
    @RequestMapping(value = "/CapNhatTrangThaiPhieu", method = RequestMethod.POST)
    public @ResponseBody String capNhat(String maPhieu, String trangThai)
            throws JsonParseException, JsonMappingException, InterruptedException, ExecutionException, IOException {
		var model=HocLaiMonDAO.getFormPayById(maPhieu);
        if(model!=null){
            model.setConfirmed(trangThai);
            if(trangThai.equals("2")){
                Timestamp timestamp = new Timestamp(System.currentTimeMillis());
                model.setDate_confirm(String.valueOf(timestamp));
                model.setCreator_confirm(danhMucDungChung.creator_login);
            }            
        }
        HocLaiMonDAO.update(model);
        return "true";
	}
    @RequestMapping(value = "/getMoneyLoaiPhieu", method = RequestMethod.POST)
    public @ResponseBody String capNhat(String maStt)
            throws JsonParseException, JsonMappingException, InterruptedException, ExecutionException, IOException {
		var model=DanhMucTinhTrangDAO.getStatusById(maStt);
        if(!model.getNote().equals("") && model.getNote()!=null){
            if(ChuyenTienThanhChu.isNumeric(model.getNote())){
                return model.getNote();
            }
        }
        return "0";
	}
    @RequestMapping(value = "/GetListTag")
    public String listtag(Model model, @RequestParam(required = false) String maTag)
            throws JsonParseException, JsonMappingException, InterruptedException, ExecutionException, IOException {		
        if(maTag==null){
            model.addAttribute("listTag", TagDAO.getAllTag());
        }else{
            maTag=maTag.replace("@", "");
            model.addAttribute("listTag", TagDAO.findByIdContaining(maTag));
        }
        return pathView+"listTagNote";
	}
    @RequestMapping(value = "/exportPhieu/{id}")
    public void export(@PathVariable("id") String id,HttpServletResponse response) throws InterruptedException, ExecutionException, IOException, XmlException, InvalidFormatException {
        var model=HocLaiMonDAO.getFormPayById(id);
        var modelsv=SinhVienDao.getStudentById(model.getStudent_id());
        DecimalFormat formatter = new DecimalFormat("###,###,###"); 
        var moneySo=Integer.parseInt(model.getMoney().replace(".0", ""));
        String money = formatter.format(moneySo);
        String tienChu=ChuyenTienThanhChu.ChuyenSangChu(money);
        try {
            File file = new File(SOURCE_FILE);
            FileInputStream fis = new FileInputStream(file.getAbsolutePath());
            XWPFDocument document = new XWPFDocument(fis);
            List<XWPFParagraph> xwpfParagraphList = document.getParagraphs();
            for (XWPFParagraph xwpfParagraph : xwpfParagraphList) {
                for (XWPFRun xwpfRun : xwpfParagraph.getRuns()) {
                    String docText = xwpfRun.getText(0);
                    if(docText!=null){
                         //replacement and setting position                    
                        docText = docText.replace("tk_LyDo", model.getNote()).replace("tk_HoTen", modelsv.getFull_name()).replace("tk_SoTien", money + "VND").replace("tk_TienBangChu", tienChu);
                        xwpfRun.setText(docText, 0);
                    }
                   
                }
            }
            try (FileOutputStream out = new FileOutputStream(NEW_FILE)) {
                document.write(out);
            }
            fis.close();                    
        } catch (Exception e) {
            e.printStackTrace();
        }
        //get and download
        File file = new File(NEW_FILE);
        FileInputStream in = new FileInputStream(NEW_FILE);
        response.setContentType(URLConnection.guessContentTypeFromStream(in));
        response.setContentLength(Files.readAllBytes(file.toPath()).length);
        response.setHeader("Content-Disposition","attachment; filename=\"" + "phieu_thu.docx" +"\"");
        
        FileCopyUtils.copy(in, response.getOutputStream());
        in.close();
    }
}
