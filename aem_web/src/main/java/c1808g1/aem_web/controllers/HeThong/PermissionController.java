package c1808g1.aem_web.controllers.HeThong;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.ExecutionException;

import javax.servlet.http.HttpServletRequest;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import c1808g1.Models.HeThong.ControllerDTO;
import c1808g1.Models.HeThong.PermissionRoleDTO;
import c1808g1.Models.HeThong.PermissionRoleViewIndexDTO;
import c1808g1.Models.HeThong.RoleDTO;
import c1808g1.Models.HeThong.SubMenuDTO;
import c1808g1.aem_web.DAO.HeThong.MenuDAO;
import c1808g1.aem_web.DAO.HeThong.PermissionRoleDao;
import c1808g1.aem_web.DAO.HeThong.QuyenDAO;
import c1808g1.aem_web.common.JSONUtils;
import c1808g1.aem_web.common.danhMucDungChung;
import c1808g1.aem_web.common.danhMucDungChung.MenuParent;

@Controller
@RequestMapping("/NhomQuyen")
public class PermissionController {
    String pathView = "NhomQuyen/";

    @RequestMapping({ "", "/Index" })
    public String index(Model model, @RequestParam(required = false) Integer idRole,
            @RequestParam(required = false) String ajaxLoad)
            throws JsonParseException, JsonMappingException, InterruptedException, ExecutionException, IOException {
        List<RoleDTO> listR = new ArrayList<RoleDTO>();
		  try {
			  listR = QuyenDAO.getAllRole();
		  } catch (InterruptedException | ExecutionException | IOException e) {
				// TODO Auto-generated catch block
				((Throwable) e).printStackTrace();
		  }
          model.addAttribute("listRole", listR);          
		  if (ajaxLoad != null && ajaxLoad.equals("table")) {
            List<PermissionRoleViewIndexDTO> listPermission = new ArrayList<PermissionRoleViewIndexDTO>();
            MenuParent[] lsParent=danhMucDungChung.GetListMenuParent();
            List<PermissionRoleDTO> listPer=PermissionRoleDao.getPermissionByRoleid(idRole);

            for (MenuParent itemPar : lsParent) {
              PermissionRoleViewIndexDTO modelItemPar=new PermissionRoleViewIndexDTO();
              modelItemPar.setMenu_root(itemPar.name());
              modelItemPar.setMenu_name(itemPar.getName());
              List<SubMenuDTO> lsSub=new ArrayList<SubMenuDTO>();
              List<ControllerDTO> listController= MenuDAO.getControllerByMenuRoot(itemPar.name());
              for (ControllerDTO itemCon : listController) {
                  if(itemCon.getActive().equals("true")){
                    PermissionRoleDTO checkPer=new PermissionRoleDTO();
                    if(listPer==null){
                        checkPer=null;
                    }else{
                        checkPer=listPer.stream().filter(p->p.getController_id().equals(itemCon.getIdcontroller())).findFirst().orElse(null);
                    }
                    if(checkPer!=null){
                        SubMenuDTO modelItemPer=new SubMenuDTO();
                        modelItemPer.setId_per(checkPer.getId().equals("")?"0":checkPer.getId());
                        modelItemPer.setId_controller(itemCon.getIdcontroller());
                        modelItemPer.setName_controller(itemCon.getNamecontroller());
                        modelItemPer.setRole_id(idRole.toString());
                        modelItemPer.setList_action(checkPer.getList_action());
                        modelItemPer.setPer_index(checkPer.getList_action().contains("Index")?"1":"0");
                        modelItemPer.setPer_create(checkPer.getList_action().contains("Create")?"1":"0");
                        modelItemPer.setPer_delete(checkPer.getList_action().contains("Delete")?"1":"0");
                        modelItemPer.setPer_export(checkPer.getList_action().contains("Export")?"1":"0");
                        lsSub.add(modelItemPer);
                    }else{
                        SubMenuDTO modelItemPer=new SubMenuDTO();
                        modelItemPer.setId_per("0");
                        modelItemPer.setId_controller(itemCon.getIdcontroller());
                        modelItemPer.setName_controller(itemCon.getNamecontroller());
                        modelItemPer.setRole_id(idRole.toString());
                        modelItemPer.setList_action("");
                        modelItemPer.setPer_index("0");
                        modelItemPer.setPer_create("0");
                        modelItemPer.setPer_delete("0");
                        modelItemPer.setPer_export("0");
                        lsSub.add(modelItemPer);
                      }
                      
                  }
              }
              modelItemPar.setList_submenu(lsSub);
              listPermission.add(modelItemPar);
            }
            model.addAttribute("listPer", listPermission);
			return pathView + "loadTable";
		  }
	    return pathView + "index";

    }
    @RequestMapping(value = "/addOrUpdate", method = RequestMethod.POST)
	public @ResponseBody String addOrUpdate(HttpServletRequest request, String idPer,int idRole,String idController,String listAction)
            throws InterruptedException, ExecutionException, IOException {
        PermissionRoleDTO model=new PermissionRoleDTO();
        model.setId(idPer);
        model.setController_id(idController);
        model.setList_action(listAction);
        model.setRole_id(String.valueOf(idRole));
        if(idPer.equals("0")){
            PermissionRoleDao.create(model);
        }else{
            PermissionRoleDao.update(model);
        }
        List<PermissionRoleDTO> listPer=PermissionRoleDao.getPermissionByRoleid(idRole);
        PermissionRoleDTO checkPer=listPer.stream().filter(p->p.getController_id().equals(idController)).findFirst().orElse(null);
        return checkPer.getId();
    }
}
