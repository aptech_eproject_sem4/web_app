package c1808g1.aem_web.controllers.HeThong;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import c1808g1.Models.HeThong.RoleDTO;
import c1808g1.Models.HeThong.PermissionRoleDTO;
import c1808g1.aem_web.DAO.HeThong.QuyenDAO;
import c1808g1.aem_web.DAO.HeThong.PermissionRoleDao;

@Controller
@RequestMapping("/Quyen/")
public class QuyenController {
	String pathView = "Quyen/";
	
	  // Index
	  @RequestMapping({"","/Index"})
	  public String index(Model model, @RequestParam(required = false) String ajaxLoad,
	      @RequestParam(required = false) Integer page) {
	        //get list data
		  List<RoleDTO> listR = new ArrayList<RoleDTO>();
		  try {
			  listR = QuyenDAO.getAllRole();
		  } catch (InterruptedException | ExecutionException | IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
		  }
		  model.addAttribute("listR", listR);
		  if (ajaxLoad != null && ajaxLoad.equals("table")) {
				return pathView + "loadTable";
		  }
	  return pathView + "index";
	  }
	  
	  @RequestMapping(value = "/addOrUpdate")
	  public @ResponseBody String addOrUpdate(RoleDTO entity){  
		// check trùng thì return "Đã có trong bảng"
		// ok thì return true
//		    if (entity == null) {
//				return "Error, please try again!";
//			}
//		    try {
//		    	RoleDTO check=null;
//				check=QuyenDAO.getRoleById(entity.getId());
//				RoleDTO result=null;			
//				if (check==null) {
//					result = QuyenDAO.create(entity);
//				} else {
//					result = QuyenDAO.update(entity);
//				}
//			} catch (InterruptedException | ExecutionException | IOException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//				return e.getMessage();
//			}
//	  return "true";
		  if(entity==null){
		  		return "error";
		  	}
		  	RoleDTO check = null;
		  	RoleDTO result = null;
		  	String val=entity.getId();
			try {								
					check=QuyenDAO.getRoleById(entity.getId());
				if (check==null) {
					entity.setAllow_edit("true");
					entity.setAllow_delete("true");
					result = QuyenDAO.create(entity);
				} else {
					entity.setAllow_edit(check.getAllow_edit());
					entity.setAllow_delete(check.getAllow_delete());
					result = QuyenDAO.update(entity);
				}
			} catch (InterruptedException | ExecutionException | IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	  return "true";
	  }
	  
	  @RequestMapping(value="/Delete/{id}")
	  public @ResponseBody String delete(@PathVariable("id") String id){
			 try {
				 RoleDTO delete = QuyenDAO.delete(id);
				 List<PermissionRoleDTO> deletePR=PermissionRoleDao.deletePermissionRoleByRoleid(Integer.valueOf(id));
			  	} catch (InterruptedException | ExecutionException | IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
			  	}
	  return "true";
	  }
	}

