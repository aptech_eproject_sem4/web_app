package c1808g1.aem_web.controllers.HeThong;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import c1808g1.Models.HeThong.ControllerDTO;
import c1808g1.Models.HeThong.PermissionRoleDTO;
import c1808g1.aem_web.DAO.HeThong.MenuDAO;
import c1808g1.aem_web.DAO.HeThong.PermissionRoleDao;
import c1808g1.aem_web.common.danhMucDungChung;
import c1808g1.aem_web.common.danhMucDungChung.MenuParent;

@Controller
@RequestMapping("/Menu/")
public class MenuController {
	String pathView = "Menu/";
	
	// Index
	@RequestMapping({ "", "/Index" })
	public String index(Model model, @RequestParam(required = false) String ajaxLoad,
			@RequestParam(required = false) Integer page, @RequestParam(required = false) String idParent) {
		// get list data
		List<ControllerDTO> listController = new ArrayList<ControllerDTO>();
		Map<String,String> listParent=new HashMap<String,String>();		
		for(MenuParent p:MenuParent.values()) {
			listParent.put(p.name(), p.getName());
		}
		try {
			if (idParent == null || idParent == "" ||idParent.equals("0")) {
				listController = MenuDAO.getControllerByMenuRoot("QLCTHOC");
			} else {
				listController = MenuDAO.getControllerByMenuRoot(idParent);
			}
			// listController=MenuDAO.getAllController();
		} catch (InterruptedException | ExecutionException | IOException e) {
			e.printStackTrace();
		}
		
		model.addAttribute("listController", listController);
		model.addAttribute("listParent", listParent);
		if (ajaxLoad != null && ajaxLoad.equals("table")) {
			return pathView + "loadTable";
		}
		return pathView + "index";
	}

	@RequestMapping(value = "/addOrUpdate")
	public @ResponseBody String addOrUpdate(ControllerDTO entity) {
		// check trùng thì return "Đã có trong bảng"
		// ok thì return true
		if (entity == null) {
			return "Error, please try again!";
		}
		try {
			ControllerDTO check = null;
			check = MenuDAO.getControllerById(entity.getIdcontroller());
			ControllerDTO result = null;
			if (check == null) {
				entity.setActive("true");
				result = MenuDAO.create(entity);
			} else {
				entity.setActive(check.getActive());
				result = MenuDAO.update(entity);
			}

		} catch (InterruptedException | ExecutionException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return e.getMessage();
		}
		return "true";
	}

	@RequestMapping(value = "/updateActive", method = RequestMethod.POST)
	public @ResponseBody String updateActive(String idParent) {
		// check trùng thì return "Đã có trong bảng"
		// ok thì return true
		String ajaxResponse = "";
		try {
			// ajaxResponse = mapper.writeValueAsString(person);
			ControllerDTO check = MenuDAO.getControllerById(idParent);
			ControllerDTO result = null;
			if (check == null) {
				ajaxResponse = "Danh mục tình trạng không tồn tại";
			} else {
				if (check.getActive().equals("true")) {
					check.setActive("false");
					result = MenuDAO.update(check);
					ajaxResponse = "Đã ẩn!";
				} else if (check.getActive().equals("false")) {
					check.setActive("true");
					result = MenuDAO.update(check);
					ajaxResponse = "Đã hủy ẩn!";
				} else {
					ajaxResponse = "Có lỗi xảy ra!";
				}
			}
		} catch (InterruptedException | ExecutionException | IOException e) {
			e.printStackTrace();
		}
		return ajaxResponse;
	}

	@RequestMapping(value = "/Delete/{id}")
	public @ResponseBody String delete(@PathVariable("id") String id) {
		try {
			if (id == null || id == "") {
				return "Not found id!";
			} else {
				ControllerDTO result = MenuDAO.delete(id);
				List<PermissionRoleDTO> deletePR=PermissionRoleDao.deletePermissionRoleByControllerid(id);
			}
		} catch (InterruptedException | ExecutionException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return e.getMessage();
		}
		return "true";
	}

	@RequestMapping(value = "/checkId", method = RequestMethod.POST)
	public @ResponseBody String searchPerson(HttpServletRequest request, String idParent) {
		// ObjectMapper mapper = new ObjectMapper();
		String ajaxResponse = "";
		try {
			// ajaxResponse = mapper.writeValueAsString(person);
			ControllerDTO check = MenuDAO.getControllerById(idParent);
			if (check == null) {
				ajaxResponse = "not exist";
			} else {
				ajaxResponse = "existed";
			}
		} catch (InterruptedException | ExecutionException | IOException e) {
			e.printStackTrace();
		}
		return ajaxResponse;
	}
}
