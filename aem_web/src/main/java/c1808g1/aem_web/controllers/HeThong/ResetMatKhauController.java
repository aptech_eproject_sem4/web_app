package c1808g1.aem_web.controllers.HeThong;
import java.io.IOException;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.concurrent.ExecutionException;

import javax.servlet.http.HttpServletRequest;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.client.HttpStatusCodeException;

import c1808g1.Models.HeThong.ControllerDTO;
import c1808g1.Models.QuanLiHoSo.EmployeeDTO;
import c1808g1.Models.QuanLiHoSo.FCDTO;
import c1808g1.Models.QuanLiHoSo.StudentDTO;
import c1808g1.aem_web.DAO.HeThong.MenuDAO;
import c1808g1.aem_web.DAO.QuanLyHoSo.GiangVienDao;
import c1808g1.aem_web.DAO.QuanLyHoSo.NhanVienDao;
import c1808g1.aem_web.DAO.QuanLyHoSo.SinhVienDao;
import c1808g1.aem_web.common.MD5;


@Controller
@RequestMapping("/ResetMatKhau/")
public class ResetMatKhauController {
String pathView = "ResetMatKhau/";
	
	// Index
	@RequestMapping({ "", "/Index" })
	public String index(Model model, @RequestParam(required = false) String ajaxLoad,
			@RequestParam(required = false) Integer page, @RequestParam(required = false) String idParent) {
		return pathView + "index";
	}
	
	@RequestMapping(value = "/resetPassword", method = RequestMethod.POST)
	public @ResponseBody String resetPassword(HttpServletRequest request, String id_role,String email) throws InterruptedException, ExecutionException, IOException {
		// ObjectMapper mapper = new ObjectMapper();
		String ajaxResponse = "";
		
			if(id_role.equals("1")) {
				StudentDTO checkExistStudent=SinhVienDao.getStudentByEmail(email);
				if(checkExistStudent==null) {
					ajaxResponse="not found email";
				}else {
					checkExistStudent.setPassword(MD5.GenerateMD5("aptech123"));
					SinhVienDao.updateStudent(checkExistStudent);
					ajaxResponse="reset success";
				}
			}else if(id_role.equals("2")){
				FCDTO checkExistFC=GiangVienDao.getFCByEmail(email);
				if(checkExistFC==null) {
					ajaxResponse="not found email";
					
				}else {
					checkExistFC.setPassword(MD5.GenerateMD5("aptech123"));
					GiangVienDao.updateFC(checkExistFC);
					ajaxResponse="reset success";
					
				}
			}else if(id_role.equals("3")){
				EmployeeDTO checkExistEmployee=NhanVienDao.getEmployeeByEmail(email);
				if(checkExistEmployee==null) {
					ajaxResponse="not found email";
					
				}else {
					checkExistEmployee.setPassword(MD5.GenerateMD5("aptech123"));
					NhanVienDao.update(checkExistEmployee);
					ajaxResponse="reset success";
					
				}
			}else {
				ajaxResponse="choose role";
				
			}

		return ajaxResponse;
	}
	
	@RequestMapping(value = "/resetMacMobile", method = RequestMethod.POST)
	public @ResponseBody String resetMacMobile(HttpServletRequest request, String email) throws InterruptedException, ExecutionException, IOException {
		// ObjectMapper mapper = new ObjectMapper();
		String ajaxResponse = "";
		
			StudentDTO checkExistStudent=SinhVienDao.getStudentByEmail(email);
			if(checkExistStudent==null) {
				ajaxResponse="not found email";
				
			}else {
				checkExistStudent.setMobile_mac(null);
				SinhVienDao.updateStudent(checkExistStudent);
				ajaxResponse="reset success";

			}

		return ajaxResponse;
	}
}
