package c1808g1.aem_web.controllers.QuanLyLichHoc;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.ExecutionException;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import c1808g1.Models.CauHinh.ShiftStudyDTO;
import c1808g1.Models.QuanLyLichHoc.AttendanceDTO;
import c1808g1.Models.QuanLyLichHoc.AttendanceStudentDTO;
import c1808g1.aem_web.DAO.CauHinh.DanhMucTinhTrangDAO;
import c1808g1.aem_web.DAO.CauHinh.PhienHocDAO;
import c1808g1.aem_web.DAO.QuanLyChuongTrinhHoc.LopDao;
import c1808g1.aem_web.DAO.QuanLyChuongTrinhHoc.MonHocDao;
import c1808g1.aem_web.DAO.QuanLyHoSo.SinhVienDao;
import c1808g1.aem_web.DAO.QuanLyLichHoc.DiemDanhDao;
import c1808g1.aem_web.DAO.QuanLyLichHoc.LichHocDao;
import c1808g1.aem_web.common.danhMucDungChung;

@Controller
@RequestMapping("/DiemDanh")
public class DiemDanhController {
  String pathView = "DiemDanh/";

  @RequestMapping({ "", "/Index" })
  public String index(Model model, @RequestParam(required = false) String maLop,
      @RequestParam(required = false) String chiNhanh, @RequestParam(required = false) String maCa,
      @RequestParam(required = false) String maLoai, @RequestParam(required = false) String ajaxLoad)
      throws JsonParseException, JsonMappingException, InterruptedException, ExecutionException, IOException,
      ParseException {
        var listAttent=DiemDanhDao.getAllAttendance();
        List<AttendanceDTO> list=new ArrayList<AttendanceDTO>();
        if(listAttent!=null){
          if(maCa!=null && !maCa.equals("")){
            listAttent = listAttent.stream().filter(f->f.getShift_id().equals(maCa)).collect(Collectors.toList());
          }
          if(chiNhanh!=null && !chiNhanh.equals("")){
            listAttent = listAttent.stream().filter(f->f.getBrand_id().equals(chiNhanh)).collect(Collectors.toList());
          }          
          if(maLoai!=null && !maLoai.equals("")){
            listAttent = listAttent.stream().filter(f->f.getStatus_id().equals(maLoai)).collect(Collectors.toList());
          }
               
          for (AttendanceDTO item : listAttent) {
            if(item.getDisable().equals("false")){
              var modelAtten=new AttendanceDTO();
              modelAtten.setId(item.getId());
              modelAtten.setNote(item.getNote());
              SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
              java.util.Date utilExD = new SimpleDateFormat("yyyy-MM-dd").parse(item.getDate_attendance());
              modelAtten.setDate_attendance(String.valueOf(formatter.format(utilExD)));
              var tenLopSchedule="";
              var maLopSchedule="";
              var modelSchedule=LichHocDao.getScheduleById(item.getSchedule_id());
              if(modelSchedule!=null){
                var modelMon=MonHocDao.getSubjectById(modelSchedule.getSubject_id());
                var modelLop=LopDao.getClassById(modelSchedule.getClass_id());
                maLopSchedule=modelSchedule.getClass_id();
                tenLopSchedule=modelLop==null?"":modelLop.getName_class();
                modelAtten.setDisable((tenLopSchedule.equals("")?"":tenLopSchedule + " - ") +  (modelMon==null?"":modelMon.getSort_name()));              
              }
              var tenCa="";
              var tenChiNhanh="";
              var tenLoaiHoc="";
              if(!item.getShift_id().equals("")){
                ShiftStudyDTO modelCa=PhienHocDAO.getShiftStudyById(item.getShift_id());
                tenCa = modelCa==null? "" : modelCa.getName_shift();
              }
              if(!item.getBrand_id().equals("")){
                var modelChiNhanh=DanhMucTinhTrangDAO.getStatusById(item.getBrand_id());
                tenChiNhanh = modelChiNhanh==null? "" : modelChiNhanh.getName_status();
              }
              if(!item.getStatus_id().equals("")){
                var modelLoaiHoc=DanhMucTinhTrangDAO.getStatusById(item.getStatus_id());
                tenLoaiHoc = modelLoaiHoc==null? "" : modelLoaiHoc.getName_status();
              }
              modelAtten.setShift_id((tenCa.equals("")?"":tenCa+" - ")+ (tenChiNhanh.equals("")?"":tenChiNhanh));
              modelAtten.setStatus_id(tenLoaiHoc.equals("")?"":tenLoaiHoc);
              modelAtten.setBrand_id(item.getBrand_id()+";"+maLopSchedule+";"+item.getStatus_id());
              list.add(modelAtten);
            }
            
          }     
          if(maLop!=null && !maLop.equals("")){
            list = list.stream().filter(f->f.getBrand_id().contains(maLop)).collect(Collectors.toList());
          } 
          Collections.sort(list, Comparator.comparing(AttendanceDTO::getDate_attendance));
        }
        model.addAttribute("list", list);
        model.addAttribute("lsLop", LopDao.getClassSort());
        model.addAttribute("lsChiNhanh", danhMucDungChung.ChiNhanh());
        model.addAttribute("lsCa", PhienHocDAO.getAllShiftStudy());
        model.addAttribute("lsLoai", danhMucDungChung.LoaiMonHoc());
		  if (ajaxLoad != null && ajaxLoad.equals("table")) {
			return pathView + "loadTable";
		  }
	    return pathView + "detail";

    }
    @RequestMapping(value = "/DanhSachSinhVien/{id}")
    public String chiTiet(Model model, @PathVariable("id") String id)
        throws InterruptedException, ExecutionException, IOException {
      var modelAtten=DiemDanhDao.getAttendanceById(id);
      var modelSchedule=LichHocDao.getScheduleById(modelAtten.getSchedule_id());
      var chuoiSV=modelSchedule.getList_student();
      var listAttenSV=DiemDanhDao.getAllAttendanceStudentFC();
      var arr=chuoiSV.split(",");
      var list=new ArrayList<AttendanceStudentDTO>();
      for (String item : arr) {
        var modelSV=SinhVienDao.getStudentById(item);
        var modelAttenSV=new AttendanceStudentDTO();
        modelAttenSV.setStudent_id(modelSV.getFull_name()+" - "+ item);
        modelAttenSV.setCheck_in(modelSV.getMobile_phone()+" - "+ modelSV.getEmail_school());
        if(listAttenSV!=null){
          var checkAtten=listAttenSV.stream().filter(f->f.getAttendance_id().equals(id)).findFirst().orElse(null);
          if(checkAtten!=null){            
            if(checkAtten.getStatus_id()!=null && !checkAtten.getAttendance_id().equals("")){
              var modelStt=DanhMucTinhTrangDAO.getStatusById(checkAtten.getStatus_id());
              modelAttenSV.setStatus_id(modelStt.getName_status());
              modelAttenSV.setNote(checkAtten.getNote());
              var checkSoLan=listAttenSV.stream().filter(f->f.getAttendance_id().equals(id) && f.getStatus_id().equals(checkAtten.getStatus_id()) && f.getStudent_id().equals(item)).collect(Collectors.toList());
              if(checkSoLan!=null){
                modelAttenSV.setCheck_out(checkSoLan.size()+"/"+modelSchedule.getCurrent_session());
              }
            }else{
              //
            }
          }
        }
        list.add(modelAttenSV);
      }
      model.addAttribute("list", list);
      return pathView +"loadSinhVien";
    }
    
}
