package c1808g1.aem_web.controllers.QuanLyLichHoc;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.google.gson.Gson;

import c1808g1.Models.CauHinh.ShiftStudyDTO;
import c1808g1.Models.CauHinh.StatusDTO;
import c1808g1.Models.QuanLiHoSo.FCDTO;
import c1808g1.Models.QuanLiHoSo.StudentDTO;
import c1808g1.Models.QuanLyChuongTrinhHoc.ClassDTO;
import c1808g1.Models.QuanLyChuongTrinhHoc.SubjectDTO;
import c1808g1.Models.QuanLyLichHoc.AttendanceDTO;
import c1808g1.Models.QuanLyLichHoc.ScheduleDTO;
import c1808g1.aem_web.DAO.CauHinh.PhienHocDAO;
import c1808g1.aem_web.DAO.QuanLyChuongTrinhHoc.LopDao;
import c1808g1.aem_web.DAO.QuanLyChuongTrinhHoc.MonHocDao;
import c1808g1.aem_web.DAO.QuanLyHoSo.GiangVienDao;
import c1808g1.aem_web.DAO.QuanLyHoSo.SinhVienDao;
import c1808g1.aem_web.DAO.QuanLyLichHoc.DiemDanhDao;
import c1808g1.aem_web.DAO.QuanLyLichHoc.LichHocDao;
import c1808g1.aem_web.common.danhMucDungChung;

@Controller
@RequestMapping("/LichHoc/")
public class LichHocController {
	String pathView = "LichHoc/";

	@RequestMapping(value = { "", "/Index" }, method = RequestMethod.GET)
	public String Index(Model model, @RequestParam(required = false) String ajaxLoad,
			@RequestParam(required = false) String filterClass, @RequestParam(required = false) String filterFc)
			throws JsonParseException, JsonMappingException, InterruptedException, ExecutionException, IOException,
			ParseException {
		List<ScheduleDTO> schedules = new ArrayList<ScheduleDTO>();

		try {
			if (filterClass == null || filterClass.equals("") || filterClass.equals("null") || filterClass.isEmpty()
					|| filterClass.equals(null)) {
				filterClass = "";
			} else {

			}
			if (filterFc == null || filterFc.equals("") || filterFc.equals("null") || filterFc.isEmpty()
					|| filterFc.equals(null)) {
				filterFc = "";
			} else {

			}

			if ((filterClass.equals("") || filterClass == null || filterClass.equals("null") || filterClass.isEmpty()
					|| filterClass.equals(null))
					&& (filterFc.equals("") || filterFc == null || filterFc.equals("null") || filterFc.isEmpty()
							|| filterFc.equals(null))) {
				schedules = LichHocDao.getAllScheduleFilter("", "");
			} else {
				schedules = LichHocDao.getAllScheduleFilter(filterClass, filterFc);
			}
			List<SubjectDTO> listAllSubject = MonHocDao.getAllSubject();
			List<ClassDTO> listAllClass = LopDao.getAllClass();
			List<FCDTO> listAllFC = GiangVienDao.getAllFC();
			List<ShiftStudyDTO> listAllShift = PhienHocDAO.getAllShiftStudy();
			List<StatusDTO> listAllBrand = danhMucDungChung.ChiNhanh();

			for (int i = 0; i < schedules.size(); i++) {
				java.util.Date utilStart_date = new SimpleDateFormat("yyyy-MM-dd")
						.parse(schedules.get(i).getStart_date());
				java.util.Date utilEnd_date = new SimpleDateFormat("yyyy-MM-dd").parse(schedules.get(i).getEnd_date());
				SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
				schedules.get(i).setStart_date(formatter.format(utilStart_date));
				schedules.get(i).setEnd_date(formatter.format(utilEnd_date));
			}

			model.addAttribute("listAllSubject", listAllSubject);
			model.addAttribute("listAllClass", listAllClass);
			model.addAttribute("listAllFC", listAllFC);
			model.addAttribute("listAllShift", listAllShift);
			model.addAttribute("listAllBrand", listAllBrand);
			model.addAttribute("schedules", schedules);
		} catch (InterruptedException | ExecutionException | IOException e) {
			model.addAttribute("students", null);
			e.printStackTrace();
		}

		if (ajaxLoad != null && ajaxLoad.equals("table")) {
			return pathView + "loadTable";
		}

		return this.pathView + "index";
	}

	@RequestMapping(value = "formSchedule", method = RequestMethod.GET)
	public String formSchedule(Model model, @RequestParam(required = false) String id)
			throws InterruptedException, ExecutionException, IOException, ParseException {
		ScheduleDTO schedule = new ScheduleDTO();
		List<SubjectDTO> listAllSubject = MonHocDao.getAllSubject();
		List<ClassDTO> listAllClass = LopDao.getAllClass();
		List<FCDTO> listAllFC = GiangVienDao.getAllFC();
		List<ShiftStudyDTO> listAllShift = PhienHocDAO.getAllShiftStudy();
		List<StatusDTO> listAllBrand = danhMucDungChung.ChiNhanh();
		List<AttendanceDTO> listAllAttendance = DiemDanhDao.getAllAttendance();
		List<StatusDTO> listAllSubjectType = danhMucDungChung.LoaiMonHoc();
		List<StudentDTO> listAllStudent = SinhVienDao.getAllStudent();
		System.out.println(listAllStudent);
		if (id == null || id.equals("")) {
			schedule = null;
		} else {
			schedule = LichHocDao.getScheduleById(id);
			java.util.Date utilStart_date = new SimpleDateFormat("yyyy-MM-dd").parse(schedule.getStart_date());
			java.util.Date utilEnd_date = new SimpleDateFormat("yyyy-MM-dd").parse(schedule.getEnd_date());
			SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
			schedule.setStart_date(formatter.format(utilStart_date));
			schedule.setEnd_date(formatter.format(utilEnd_date));
		}
		for (int i = 0; i < listAllAttendance.size(); i++) {
			java.util.Date utilDate_attendance = new SimpleDateFormat("yyyy-MM-dd")
					.parse(listAllAttendance.get(i).getDate_attendance());
			SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
			listAllAttendance.get(i).setDate_attendance(formatter.format(utilDate_attendance));
		}

		model.addAttribute("listAllSubject", listAllSubject);
		model.addAttribute("listAllClass", listAllClass);
		model.addAttribute("listAllFC", listAllFC);
		model.addAttribute("listAllShift", listAllShift);
		model.addAttribute("listAllBrand", listAllBrand);
		model.addAttribute("listAllAttendance", listAllAttendance);
		model.addAttribute("listAllSubjectType", listAllSubjectType);
		model.addAttribute("listAllStudent", listAllStudent);
		model.addAttribute("schedule", schedule);
		model.addAttribute("jsonListAllBrand", new Gson().toJson(listAllBrand));
		model.addAttribute("jsonListAllShift", new Gson().toJson(listAllShift));
		model.addAttribute("jsonListAllSubjectType", new Gson().toJson(listAllSubjectType));
		return this.pathView + "create";
	}

	@RequestMapping(value = "/Delete/{id}")
	public @ResponseBody String delete(@PathVariable("id") String id) {
		try {
			if (id == null || id == "") {
				return "Not found id!";
			} else {
				// var sinhVien = SinhVienDao.getStudentById(id);
				ScheduleDTO student = LichHocDao.getScheduleById(id);
				if (student != null) {
					LichHocDao.delete(id);
				}
			}
		} catch (InterruptedException | ExecutionException | IOException e) {

			e.printStackTrace();
			return e.getMessage();
		}
		return "true";
	}

	@RequestMapping(value = "/getListStudentByClassId", method = RequestMethod.GET)
	public @ResponseBody String getListStudentByClassId(HttpServletRequest request, String current_class)
			throws ParseException {
		// ObjectMapper mapper = new ObjectMapper();
		String ajaxResponse = "";
		try {
			// ajaxResponse = mapper.writeValueAsString(person);
			List<StudentDTO> listStudents = SinhVienDao.getAllStudent().stream()
					.filter(s -> s.getCurrent_class().equals(current_class)).collect(Collectors.toList());
			if (listStudents == null) {
				ajaxResponse = "error%Lớp này chưa có sinh viên!";
			} else {
				SimpleDateFormat smp = new SimpleDateFormat("yyyy-MM-dd");
				SimpleDateFormat smf = new SimpleDateFormat("dd/MM/yyyy");
				for (int i = 0; i < listStudents.size(); i++) {
					listStudents.get(i)
							.setApplication_date(smf.format((smp.parse(listStudents.get(i).getApplication_date()))));
					listStudents.get(i)
							.setDate_of_doing(smf.format((smp.parse(listStudents.get(i).getDate_of_doing()))));
					listStudents.get(i).setCurrent_class(
							LopDao.getClassById(listStudents.get(i).getCurrent_class()).getName_class());
				}
				ajaxResponse = new Gson().toJson(listStudents);
			}
		} catch (InterruptedException | ExecutionException | IOException e) {
			e.printStackTrace();
		}
		return ajaxResponse;
	}

	@RequestMapping(value = "/getSubjectBySubjectId", method = RequestMethod.GET)
	public @ResponseBody String getSessionBySubjectId(HttpServletRequest request, String id_subject)
			throws ParseException {
		// ObjectMapper mapper = new ObjectMapper();
		String ajaxResponse = "";
		try {
			// ajaxResponse = mapper.writeValueAsString(person);
			SubjectDTO subject = MonHocDao.getSubjectById(id_subject);
			if (subject == null) {
				ajaxResponse = "error%Không có môn học này!";
			} else {
				ajaxResponse = new Gson().toJson(subject);
			}
		} catch (InterruptedException | ExecutionException | IOException e) {
			e.printStackTrace();
		}
		return ajaxResponse;
	}

	@RequestMapping(value = "/addOrUpdateSchedule", method = RequestMethod.POST)
	public @ResponseBody String addOrUpdateSchedule(@RequestBody ScheduleDTO schedule)
			throws ParseException, InterruptedException, ExecutionException, IOException {
		String status = null;
		if (schedule == null) {
			status = "no data";
		} else {
			List<String> formatStrings = Arrays.asList("dd/MM/yyyy", "yyyy-MM-dd");
			java.util.Date utilStart_date = null;
			java.util.Date utilEnd_date = null;
			for (String formatString : formatStrings) {
				try {
					utilStart_date = new SimpleDateFormat(formatString).parse(schedule.getStart_date());
					System.out.println(formatString + " " + utilStart_date.toString());
					utilEnd_date = new SimpleDateFormat(formatString).parse(schedule.getEnd_date());
					System.out.println(formatString + " " + utilEnd_date.toString());
				} catch (ParseException e) {
				}
			}

			java.sql.Date sqlApplication_date = new java.sql.Date(utilStart_date.getTime());
			schedule.setStart_date(String.valueOf(sqlApplication_date));
			java.sql.Date sqlDate_of_doing = new java.sql.Date(utilEnd_date.getTime());
			schedule.setEnd_date(String.valueOf(sqlDate_of_doing));

			if (schedule.getId() == null || schedule.getId().toString().equals("")) {
				ScheduleDTO result = LichHocDao.create(schedule);
				status = "success?id_schedule=" + result.getId();
			} else {
				ScheduleDTO check = LichHocDao.getScheduleById(schedule.getId());
				if (check == null) {
					LichHocDao.create(schedule);
					status = "success";
				} else {
					check.setBrand_id(schedule.getBrand_id());
					check.setClass_id(schedule.getClass_id());
					check.setCoef_salary(schedule.getCoef_salary());
					check.setCurrent_session(schedule.getCurrent_session());
					check.setEnd_date(schedule.getEnd_date());
					check.setList_fc(schedule.getList_fc());
					check.setList_student(schedule.getList_student());
					check.setMax_session(schedule.getMax_session());
					check.setNote(schedule.getNote());
					check.setNumber_session(schedule.getNumber_session());
					check.setShift_id(schedule.getShift_id());
					check.setStart_date(schedule.getStart_date());
					check.setSubject_id(schedule.getSubject_id());
					ScheduleDTO result = LichHocDao.update(check);
					status = "success?id_schedule=" + result.getId();
				}
			}
		}
		return new Gson().toJson(status);
	}

	@RequestMapping(value = "/addOrUpdateAttendance", method = RequestMethod.POST)
	public @ResponseBody String addOrUpdateAttendance(@RequestBody AttendanceDTO attendance)
			throws ParseException, InterruptedException, ExecutionException, IOException {
		String status = null;
		if (attendance == null) {
			status = "no data";
		} else {
			List<String> formatStrings = Arrays.asList("dd/MM/yyyy", "yyyy-MM-dd");
			java.util.Date utilDate_attendance = null;
			for (String formatString : formatStrings) {
				try {

					utilDate_attendance = new SimpleDateFormat(formatString).parse(attendance.getDate_attendance());
					System.out.println(formatString + " " + utilDate_attendance.toString());
				} catch (ParseException e) {
				}
			}
			java.sql.Date sqlDate_attendance = new java.sql.Date(utilDate_attendance.getTime());
			attendance.setDate_attendance(String.valueOf(sqlDate_attendance));
			if (attendance.getId() == null || attendance.getId().toString().equals("")) {
				DiemDanhDao.createAttendance(attendance);
				status = "success";
			} else {
				AttendanceDTO check = DiemDanhDao.getAttendanceById(attendance.getId());
				if (check == null) {
					DiemDanhDao.createAttendance(attendance);
					status = "success";
				} else {
					check.setBrand_id(attendance.getBrand_id());
					check.setDate_attendance(attendance.getDate_attendance());
					check.setDisable(attendance.getDisable());
					check.setNote(attendance.getNote());
					check.setSchedule_id(attendance.getSchedule_id());
					check.setShift_id(attendance.getShift_id());
					check.setStatus_id(attendance.getStatus_id());
					DiemDanhDao.updateAttendance(check);
					status = "success";
				}
			}
		}
		List<AttendanceDTO> recountCurrentSession=DiemDanhDao.getAllAttendance().stream().filter(s -> s.getSchedule_id()==attendance.getSchedule_id()).collect(Collectors.toList());
		return new Gson().toJson(status);
	}
	
	@RequestMapping(value = "/deleteAttendance", method = RequestMethod.DELETE)
	public @ResponseBody String deleteAttendance(HttpServletRequest servlet, String id)
			throws ParseException, InterruptedException, ExecutionException, IOException {
		String status = null;
		if(id==null||id.toString().equals("")) {
			status="no id";
		}else {
			AttendanceDTO check=DiemDanhDao.getAttendanceById(id);
			if(check==null) {
				status="no result";
			}else {
				DiemDanhDao.deleteAttendance(id);
				status="success";
			}
		}
		return new Gson().toJson(status);
	}
}
