package c1808g1.aem_web.controllers.QuanLyHoSo;

import java.io.IOException;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ExecutionException;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.google.gson.Gson;

import c1808g1.Models.CauHinh.StatusDTO;
import c1808g1.Models.HeThong.RoleDTO;
import c1808g1.Models.QuanLiHoSo.EmployeeDTO;
import c1808g1.Models.QuanLiHoSo.FCDTO;
import c1808g1.Models.QuanLiHoSo.GradeSalaryFcDTO;
import c1808g1.Models.QuanLiHoSo.ScoreFCDTO;
import c1808g1.Models.QuanLiHoSo.StudentDTO;
import c1808g1.Models.QuanLyChuongTrinhHoc.SubjectDTO;
import c1808g1.aem_web.DAO.CauHinh.DanhMucTinhTrangDAO;
import c1808g1.aem_web.DAO.HeThong.QuyenDAO;
import c1808g1.aem_web.DAO.QuanLyChuongTrinhHoc.LopDao;
import c1808g1.aem_web.DAO.QuanLyChuongTrinhHoc.MonHocDao;
import c1808g1.aem_web.DAO.QuanLyHoSo.GiangVienDao;
import c1808g1.aem_web.DAO.QuanLyHoSo.GradeSalaryFcDao;
import c1808g1.aem_web.DAO.QuanLyHoSo.SinhVienDao;
import c1808g1.aem_web.common.MD5;
import c1808g1.aem_web.common.danhMucDungChung;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
@RequestMapping("/FC/")
public class FCController {
	String pathView = "FC/";

	@RequestMapping({ "", "/Index" })
	public String index(Model model, @RequestParam(required = false) String ajaxLoad,
			@RequestParam(required = false) Integer page, @RequestParam(required = false) String status,
			@RequestParam(required = false) String searchValue)
			throws JsonParseException, JsonMappingException, InterruptedException, ExecutionException, IOException {
		// get list data
		List<FCDTO> listFc = new ArrayList<FCDTO>();
		try {
			if ((String.valueOf(status).equals("null") || String.valueOf(status).equals("")
					|| String.valueOf(status) == null || String.valueOf(status).equals(null)
					|| String.valueOf(status).isEmpty())
					&& (searchValue == null || searchValue.equals("") || searchValue.equals("null")
							|| searchValue.equals(null) || searchValue.isEmpty())) {
				listFc = GiangVienDao.getFCByMultipleParameter("", "");
			} else {
				listFc = GiangVienDao.getFCByMultipleParameter(status, searchValue);
			}
		} catch (InterruptedException | ExecutionException | IOException e) {
			e.printStackTrace();
		}
		List<RoleDTO> listRole = QuyenDAO.getAllOrderbyASC();
		List<StatusDTO> listStatus = danhMucDungChung.TrangThaiFC();
		List<FCDTO> fcs = GiangVienDao.getAllFC();
		List<ScoreFCDTO> sfc = GiangVienDao.getAllScoreFC();
		List<SubjectDTO> sbj = MonHocDao.getAllSubject();
		model.addAttribute("fcs", fcs);
		model.addAttribute("sbj", sbj);
		model.addAttribute("sfc", sfc);
		model.addAttribute("listFc", listFc);
		model.addAttribute("listRole", listRole);
		model.addAttribute("listStatus", listStatus);
		if (ajaxLoad != null && ajaxLoad.equals("table")) {
			return pathView + "loadTable";
		}
		return pathView + "index";
	}

	@RequestMapping(value = "ajaxFilterFC", method = RequestMethod.GET)
	public @ResponseBody String ajaxFilterFC(HttpServletRequest request, @RequestParam(required = false) String status,
			@RequestParam(required = false) String searchValue)
			throws JsonParseException, JsonMappingException, InterruptedException, ExecutionException, IOException {
		// get list data
		List<FCDTO> listFc = new ArrayList<FCDTO>();
		try {
			if ((String.valueOf(status).equals("null") || String.valueOf(status).equals("")
					|| String.valueOf(status) == null || String.valueOf(status).equals(null)
					|| String.valueOf(status).isEmpty())
					&& (searchValue == null || searchValue.equals("") || searchValue.equals("null")
							|| searchValue.equals(null) || searchValue.isEmpty())) {
				listFc = GiangVienDao.getFCByMultipleParameter("", "");
			} else {
				listFc = GiangVienDao.getFCByMultipleParameter(status, searchValue);
			}
		} catch (InterruptedException | ExecutionException | IOException e) {
			e.printStackTrace();
		}
		System.out.println(new Gson().toJson(listFc));
		return new Gson().toJson(listFc);
	}

	@RequestMapping(value = "/formFC", method = RequestMethod.GET)
	public String CreateForm(Model model, @RequestParam(required = false) String idFc) {

		try {
			List<GradeSalaryFcDTO> lsgsf = new ArrayList<GradeSalaryFcDTO>();
			if (idFc != null && !idFc.equals("")) {
				// search
				FCDTO fc = GiangVienDao.getFCById(idFc);
				if (fc == null)
					return "redirect:/FC/formFC";
				model.addAttribute("fc", fc);
				lsgsf = GradeSalaryFcDao.getGradeFcByFCId(idFc);
				System.out.println(fc.getList_role().split(","));
				SimpleDateFormat smp = new SimpleDateFormat("yyyy-MM-dd");
				SimpleDateFormat smf = new SimpleDateFormat("dd/MM/yyyy");
				if (lsgsf != null) {
					for (int i = 0; i < lsgsf.size(); i++) {
						lsgsf.get(i).setStart_date(smf.format((smp.parse(lsgsf.get(i).getStart_date()))));
					}
				} else {
					lsgsf = null;
				}
				model.addAttribute("gsfc", lsgsf);
				model.addAttribute("fc-roles", "d");
			}

			List<StatusDTO> listStatus = danhMucDungChung.TrangThaiFC();
			List<FCDTO> fcs = GiangVienDao.getAllFC();
			List<ScoreFCDTO> sfc = GiangVienDao.getAllScoreFC();
			List<SubjectDTO> sbj = MonHocDao.getAllSubject();
			model.addAttribute("fcs", fcs);
			model.addAttribute("sbj", sbj);
			model.addAttribute("sfc", sfc);

			model.addAttribute("listRole", QuyenDAO.getAllOrderbyASC());
			model.addAttribute("listStatus", listStatus);
			model.addAttribute("jsonListStatus", new Gson().toJson(listStatus));

		} catch (Exception e) {
			e.printStackTrace();
		}

		return this.pathView + "create";
	}

	@RequestMapping(value = "/addOrUpdateFc")
	public @ResponseBody String addOrUpdateFC(FCDTO entity) {
		// check trùng thì return "Đã có trong bảng"
		// ok thì return true

		if (entity == null) {
			return "Error, please try again!";
		}

		try {
			FCDTO check = null;
			check = GiangVienDao.getFCById(entity.getId_fc());
			if (check == null) {
				Timestamp timestamp = new Timestamp(System.currentTimeMillis());
				entity.setDate_create(String.valueOf(timestamp));
				String pwMD = MD5.GenerateMD5("aptech123");
				entity.setPassword(pwMD);
				GiangVienDao.createFC(entity);
			} else {
				entity.setPassword(check.getPassword());
				entity.setStatus_id(check.getStatus_id());
				entity.setStart_date(check.getStart_date());
				entity.setEnd_date(check.getEnd_date());
				entity.setNote_status(check.getNote_status());
				entity.setDate_create(check.getDate_create());
				GiangVienDao.updateFC(entity);
			}
		} catch (InterruptedException | ExecutionException | IOException e) {
			e.printStackTrace();
			return e.getMessage();
		}
		return "true";
	}

	//

	@RequestMapping(value = "/checkId", method = RequestMethod.POST)
	public @ResponseBody String searchPerson(HttpServletRequest request, String id_fc) {
		// ObjectMapper mapper = new ObjectMapper();
		String ajaxResponse = "";
		try {
			// ajaxResponse = mapper.writeValueAsString(person);
			FCDTO check = GiangVienDao.getFCById(id_fc);
			if (check == null) {
				ajaxResponse = "Mã FC chưa tồn tại, có thể tạo mới";
			} else {
				ajaxResponse = "Mã FC đã tồn tại, có thể cập nhật,không thể tạo mới!";
			}
		} catch (InterruptedException | ExecutionException | IOException e) {
			e.printStackTrace();
		}
		return ajaxResponse;
	}

	@RequestMapping(value = "/checkEmail", method = RequestMethod.POST)
	public @ResponseBody String checkEmail(HttpServletRequest request, String email_school) {
		// ObjectMapper mapper = new ObjectMapper();
		String ajaxResponse = "";
		try {
			// ajaxResponse = mapper.writeValueAsString(person);
			FCDTO check = GiangVienDao.getFCByEmail(email_school);
			if (check == null) {
				ajaxResponse = "Email chưa tồn tại, có thể tạo mới.1";
			} else {
				ajaxResponse = "Email đã tồn tại, có thể cập nhật,không thể tạo mới!.0";
			}
		} catch (InterruptedException | ExecutionException | IOException e) {
			e.printStackTrace();
		}
		return ajaxResponse;

	}

	//

	@RequestMapping(value = "/addOrUpdateGradeSalaryFc")
	public @ResponseBody String addOrUpdate(GradeSalaryFcDTO entity) {
		// check trùng thì return "Đã có trong bảng"
		// ok thì return true

		if (entity == null) {
			return "Error, please try again!";
		}

		try {
			GradeSalaryFcDTO check = null;
			check = GradeSalaryFcDao.getGradeSalaryFcById(entity.getId());
			if (check == null) {
				List<String> formatStrings = Arrays.asList("dd/MM/yyyy", "yyyy-MM-dd", "MM/dd/yyyy");
				java.util.Date utilStart_date = null;
				for (String formatString : formatStrings) {
					try {
						utilStart_date = new SimpleDateFormat(formatString).parse(entity.getStart_date());
						System.out.println(formatString + " " + utilStart_date.toString());
					} catch (ParseException e) {
					}
				}
				java.sql.Date sqlStart_date = new java.sql.Date(utilStart_date.getTime());
				entity.setStart_date(String.valueOf(sqlStart_date));
				Timestamp timestamp = new Timestamp(System.currentTimeMillis());
				entity.setDate_create(String.valueOf(timestamp));
				String[] parts = entity.getHour_salary().split(",");
				String hs = "";
				for (int i = 0; i < parts.length; i++) {
					hs += parts[i];
				}
				entity.setHour_salary(hs);
				GradeSalaryFcDao.create(entity);
			} else {
				entity.setId(check.getId());
				entity.setDate_create(check.getDate_create());
				List<String> formatStrings = Arrays.asList("dd/MM/yyyy", "yyyy-MM-dd");
				java.util.Date utilStart_date = null;
				for (String formatString : formatStrings) {
					try {
						utilStart_date = new SimpleDateFormat(formatString).parse(entity.getStart_date());
						System.out.println(formatString + " " + utilStart_date.toString());
					} catch (ParseException e) {
					}
				}
				java.sql.Date sqlStart_date = new java.sql.Date(utilStart_date.getTime());
				entity.setStart_date(String.valueOf(sqlStart_date));
				String[] parts = entity.getHour_salary().split(",");
				String hs = "";
				for (int i = 0; i < parts.length; i++) {
					hs += parts[i];
				}
				entity.setHour_salary(hs);
				GradeSalaryFcDao.update(entity);
			}
		} catch (InterruptedException | ExecutionException | IOException e) {
			e.printStackTrace();
			return e.getMessage();
		}

		return "true";
	}

	@RequestMapping(value = "/addOrUpdateFcStatus")
	public @ResponseBody String addOrUpdateFcStatus(FCDTO entity) throws ParseException {
		// check trùng thì return "Đã có trong bảng"
		// ok thì return true
		if (entity == null) {
			return "Error, please try again!";
		}
		try {
			FCDTO check = null;
			check = GiangVienDao.getFCById(entity.getId_fc());
			FCDTO result = new FCDTO();
			List<String> formatStrings = Arrays.asList("yyyy-MM-dd", "dd/MM/yyyy");
			if (check == null) {
				return "not found id";
			} else {
				if (check.getStart_date() == null || check.getStart_date().equals(null)
						|| check.getStart_date().equals("")) {
					check.setStart_date(String.valueOf(entity.getStart_date()));
				} else {
					if (!(check.getStart_date().equals(entity.getStart_date()))) {
//						java.util.Date utilDateStart = new SimpleDateFormat("dd/MM/yyyy").parse(entity.getStart_date());
//						java.sql.Date sqlDateStart = new java.sql.Date(utilDateStart.getTime());
						check.setStart_date(String.valueOf(entity.getStart_date()));
					} else {

					}
				}

				if (check.getEnd_date() == null || check.getEnd_date().equals(null) || check.getEnd_date().equals("")) {
					check.setEnd_date(String.valueOf(entity.getEnd_date()));
				} else {
					if (!(check.getEnd_date().equals(entity.getEnd_date()))) {
						check.setEnd_date(String.valueOf(entity.getEnd_date()));
					} else {

					}
				}

				check.setStatus_id(entity.getStatus_id());
				check.setNote_status(entity.getNote_status());
				result = GiangVienDao.updateFC(check);
			}
		} catch (InterruptedException | ExecutionException | IOException e) {
			e.printStackTrace();
			return e.getMessage();
		}
		return "true";
	}

	@RequestMapping(value = "/changeActiveAccount", method = RequestMethod.POST)
	public @ResponseBody String changeActiveAccount(HttpServletRequest request, String id_fc) {
		// ObjectMapper mapper = new ObjectMapper();
		String ajaxResponse = "";
		try {
			// ajaxResponse = mapper.writeValueAsString(person);
			FCDTO check = GiangVienDao.getFCById(id_fc);
			if (check.getActive_account().equals("true")) {
				check.setActive_account("false");
				ajaxResponse = "Đã khóa!";
			} else {
				check.setActive_account("true");
				ajaxResponse = "Đã mở khóa!";
			}
			GiangVienDao.updateFC(check);
		} catch (InterruptedException | ExecutionException | IOException e) {
			e.printStackTrace();
		}
		return ajaxResponse;
	}

	@RequestMapping(value = "/Delete/{id}")
	public @ResponseBody String delete(@PathVariable("id") String id) {
		try {
			if (id == null || id == "") {
				return "Not found id!";
			} else {
				FCDTO check=GiangVienDao.getFCById(id);
				if(check!=null) {
					GiangVienDao.deleteFC(id);	
				}
			}
		} catch (InterruptedException | ExecutionException | IOException e) {

			e.printStackTrace();
			return e.getMessage();
		}
		return "true";
	}

	@RequestMapping(value = "/DeleteGSFC/{id}")
	public @ResponseBody String deleteGSFC(@PathVariable("id") String id) {
		String data = null;
		try {
			if (id == null || id == "") {
				data = "Not found id!";
			} else {
				GradeSalaryFcDTO check = GradeSalaryFcDao.getGradeSalaryFcById(id);
				if (check != null) {
					GradeSalaryFcDao.delete(id);
					data = "success";
				} else {
					data = "Not found id!";
				}
			}
		} catch (InterruptedException | ExecutionException | IOException e) {

			e.printStackTrace();
			return e.getMessage();
		}
		return data;
	}

}
