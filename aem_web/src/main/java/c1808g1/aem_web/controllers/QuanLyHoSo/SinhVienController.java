package c1808g1.aem_web.controllers.QuanLyHoSo;

import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Type;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.stream.Collectors;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.ClientAnchor;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Drawing;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Picture;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.util.IOUtils;
import org.apache.poi.xssf.usermodel.XSSFClientAnchor;
import org.apache.poi.xssf.usermodel.XSSFColor;
import org.apache.poi.xssf.usermodel.XSSFDrawing;
import org.apache.poi.xssf.usermodel.XSSFFactory;
import org.apache.poi.xssf.usermodel.XSSFPicture;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbookFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.core.io.ClassPathResource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

import c1808g1.Models.CauHinh.StatusDTO;
import c1808g1.Models.QuanLiHoSo.HosoStudentDTO;
import c1808g1.Models.QuanLiHoSo.StatusStudentDTO;
import c1808g1.Models.QuanLiHoSo.StudentDTO;
import c1808g1.Models.QuanLyChuongTrinhHoc.ClassDTO;
import c1808g1.Models.QuanLyChuongTrinhHoc.CourseDTO;
import c1808g1.aem_web.DAO.CauHinh.DanhMucTinhTrangDAO;
import c1808g1.aem_web.DAO.QuanLyChuongTrinhHoc.KhoaHocDao;
import c1808g1.aem_web.DAO.QuanLyChuongTrinhHoc.LopDao;
import c1808g1.aem_web.DAO.QuanLyHoSo.SinhVienDao;
import c1808g1.aem_web.DAO.QuanLyHoSo.StatusStudentDao;
import c1808g1.aem_web.common.FileDinhKem;
import c1808g1.aem_web.common.MD5;
import c1808g1.aem_web.common.danhMucDungChung;

@Controller
@RequestMapping("/SinhVien/")
public class SinhVienController {
	String pathView = "SinhVien/";

	@RequestMapping(value = { "", "/Index" }, method = RequestMethod.GET)
	public String Index(Model model, @RequestParam(required = false) String ajaxLoad,
			@RequestParam(required = false) Integer page, @RequestParam(required = false) String course_family,
			@RequestParam(required = false) String course_id, @RequestParam(required = false) String current_class,
			@RequestParam(required = false) String searchValue, @RequestParam(required = false) String from_date,
			@RequestParam(required = false) String to_date, @RequestParam(required = false) String status_id)
			throws JsonParseException, JsonMappingException, InterruptedException, ExecutionException, IOException {
		List<StudentDTO> students = new ArrayList<StudentDTO>();

		List<StatusDTO> statusDtos = new ArrayList<StatusDTO>();

		List<StatusStudentDTO> statusStudents = new ArrayList<StatusStudentDTO>();

		try {
			if (course_family == null || course_family.equals("") || course_family.equals("null")
					|| course_family.isEmpty() || course_family.equals(null)) {
				course_family = "";
			} else {

			}
			if (course_id == null || course_id.equals("") || course_id.equals("null") || course_id.isEmpty()
					|| course_id.equals(null)) {
				course_id = "";
			} else {

			}
			if (current_class == null || current_class.equals("") || current_class.equals("null")
					|| current_class.isEmpty() || current_class.equals(null)) {
				current_class = "";
			} else {

			}
			if (searchValue == null || searchValue.equals("") || searchValue.equals("null") || searchValue.isEmpty()
					|| searchValue.equals(null)) {
				searchValue = "";
			} else {

			}
			if (from_date == null || from_date.equals("") || from_date.equals("null") || from_date.isEmpty()
					|| from_date.equals(null)) {
				from_date = "";
			} else {

			}
			if (to_date == null || to_date.equals("") || to_date.equals("null") || to_date.isEmpty()
					|| to_date.equals(null)) {
				to_date = "";
			} else {

			}
			if (status_id == null || status_id.equals("") || status_id.equals("null") || status_id.isEmpty()
					|| status_id.equals(null)) {
				status_id = "";
			} else {

			}

			if ((course_family.equals("") || course_family == null || course_family.equals("null")
					|| course_family.isEmpty() || course_family.equals(null))
					&& (course_id.equals("") || course_id == null || course_id.equals("null") || course_id.isEmpty()
							|| course_id.equals(null))
					&& (current_class.equals("") || current_class == null || current_class.equals("null")
							|| current_class.isEmpty() || current_class.equals(null))
					&& (searchValue.equals("") || searchValue == null || searchValue.equals("null")
							|| searchValue.isEmpty() || searchValue.equals(null))
					&& (from_date.equals("") || from_date == null || from_date.equals("null") || from_date.isEmpty()
							|| from_date.equals(null))
					&& (to_date.equals("") || to_date == null || to_date.equals("null") || to_date.isEmpty()
							|| to_date.equals(null))
					&& (status_id.equals("") || status_id == null || status_id.equals("null") || status_id.isEmpty()
							|| status_id.equals(null))) {
				students = SinhVienDao.getAllStudentFilter("", "", "", "", "", "", 1, "");
			} else {
				students = SinhVienDao.getAllStudentFilter(course_family, course_id, current_class, searchValue,
						from_date, to_date, 1, status_id);
			}

			statusDtos = danhMucDungChung.TrangThaiSV();

			statusStudents = StatusStudentDao.getAllStatusStudent();
			List<StatusDTO> status = DanhMucTinhTrangDAO.getAllStatus();
			model.addAttribute("listStatus", status);

			model.addAttribute("getAllCourses", KhoaHocDao.getAllCourse());
			model.addAttribute("courses", KhoaHocDao.getAllCourse().stream().filter(x -> x.getCourse_root() == "")
					.collect(Collectors.toCollection(ArrayList<CourseDTO>::new)));

			// getAllStatusStudents =>
			model.addAttribute("getAllStatus", statusDtos);
			String jsonGetAllStatus = new Gson().toJson(statusDtos);
			model.addAttribute("jsonGetAllStatus", jsonGetAllStatus);

			model.addAttribute("classes", LopDao.getAllClass());

			model.addAttribute("getAllStatusStudent", statusStudents);
			String jsonGetAllStatusStudent = new Gson().toJson(statusStudents);
			model.addAttribute("jsonGetAllStatusStudent", jsonGetAllStatusStudent);
			// System.out.println(students.get(0));
			model.addAttribute("students", students);

		} catch (InterruptedException | ExecutionException | IOException e) {
			model.addAttribute("students", null);
			e.printStackTrace();
		}

		if (ajaxLoad != null && ajaxLoad.equals("table")) {
			return pathView + "loadTable";
		}

		return this.pathView + "index";
	}

	@RequestMapping(value = "/ajaxFilterStudent", method = RequestMethod.GET)
	public @ResponseBody String ajaxFilterStudent(HttpServletRequest request,
			@RequestParam(required = false) Integer page, @RequestParam(required = false) String course_family,
			@RequestParam(required = false) String course_id, @RequestParam(required = false) String current_class,
			@RequestParam(required = false) String searchValue, @RequestParam(required = false) String from_date,
			@RequestParam(required = false) String to_date, @RequestParam(required = false) String status_id)
			throws JsonParseException, JsonMappingException, InterruptedException, ExecutionException, IOException {
		List<StudentDTO> students = new ArrayList<StudentDTO>();

		try {
			if (course_family == null || course_family.equals("") || course_family.equals("null")
					|| course_family.isEmpty() || course_family.equals(null)) {
				course_family = "";
			} else {

			}
			if (course_id == null || course_id.equals("") || course_id.equals("null") || course_id.isEmpty()
					|| course_id.equals(null)) {
				course_id = "";
			} else {

			}
			if (current_class == null || current_class.equals("") || current_class.equals("null")
					|| current_class.isEmpty() || current_class.equals(null)) {
				current_class = "";
			} else {

			}
			if (searchValue == null || searchValue.equals("") || searchValue.equals("null") || searchValue.isEmpty()
					|| searchValue.equals(null)) {
				searchValue = "";
			} else {

			}
			if (from_date == null || from_date.equals("") || from_date.equals("null") || from_date.isEmpty()
					|| from_date.equals(null)) {
				from_date = "";
			} else {

			}
			if (to_date == null || to_date.equals("") || to_date.equals("null") || to_date.isEmpty()
					|| to_date.equals(null)) {
				to_date = "";
			} else {

			}
			if (status_id == null || status_id.equals("") || status_id.equals("null") || status_id.isEmpty()
					|| status_id.equals(null)) {
				status_id = "";
			} else {

			}

			if ((course_family.equals("") || course_family == null || course_family.equals("null")
					|| course_family.isEmpty() || course_family.equals(null))
					&& (course_id.equals("") || course_id == null || course_id.equals("null") || course_id.isEmpty()
							|| course_id.equals(null))
					&& (current_class.equals("") || current_class == null || current_class.equals("null")
							|| current_class.isEmpty() || current_class.equals(null))
					&& (searchValue.equals("") || searchValue == null || searchValue.equals("null")
							|| searchValue.isEmpty() || searchValue.equals(null))
					&& (from_date.equals("") || from_date == null || from_date.equals("null") || from_date.isEmpty()
							|| from_date.equals(null))
					&& (to_date.equals("") || to_date == null || to_date.equals("null") || to_date.isEmpty()
							|| to_date.equals(null))
					&& (status_id.equals("") || status_id == null || status_id.equals("null") || status_id.isEmpty()
							|| status_id.equals(null))) {
				students = SinhVienDao.getAllStudentFilter("", "", "", "", "", "", 1, "");
			} else {
				students = SinhVienDao.getAllStudentFilter(course_family, course_id, current_class, searchValue,
						from_date, to_date, 1, status_id);
			}

		} catch (InterruptedException | ExecutionException | IOException e) {
			e.printStackTrace();
		}
		System.out.println(new Gson().toJson(students));
		return new Gson().toJson(students);
	}

	@RequestMapping(value = "/scrollToGetStudent", method = RequestMethod.GET)
	public @ResponseBody String scrollToGetStudent(HttpServletRequest request,
			@RequestParam(required = false) Integer page, @RequestParam(required = false) String course_family,
			@RequestParam(required = false) String course_id, @RequestParam(required = false) String current_class,
			@RequestParam(required = false) String searchValue, @RequestParam(required = false) String from_date,
			@RequestParam(required = false) String to_date, @RequestParam(required = false) String status_id) {
		// ObjectMapper mapper = new ObjectMapper();
		String ajaxResponse = null;
		List<StudentDTO> students = new ArrayList<StudentDTO>();
		try {
			if (course_family == null || course_family.equals("") || course_family.equals("null")
					|| course_family.isEmpty() || course_family.equals(null)) {
				course_family = "";
			} else {

			}
			if (course_id == null || course_id.equals("") || course_id.equals("null") || course_id.isEmpty()
					|| course_id.equals(null)) {
				course_id = "";
			} else {

			}
			if (current_class == null || current_class.equals("") || current_class.equals("null")
					|| current_class.isEmpty() || current_class.equals(null)) {
				current_class = "";
			} else {

			}
			if (searchValue == null || searchValue.equals("") || searchValue.equals("null") || searchValue.isEmpty()
					|| searchValue.equals(null)) {
				searchValue = "";
			} else {

			}
			if (from_date == null || from_date.equals("") || from_date.equals("null") || from_date.isEmpty()
					|| from_date.equals(null)) {
				from_date = "";
			} else {

			}
			if (to_date == null || to_date.equals("") || to_date.equals("null") || to_date.isEmpty()
					|| to_date.equals(null)) {
				to_date = "";
			} else {

			}
			if (status_id == null || status_id.equals("") || status_id.equals("null") || status_id.isEmpty()
					|| status_id.equals(null)) {
				status_id = "";
			} else {

			}

			if ((course_family.equals("") || course_family == null || course_family.equals("null")
					|| course_family.isEmpty() || course_family.equals(null))
					&& (course_id.equals("") || course_id == null || course_id.equals("null") || course_id.isEmpty()
							|| course_id.equals(null))
					&& (current_class.equals("") || current_class == null || current_class.equals("null")
							|| current_class.isEmpty() || current_class.equals(null))
					&& (searchValue.equals("") || searchValue == null || searchValue.equals("null")
							|| searchValue.isEmpty() || searchValue.equals(null))
					&& (from_date.equals("") || from_date == null || from_date.equals("null") || from_date.isEmpty()
							|| from_date.equals(null))
					&& (to_date.equals("") || to_date == null || to_date.equals("null") || to_date.isEmpty()
							|| to_date.equals(null))
					&& (status_id.equals("") || status_id == null || status_id.equals("null") || status_id.isEmpty()
							|| status_id.equals(null))) {
				students = SinhVienDao.getAllStudentFilter("", "", "", "", "", "", page, "");
			} else {
				students = SinhVienDao.getAllStudentFilter(course_family, course_id, current_class, searchValue,
						from_date, to_date, page, status_id);
			}
		} catch (InterruptedException | ExecutionException | IOException e) {
			e.printStackTrace();
		}
		ajaxResponse = new Gson().toJson(students);
		return ajaxResponse;
	}

	@RequestMapping(value = "/Delete/{id}")
	public @ResponseBody String delete(@PathVariable("id") String id) {
		try {
			if (id == null || id == "") {
				return "Not found id!";
			} else {
				// var sinhVien = SinhVienDao.getStudentById(id);
				StudentDTO student = SinhVienDao.getStudentById(id);
				if (student != null) {
					SinhVienDao.deleteStudent(id);
					FileDinhKem.DeleteFile(FileDinhKem.PATHFILE_AVATAR+student.getImage_student());
				}
			}
		} catch (InterruptedException | ExecutionException | IOException e) {

			e.printStackTrace();
			return e.getMessage();
		}
		return "true";
	}

	// update status
	@RequestMapping(value = "/changeActiveAccount/{id_student}", method = RequestMethod.POST)
	public @ResponseBody String changeStatus(HttpServletRequest request,
			@PathVariable("id_student") String id_student) {
		// ObjectMapper mapper = new ObjectMapper();
		String ajaxResponse = "";
		try {
			// ajaxResponse = mapper.writeValueAsString(person);
			StudentDTO check = SinhVienDao.getStudentById(id_student);
			if (check.getActive_account().equals("true")) {
				check.setActive_account("false");
				ajaxResponse = "Đã khóa!";
			} else {
				check.setActive_account("true");
				ajaxResponse = "Đã mở khóa!";
			}
			SinhVienDao.updateStudent(check);
		} catch (InterruptedException | ExecutionException | IOException e) {
			e.printStackTrace();
		}
		return ajaxResponse;
	}

//	@RequestMapping(value = "/findStatusStudentByIdStudent/{id_student}", method = RequestMethod.GET)
//	public @ResponseBody List<StatusStudentDTO> findStatusStudentByIdStudent(HttpServletRequest request,
//			@PathVariable("id_student") String id_student) {
//		// ObjectMapper mapper = new ObjectMapper();
//		List<StatusStudentDTO> ajaxResponse = new ArrayList<StatusStudentDTO>();
//		try {
//			// ajaxResponse = mapper.writeValueAsString(person);
//			ajaxResponse = StatusStudentDao.findStatusStudentByIdStudent(id_student);
//		} catch (InterruptedException | ExecutionException | IOException e) {
//			e.printStackTrace();
//		}
//		return ajaxResponse;
//	}

	@RequestMapping(value = "/formStudent", method = RequestMethod.GET, headers = "Accept=application/json")
	public String CreateForm(Model model, @RequestParam(required = false) String studentId) {
		try {

			if (studentId != null && !studentId.equals("")) {

				StudentDTO student = SinhVienDao.getStudentById(studentId);
				if (student == null) {
					return "redirect:/SinhVien/formStudent";
				} else {
//					java.util.Date utilApplication_date = null;
//					java.util.Date utilDate_of_doing = null;
//					java.util.Date utilDob = null;
					java.util.Date utilApplication_date = new SimpleDateFormat("yyyy-MM-dd")
							.parse(student.getApplication_date());
					System.out.println("yyyy-MM-dd" + " " + utilApplication_date.toString());
					java.util.Date utilDate_of_doing = new SimpleDateFormat("yyyy-MM-dd")
							.parse(student.getDate_of_doing());
					System.out.println("yyyy-MM-dd" + " " + utilDate_of_doing.toString());
					java.util.Date utilDob = new SimpleDateFormat("yyyy-MM-dd").parse(student.getDob());
					System.out.println("yyyy-MM-dd" + " " + utilDob.toString());
					SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
					student.setApplication_date(String.valueOf(formatter.format(utilApplication_date)));
					student.setDate_of_doing(String.valueOf(formatter.format(utilDate_of_doing)));
					student.setDob(String.valueOf(formatter.format(utilDob)));
				}
				model.addAttribute("student", student);
				if (student.getHo_so() == null || student.getHo_so().equals(null) || student.getHo_so().equals("")
						|| student.getHo_so().equals("[]")) {
					model.addAttribute("jsonHoso", null);
				} else {
					JsonArray jsonHoso=new JsonArray();
					jsonHoso=new Gson().fromJson(student.getHo_so(), JsonArray.class);
					Type hosoListType = new TypeToken<ArrayList<HosoStudentDTO>>(){}.getType();	 
					ArrayList<HosoStudentDTO> hosoArray = new Gson().fromJson(jsonHoso, hosoListType);
					model.addAttribute("jsonHoso", hosoArray);
				}
			}
			
			List<StudentDTO> students = SinhVienDao.getAllStudentFilter("", "", "", "", "", "", 1, "");

			model.addAttribute("students", students);

			// create form
			model.addAttribute("getAllStatusStudents", StatusStudentDao.getAllStatusStudent());

			// select course family co o student index javasript
			List<CourseDTO> courses = KhoaHocDao.getAllCourse();
			List<StatusDTO> statusDtos = new ArrayList<StatusDTO>();
			statusDtos = danhMucDungChung.TrangThaiSV();
			model.addAttribute("getAllStatus", statusDtos);
			String jsonGetAllStatus = new Gson().toJson(statusDtos);
			model.addAttribute("jsonGetAllStatus", jsonGetAllStatus);

			model.addAttribute("allCourse", courses);

			model.addAttribute("courses", courses.stream().filter(x -> x.getCourse_root() == "")
					.collect(Collectors.toCollection(ArrayList<CourseDTO>::new)));

			model.addAttribute("getAllStatusStudents", danhMucDungChung.TrangThaiSV());
			model.addAttribute("danhMucQuanHe", danhMucDungChung.DanhMucQuanHe());
			String jsonDanhMucQuanHe = new Gson().toJson(danhMucDungChung.DanhMucQuanHe());
			model.addAttribute("jsonDanhMucQuanHe", jsonDanhMucQuanHe);

			model.addAttribute("classes", LopDao.getAllClass());

		} catch (Exception e) {
			e.printStackTrace();
		}

		return this.pathView + "create";
	}

	@RequestMapping(value = "/addOrUpdate")
	public @ResponseBody String addOrUpdate(StudentDTO entity, MultipartFile fileData,HttpSession session) throws ParseException {
		// check trùng thì return "Đã có trong bảng"
		// ok thì return true
		String defaultImage="dealftUser.png";
		String path=session.getServletContext().getRealPath("/static/upload/avatar/");
		String fileName = new Date().getTime()+"_"+fileData.getOriginalFilename().replace('\\','_');
		String fullPath=(path+fileName).replace('\\', '/');
		System.out.println(path+fileName);
		if (entity == null) {
			return "Error, please try again!";
		}
		try {
			StudentDTO check = null;
			check = SinhVienDao.getStudentById(entity.getId_student());
			if (check == null) {
				if (fileData.getOriginalFilename().equals("") || fileData.getOriginalFilename() == null
						|| fileData.getOriginalFilename().equals(null)) {
					entity.setImage_student("");
				} else {
//					String path = FileDinhKem.PATHFILE_AVATAR;
					File tmp = new File(fullPath); 
	                fileData.transferTo(tmp);
					entity.setImage_student(fileName);
				}
				// value current class
				List<String> formatStrings = Arrays.asList("dd/MM/yyyy", "yyyy-MM-dd");
				java.util.Date utilApplication_date = null;
				java.util.Date utilDate_of_doing = null;
				java.util.Date utilDob = null;
				for (String formatString : formatStrings) {
					try {
						utilApplication_date = new SimpleDateFormat(formatString).parse(entity.getApplication_date());
						System.out.println(formatString + " " + utilApplication_date.toString());
						utilDate_of_doing = new SimpleDateFormat(formatString).parse(entity.getDate_of_doing());
						System.out.println(formatString + " " + utilDate_of_doing.toString());
						utilDob = new SimpleDateFormat(formatString).parse(entity.getDob());
						System.out.println(formatString + " " + utilDob.toString());
					} catch (ParseException e) {
					}
				}

				java.sql.Date sqlApplication_date = new java.sql.Date(utilApplication_date.getTime());
				entity.setApplication_date(String.valueOf(sqlApplication_date));
				java.sql.Date sqlDate_of_doing = new java.sql.Date(utilDate_of_doing.getTime());
				entity.setDate_of_doing(String.valueOf(sqlDate_of_doing));
				java.sql.Date sqlDob = new java.sql.Date(utilDob.getTime());
				entity.setDob(String.valueOf(sqlDob));

				entity.setFirst_class(entity.getCurrent_class());
				String pwMD = MD5.GenerateMD5("aptech123");
				entity.setPassword(pwMD);
				entity.setRole_id("1");
				entity.setCentre_name("aptech");
				SinhVienDao.createStudent(entity);

				ClassDTO checkClass = LopDao.getClassById(entity.getCurrent_class());
				checkClass.setSlot_regis(String.valueOf(Integer.valueOf(checkClass.getSlot_regis()) + 1));
				LopDao.update(checkClass);
			} else {
				if (fileData.getOriginalFilename() == null || fileData.getOriginalFilename().equals("")
						|| fileData.getOriginalFilename().equals(null)) {
					if (check.getImage_student() == null || check.getImage_student().equals("")
							|| check.getImage_student().equals(null)) {
						entity.setImage_student("");
					} else if (check.getImage_student().equals(defaultImage)) {
						entity.setImage_student("");
					} else {
						entity.setImage_student(check.getImage_student());
					}
				} else {
					File oldFile=new File(path+check.getImage_student());
					oldFile.delete();
//					String path = FileDinhKem.PATHFILE_AVATAR;
					File tmp = new File(fullPath);
	                fileData.transferTo(tmp);
					entity.setImage_student(fileName);
				}
				List<String> formatStrings = Arrays.asList("dd/MM/yyyy", "yyyy-MM-dd");
				java.util.Date utilApplication_date = null;
				java.util.Date utilDate_of_doing = null;
				java.util.Date utilDob = null;
				for (String formatString : formatStrings) {
					try {
						utilApplication_date = new SimpleDateFormat(formatString).parse(entity.getApplication_date());
						System.out.println(formatString + " " + utilApplication_date.toString());
						utilDate_of_doing = new SimpleDateFormat(formatString).parse(entity.getDate_of_doing());
						System.out.println(formatString + " " + utilDate_of_doing.toString());
						utilDob = new SimpleDateFormat(formatString).parse(entity.getDob());
						System.out.println(formatString + " " + utilDob.toString());
					} catch (ParseException e) {
					}
				}

				java.sql.Date sqlApplication_date = new java.sql.Date(utilApplication_date.getTime());
				entity.setApplication_date(String.valueOf(sqlApplication_date));
				java.sql.Date sqlDate_of_doing = new java.sql.Date(utilDate_of_doing.getTime());
				entity.setDate_of_doing(String.valueOf(sqlDate_of_doing));
				java.sql.Date sqlDob = new java.sql.Date(utilDob.getTime());
				entity.setDob(String.valueOf(sqlDob));
				if (entity.getCentre_name() == null || entity.getCentre_name().equals("")
						|| entity.getCentre_name().equals(null)) {
					entity.setCentre_name(check.getCentre_name());
				} else {

				}
				if (entity.getCs() == null || entity.getCs().equals("") || entity.getCs().equals(null)) {
					entity.setCs(check.getCs());
				} else {

				}
				if (entity.getHo_so() == null || entity.getHo_so().equals("") || entity.getHo_so().equals(null)) {
					entity.setHo_so(check.getHo_so());
				} else {

				}
				if (entity.getMobile_mac() == null || entity.getMobile_mac().equals("")
						|| entity.getMobile_mac().equals(null)) {
					entity.setMobile_mac(check.getMobile_mac());
				} else {

				}
				if (entity.getRole_id() == null || entity.getRole_id().equals(null)
						|| entity.getRole_id().equals(null)) {
					entity.setRole_id(check.getRole_id());
				} else {

				}
				if (entity.getStatus_id() == null || entity.getStatus_id().equals(null)
						|| entity.getStatus_id().equals(null)) {
					entity.setStatus_id(check.getStatus_id());
				} else {

				}
				if (entity.getTemp_id() == null || entity.getTemp_id().equals(null)
						|| entity.getTemp_id().equals(null)) {
					entity.setTemp_id(check.getTemp_id());
				} else {

				}
				if (entity.getPassword() == null || entity.getPassword().equals(null)
						|| entity.getPassword().equals(null)) {
					entity.setPassword(check.getPassword());
				} else {

				}
				SinhVienDao.updateStudent(entity);

				ClassDTO checkOldClass = LopDao.getClassById(check.getCurrent_class());
				checkOldClass.setSlot_regis(String.valueOf(Integer.valueOf(checkOldClass.getSlot_regis()) - 1));
				LopDao.update(checkOldClass);
				ClassDTO checkClass = LopDao.getClassById(entity.getCurrent_class());
				checkClass.setSlot_regis(String.valueOf(Integer.valueOf(checkClass.getSlot_regis()) + 1));
				LopDao.update(checkClass);
			}
		} catch (InterruptedException | ExecutionException | IOException e) {
			e.printStackTrace();
			return e.getMessage();
		}
		return "true";
	}

	@RequestMapping(value = "/addOrUpdateQuanhe", method = RequestMethod.POST)
	public @ResponseBody String addOrUpdateQuanhe(String idStudent, String idHoso, String idQuanhe, String nameQuanhe,
			String nghenghiepQuanhe, String sdtQuanhe, String diachiQuanhe, String macdinhQuanhe)
			throws ParseException, InterruptedException, ExecutionException, IOException {
		String status = null;
		StudentDTO check = SinhVienDao.getStudentById(idStudent);
		String checkHoso = check.getHo_so();
		// nếu hoso null hoặc empty
		if (checkHoso == null || checkHoso.equals(null) || checkHoso.equals("") || checkHoso.equals("[]")) {
			JsonArray jsonArray=new JsonArray();
			JsonObject jsonObject1=new JsonObject();
			jsonObject1.addProperty("idHoso", idHoso);
			jsonObject1.addProperty("idQuanhe", idQuanhe);
			jsonObject1.addProperty("nameQuanhe", nameQuanhe);
			jsonObject1.addProperty("nghenghiepQuanhe", nghenghiepQuanhe);
			jsonObject1.addProperty("sdtQuanhe", sdtQuanhe);
			jsonObject1.addProperty("diachiQuanhe", diachiQuanhe);
			jsonObject1.addProperty("macdinhQuanhe", macdinhQuanhe);

			// Add Object
			jsonArray.add(jsonObject1);
			check.setHo_so(jsonArray.toString());
			SinhVienDao.updateStudent(check);
			return new Gson().toJson("success");
		} else {
			JsonArray checkArray = new JsonArray();
			checkArray=new Gson().fromJson(checkHoso, JsonArray.class);
			// check từng object trong jsonarray
			int countAdded = 0;
			int countNotEquals = 0;
			for (int i = 0; i < checkArray.size(); i++) {
				JsonObject checkObject = checkArray.get(i).getAsJsonObject();
				// nếu jsonObject tại i có field idHoso và idHoso không null và không empty
				if (checkObject.has("idHoso") && !checkObject.get("idHoso").isJsonNull()&&!checkObject.get("idHoso").equals("")&&checkObject.get("idHoso").toString()!=null) {
					String checkIdHoso = checkObject.get("idHoso").getAsString();
					// nếu idHoso trong db trùng với idHoso được đưa vào
					if (checkIdHoso.equals(idHoso)) {
//						checkObject.put("idHoso", idHoso);
						// cập nhật thông tin object trùng idHoso
						if (checkObject.get("idQuanhe").getAsString().equals(idQuanhe)) {

						} else {
							checkObject.addProperty("idQuanhe", idQuanhe);
						}
						if (checkObject.get("nameQuanhe").getAsString().equals(nameQuanhe)) {

						} else {
							checkObject.addProperty("nameQuanhe", nameQuanhe);
						}
						if (checkObject.get("nghenghiepQuanhe").getAsString().equals(nghenghiepQuanhe)) {

						} else {
							checkObject.addProperty("nghenghiepQuanhe", nghenghiepQuanhe);
						}
						if (checkObject.get("sdtQuanhe").getAsString().equals(sdtQuanhe)) {

						} else {
							checkObject.addProperty("sdtQuanhe", sdtQuanhe);
						}
						if (checkObject.get("diachiQuanhe").getAsString().equals(sdtQuanhe)) {

						} else {
							checkObject.addProperty("diachiQuanhe", diachiQuanhe);
						}
						if (checkObject.get("macdinhQuanhe").getAsString().equals(macdinhQuanhe)) {

						} else {
							checkObject.addProperty("macdinhQuanhe", macdinhQuanhe);
						}
						// lưu jsonarray chứa jsonobject đã cập nhật vào hoso
//						checkArray.put(checkObject);
						check.setHo_so(checkArray.toString());
						SinhVienDao.updateStudent(check);
						// tăng count thành 1 khi update thành công
						countAdded += 1;
						status = "success";
					} else {
						// tăng lên 1 khi không trùng để update
						countNotEquals += 1;
					}
					// dừng vòng lặp khi count = 1
					if (countAdded == 1) {
						break;
					} else if (countAdded > 1) {
						status = "error";
					}
					// nếu count=0 < độ dài mảng 1 đơn vị thì add
					if (countNotEquals == (checkArray.size())) {

						JsonObject jsonObject1 = new JsonObject();
						jsonObject1.addProperty("idHoso", idHoso);
						jsonObject1.addProperty("idQuanhe", idQuanhe);
						jsonObject1.addProperty("nameQuanhe", nameQuanhe);
						jsonObject1.addProperty("nghenghiepQuanhe", nghenghiepQuanhe);
						jsonObject1.addProperty("sdtQuanhe", sdtQuanhe);
						jsonObject1.addProperty("diachiQuanhe", diachiQuanhe);
						jsonObject1.addProperty("macdinhQuanhe", macdinhQuanhe);

						// Add Object
						checkArray.add(jsonObject1);
						check.setHo_so(checkArray.toString());
						SinhVienDao.updateStudent(check);
						status = "success";
					} else if (countNotEquals >= checkArray.size()) {
						status = "error";
					}
				} else {
					status = "not found id";
				}
			}
			return new Gson().toJson(status);
		}
	}

	@RequestMapping(value = "/deleteQuanhe", method = RequestMethod.POST)
	public @ResponseBody String deleteQuanhe(String idStudent, String idHoso)
			throws ParseException, InterruptedException, ExecutionException, IOException {
		StudentDTO check = SinhVienDao.getStudentById(idStudent);
		String checkHoso = check.getHo_so();
		if (checkHoso == null || checkHoso.equals(null) || checkHoso.equals("") || checkHoso.equals("[]")) {
			return new Gson().toJson("nothing to delete");
		} else {
			JsonArray checkArray = new JsonArray();
			checkArray=new Gson().fromJson(checkHoso, JsonArray.class);
			for (int i = 0; i < checkArray.size(); i++) {
				JsonObject checkObject = checkArray.get(i).getAsJsonObject();
				if (checkObject.get("idHoso").getAsString().equals(idHoso)) {
					checkArray.remove(i);
					check.setHo_so(checkArray.toString());
					SinhVienDao.updateStudent(check);
					break;
				} else {

				}
			}
			return new Gson().toJson("delete success");
		}
	}

	@RequestMapping(value = "/checkMacdinhQuanhe", method = RequestMethod.POST)
	public @ResponseBody String checkMacdinhQuanhe(String idStudent, String idHoso, String macdinhQuanhe)
			throws ParseException, InterruptedException, ExecutionException, IOException {
		StudentDTO check = SinhVienDao.getStudentById(idStudent);
		String checkHoso = check.getHo_so();
		if (checkHoso == null || checkHoso.equals(null) || checkHoso.equals("") || checkHoso.equals("[]")) {
			return new Gson().toJson("nothing to set");
		} else {
//			String returnNameQuanhe = null;
			JsonArray checkArray = new JsonArray();
			checkArray=new Gson().fromJson(checkHoso, JsonArray.class);
//			String checkIdMacdinhIsTrue = null;
			// check từng object trong jsonarray
			if (macdinhQuanhe.equals("true")) {
				// set macdinhQuanhe hiện tại bằng false
				for (int i = 0; i < checkArray.size(); i++) {
					JsonObject checkObject = checkArray.get(i).getAsJsonObject();
					if (checkObject.get("macdinhQuanhe").getAsString().equals("true")) {
						checkObject.addProperty("macdinhQuanhe", "false");
						check.setHo_so(checkArray.toString());
						SinhVienDao.updateStudent(check);
					} else {

					}
				}
				// set macdinhQuanhe cho idHoso đưa vào
				for (int i = 0; i < checkArray.size(); i++) {
					JsonObject checkObject = checkArray.get(i).getAsJsonObject();
					if (checkObject.get("idHoso").getAsString().equals(idHoso)) {
						checkObject.addProperty("macdinhQuanhe", "true");
//						returnNameQuanhe = checkObject.getString("nameQuanhe");
						check.setHo_so(checkArray.toString());
						SinhVienDao.updateStudent(check);
//						returnNameQuanhe = checkObject.getString("nameQuanhe");
						break;
					} else {

					}
				}
				return new Gson().toJson("setting default success");
			} else {
				for (int i = 0; i < checkArray.size(); i++) {
					JsonObject checkObject = checkArray.get(i).getAsJsonObject();
					if (checkObject.get("idHoso").getAsString().equals(idHoso)) {
						checkObject.addProperty("macdinhQuanhe", "false");
//						returnNameQuanhe = checkObject.getString("nameQuanhe");
						check.setHo_so(checkArray.toString());
						SinhVienDao.updateStudent(check);
//						returnNameQuanhe = checkObject.getString("nameQuanhe");
						break;
					} else {

					}
				}
				return new Gson().toJson("cancel setting default success");
			}

		}
	}

	@RequestMapping(value = "/getListStudentStatusByStudentId", method = RequestMethod.GET)
	public @ResponseBody String getListStudentStatusByStudentId(HttpServletRequest request, String id_student)
			throws ParseException {
		// ObjectMapper mapper = new ObjectMapper();
		String ajaxResponse = "";
		try {
			// ajaxResponse = mapper.writeValueAsString(person);
			List<StatusStudentDTO> statusStudents = StatusStudentDao.getListStudentStatusByStudentId(id_student);
			if (statusStudents == null) {
				ajaxResponse = "error%Mã sinh viên chưa tồn tại";
			} else {
				SimpleDateFormat smp = new SimpleDateFormat("yyyy-MM-dd");
				SimpleDateFormat smf = new SimpleDateFormat("dd/MM/yyyy");
				for (int i = 0; i < statusStudents.size(); i++) {
					statusStudents.get(i).setStart_date(smf.format((smp.parse(statusStudents.get(i).getStart_date()))));
					statusStudents.get(i).setEnd_date(smf.format((smp.parse(statusStudents.get(i).getEnd_date()))));
				}
				ajaxResponse = new Gson().toJson(statusStudents);
			}
		} catch (InterruptedException | ExecutionException | IOException e) {
			e.printStackTrace();
		}
		return ajaxResponse;
	}

	@RequestMapping(value = "/checkId", method = RequestMethod.POST)
	public @ResponseBody String searchPerson(HttpServletRequest request, String id_student) {
		// ObjectMapper mapper = new ObjectMapper();
		String ajaxResponse = "";
		try {
			// ajaxResponse = mapper.writeValueAsString(person);
			StudentDTO check = SinhVienDao.getStudentById(id_student);
			if (check == null) {
				ajaxResponse = "not exist";
			} else {
				ajaxResponse = "existed";
			}
		} catch (InterruptedException | ExecutionException | IOException e) {
			e.printStackTrace();
		}
		return ajaxResponse;
	}

	@RequestMapping(value = "/checkEmail", method = RequestMethod.POST)
	public @ResponseBody String checkEmail(HttpServletRequest request, String email_school) {
		// ObjectMapper mapper = new ObjectMapper();
		String ajaxResponse = "";
		try {
			// ajaxResponse = mapper.writeValueAsString(person);
			StudentDTO check = SinhVienDao.getStudentBySchoolEmail(email_school);
			if (check == null) {
				ajaxResponse = "not exist";
			} else {
				ajaxResponse = "existed";
			}
		} catch (InterruptedException | ExecutionException | IOException e) {
			e.printStackTrace();
		}
		return ajaxResponse;
	}

	@RequestMapping(value = "/getStudentById", method = RequestMethod.GET)
	public @ResponseBody String getStudentById(HttpServletRequest request, String id) {
		// ObjectMapper mapper = new ObjectMapper();
		String ajaxResponse = null;
		try {
			// ajaxResponse = mapper.writeValueAsString(person);
			StudentDTO check = SinhVienDao.getStudentById(id);
			if (check == null) {
				ajaxResponse = "error%Không có học sinh này";
			} else {
				ajaxResponse = new Gson().toJson(check);
			}
		} catch (InterruptedException | ExecutionException | IOException e) {
			e.printStackTrace();
		}
		return ajaxResponse;
	}

	@RequestMapping(value = "/getLatestStatusStudentByStudentIdAndStatusId", method = RequestMethod.GET)
	public @ResponseBody String getStudentStatusById(HttpServletRequest request, String student_id, String status_id) {
		// ObjectMapper mapper = new ObjectMapper();
		String ajaxResponse = null;
		try {
			// ajaxResponse = mapper.writeValueAsString(person);
			StatusStudentDTO check = StatusStudentDao.getLatestStatusStudentByStudentIdAndStatusId(student_id,
					status_id);
			if (check == null) {
				ajaxResponse = "error%Không có tình trạng học sinh này";
			} else {
				ajaxResponse = new Gson().toJson(check);
			}
		} catch (InterruptedException | ExecutionException | IOException e) {
			e.printStackTrace();
		}
		return ajaxResponse;
	}

	@RequestMapping(value = "/checkClassValid", method = RequestMethod.GET)
	public @ResponseBody String checkClassValid(HttpServletRequest request, String class_id) {
		// ObjectMapper mapper = new ObjectMapper();
		String ajaxResponse = null;
		try {
			// ajaxResponse = mapper.writeValueAsString(person);
			ClassDTO classStudent = LopDao.getClassById(class_id);
			System.out.println(classStudent);
			if (classStudent == null) {
				ajaxResponse = "error%Không có lớp  này";
			} else {
				Integer res = (Integer.parseInt(classStudent.getSlot_total())
						- Integer.parseInt(classStudent.getSlot_regis()));
				if (res <= 0) {
					ajaxResponse = "errorfull% Lớp này đã hết chỗ!";
				} else {
					ajaxResponse = "success%Lớp còn " + String.valueOf(res) + " chỗ!";
				}
			}
		} catch (InterruptedException | ExecutionException | IOException e) {
			e.printStackTrace();
		}
		return ajaxResponse;
	}

	@RequestMapping(value = "/updateStatusStudent", method = RequestMethod.POST)
	public @ResponseBody String checkEmail(HttpServletRequest request, StatusStudentDTO statusStudentData) {
		// ObjectMapper mapper = new ObjectMapper();
		String ajaxResponse = "";
		try {
			statusStudentData.setCreator("1");

			// ajaxResponse = mapper.writeValueAsString(person);
//			StudentDTO student = SinhVienDao.getStudentById(statusStudentData.getStudent_id());

			Timestamp timestamp = new Timestamp(System.currentTimeMillis());
			statusStudentData.setDate_create(String.valueOf(timestamp));

			List<String> formatStrings = Arrays.asList("dd/MM/yyyy", "yyyy-MM-dd");
			java.util.Date utilStart_date = null;
			java.util.Date utilEnd_date = null;
			for (String formatString : formatStrings) {
				try {
					utilStart_date = new SimpleDateFormat(formatString).parse(statusStudentData.getStart_date());
					System.out.println(formatString + " " + utilStart_date.toString());
					utilEnd_date = new SimpleDateFormat(formatString).parse(statusStudentData.getEnd_date());
					System.out.println(formatString + " " + utilEnd_date.toString());
				} catch (ParseException e) {
				}
			}
			java.sql.Date sqlStart_date = new java.sql.Date(utilStart_date.getTime());
			statusStudentData.setStart_date(String.valueOf(sqlStart_date));
			java.sql.Date sqlEnd_date = new java.sql.Date(utilEnd_date.getTime());
			statusStudentData.setEnd_date(String.valueOf(sqlEnd_date));

			StatusStudentDTO statusStudent = StatusStudentDao.create(statusStudentData);

			StudentDTO check = SinhVienDao.getStudentById(statusStudentData.getStudent_id());
			check.setStatus_id(statusStudentData.getStatus_id());
			SinhVienDao.updateStudent(check);
			if (statusStudent == null) {
				ajaxResponse = "error%Cập nhật không thành công";
			} else {
				ajaxResponse = "success%Cập nhật thành công";
			}
		} catch (InterruptedException | ExecutionException | IOException e) {
			e.printStackTrace();
		}
		return ajaxResponse;
	}

	@RequestMapping(value = "/importFromExcel", method = RequestMethod.POST)
	public @ResponseBody String importFromExcel(HttpServletRequest request,
			@RequestParam("file") MultipartFile readExcelDataFile)
			throws IOException, InterruptedException, ExecutionException, ParseException {
		String data = new String();

		Workbook workbook = new XSSFWorkbook(readExcelDataFile.getInputStream());
//		Workbook workbook = new HSSFWorkbook(readExcelDataFile.getInputStream());

		Sheet firstSheet = workbook.getSheetAt(0);
//		Iterator<Row> rowIterator = firstSheet.iterator();
//		rowIterator.next();

		int row = 1;
		// check khi nào còn row data trong excel file
//		while (rowIterator.hasNext()) {
		int rowNum = 0;
		for (Row r : firstSheet) {
			int checkInvalid = 0;
//			Row nextRow = rowIterator.next();
//			Iterator<Cell> cellIterator = nextRow.cellIterator();
			Boolean checkId_student = false;
			StudentDTO student = new StudentDTO();
			StudentDTO checkStudent = new StudentDTO();
			ClassDTO checkClass = new ClassDTO();
			ClassDTO checkOldClass = new ClassDTO();
			String rowId = null;
			String id_student = new String();
			String first_name = new String();
			String last_name = new String();
			String full_name = new String();
			String active_account = new String();
			String current_class = new String();
			String date_of_doing = new String();
			String sex = new String();
			String dob = new String();
			String mobile_phone = new String();
			String home_phone = new String();
			String contact_phone = new String();
			String email_student = new String();
			String email_school = new String();
			String password = new String();
			String address = new String();
			String contact_address = new String();
			String application_date = new String();
			String district = new String();
			String city = new String();
			String cs = new String();
			String course_id = new String();
			String course_family = new String();
			String high_school = new String();
			String university = new String();
			String temp_id = new String();
			String centre_name = new String();
			String role_id = new String();
			// check khi nào còn cell data trong row data hiện tại
			int lastCellIndex = 0;
			int columnIndex = 0;
			rowNum = r.getRowNum();
			if (rowNum <= 0) {

			} else {
//				while (cellIterator.hasNext()) {
				for (int cn = 0; cn < r.getLastCellNum(); cn++) {
					DataFormatter formatter = new DataFormatter();
//					Cell nextCell = cellIterator.next();
					Cell nextCell = r.getCell(cn, Row.MissingCellPolicy.CREATE_NULL_AS_BLANK);
					String checkNullNextCell = formatter.formatCellValue(nextCell);
					if (checkNullNextCell == null || checkNullNextCell.equals("")) {
						columnIndex = lastCellIndex + 1;
					} else {
						columnIndex = nextCell.getColumnIndex();
					}
					lastCellIndex = columnIndex;
					// switch case check từng cell data
					switch (columnIndex) {
					case 0:
						id_student = nextCell.getStringCellValue();
						if (id_student == null || id_student.equals("")) {
							checkInvalid += 1;
							data = "Please enter id_student for the " + String.valueOf(row)
									+ "th row in the import file!\n";
						} else {
							System.out.println("id_student: " + String.valueOf(id_student));
							rowId = id_student;
							checkStudent = SinhVienDao.getStudentById(id_student);
							if (checkStudent == null) {
								checkId_student = false;
								student.setId_student(id_student);
							} else {
								checkId_student = true;
							}
						}
						break;
					case 1:
						first_name = nextCell.getStringCellValue();
						if (first_name == null || first_name.equals("")) {
							checkInvalid += 1;
							data = "Please enter first_name for the student whose id_student is " + rowId
									+ " in the import file!\n";
						} else {
							if (checkId_student == false) {
								student.setFirst_name(first_name);
							} else {
								checkStudent.setFirst_name(first_name);
							}
						}
						break;
					case 2:
						last_name = nextCell.getStringCellValue();
						if (last_name == null || last_name.equals("")) {
							checkInvalid += 1;
							data = "Please enter last_name for the student whose id_student is " + rowId
									+ " in the import file!\n";
						} else {
							if (checkId_student == false) {
								student.setLast_name(last_name);
							} else {
								checkStudent.setLast_name(last_name);
							}
						}
						break;
					case 3:
						full_name = nextCell.getStringCellValue();
						if (full_name == null || full_name.equals("")) {
							checkInvalid += 1;
							data = "Please enter full_name for the student whose id_student is " + rowId
									+ " in the import file!\n";
						} else {
							if (checkId_student == false) {
								student.setFull_name(full_name);
							} else {
								checkStudent.setFull_name(full_name);
							}
						}
						break;
					case 4:
						active_account = formatter.formatCellValue(nextCell);
						if (active_account.equals("true") || active_account.equals("false")
								|| active_account.equals("TRUE") || active_account.equals("FALSE")) {
							if (checkId_student == false) {
								student.setActive_account(active_account);
							} else {
								checkStudent.setActive_account(active_account);
							}
						} else {
							checkInvalid += 1;
							data = "Please enter valid active_account for the student whose id_student is " + rowId
									+ " in the import file!\nValid active_account type is:[True] is enable/[False] is disable!\n";
						}
						break;
					case 5:
						current_class = nextCell.getStringCellValue();
						if (current_class == null || current_class.equals("")) {
							checkInvalid += 1;
							data = "Please enter current_class for the student whose id_student is " + rowId
									+ " in the import file!\n";
						} else {
							checkClass = LopDao.getClassById(current_class);
							if (checkClass == null) {
								checkInvalid += 1;
								data = "Cannot find current_class " + current_class
										+ " of student whose id_student is " + rowId + " in the import file!\n";
							} else {
								Integer res = (Integer.parseInt(checkClass.getSlot_total())
										- Integer.parseInt(checkClass.getSlot_regis()));

								if (checkId_student == false) {
									if (res <= 0) {
										checkInvalid += 1;
										data = "Current_class " + current_class + " of student whose id_student is "
												+ rowId
												+ " in the import file has run out, please choose another class!\n";
									} else {
										student.setFirst_class(current_class);
										student.setCurrent_class(current_class);
										checkClass.setSlot_regis(
												String.valueOf(Integer.valueOf(checkClass.getSlot_regis()) + 1));
									}
								} else {
									if (checkStudent.getCurrent_class() == null
											|| checkStudent.getCurrent_class().equals("")) {
										if (res <= 0) {
											checkInvalid += 1;
											data = "Current_class " + current_class
													+ " of student whose id_student is " + rowId
													+ " in the import file has run out, please choose another class!\n";
										} else {
											if (checkStudent.getFirst_class().equals("")
													|| checkStudent.getFirst_class() == null
													|| checkStudent.getFirst_class().equals(null)) {
												checkStudent.setFirst_class(current_class);
												checkStudent.setCurrent_class(current_class);
											} else {
												checkStudent.setCurrent_class(current_class);
											}
											checkClass.setSlot_regis(
													String.valueOf(Integer.valueOf(checkClass.getSlot_regis()) + 1));
										}
									} else {
										checkOldClass = LopDao.getClassById(checkStudent.getCurrent_class());
										if (checkStudent.getCurrent_class().equals(current_class)) {

										} else {
											if (res <= 0) {
												checkInvalid += 1;
												data = "Current_class " + current_class
														+ " of student whose id_student is " + rowId
														+ " in the import file has run out, please choose another class!\n";
											} else {

												checkOldClass.setSlot_regis(String
														.valueOf(Integer.valueOf(checkOldClass.getSlot_regis()) - 1));
												if (checkStudent.getFirst_class().equals("")
														|| checkStudent.getFirst_class() == null
														|| checkStudent.getFirst_class().equals(null)) {
													checkStudent.setFirst_class(current_class);
													checkStudent.setCurrent_class(current_class);
												} else {
													checkStudent.setCurrent_class(current_class);
												}
												checkClass.setSlot_regis(String
														.valueOf(Integer.valueOf(checkClass.getSlot_regis()) + 1));
											}
										}
									}
								}
							}
						}
						break;
					case 6:
//						Date date_of_doing_value = nextCell.getDateCellValue();
//						java.sql.Date sqlDate_of_doing = new java.sql.Date(date_of_doing_value.getTime());
						Date formatDate_of_doing = new SimpleDateFormat("dd/MM/yyyy")
								.parse(formatter.formatCellValue(nextCell));
						java.sql.Date sqlDate_of_doing = new java.sql.Date(formatDate_of_doing.getTime());
						date_of_doing = String.valueOf(sqlDate_of_doing);

						if (date_of_doing == null || date_of_doing.equals("")) {
							checkInvalid += 1;
							data = "Please enter valid date_of_doing for the student whose id_student is " + rowId
									+ " in the import file!\nValid date_of_doing type is: Day/Month/Year!\n";
						} else {
							if (checkId_student == false) {
								student.setDate_of_doing(date_of_doing);
							} else {
								checkStudent.setDate_of_doing(date_of_doing);
							}
						}
						break;
					case 7:
						sex = null;
						if (nextCell.getCellType() == CellType.STRING) {
							if (nextCell.getStringCellValue().equals("Nam")
									|| nextCell.getStringCellValue().equals("Male")) {
								sex = "true";
							} else if (nextCell.getStringCellValue().equals("Nữ")
									|| nextCell.getStringCellValue().equals("Female")) {
								sex = "false";
							} else {
								checkInvalid += 1;
								data = "Please enter valid sex for the student whose id_student is " + rowId
										+ " in the import file!\nValid sex is:[Nam/Male]/[Nữ/Female]!\n";
							}
						} else if (nextCell.getCellType() == CellType.NUMERIC) {
							if (Integer.valueOf(String.valueOf(nextCell.getNumericCellValue())).equals(1)) {
								sex = "true";
							} else if (Integer.valueOf(String.valueOf(nextCell.getNumericCellValue())).equals(0)) {
								sex = "false";
							} else {
								checkInvalid += 1;
								data = "Please enter valid sex for the student whose id_student is " + rowId
										+ " in the import file!\nValid sex is:[1] is male/[0] is female!\n";
							}
						} else if (nextCell.getCellType() == CellType.BOOLEAN) {
							sex = String.valueOf(nextCell.getBooleanCellValue());
						} else {
							checkInvalid += 1;
							data = "Please enter valid sex for the student whose id_student is " + rowId
									+ " in the import file!\nValid sex is:[Nam/Male/1] is male/[Nữ/Female/0] is female!\n";
						}
						if (checkInvalid > 0) {

						} else {
							if (checkId_student == false) {
								student.setSex(sex);
							} else {
								checkStudent.setSex(sex);
							}
						}
						break;
					case 8:
						Date formatDob = new SimpleDateFormat("dd/MM/yyyy").parse(formatter.formatCellValue(nextCell));
						java.sql.Date sqlDob = new java.sql.Date(formatDob.getTime());
						dob = String.valueOf(sqlDob);
						if (dob == null || dob.equals("")) {
							checkInvalid += 1;
							data = "Please enter valid dob for the student whose id_student is " + rowId
									+ " in the import file!\nValid dob type is: [Day/Month/Year]!\n";
						} else {

							if (checkId_student == false) {
								student.setDob(dob);
							} else {
								checkStudent.setDob(dob);
							}
						}
						break;
					case 9:
						mobile_phone = formatter.formatCellValue(nextCell);
						if (mobile_phone == null || mobile_phone.equals("")) {
							if (checkId_student == false) {
								student.setMobile_phone("");
							} else {
								checkStudent.setMobile_phone("");
							}
						} else {
							if (checkId_student == false) {
								student.setMobile_phone(mobile_phone);
							} else {
								checkStudent.setMobile_phone(mobile_phone);
							}
						}
						break;
					case 10:
						home_phone = formatter.formatCellValue(nextCell);
						if (home_phone == null || home_phone.equals("")) {
							if (checkId_student == false) {
								student.setHome_phone("");
							} else {
								checkStudent.setHome_phone("");
							}
						} else {
							if (checkId_student == false) {
								student.setHome_phone(home_phone);
							} else {
								checkStudent.setHome_phone(home_phone);
							}
						}
						break;
					case 11:
						contact_phone = formatter.formatCellValue(nextCell);
						if (contact_phone == null || contact_phone.equals("")) {
							checkInvalid += 1;
							data = "Please enter contact_phone for the student whose id_student is " + rowId
									+ " in the import file!\n";
						} else {
							if (checkId_student == false) {
								student.setContact_phone(contact_phone);
							} else {
								checkStudent.setContact_phone(contact_phone);
							}
						}
						break;
					case 12:
						email_student = formatter.formatCellValue(nextCell);
						if (email_student == null || email_student.equals("")) {
							if (checkId_student == false) {
								student.setEmail_student("");
							} else {
								checkStudent.setEmail_student("");
							}
						} else {
							if (checkId_student == false) {
								student.setEmail_student(email_student);
							} else {
								checkStudent.setEmail_student(email_student);
							}
						}
						break;
					case 13:
						email_school = formatter.formatCellValue(nextCell);
						if (checkId_student == false) {
							if (email_school == null || email_school.equals("")) {
								student.setEmail_school("");
							} else {
								StudentDTO check = SinhVienDao.getStudentBySchoolEmail(email_school);
								if (check == null) {
									student.setEmail_school(email_school);
								} else {
									checkInvalid += 1;
									data = "Email school of the student whose id_student is " + rowId
											+ " has existed, please choose another email!\n";
								}
							}
						} else {
							if (email_school == null || email_school.equals("")) {
								checkStudent.setEmail_school("");
							} else {
								if (checkStudent.getEmail_school().equals(email_school)) {

								} else {
									StudentDTO check = SinhVienDao.getStudentBySchoolEmail(email_school);
									if (check == null) {
										checkStudent.setEmail_school(email_school);
									} else {
										checkInvalid += 1;
										data = "Email school of the student whose id_student is " + rowId
												+ " has existed, please choose another email!\n";
									}
								}
							}
						}

						break;
					case 14:
						password = formatter.formatCellValue(nextCell);
						if (checkId_student == false) {
							if (password == null || password.equals("")) {
								student.setPassword(MD5.GenerateMD5("aptech123"));
							} else {
								student.setPassword(MD5.GenerateMD5(password));
							}
						} else {
							if (password == null || password.equals("")) {
								checkStudent.setPassword(MD5.GenerateMD5("aptech123"));
							} else {
								checkStudent.setPassword(MD5.GenerateMD5(password));
							}
						}
						break;

					case 15:
						address = formatter.formatCellValue(nextCell);
						if (address == null || address.equals("")) {
							if (checkId_student == false) {
								student.setAddress("");
							} else {
								checkStudent.setAddress("");
							}
						} else {
							if (checkId_student == false) {
								student.setAddress(address);
							} else {
								checkStudent.setAddress(address);
							}
						}
						break;
					case 16:
						contact_address = formatter.formatCellValue(nextCell);
						if (contact_address == null || contact_address.equals("")) {
							if (checkId_student == false) {
								student.setContact_address("");
							} else {
								checkStudent.setContact_address("");
							}
						} else {
							if (checkId_student == false) {
								student.setContact_address(contact_address);
							} else {
								checkStudent.setContact_address(contact_address);
							}
						}
						break;
					case 17:
						Date formatApplication_date = new SimpleDateFormat("dd/MM/yyyy")
								.parse(formatter.formatCellValue(nextCell));
						java.sql.Date sqlApplication_date = new java.sql.Date(formatApplication_date.getTime());
						application_date = String.valueOf(sqlApplication_date);
						if (application_date == null || String.valueOf(application_date).equals("")) {
							checkInvalid += 1;
							data = "Please enter valid application_date for the student whose id_student is " + rowId
									+ " in the import file!\nValid application_date type is: [Day/Month/Year]!\n";
						} else {
							if (checkId_student == false) {
								student.setApplication_date(application_date);
							} else {
								checkStudent.setApplication_date(application_date);
							}
						}
						break;
					case 18:
						district = formatter.formatCellValue(nextCell);
						if (district == null || district.equals("")) {
							if (checkId_student == false) {
								student.setDistrict("");
							} else {
								checkStudent.setDistrict("");
							}
						} else {
							if (checkId_student == false) {
								student.setDistrict(district);
							} else {
								checkStudent.setDistrict(district);
							}
						}
						break;
					case 19:
						city = formatter.formatCellValue(nextCell);
						if (city == null || city.equals("")) {
							if (checkId_student == false) {
								student.setCity("");
							} else {
								checkStudent.setCity("");
							}
						} else {
							if (checkId_student == false) {
								student.setCity(city);
							} else {
								checkStudent.setCity(city);
							}
						}
						break;
					case 20:
						cs = formatter.formatCellValue(nextCell);
						if (cs == null || cs.equals("")) {
							if (checkId_student == false) {
								student.setCs("");
							} else {
								checkStudent.setCs("");
							}
						} else {
							if (checkId_student == false) {
								student.setCs(cs);
							} else {
								checkStudent.setCs(cs);
							}
						}
						break;
					case 21:
						course_id = formatter.formatCellValue(nextCell);
						if (course_id == null || course_id.equals("")) {
							if (checkId_student == false) {
								student.setCourse_id("");
							} else {
								checkStudent.setCourse_id("");
							}
						} else {
							CourseDTO checkCourse = KhoaHocDao.getCourseById(course_id);
							if (checkCourse == null) {
								checkInvalid += 1;
								data = "Cannot find course_id " + course_id + " of the whose id_student is " + rowId
										+ " in the import file!\n";
							} else {
								if (checkId_student == false) {
									student.setCourse_id(course_id);
								} else {
									checkStudent.setCourse_id(course_id);
								}
							}
						}
						break;
					case 22:
						course_family = formatter.formatCellValue(nextCell);
						if (course_id.equals("") || course_id.equals(null) || course_id == null) {
							if (course_family.equals("") || course_family.equals(null) || course_family == null) {
								if (checkId_student == false) {
									student.setCourse_family("");
								} else {
									checkStudent.setCourse_family("");
								}
							} else {
								checkInvalid += 1;
								data = "Cannot insert course_family " + course_family
										+ " because not found course_id of the whose id_student is " + rowId
										+ " in the import file!\n";
							}
						} else {
							if (course_family == null || course_family.equals("")) {
								if (checkId_student == false) {
									student.setCourse_family("");
								} else {
									checkStudent.setCourse_family("");
								}
							} else {
								List<CourseDTO> checkCourse = KhoaHocDao.getCoursByCourseroot(course_id);
								int i = 0;
								String checkCourse_family = new String();
								while (i < checkCourse.size()) {
									if (checkCourse.get(i).getId_course().equals(course_family)) {
										checkCourse_family = course_family;
										break;
									} else {

									}
									i++;
								}
								if (checkCourse_family == null || checkCourse_family.equals("")) {
									checkInvalid += 1;
									data = "Cannot insert course_family " + course_family
											+ " because it doesn't belong to course_id " + course_id
											+ " of the whose id_student is " + rowId + " in the import file!\n";
								} else {
									if (checkId_student == false) {
										student.setCourse_family(course_family);
									} else {
										checkStudent.setCourse_family(course_family);
									}
								}
							}
						}
						break;
					case 23:
						high_school = formatter.formatCellValue(nextCell);
						if (high_school == null || high_school.equals("")) {
							if (checkId_student == false) {
								student.setHigh_school("");
							} else {
								checkStudent.setHigh_school("");
							}
						} else {
							if (checkId_student == false) {
								student.setHigh_school(high_school);
							} else {
								checkStudent.setHigh_school(high_school);
							}
						}
						break;

					case 24:
						university = formatter.formatCellValue(nextCell);
						if (university == null || university.equals("")) {
							if (checkId_student == false) {
								student.setUniversity("");
							} else {
								checkStudent.setUniversity("");
							}
						} else {
							if (checkId_student == false) {
								student.setUniversity(university);
							} else {
								checkStudent.setUniversity(university);
							}
						}
						break;

					case 25:
						temp_id = formatter.formatCellValue(nextCell);
						if (temp_id == null || temp_id.equals("")) {
							if (checkId_student == false) {
								student.setTemp_id("");
							} else {
								checkStudent.setTemp_id("");
							}
						} else {
							if (checkId_student == false) {
								student.setTemp_id(temp_id);
							} else {
								checkStudent.setTemp_id(temp_id);
							}
						}
						break;
					case 26:
						centre_name = formatter.formatCellValue(nextCell);
						if (centre_name == null || centre_name.equals("")) {
							if (checkId_student == false) {
								student.setCentre_name("");
							} else {
								checkStudent.setCentre_name("");
							}
						} else {
							if (checkId_student == false) {
								student.setCentre_name(centre_name);
							} else {
								checkStudent.setCentre_name(centre_name);
							}
						}
						break;
					case 27:
						role_id = formatter.formatCellValue(nextCell);
						if (role_id == null || role_id.equals("")) {
							if (checkId_student == false) {
								student.setRole_id("");
							} else {
								checkStudent.setRole_id("");
							}
						} else {
							if (checkId_student == false) {
								student.setRole_id(role_id);
							} else {
								checkStudent.setRole_id(role_id);
							}
						}
						break;
					}
					// end check từng cell data
				}
				// end check row data hiện tại
				// kiểm tra số lượng invalid, >0 thì
				System.out.println(checkInvalid);
				if (checkInvalid > 0) {
					System.out.println(data);
					break;
				} else {
					if (checkId_student == false) {
						student.setRole_id("1");
						student.setCentre_name("aptech");
						student.setActive_account("true");
						SinhVienDao.createStudent(student);
						LopDao.update(checkClass);
					} else {
						SinhVienDao.updateStudent(checkStudent);
						LopDao.update(checkClass);
						if (checkOldClass == null) {

						} else {
							LopDao.update(checkOldClass);
						}
					}
					System.out.println("inserted id_student:" + rowId);
					checkId_student = false;
					row += 1;
					data = "success";
				}
			}
		}
		workbook.close();
		return data;
	}
	
	@RequestMapping(value = "/exportToExcel", method = RequestMethod.GET)
	public void downloadExportExcel(HttpServletResponse response,HttpSession session) throws IOException, InterruptedException, ExecutionException, ParseException {
		long startTime = System.nanoTime();
		response.setContentType("application/octet-stream");
		response.setHeader("Content-Disposition","attachment; filename=exportStudents.xlsx");
		//declare new byte type array input stream(array data) and get it from result of method exportToExcel() below
		ByteArrayInputStream bais=exportToExcel(session);
		//transfer array data to response(read byte type array data input stream and send data outside)
		bais.transferTo(response.getOutputStream());	
        long elapsedTime = (System.nanoTime() - startTime);
     // nano time divine(%) 1.000.000.000(1 billion) to second time 
        System.out.println("download Excel: "+elapsedTime/1000000000+" seconds");
     // create a cookie
        Cookie cookie = new Cookie("DownloadCompleteChecker", null);

        // expires yesterday days
        cookie.setMaxAge(-1 * 24 * 60 * 60);

        // optional properties
        cookie.setHttpOnly(false);

        // add cookie to response
        response.addCookie(cookie);


        // return response entity
//        return new ResponseEntity<>(response,HttpStatus.OK);
	}

	public ByteArrayInputStream exportToExcel(HttpSession session)
			throws IOException, InterruptedException, ExecutionException, ParseException {
		String defaultImage="dealftUser.png";
		Workbook workbook;
		Sheet sheet = null;
//		String excelFilePath = "exportStudents.xlsx";
		int rowCount = 1;
		try {
//			File deleteFile = new File(excelFilePath);
//			if (deleteFile.delete()) {
//				System.out.println("File " + deleteFile.getName() + " is deleted.");
//			} else {
//				System.out.println("File " + deleteFile.getName() + " not found.");
//			}
			workbook = new XSSFWorkbook();
			sheet = workbook.createSheet("Students");
			sheet.setDefaultColumnWidth(20);

//			int rowCount = sheet.getLastRowNum();

			Row headerRow = sheet.createRow(0);

			Cell headerCell = headerRow.createCell(0);
			CellStyle headerCellStyle=sheet.getWorkbook().createCellStyle();
			headerCellStyle.setFillForegroundColor(IndexedColors.AQUA.getIndex());
			headerCellStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
			headerCell.setCellStyle(headerCellStyle);
			headerCell.setCellValue("Id student");

			headerCell = headerRow.createCell(1);
			headerCell.setCellStyle(headerCellStyle);
			headerCell.setCellValue("Image student");

			headerCell = headerRow.createCell(2);
			headerCell.setCellStyle(headerCellStyle);
			headerCell.setCellValue("Student name");

			headerCell = headerRow.createCell(3);
			headerCell.setCellStyle(headerCellStyle);
			headerCell.setCellValue("Class");

			headerCell = headerRow.createCell(4);
			headerCell.setCellStyle(headerCellStyle);
			headerCell.setCellValue("Course");

			headerCell = headerRow.createCell(5);
			headerCell.setCellStyle(headerCellStyle);
			headerCell.setCellValue("Course family");

			headerCell = headerRow.createCell(6);
			headerCell.setCellStyle(headerCellStyle);
			headerCell.setCellValue("Phone number");

			headerCell = headerRow.createCell(7);
			headerCell.setCellStyle(headerCellStyle);
			headerCell.setCellValue("Email student");

			headerCell = headerRow.createCell(8);
			headerCell.setCellStyle(headerCellStyle);
			headerCell.setCellValue("Email school");

			headerCell = headerRow.createCell(9);
			headerCell.setCellStyle(headerCellStyle);
			headerCell.setCellValue("Date of doing");

			headerCell = headerRow.createCell(10);
			headerCell.setCellStyle(headerCellStyle);
			headerCell.setCellValue("Status");

			List<StudentDTO> listStudents = SinhVienDao.getAllStudent();
			for (int i = 0; i < listStudents.size(); i++) {
				Row row = sheet.createRow(rowCount++);

				row.setHeightInPoints(100);

				int columnCount = 0;

				Cell cell = row.createCell(columnCount++);
				cell.setCellValue(
						listStudents.get(i).getId_student() == null || listStudents.get(i).getId_student().equals("")
								? "No id student"
								: listStudents.get(i).getId_student());

				// ============= Inserting image - START
				/* Read input PNG / JPG Image into FileInputStream Object */
				CreationHelper helper = workbook.getCreationHelper();
				
				String path=session.getServletContext().getRealPath("/static/upload/avatar/");
				
				InputStream my_banner_image = null;
				if (listStudents.get(i).getImage_student() == null
						|| listStudents.get(i).getImage_student().equals("")) {
					String fileName = defaultImage;
					String fullPath=(path+fileName).replace('\\', '/');
					my_banner_image = new FileInputStream(fullPath);
				} else {
					String fileName = listStudents.get(i).getImage_student();
					String fullPath=(path+fileName).replace('\\', '/');
					my_banner_image = new FileInputStream(fullPath);
				}

				/* Convert picture to be added into a byte array */
				byte[] bytes = IOUtils.toByteArray(my_banner_image);
				/* Add Picture to Workbook, Specify picture type as PNG and Get an Index */
				int my_picture_id = workbook.addPicture(bytes, Workbook.PICTURE_TYPE_PNG);
				/* Close the InputStream. We are ready to attach the image to workbook now */
				my_banner_image.close();
				/* Create the drawing container */
//				XSSFDrawing drawing = (XSSFDrawing) sheet.createDrawingPatriarch();
				Drawing<?> drawing = sheet.createDrawingPatriarch();
				/* Create an anchor point */
				// ============= Inserting image - END

				// ========adding image START
//				XSSFClientAnchor my_anchor = new XSSFClientAnchor();
				ClientAnchor my_anchor = helper.createClientAnchor();
				/* Define top left corner, and we can resize picture suitable from there */

				my_anchor.setCol1(columnCount++);
				my_anchor.setRow1(rowCount - 1);

				/* Invoke createPicture and pass the anchor point and ID */
//				XSSFPicture my_picture = drawing.createPicture(my_anchor, my_picture_id);
				Picture pict = drawing.createPicture(my_anchor, my_picture_id);
				pict.resize(1, 1);
				// ========adding image END

				cell = row.createCell(columnCount++);
				cell.setCellValue(
						listStudents.get(i).getFull_name() == null || listStudents.get(i).getFull_name().equals("")
								? "No name"
								: listStudents.get(i).getFull_name());

				cell = row.createCell(columnCount++);
				if (listStudents.get(i).getCurrent_class() == null
						|| listStudents.get(i).getCurrent_class().equals("")) {
					cell.setCellValue("No class");
				} else {
					ClassDTO check_class = LopDao.getClassById(listStudents.get(i).getCurrent_class());
					if (check_class == null) {
						cell.setCellValue("No class");
					} else {
						String name_class = LopDao.getClassById(listStudents.get(i).getCurrent_class()).getName_class();
						cell.setCellValue(name_class);
					}
				}

				cell = row.createCell(columnCount++);
				if (listStudents.get(i).getCourse_id() == null || listStudents.get(i).getCourse_id().equals("")) {
					cell.setCellValue("No course");
				} else {
					CourseDTO check_course = KhoaHocDao.getCourseById(listStudents.get(i).getCourse_id());
					if (check_course == null) {
						cell.setCellValue("No course");
					} else {
						String name_course = KhoaHocDao.getCourseById(listStudents.get(i).getCourse_id())
								.getName_course();
						cell.setCellValue(name_course);
					}
				}

				cell = row.createCell(columnCount++);
				if (listStudents.get(i).getCourse_family() == null
						|| listStudents.get(i).getCourse_family().equals("")) {
					cell.setCellValue("No course family");
				} else {
					CourseDTO check_course_family = KhoaHocDao.getCourseById(listStudents.get(i).getCourse_family());
					if (check_course_family == null) {
						cell.setCellValue("No course family");
					} else {
						String name_course = KhoaHocDao.getCourseById(listStudents.get(i).getCourse_family())
								.getName_course();
						cell.setCellValue(name_course);
					}
				}

				cell = row.createCell(columnCount++);
				cell.setCellValue(listStudents.get(i).getContact_phone() == null
						|| listStudents.get(i).getContact_phone().equals("") ? "No contact phone"
								: listStudents.get(i).getContact_phone());

				cell = row.createCell(columnCount++);
				cell.setCellValue(listStudents.get(i).getEmail_student() == null
						|| listStudents.get(i).getEmail_student().equals("") ? "No email student"
								: listStudents.get(i).getEmail_student());

				cell = row.createCell(columnCount++);
				cell.setCellValue(listStudents.get(i).getEmail_school() == null
						|| listStudents.get(i).getEmail_school().equals("") ? "No email school"
								: listStudents.get(i).getEmail_school());

				cell = row.createCell(columnCount++);
				cell.setCellValue(listStudents.get(i).getDate_of_doing() == null
						|| listStudents.get(i).getDate_of_doing().equals("") ? "No date of doing"
								: listStudents.get(i).getDate_of_doing());

				cell = row.createCell(columnCount++);
				if (listStudents.get(i).getStatus_id() == null || listStudents.get(i).getStatus_id().equals("")) {
					cell.setCellValue("No status");
				} else {
					StatusDTO check_status = DanhMucTinhTrangDAO.getStatusById(listStudents.get(i).getStatus_id());
					if (check_status == null) {
						cell.setCellValue("No status");
					} else {
						String name_status = DanhMucTinhTrangDAO.getStatusById(listStudents.get(i).getStatus_id())
								.getName_status();
						cell.setCellValue(name_status);
					}
				}
//				sheet.autoSizeColumn(columnCount);
			}
//			FileOutputStream fos = new FileOutputStream(new File(excelFilePath));
//			workbook.write(fos);
//			workbook.close();
//			fos.close();
			ByteArrayOutputStream baos=new ByteArrayOutputStream();
			workbook.write(baos);
			workbook.close();			
	        System.out.println("size: "+baos.size()/1048576+"MB");
			return new ByteArrayInputStream(baos.toByteArray());
		} catch (IOException | EncryptedDocumentException ex) {
			ex.printStackTrace();
			return null;
		}
	}
	
	@RequestMapping(value = "/downloadSampleFile", method = RequestMethod.GET)
	public void downloadSampleFile(HttpServletResponse response,HttpSession session) throws IOException, InterruptedException, ExecutionException, ParseException {
		long startTime = System.nanoTime();
		response.setContentType("application/octet-stream");
		response.setHeader("Content-Disposition","attachment; filename=sampleFile.xlsx");
		String path=session.getServletContext().getRealPath("/static/upload/file/");
		String fileName = "sampleFile.xlsx";
		String fullPath=(path+fileName);
		Path pathFinal = Paths.get(fullPath);
		byte[] data=Files.readAllBytes(pathFinal);
		//declare new byte type array input stream(array data) and get it from result of method exportToExcel() below
		ByteArrayInputStream baisArrayInputStream=new ByteArrayInputStream(data);
		//transfer array data to response(read byte type array data input stream and send data outside)
		baisArrayInputStream.transferTo(response.getOutputStream());	
        long elapsedTime = (System.nanoTime() - startTime);
     // nano time divine(%) 1.000.000.000(1 billion) to second time 
        System.out.println("download SampleFile: "+elapsedTime/1000000000+" seconds");
     // create a cookie
        Cookie cookie = new Cookie("DownloadCompleteChecker", null);

        // expires yesterday days
        cookie.setMaxAge(-1 * 24 * 60 * 60);

        // optional properties
        cookie.setHttpOnly(false);

        // add cookie to response
        response.addCookie(cookie);


        // return response entity
//        return new ResponseEntity<>(response,HttpStatus.OK);
	}
	
}
