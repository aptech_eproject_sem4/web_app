package c1808g1.aem_web.controllers.QuanLyHoSo;

import java.io.IOException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ExecutionException;
import java.util.stream.Collectors;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;

import c1808g1.Models.QuanLiHoSo.FCDTO;
import c1808g1.Models.QuanLiHoSo.ScoreFCDTO;
import c1808g1.Models.QuanLyChuongTrinhHoc.SubjectDTO;
import c1808g1.aem_web.DAO.QuanLyChuongTrinhHoc.MonHocDao;
import c1808g1.aem_web.DAO.QuanLyHoSo.GiangVienDao;

@Controller
@RequestMapping("/DiemThiFc/")
public class DiemThiFcController {
	String pathView = "DiemThiFc/";

	// Index
//	@RequestMapping({ "", "/Index" })
//	public String index(Model model, @RequestParam(required = false) String ajaxLoad,
//			@RequestParam(required = false) Integer page, @RequestParam(required = false) String subject_id,
//			@RequestParam(required = false) String fc_id, @RequestParam(required = false) String searchValue)
//			throws JsonParseException, JsonMappingException, InterruptedException, ExecutionException, IOException {
//		// get list data
//		List<ScoreFCDTO> lssfc = new ArrayList<ScoreFCDTO>();
//		List<FCDTO> listFC = new ArrayList<FCDTO>();
//		List<SubjectDTO> listSubject = new ArrayList<SubjectDTO>();
//		try {
//			if ((String.valueOf(subject_id).equals("null") || String.valueOf(subject_id).equals("")
//					|| String.valueOf(subject_id).equals("0") || String.valueOf(subject_id) == null
//					|| String.valueOf(subject_id).equals(null) || String.valueOf(subject_id).isEmpty())
//					&& (String.valueOf(fc_id).equals("null") || String.valueOf(fc_id).equals("")
//							|| String.valueOf(fc_id).equals("0") || String.valueOf(fc_id) == null
//							|| String.valueOf(fc_id).equals(null) || String.valueOf(fc_id).isEmpty())
//					&& (searchValue == null || searchValue.equals("") || searchValue.equals("null")
//							|| searchValue.equals(null) || searchValue.isEmpty())) {
//				lssfc = GiangVienDao.getScoreFCOrderByDatecreateDESC();
//			} else {
//				//kiểm tra nếu search value bằng null thì lấy ra dựa theo idsubject và idfc
//				if (searchValue == null || searchValue.equals("") || searchValue.equals("null")
//						|| searchValue.equals(null) || searchValue.isEmpty()) {
//					lssfc = GiangVienDao.getScoreFCByMultipleParameter(subject_id, fc_id);
//				} else {
//					List<SubjectDTO> checkSubject = MonHocDao.checkSubjectByValue(searchValue);
//					List<FCDTO> checkFC = GiangVienDao.checkFCByValue(searchValue);
//					List<ScoreFCDTO> checkExistSubjectId = null;
//					List<ScoreFCDTO> checkExistFCId = null;
//					List<String> existedSubjectId = new ArrayList<String>();
//					List<String> existedFCId = new ArrayList<String>();
//					List<ScoreFCDTO> listScoreFC1=null;
//					List<ScoreFCDTO> listScoreFC2=null;
//					
//					//lấy ra danh sách subject có id, name, sortname trùng với searchValue
//					if (checkSubject == null || checkSubject.size() <= 0) {
//
//					} else {
//						//lấy ra danh sách scorefc có id trùng với id có trong danh sách subject trên
//						for (int i = 0; i < checkSubject.size(); i++) {
//							String idSubject=checkSubject.get(i).getId_subject();
//							checkExistSubjectId=null;
//							checkExistSubjectId = GiangVienDao
//									.getScoreFCByMultipleParameter(idSubject, "0");
//							if (checkExistSubjectId == null || checkExistSubjectId.size() <= 0) {
//
//							} else {
//								//đưa id từ danh sách trùng vào danh sách idsubject
//								for (int j = 0; j < checkExistSubjectId.size(); j++) {
//									existedSubjectId.add(checkExistSubjectId.get(j).getSubject_id());
//								}
//							}
//						}
//						
//					}
//					//lấy ra danh sách fc có is,name trùng với searchValue
//					if (checkFC == null || checkFC.size() <= 0) {
//
//					} else {
//						//lấy ra danh sách scorefc có id trùng với id có trong danh sách fc trên
//						for (int i = 0; i < checkFC.size(); i++) {
//							String idFC=checkFC.get(i).getId_fc();
//							checkExistFCId=null;
//							checkExistFCId = GiangVienDao
//									.getScoreFCByMultipleParameter("0", idFC);
//							if (checkExistFCId == null || checkExistFCId.size() <= 0) {
//
//							} else {
//								//đưa id từ danh sách trùng vào danh sách idfc
//								for (int j = 0; j < checkExistFCId.size(); j++) {
//									existedFCId.add(checkExistFCId.get(j).getFc_id());
//								}
//							}
//						}
//						
//					}
//					//kiểm tra nếu danh sách idsubject null
//					if(existedSubjectId==null||existedSubjectId.size()<=0) {
//						//nếu sanh sách idfc null thì lấy hết
//						if(existedFCId==null||existedFCId.size()<=0) {
//							//nếu cả idsubject và idfc đều null thì lấy hết vì không có id trùng kết quả search trong scorefc
//							lssfc=null;
//							lssfc=GiangVienDao.getScoreFCOrderByDatecreateDESC();
//							//nếu danh sách idfc ko null thì lấy theo "0" và từng id trong danh sách idfc
//						}else {
//							//lấy ra danh sách the idfc
//							for(int i=0;i<existedFCId.size();i++) {
//								String idFC=existedFCId.get(i);
//								listScoreFC1=null;
//								listScoreFC1=GiangVienDao.getScoreFCByMultipleParameter("0", idFC);
//							}
//						}
//						//nếu không null
//					}else {
//						//nếu sanh sách idfc null thì lấy theo từng id trong danh sách idsubject và "0"
//						if(existedFCId==null||existedFCId.size()<=0) {
//							for(int i=0;i<existedSubjectId.size();i++) {
//								String idSubject=existedSubjectId.get(i);
//								listScoreFC1=null;
//								listScoreFC1=GiangVienDao.getScoreFCByMultipleParameter(idSubject, "0");
//							}
//							//nếu danh sách idfc ko null thì lấy theo từng id trong danh sách idsubject và từng id trong danh sách idfc
//						}else {
//							for(int i=0;i<existedSubjectId.size();i++) {
//								for(int j=0;j<existedFCId.size();j++) {
//									String idSubject=existedSubjectId.get(i);
//									String idFC=existedFCId.get(j);
//									listScoreFC1=null;
//									listScoreFC1=GiangVienDao.getScoreFCByMultipleParameter(idSubject,idFC);
//								}
//							}
//						}
//					}
//					listScoreFC2=null;
//					listScoreFC2=GiangVienDao.getScoreFCByMultipleParameter(subject_id, fc_id);
//					for(int i=0;i<listScoreFC1.size();i++) {
//						String string1=listScoreFC1.get(i).toString();
//						for(int j=0;j<listScoreFC2.size();j++) {
//							String string2=listScoreFC2.get(j).toString();
//							if(string1.equals(string2)) {
//								lssfc.add(listScoreFC2.get(j));
//							}
//						}
//					}
//				}
//			}
//			
//			
//			listFC = GiangVienDao.getAllFC();
//			listSubject = MonHocDao.getAllSubject();
//		} catch (IOException | ExecutionException | InterruptedException e) {
//			e.printStackTrace();
//		}
//
//		model.addAttribute("listSFc", lssfc);
//		model.addAttribute("listFC", listFC);
//		model.addAttribute("listSubject", listSubject);
//		if (ajaxLoad != null && ajaxLoad.equals("table")) {
//			return pathView + "loadTable";
//		}
//		return pathView + "index";
//	}
	
	@RequestMapping({ "", "/Index" })
	public String index(Model model, @RequestParam(required = false) String ajaxLoad,
			@RequestParam(required = false) Integer page, @RequestParam(required = false) String subject_id,
			@RequestParam(required = false) String fc_id, @RequestParam(required = false) String searchValue)
			throws JsonParseException, JsonMappingException, InterruptedException, ExecutionException, IOException {
		// get list data
		List<ScoreFCDTO> lssfc = new ArrayList<ScoreFCDTO>();
		List<FCDTO> listFC = new ArrayList<FCDTO>();
		List<SubjectDTO> listSubject = new ArrayList<SubjectDTO>();
		try {
			if ((String.valueOf(subject_id).equals("null") || String.valueOf(subject_id).equals("")
					|| String.valueOf(subject_id).equals("0") || String.valueOf(subject_id) == null
					|| String.valueOf(subject_id).equals(null) || String.valueOf(subject_id).isEmpty())
					&& (String.valueOf(fc_id).equals("null") || String.valueOf(fc_id).equals("")
							|| String.valueOf(fc_id).equals("0") || String.valueOf(fc_id) == null
							|| String.valueOf(fc_id).equals(null) || String.valueOf(fc_id).isEmpty())
					&& (searchValue == null || searchValue.equals("") || searchValue.equals("null")
							|| searchValue.equals(null) || searchValue.isEmpty())) {
				lssfc = GiangVienDao.getScoreFCOrderByDatecreateDESC();
			} else {
				if((searchValue == null || searchValue.equals("") || searchValue.equals("null")
						|| searchValue.equals(null) || searchValue.isEmpty())) {
					lssfc=GiangVienDao.getScoreFCByMultipleParameter(subject_id, fc_id);
				}else if(!(String.valueOf(subject_id).equals("null") || String.valueOf(subject_id).equals("")
						|| String.valueOf(subject_id).equals("0") || String.valueOf(subject_id) == null
						|| String.valueOf(subject_id).equals(null) || String.valueOf(subject_id).isEmpty()) && !(String.valueOf(fc_id).equals("null") || String.valueOf(fc_id).equals("")
								|| String.valueOf(fc_id).equals("0") || String.valueOf(fc_id) == null
								|| String.valueOf(fc_id).equals(null) || String.valueOf(fc_id).isEmpty())){
					lssfc=GiangVienDao.getScoreFCByMultipleParameter(subject_id, fc_id);
				}else if((String.valueOf(subject_id).equals("null") || String.valueOf(subject_id).equals("")
						|| String.valueOf(subject_id).equals("0") || String.valueOf(subject_id) == null
						|| String.valueOf(subject_id).equals(null) || String.valueOf(subject_id).isEmpty()) && (String.valueOf(fc_id).equals("null") || String.valueOf(fc_id).equals("")
								|| String.valueOf(fc_id).equals("0") || String.valueOf(fc_id) == null
								|| String.valueOf(fc_id).equals(null) || String.valueOf(fc_id).isEmpty())){
					lssfc=GiangVienDao.getScoreFCBySearchValue(searchValue);
				}else if((String.valueOf(subject_id).equals("null") || String.valueOf(subject_id).equals("")
						|| String.valueOf(subject_id).equals("0") || String.valueOf(subject_id) == null
						|| String.valueOf(subject_id).equals(null) || String.valueOf(subject_id).isEmpty())){
					lssfc=GiangVienDao.getScoreFCByFCIdAndSearchValue(fc_id, searchValue);
				}else if((String.valueOf(fc_id).equals("null") || String.valueOf(fc_id).equals("")
						|| String.valueOf(fc_id).equals("0") || String.valueOf(fc_id) == null
						|| String.valueOf(fc_id).equals(null) || String.valueOf(fc_id).isEmpty())) {
					lssfc=GiangVienDao.getScoreFCBySubjectIdAndSearchValue(subject_id, searchValue);
				}else {
					lssfc=null;
				}		
			}
			
			
			listFC = GiangVienDao.getAllFC();
			listSubject = MonHocDao.getAllSubject();
		} catch (IOException | ExecutionException | InterruptedException e) {
			e.printStackTrace();
		}

		model.addAttribute("listSFc", lssfc);
		model.addAttribute("listFC", listFC);
		model.addAttribute("listSubject", listSubject);
		if (ajaxLoad != null && ajaxLoad.equals("table")) {
			return pathView + "loadTable";
		}
		return pathView + "index";
	}

	@RequestMapping(value = "/addOrUpdate")
	public @ResponseBody String addOrUpdate(ScoreFCDTO entity) {
		if (entity == null) {
			return "Error, please try again!";
		}
		try {
			ScoreFCDTO check = null;
			check = GiangVienDao.getScoreFCById(entity.getId());
			ScoreFCDTO result = null;
			if (check == null) {
				Timestamp timestamp = new Timestamp(System.currentTimeMillis());
				entity.setDate_create(String.valueOf(timestamp));
				result = GiangVienDao.createScoreFC(entity);
			} else {
				entity.setDate_create(check.getDate_create());
				result = GiangVienDao.updateScoreFC(entity);
			}
		} catch (InterruptedException | ExecutionException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return "true";
	}

	@RequestMapping(value = "/Delete/{id}")
	public @ResponseBody String delete(@PathVariable("id") String id) {
		ScoreFCDTO result = new ScoreFCDTO();
		try {
			result = GiangVienDao.deleteScoreFC(id);
		} catch (InterruptedException | ExecutionException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return e.getMessage();
		}
		return "true";
	}
}
