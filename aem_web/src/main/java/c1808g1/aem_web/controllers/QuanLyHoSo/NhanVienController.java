package c1808g1.aem_web.controllers.QuanLyHoSo;

import java.io.IOException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;

import c1808g1.Models.HeThong.RoleDTO;
import c1808g1.Models.QuanLiHoSo.EmployeeDTO;
import c1808g1.aem_web.DAO.HeThong.QuyenDAO;

import c1808g1.aem_web.DAO.QuanLyHoSo.NhanVienDao;
import c1808g1.aem_web.common.MD5;

@Controller
@RequestMapping("/NhanVien/")
public class NhanVienController {
	String pathView = "NhanVien/";

	// Index
	@RequestMapping({ "", "/Index" })
	public String index(Model model,
	 		@RequestParam(required = false) String ajaxLoad,
			@RequestParam(required = false) Integer page,
			@RequestParam(required = false) Integer status,
			@RequestParam(required = false) String searchValue)
			throws JsonParseException, JsonMappingException, InterruptedException, ExecutionException, IOException {
		// get list data
		List<EmployeeDTO> listEmployee = new ArrayList<EmployeeDTO>();
		try {
			if ((String.valueOf(status).equals("null") || String.valueOf(status).equals("")
					|| String.valueOf(status) == null || String.valueOf(status).equals(null)
					|| String.valueOf(status).isEmpty())
					&& (searchValue == null || searchValue.equals("") || searchValue.equals("null")
							|| searchValue.equals(null) || searchValue.isEmpty())) {
				listEmployee = NhanVienDao.getAllEmployee();
			} else {
				if (String.valueOf(status).equals("1")) {
					listEmployee = NhanVienDao.getEmployeeByMultipleParameter(true, searchValue);
				} else if (String.valueOf(status).equals("0")) {
					listEmployee = NhanVienDao.getEmployeeByMultipleParameter(false, searchValue);
				} else {
					List<EmployeeDTO> list1 = NhanVienDao.getEmployeeByMultipleParameter(true, searchValue);
					List<EmployeeDTO> list2 = NhanVienDao.getEmployeeByMultipleParameter(false, searchValue);
					if (list1 == null || list1.isEmpty()) {
					} else {
						listEmployee.addAll(list1);
					}
					if (list2 == null || list2.isEmpty()) {
					} else {
						listEmployee.addAll(list2);
					}
				}
			}
		} catch (InterruptedException | ExecutionException | IOException e) {
			e.printStackTrace();
		}
		List<RoleDTO> listRole = QuyenDAO.getAllOrderbyASC();
		model.addAttribute("listEmployee", listEmployee);
		model.addAttribute("listRole", listRole);
		if (ajaxLoad != null && ajaxLoad.equals("table")) {
			return pathView + "loadTable";
		}
		return pathView + "index";
	}

	@RequestMapping(value = "/addOrUpdate")
	public @ResponseBody String addOrUpdate(EmployeeDTO entity) {
		// check trùng thì return "Đã có trong bảng"
		// ok thì return true
		if (entity == null) {
			return "Error, please try again!";
		}
		try {
			EmployeeDTO check = null;
			check = NhanVienDao.getEmployeeById(entity.getId_emp());
			EmployeeDTO result = new EmployeeDTO();
			if (check == null) {
				Timestamp timestamp = new Timestamp(System.currentTimeMillis());
				entity.setDate_create(String.valueOf(timestamp));
				String pwMD = MD5.GenerateMD5("aptech123");
				entity.setPassword(pwMD);
				result = NhanVienDao.create(entity);
			} else {
				if (entity.getList_role().equals("") || entity.getList_role().isEmpty() || entity.getList_role() == null
						|| entity.getList_role().equals("0")) {
					entity.setList_role(check.getList_role());
				}
				entity.setDate_create(check.getDate_create());
				entity.setPassword(check.getPassword());
				result = NhanVienDao.update(entity);
			}
		} catch (InterruptedException | ExecutionException | IOException e) {
			e.printStackTrace();
			return e.getMessage();
		}
		return "true";
	}

	@RequestMapping(value = "/Delete/{id}")
	public @ResponseBody String delete(@PathVariable("id") String id) {
		try {
			if (id == null || id == "") {
				return "Not found id!";
			} else {
				NhanVienDao.delete(id);
			}
		} catch (InterruptedException | ExecutionException | IOException e) {

			e.printStackTrace();
			return e.getMessage();
		}
		return "true";
	}

	@RequestMapping(value = "/checkId", method = RequestMethod.POST)
	public @ResponseBody String searchPerson(HttpServletRequest request, String id_emp) {
		// ObjectMapper mapper = new ObjectMapper();
		String ajaxResponse = "";
		try {
			// ajaxResponse = mapper.writeValueAsString(person);
			EmployeeDTO check = NhanVienDao.getEmployeeById(id_emp);
			if (check == null) {
				ajaxResponse = "not exist";
			} else {
				ajaxResponse = "existed";
			}
		} catch (InterruptedException | ExecutionException | IOException e) {
			e.printStackTrace();
		}
		return ajaxResponse;
	}
	
	@RequestMapping(value = "/changeActiveAccount", method = RequestMethod.POST)
	public @ResponseBody String changeActiveAccount(HttpServletRequest request, String id_emp) {
		// ObjectMapper mapper = new ObjectMapper();
		String ajaxResponse = "";
		try {
			// ajaxResponse = mapper.writeValueAsString(person);
			EmployeeDTO check = NhanVienDao.getEmployeeById(id_emp);
			if (check.getActive_account().equals("true")) {
				check.setActive_account("false");
				ajaxResponse="Đã khóa!";
			} else {
				check.setActive_account("true");
				ajaxResponse="Đã mở khóa!";
			}
			NhanVienDao.update(check);
		} catch (InterruptedException | ExecutionException | IOException e) {
			e.printStackTrace();
		}
		return ajaxResponse;
	}
}
