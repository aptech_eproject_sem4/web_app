package c1808g1.aem_web.controllers.QuanLyChuongTrinhHoc;

import java.io.IOException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;

import c1808g1.Models.CauHinh.SkuDTO;
import c1808g1.Models.CauHinh.StatusDTO;
import c1808g1.Models.CauHinh.Type_SubjectDTO;
import c1808g1.Models.QuanLyChuongTrinhHoc.SemesterDTO;
import c1808g1.Models.QuanLyChuongTrinhHoc.SubjectDTO;
import c1808g1.aem_web.DAO.CauHinh.DanhMucTinhTrangDAO;
import c1808g1.aem_web.DAO.CauHinh.SkuDAO;
import c1808g1.aem_web.DAO.QuanLyChuongTrinhHoc.HocKyDao;
import c1808g1.aem_web.DAO.QuanLyChuongTrinhHoc.KhoaHocDao;
import c1808g1.aem_web.DAO.QuanLyChuongTrinhHoc.MonHocDao;
import c1808g1.aem_web.common.danhMucDungChung;
import c1808g1.aem_web.models.GroupType;

@Controller
@RequestMapping("/MonHoc/")
public class MonHocController {
	String pathView = "MonHoc/";

	// Index
	@RequestMapping({ "", "/Index" })
	public String index(Model model, @RequestParam(required = false) String ajaxLoad,
			@RequestParam(required = false) Integer page, @RequestParam(required = false) Integer idSemester,
			@RequestParam(required = false) String idStatus, @RequestParam(required = false) String searchValue)
			throws JsonParseException, JsonMappingException, InterruptedException, ExecutionException, IOException {
		// get list data
		List<SubjectDTO> listSubject = new ArrayList<SubjectDTO>();
		try {
			if ((String.valueOf(idSemester) == "null"||String.valueOf(idSemester) == ""||String.valueOf(idSemester) == "0"||idSemester==0) && (idStatus == null||idStatus==""||idStatus=="null"||idStatus=="0") && (searchValue == null||searchValue==""||searchValue=="null")) {
				listSubject = MonHocDao.getSubjectOrderByOrderNumberASC();
			} else {
				listSubject = MonHocDao.getSubjectByMultipleParameter(idSemester, idStatus, searchValue);
			}
		} catch (InterruptedException | ExecutionException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		List<StatusDTO> listStatus = danhMucDungChung.LoaiMonHoc();
		List<SemesterDTO> listSemester = HocKyDao.getAllSemester();
		List<SkuDTO> listSku = SkuDAO.getSkuByType(false);
		model.addAttribute("listSubject", listSubject);
		model.addAttribute("listStatus", listStatus);
		model.addAttribute("listSemester", listSemester);
		model.addAttribute("listSku", listSku);
		if (ajaxLoad != null && ajaxLoad.equals("table")) {
			return pathView + "loadTable";
		}
		return pathView + "index";
	}


	@RequestMapping(value = "/addOrUpdate")
	public @ResponseBody String addOrUpdate(SubjectDTO entity) {
		// check trùng thì return "Đã có trong bảng"
		// ok thì return true
		if (entity == null) {
			return "Error, please try again!";
		}
		try {
			SubjectDTO check = null;
			check = MonHocDao.getSubjectById(entity.getId_subject());
			SubjectDTO result = null;
			if (check == null) {
				Timestamp timestamp = new Timestamp(System.currentTimeMillis());
				entity.setDate_create(String.valueOf(timestamp));
				result = MonHocDao.create(entity);
			} else {
				entity.setDate_create(check.getDate_create());
				result = MonHocDao.update(entity);
			}
		} catch (InterruptedException | ExecutionException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return e.getMessage();
		}
		return "true";
	}

	@RequestMapping(value = "/Delete/{id}")
	public @ResponseBody String delete(@PathVariable("id") String id) {
		try {
			if (id == null || id == "") {
				return "Not found id!";
			} else {
				SubjectDTO result = MonHocDao.delete(id);
			}
		} catch (InterruptedException | ExecutionException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return e.getMessage();
		}
		return "true";
	}

	@RequestMapping(value = "/checkId", method = RequestMethod.POST)
	public @ResponseBody String searchPerson(HttpServletRequest request, String idSubject) {
		// ObjectMapper mapper = new ObjectMapper();
		String ajaxResponse = "";
		try {
			// ajaxResponse = mapper.writeValueAsString(person);
			SubjectDTO check = MonHocDao.getSubjectById(idSubject);
			if (check == null) {
				ajaxResponse = "not exist";
			} else {
				ajaxResponse = "existed";
			}
		} catch (InterruptedException | ExecutionException | IOException e) {
			e.printStackTrace();
		}
		return ajaxResponse;
	}

	@RequestMapping(value = "/countSession", method = RequestMethod.POST)
	public @ResponseBody String countSession(HttpServletRequest request, String idSubject, String hour) {
		// ObjectMapper mapper = new ObjectMapper();
		String ajaxResponse = "";
		try {
			// ajaxResponse = mapper.writeValueAsString(person);
			SkuDTO skuValue = SkuDAO.getSkuById(idSubject);
			double doubleValue = (Double.valueOf(hour) * Double.valueOf(skuValue.getUnit1_value()))
					/ Double.valueOf(skuValue.getUnit2_value());
			int intValue = (int) Math.round(doubleValue);
			ajaxResponse = String.valueOf(intValue);
		} catch (InterruptedException | ExecutionException | IOException e) {
			e.printStackTrace();
		}
		return ajaxResponse;
	}
	
	@RequestMapping(value = "/countHour", method = RequestMethod.POST)
	public @ResponseBody String countHour(HttpServletRequest request, String idSubject, String session) {
		// ObjectMapper mapper = new ObjectMapper();
		String ajaxResponse = "";
		try {
			// ajaxResponse = mapper.writeValueAsString(person);
			SkuDTO skuValue = SkuDAO.getSkuById(idSubject);
			double doubleValue = (Double.valueOf(session) * Double.valueOf(skuValue.getUnit2_value()))
					/ Double.valueOf(skuValue.getUnit1_value());
			double intValue = doubleValue;
			ajaxResponse = String.valueOf(intValue);
		} catch (InterruptedException | ExecutionException | IOException e) {
			e.printStackTrace();
		}
		return ajaxResponse;
	}
}
