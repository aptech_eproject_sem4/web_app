package c1808g1.aem_web.controllers.QuanLyChuongTrinhHoc;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;

import c1808g1.Models.QuanLyChuongTrinhHoc.CourseDTO;
import c1808g1.Models.QuanLyChuongTrinhHoc.SemesterDTO;

import c1808g1.aem_web.DAO.QuanLyChuongTrinhHoc.HocKyDao;
import c1808g1.aem_web.DAO.QuanLyChuongTrinhHoc.KhoaHocDao;

@Controller
@RequestMapping("/HocKy/")
public class HocKyController {
	String pathView = "HocKy/";

	// Index
	@RequestMapping({ "", "/Index" })
	public String index(Model model, @RequestParam(required = false) String ajaxLoad,
			@RequestParam(required = false) Integer page, @RequestParam(required = false) String idCourse) throws JsonParseException, JsonMappingException, InterruptedException, ExecutionException, IOException {
		// get list data
		List<SemesterDTO> lss = new ArrayList<SemesterDTO>();
		List<CourseDTO> lsc = KhoaHocDao.getCourseRootNotNull();
		try {
			if (idCourse == null || idCourse == "" || idCourse.equals("0")) {
				lss = HocKyDao.findCourseByCourseroot(lsc.get(0).getId_course());
			} else {
				lss = HocKyDao.findCourseByCourseroot(idCourse);
			}

		} catch (IOException | ExecutionException | InterruptedException e) {
			e.printStackTrace();
		}
		

		model.addAttribute("listC", lsc);
		model.addAttribute("lss", lss);
		if (ajaxLoad != null && ajaxLoad.equals("table")) {
			return pathView + "loadTable";
		}
		return pathView + "index";
	}

	@RequestMapping(value = "/addOrUpdate")
	public @ResponseBody String addOrUpdate(SemesterDTO entity) {
		if (entity == null) {
			return "Error, please try again!";
		}
		try {
			SemesterDTO check = null;
			check = HocKyDao.getSemesterById(entity.getId());
			SemesterDTO result = null;
			if (check == null) {
				result = HocKyDao.create(entity);
			} else {
				result = HocKyDao.update(entity);
			}
		} catch (InterruptedException | ExecutionException | IOException e) {
			
			e.printStackTrace();
		}
		return "true";
	}

	@RequestMapping(value = "/Delete/{id}")
	public @ResponseBody String delete(@PathVariable("id") String id) {
		SemesterDTO result = new SemesterDTO();
		try {
			result = HocKyDao.delete(id);
		} catch (InterruptedException | ExecutionException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return e.getMessage();
		}
		return "true";
	}
//	@RequestMapping(value = "/checkId", method = RequestMethod.POST)
//	public @ResponseBody String searchPerson(HttpServletRequest request, String idTag) {
//		// ObjectMapper mapper = new ObjectMapper();
//		String ajaxResponse = "";
//		try {
//			// ajaxResponse = mapper.writeValueAsString(person);
//			TagDTO check = TagDAO.getTagById(idTag);
//			if (check == null) {
//				ajaxResponse = "Mã không tồn tại, có thể tạo mới, không thể cập nhật!";
//			} else {
//				ajaxResponse = "Mã đã tồn tại, có thể cập nhật,không thể tạo mới!";
//			}
//		} catch (InterruptedException | ExecutionException | IOException e) {
//			e.printStackTrace();
//		}
//		return ajaxResponse;
//	}
}
