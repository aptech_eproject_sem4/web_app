package c1808g1.aem_web.controllers.QuanLyChuongTrinhHoc;

import java.io.IOException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import c1808g1.Models.QuanLyChuongTrinhHoc.ClassDTO;
import c1808g1.aem_web.DAO.QuanLyChuongTrinhHoc.LopDao;

@Controller
@RequestMapping("/LopHoc/")
public class LopHocController {
	String pathView = "LopHoc/";

	// Index
	@RequestMapping({ "", "/Index" })
	public String index(Model model, @RequestParam(required = false) String ajaxLoad,
			@RequestParam(required = false) Integer page) {
		// get list data
		List<ClassDTO> listClass = new ArrayList<ClassDTO>();
		try {
			listClass = LopDao.getClassSort();
			/* listClass = LopDao.getAllClass(); */
		} catch (InterruptedException | ExecutionException | IOException e) {

			e.printStackTrace();
		}
		model.addAttribute("listClass", listClass);
		if (ajaxLoad != null && ajaxLoad.equals("table")) {
			return pathView + "loadTable";
		}
		return pathView + "index";
	}

	@RequestMapping("/addOrUpdate")
	public @ResponseBody String addOrUpdate(ClassDTO entity) {
		if (entity == null) {
			return "error";
		}
		try {
			ClassDTO check = null;
			check = LopDao.getClassById(entity.getId_class());
			ClassDTO result = null;
			if (check == null) {
//				long millis = System.currentTimeMillis();
//				java.sql.Date date = new java.sql.Date(millis);		
				Timestamp timestamp = new Timestamp(System.currentTimeMillis());
				entity.setDate_create(String.valueOf(timestamp));
				result = LopDao.create(entity);
			} else {
				entity.setDate_create(check.getDate_create());
				result = LopDao.update(entity);
			}
		} catch (InterruptedException | ExecutionException | IOException e) {

			e.printStackTrace();
		}
		return "true";
	}

	@RequestMapping(value = "/Delete/{id_class}")
	public @ResponseBody String Delete(@PathVariable("id_class") String id) {
		ClassDTO delete = new ClassDTO();
		try {
			delete = LopDao.delete(id);
		} catch (InterruptedException | ExecutionException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return "true";
	}

	@RequestMapping(value = "/checkIdClass", method = RequestMethod.POST)
	public @ResponseBody String searchPerson(HttpServletRequest request, String idClass) {
		// ObjectMapper mapper = new ObjectMapper();
		String ajaxResponse = "";
		try {
			// ajaxResponse = mapper.writeValueAsString(person);
			ClassDTO check = LopDao.getClassById(idClass);
			if (check == null) {
				ajaxResponse = "not exist";
			} else {
				ajaxResponse = "existed";
			}
		} catch (InterruptedException | ExecutionException | IOException e) {
			e.printStackTrace();
		}
		return ajaxResponse;
	}
}
