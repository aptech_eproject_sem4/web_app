package c1808g1.aem_web.controllers.QuanLyChuongTrinhHoc;

import java.io.IOException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.google.gson.Gson;

import c1808g1.Models.QuanLyChuongTrinhHoc.CourseDTO;

import c1808g1.aem_web.DAO.QuanLyChuongTrinhHoc.KhoaHocDao;

@Controller
@RequestMapping("/KhoaHoc/")
public class KhoaHocController {
	String pathView = "KhoaHoc/";

	// Index
	@RequestMapping({ "", "/Index" })
	public String index(Model model, @RequestParam(required = false) String ajaxLoad,
			@RequestParam(required = false) Integer page, @RequestParam(required = false) String idCourse)
			throws JsonParseException, JsonMappingException, InterruptedException, ExecutionException, IOException {
		// get list data
		List<CourseDTO> lsc = new ArrayList<CourseDTO>();
		List<CourseDTO> lscr = KhoaHocDao.getCourseRootIsNull();
		try {
			if (idCourse == null || idCourse == "" || idCourse.equals("0")) {
				lsc = KhoaHocDao.getCoursByCourseroot(lscr.get(0).getId_course());
			} else {
				lsc = KhoaHocDao.getCoursByCourseroot(idCourse);
			}

		} catch (IOException | ExecutionException | InterruptedException e) {
			e.printStackTrace();
		}

		model.addAttribute("lsc", lsc);
		model.addAttribute("lscr", lscr);

		if (ajaxLoad != null && ajaxLoad.equals("table")) {
			return pathView + "loadTable";
		}
		return pathView + "index";
	}

	@RequestMapping(value = "/addOrUpdate")
	public @ResponseBody String addOrUpdate(CourseDTO entity) {
		if (entity == null) {
			return "Error, please try again!";
		}
		try {
			CourseDTO check = null;
			check = KhoaHocDao.getCourseById(entity.getId_course());
			CourseDTO result = null;
			if (check == null) {
				Timestamp timestamp = new Timestamp(System.currentTimeMillis());
				entity.setDate_create(String.valueOf(timestamp));
				result = KhoaHocDao.create(entity);
			} else {
				entity.setDate_create(check.getDate_create());
				result = KhoaHocDao.update(entity);
			}
		} catch (InterruptedException | ExecutionException | IOException e) {
			e.printStackTrace();
		}
		return "true";
	}

	@RequestMapping(value = "/Delete/{id}")
	public @ResponseBody String delete(@PathVariable("id") String id) {
		CourseDTO result = new CourseDTO();
		try {
			result = KhoaHocDao.delete(id);
		} catch (InterruptedException | ExecutionException | IOException e) {

			e.printStackTrace();
			return e.getMessage();
		}
		return "true";
	}

	@RequestMapping(value = "/checkIdCourse", method = RequestMethod.POST)
	public @ResponseBody String searchPerson(HttpServletRequest request, String idCourse) {
		// ObjectMapper mapper = new ObjectMapper();
		String ajaxResponse = "";
		try {
			// ajaxResponse = mapper.writeValueAsString(person);
			CourseDTO check = KhoaHocDao.getCourseById(idCourse);
			if (check == null) {
				ajaxResponse = "not exist";
			} else {
				ajaxResponse = "existed";
			}
		} catch (InterruptedException | ExecutionException | IOException e) {
			e.printStackTrace();
		}
		return ajaxResponse;
	}

	@RequestMapping(value = "/courseByRootId", method = RequestMethod.GET)
	public @ResponseBody String CourseByRootId(HttpServletRequest request,
			@RequestParam("courseRoot") String CourseRootId) {
		// ObjectMapper mapper = new ObjectMapper();
		String ajaxResponse = "";

		try {
			// ajaxResponse = mapper.writeValueAsString(person);
			List<CourseDTO> check = KhoaHocDao.getCoursByCourseroot(CourseRootId);

			String data = new Gson().toJson(check);

			if (check == null) {
				ajaxResponse = "not found";
			} else {
				ajaxResponse = data;
			}
		} catch (InterruptedException | ExecutionException | IOException e) {
			e.printStackTrace();
		}
		return ajaxResponse;
	}

}