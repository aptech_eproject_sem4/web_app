package c1808g1.aem_web.controllers.CauHinh;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ExecutionException;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;


import c1808g1.Models.CauHinh.HolidayDTO;
import c1808g1.aem_web.DAO.CauHinh.NgayNghiDAO;


@Controller
@RequestMapping("/NgayNghi")
public class NgayNghiController {
	String pathView = "NgayNghi/";

	// Index
	@RequestMapping({ "", "/Index" })
	public String index(Model model, @RequestParam(required = false) String ajaxLoad,
			@RequestParam(required = false) Integer page) {
		// get list data
		List<HolidayDTO> listHoliday = new ArrayList<HolidayDTO>();
		try {
			listHoliday = NgayNghiDAO.getHolidaySort();
			/* listHoliday = NgayNghiDAO.getAllHoliday(); */
		} catch (InterruptedException | ExecutionException | IOException e) {
			e.printStackTrace();
		}
		model.addAttribute("listHoliday", listHoliday);
		if (ajaxLoad != null && ajaxLoad.equals("table")) {
			return pathView + "loadTable";
		}
		return pathView + "index";
	}

	@RequestMapping(value = "/addOrUpdate")
	public @ResponseBody String addOrUpdate(HolidayDTO entity) throws ParseException {
		// check trùng date off nếu là create thì return "Ngày nghỉ ngày đã có trong
		// bảng"
		// ok thì return true
		if (entity == null) {
			return "error";
		}
		HolidayDTO check = null;
		HolidayDTO result = null;
		
		try {
			check = NgayNghiDAO.getHolidayById(entity.getId());
			if (check == null) {
				java.util.Date utilDate= null;
				List<String> formatStrings = Arrays.asList("dd/MM/yyyy","yyyy-MM-dd");
				for (String formatString : formatStrings)
			    {
			        try
			        {
			            utilDate=new SimpleDateFormat(formatString).parse(entity.getDate_off());
			            System.out.println(formatString+" "+utilDate.toString());
			        }
			        catch (ParseException e) {}
			    }
//				java.util.Date utilDate = new SimpleDateFormat("dd/MM/yyyy").parse(entity.getDate_off());
				java.sql.Date sqlDate = new java.sql.Date(utilDate.getTime());
				String date = String.valueOf(sqlDate);
				entity.setDate_off(date);
				result = NgayNghiDAO.create(entity);
			} else {
				java.util.Date utilDate= null;
				List<String> formatStrings = Arrays.asList("dd/MM/yyyy","yyyy-MM-dd");
				for (String formatString : formatStrings)
			    {
			        try
			        {
			            utilDate=new SimpleDateFormat(formatString).parse(entity.getDate_off());
			            System.out.println(formatString+" "+utilDate.toString());
			        }
			        catch (ParseException e) {}
			    }
				java.sql.Date sqlDate = new java.sql.Date(utilDate.getTime());
				String date = String.valueOf(sqlDate);
				entity.setDate_off(date);
				result = NgayNghiDAO.update(entity);
			}
		} catch (InterruptedException | ExecutionException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return "true";
	}

	@RequestMapping(value = "/Delete/{id}")
	public @ResponseBody String Delete(@PathVariable("id") int id) {
		HolidayDTO delete = new HolidayDTO();
		try {
			delete = NgayNghiDAO.delete(String.valueOf(id));
		} catch (InterruptedException | ExecutionException | IOException e) {
			
			e.printStackTrace();
		}
		return "true";
	}

	@RequestMapping(value = "/checkDateoff", method = RequestMethod.POST)
	public @ResponseBody String searchPerson(HttpServletRequest request, String dateoff) throws ParseException {
		String ajaxResponse = "";
		try {
			java.util.Date utilDate = new SimpleDateFormat("dd-MM-yyyy").parse(dateoff);
			java.sql.Date sqlDate = new java.sql.Date(utilDate.getTime());
			String date = String.valueOf(sqlDate);
			HolidayDTO check= NgayNghiDAO.getHolidayByDateoff(date);
			if (check == null) {
				ajaxResponse = "not exist";
			} else {
				ajaxResponse = "existed";
			}
		} catch (InterruptedException | ExecutionException | IOException e) {
			e.printStackTrace();
		}
		return ajaxResponse;
	}

}
