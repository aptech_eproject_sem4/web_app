package c1808g1.aem_web.controllers.CauHinh;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import c1808g1.Models.CauHinh.ShiftStudyDTO;
import c1808g1.aem_web.DAO.CauHinh.PhienHocDAO;

@Controller
@RequestMapping("/PhienHoc/")
public class PhienHocController {
	String pathView = "PhienHoc/";
	
	  // Index
	  @RequestMapping({"","/Index"})
	  public String index(Model model, @RequestParam(required = false) String ajaxLoad,
	      @RequestParam(required = false) Integer page) {
	        //get list data
		  List<ShiftStudyDTO> listSS = new ArrayList<ShiftStudyDTO>();
		  try {
			  listSS = PhienHocDAO.getAllShiftStudy();
		  } catch (InterruptedException | ExecutionException | IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
		  }
		  model.addAttribute("listSS", listSS);
		  if (ajaxLoad != null && ajaxLoad.equals("table")) {
				return pathView + "loadTable";
		  }
	  return pathView + "index";
	  }
	  
	  @RequestMapping(value = "/addOrUpdate")
	  public @ResponseBody String addOrUpdate(ShiftStudyDTO entity){  
		// check trùng thì return "Đã có trong bảng"
		// ok thì return true
		  //id string
//		    if (entity == null) {
//				return "Error, please try again!";
//			}
//		    try {
//		    	ShiftStudyDTO check=null;
//				check=PhienHocDAO.getShiftStudyById(entity.getId());
//				ShiftStudyDTO result=null;			
//				if (check==null) {
//					result = PhienHocDAO.create(entity);
//				} else {
//					result = PhienHocDAO.update(entity);
//				}
//			} catch (InterruptedException | ExecutionException | IOException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//				return e.getMessage();
//			}
//	  return "true";
		  //id identity
		  if(entity==null){
		  		return "error";
		  	}
		  ShiftStudyDTO check = null;
		  ShiftStudyDTO result = null;
		  	String val=entity.getId();
			try {								
					check=PhienHocDAO.getShiftStudyById(entity.getId());
				if (check==null) {
					result = PhienHocDAO.create(entity);
				} else {
					result = PhienHocDAO.update(entity);
				}
			} catch (InterruptedException | ExecutionException | IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	  return "true";
	  }
	  
	  @RequestMapping(value="/Delete/{id}")
	  public @ResponseBody String delete(@PathVariable("id") String id){
		  ShiftStudyDTO delete = new ShiftStudyDTO();
			 try {
				 delete = PhienHocDAO.delete(id);
			  	} catch (InterruptedException | ExecutionException | IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
			  	}
	  return "true";
	  }
	  
	 

	}
