package c1808g1.aem_web.controllers.CauHinh;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import c1808g1.Models.CauHinh.SkuDTO;
import c1808g1.aem_web.DAO.CauHinh.SkuDAO;

@Controller
@RequestMapping("/Sku/")
public class SkuController {
	String pathView = "Sku/";
  // Index
  @RequestMapping({"","/Index"})
  public String index(Model model, @RequestParam(required = false) String ajaxLoad,
      @RequestParam(required = false) Integer page) {
        //get list data
	  List<SkuDTO> lss=new ArrayList<SkuDTO>();
	  try {
		  lss = SkuDAO.getAllSku();
	  }catch(IOException|ExecutionException|InterruptedException e) {
		  e.printStackTrace();
	  }
	  model.addAttribute("lss",lss);
        if(ajaxLoad!=null && ajaxLoad.equals("table")){
          return pathView+"loadTable";
        }
    return pathView + "index";
}
  @RequestMapping(value = "/addOrUpdate")
  public @ResponseBody String addOrUpdate(SkuDTO entity){  
	  if (entity == null) {
			return "Error, please try again!";
		}
		try {
			SkuDTO check =null;
			check = SkuDAO.getSkuById(entity.getId());
			SkuDTO result=null;
			if (check==null) {
				result = SkuDAO.create(entity);
			} else {
				result = SkuDAO.update(entity);
			}
		} catch (InterruptedException | ExecutionException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return "true";
	}

  @RequestMapping(value="/Delete/{id}")
  public @ResponseBody String delete(@PathVariable("id") String id){
	  SkuDTO result = new SkuDTO();
	  try {
			 result = SkuDAO.delete(id);
		} catch (InterruptedException | ExecutionException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return e.getMessage();
		}
    return "true";
  }
}