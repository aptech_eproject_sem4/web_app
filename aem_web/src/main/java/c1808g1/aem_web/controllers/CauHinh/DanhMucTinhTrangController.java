package c1808g1.aem_web.controllers.CauHinh;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;


import c1808g1.Models.CauHinh.StatusDTO;
import c1808g1.aem_web.DAO.CauHinh.DanhMucTinhTrangDAO;
import c1808g1.aem_web.common.danhMucDungChung;
import c1808g1.aem_web.models.GroupType;

@Controller
@RequestMapping("/DanhMucTinhTrang/")
public class DanhMucTinhTrangController {
	String pathView = "DanhMucTinhTrang/";

	// Index
	@RequestMapping({ "", "/Index" })
	public String index(Model model, @RequestParam(required = false) String ajaxLoad,
			@RequestParam(required = false) Integer page, @RequestParam(required = false) String idGroup) {
		// get list data
		List<StatusDTO> listStatus = new ArrayList<StatusDTO>();
		List<GroupType> listGroup = danhMucDungChung.GetAllListGroupType();
		try {
			if (idGroup == null || idGroup == "" || Integer.valueOf(idGroup) <= 0) {
				listStatus = DanhMucTinhTrangDAO.getStatusByGrouptype(listGroup.get(0).getId());
			} else {
				listStatus = DanhMucTinhTrangDAO.getStatusByGrouptype(idGroup);
			}
			// listStatus=DanhMucTinhTrangDAO.getAllStatus();
		} catch (InterruptedException | ExecutionException | IOException e) {
			e.printStackTrace();
		}
		model.addAttribute("listStatus", listStatus);
		model.addAttribute("listGroup", listGroup);
		if (ajaxLoad != null && ajaxLoad.equals("table")) {
			return pathView + "loadTable";
		}
		return pathView + "index";
	}

	@RequestMapping(value = "/addOrUpdate")
	public @ResponseBody String addOrUpdate(StatusDTO entity) {
		// check trùng thì return "Đã có trong bảng"
		// ok thì return true
		if (entity == null) {
			return "Error, please try again!";
		}
		try {
			StatusDTO check = null;
			check = DanhMucTinhTrangDAO.getStatusById(entity.getId_status());
			StatusDTO result = null;
			if (check == null) {
				entity.setActive("true");
				result = DanhMucTinhTrangDAO.create(entity);
			} else {
				entity.setActive(check.getActive());
				result = DanhMucTinhTrangDAO.update(entity);
			}

		} catch (InterruptedException | ExecutionException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return e.getMessage();
		}
		return "true";
	}

	@RequestMapping(value = "/updateActive", method = RequestMethod.POST)
	public @ResponseBody String updateActive(String idGroup) {
		// check trùng thì return "Đã có trong bảng"
		// ok thì return true
		String ajaxResponse = "";
		try {
			// ajaxResponse = mapper.writeValueAsString(person);
			StatusDTO check = DanhMucTinhTrangDAO.getStatusById(idGroup);
			StatusDTO result = null;
			if (check == null) {
				ajaxResponse = "Danh mục tình trạng không tồn tại";
			} else {
				if (check.getActive().equals("true")) {
					check.setActive("false");
					result = DanhMucTinhTrangDAO.update(check);
					ajaxResponse = "Đã ẩn!";
				} else if (check.getActive().equals("false")) {
					check.setActive("true");
					result = DanhMucTinhTrangDAO.update(check);
					ajaxResponse = "Đã hủy ẩn!";
				} else {
					ajaxResponse = "Có lỗi xảy ra!";
				}
			}
		} catch (InterruptedException | ExecutionException | IOException e) {
			e.printStackTrace();
		}
		return ajaxResponse;
	}

	@RequestMapping(value = "/Delete/{id}")
	public @ResponseBody String delete(@PathVariable("id") String id) {
		try {
			if (id == null || id == "") {
				return "Not found id!";
			} else {
				StatusDTO result = DanhMucTinhTrangDAO.delete(id);
			}
		} catch (InterruptedException | ExecutionException | IOException e) {
			e.printStackTrace();
			return e.getMessage();
		}
		return "true";
	}

	@RequestMapping(value = "/checkId", method = RequestMethod.POST)
	public @ResponseBody String searchPerson(HttpServletRequest request, String idGroup) {
		// ObjectMapper mapper = new ObjectMapper();
		String ajaxResponse = "";
		try {
			// ajaxResponse = mapper.writeValueAsString(person);
			StatusDTO check = DanhMucTinhTrangDAO.getStatusById(idGroup);
			if (check == null) {
				ajaxResponse = "not exist";
			} else {
				ajaxResponse = "existed";
			}
		} catch (InterruptedException | ExecutionException | IOException e) {
			e.printStackTrace();
		}
		return ajaxResponse;
	}
}
