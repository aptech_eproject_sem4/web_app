package c1808g1.aem_web.controllers.CauHinh;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import c1808g1.Models.CauHinh.TagDTO;
import c1808g1.aem_web.DAO.CauHinh.TagDAO;
import c1808g1.aem_web.common.danhMucDungChung;

@Controller
@RequestMapping("/Tag/")
public class TagController {
	String pathView = "Tag/";

	// Index
	@RequestMapping({ "", "/Index" })
	public String index(Model model, @RequestParam(required = false) String ajaxLoad,
			@RequestParam(required = false) Integer page) {
		// get list data
		List<TagDTO> listTag = new ArrayList<TagDTO>();
		try {
			listTag = TagDAO.getAllTag();
		} catch (InterruptedException | ExecutionException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		model.addAttribute("listTag", listTag);
		if (ajaxLoad != null && ajaxLoad.equals("table")) {
			return pathView + "loadTable";
		}
		return pathView + "index";
	}

	@RequestMapping("/addOrUpdate")
	public @ResponseBody String addOrUpdate(TagDTO entity) {
		if (entity == null) {
			return "error";
		}
		try {
			TagDTO check = null;
			check = TagDAO.getTagById(entity.getId_tag());
			TagDTO result = null;
			if (check == null) {
				result = TagDAO.create(entity);
			} else {
				result = TagDAO.update(entity);
			}
		} catch (InterruptedException | ExecutionException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return "true";
	}

	@RequestMapping(value = "/Delete/{id_tag}")
	public @ResponseBody String Delete(@PathVariable("id_tag") String id) {
		TagDTO delete = new TagDTO();
		try {
			delete = TagDAO.delete(id);
		} catch (InterruptedException | ExecutionException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return "true";
	}

	@RequestMapping(value = "/checkId", method = RequestMethod.POST)
	public @ResponseBody String searchPerson(HttpServletRequest request, String idTag) {
		// ObjectMapper mapper = new ObjectMapper();
		String ajaxResponse = "";
		try {
			// ajaxResponse = mapper.writeValueAsString(person);
			TagDTO check = TagDAO.getTagById(idTag);
			if (check == null) {
				ajaxResponse = "not exist";
			} else {
				ajaxResponse = "existed";
			}
		} catch (InterruptedException | ExecutionException | IOException e) {
			e.printStackTrace();
		}
		return ajaxResponse;
	}

}
