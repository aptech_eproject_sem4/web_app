package c1808g1.aem_web.controllers;

import java.io.IOException;
import java.util.concurrent.ExecutionException;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import c1808g1.aem_web.DAO.LoginDAO;
import c1808g1.aem_web.common.MD5;
import c1808g1.aem_web.common.danhMucDungChung;

@Controller
public class LoginController {
	// login page
	@RequestMapping("/login")
	public String Login(Model model) {
		return "Login/index";
	}

	@RequestMapping(value = "/SubmitLogin")
	public @ResponseBody String submitLogin(HttpServletResponse response,String email, String password, String type)
			throws JsonParseException, JsonMappingException, InterruptedException, ExecutionException, IOException {
		var pwd=MD5.GenerateMD5(password);
		var check=LoginDAO.checkUserLogin(type,email,pwd);
		if(check==null){
			return "false";
		}
		var valueCookie=check.getId_user()+"|"+check.getEmail()+"|"+check.getName_user()
						+"|"+check.getCreator()+"|"+check.getId_role()+"|"+check.getList_role()+"|"+check.getImage_student();
		Cookie cookie=new Cookie("sid", valueCookie);
		cookie.setMaxAge(7 * 24 * 60 * 60);
		//add cookie to response
		response.addCookie(cookie);
		return "true";
	}
	@RequestMapping(value = "/LogOut")
	public String logout(HttpServletResponse response){
		Cookie cookie=new Cookie("sid", null);
		cookie.setMaxAge(0);
		//add cookie to response
		response.addCookie(cookie);
		return "Login/index";
	}
	@RequestMapping(value = "/ChangePassword", method = RequestMethod.POST)
	public @ResponseBody String addOrUpdate(HttpServletRequest request, String oldPwd,String newPwd)
            throws InterruptedException, ExecutionException, IOException {
        
        var old=MD5.GenerateMD5(oldPwd);
        var newpass=MD5.GenerateMD5(newPwd);
		var result=LoginDAO.resetPassword(danhMucDungChung.id_rolelogin, danhMucDungChung.id_userlogin, old,newpass);
		if(result.toLowerCase().contains("success")){
			return "true";
		}

        return "false";
    }
}

