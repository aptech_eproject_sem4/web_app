package c1808g1.aem_web.controllers;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import c1808g1.aem_web.common.danhMucDungChung;



@Controller
@RequestMapping("")
public class MainController {
	//fist test page
	@RequestMapping({"","/Home","/"})
	public String home(Model model,HttpServletRequest request) {
		var userlogin=danhMucDungChung.extractCookie(request);
		if(userlogin.equals("")){
			return "Login/index";
		}
		return "Home/index";
	}
	
}
