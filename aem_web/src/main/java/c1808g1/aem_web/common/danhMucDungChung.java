package c1808g1.aem_web.common;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;

import c1808g1.Models.CauHinh.StatusDTO;
import c1808g1.aem_web.DAO.CauHinh.DanhMucTinhTrangDAO;
import c1808g1.aem_web.models.GroupType;

public class danhMucDungChung {
    //default image path
	public static final String DEFAULTIAMGEPATH = "/upload/avatar/dealftUser.png";
	
	// api
		
	public static final String IP_API = "http://localhost:8081/api/";
	//public static final String IP_API = "http://115.73.214.162:8888/aem_api/api/";
    public static String id_userlogin;//mã user đăng nhập
    public static String id_rolelogin;//quyền 1:SV, 2:FC, 3:NV
    public static String creator_login;//cột creator cho các form cần dùng
    public static String list_rolelogin;//để check quyền
    public static String name_userlogin;//tên user đăng nhập
    public static String email_userlogin;//email user đăng nhập

    // region Menu
    public enum MenuParent {
        QLHOSO("Quản lý hồ sơ"), 
        QLCTHOC("Quản lý chương trình học"), 
        DONTU("Đơn từ"),
        QLLICHHOC("Quản lý lịch học"),
        BAOCAOSV("Báo cáo sinh viên"),
        BAOCAOFC("Báo cáo FC"),
        CAUHINH("Cấu hình"),
        HETHONG("Hệ thống");

        private String name;

        MenuParent(String name) {
            this.name = name;
        }

        public String getName() {
            return name;
        }
    }

    public static MenuParent[] GetListMenuParent() {
        return MenuParent.values();
    }

    public static String GetNameMenuParent(String key) {
        String name = "";
        MenuParent menu = MenuParent.valueOf(key);
        name = menu.getName();
        return name;
    }

    // region Status
    public static List<GroupType> GetAllListGroupType() {
        List<GroupType> ls = new ArrayList<GroupType>();
        ls.add(new GroupType("1", "Trạng thái sinh viên"));
        ls.add(new GroupType("2", "Trạng thái giảng viên"));
        ls.add(new GroupType("3", "Trạng thái điểm danh"));
        ls.add(new GroupType("4", "Trạng thái điểm danh SV"));
        ls.add(new GroupType("5", "Trạng thái điểm danh FC"));
        ls.add(new GroupType("6", "Chi nhánh Aptech"));
        ls.add(new GroupType("7", "Loại môn học"));
        ls.add(new GroupType("8", "Loại phiếu thu"));
        ls.add(new GroupType("9", "Danh mục quan hệ"));
        return ls;
    }

    public static List<StatusDTO> TrangThaiSV() {
        return GetListStatusByGroupType("1");
    }

    public static List<StatusDTO> TrangThaiFC() {
        return GetListStatusByGroupType("2");
    }

    public static List<StatusDTO> TrangThaiDiemDanh() {
        return GetListStatusByGroupType("3");
    }

    public static List<StatusDTO> TrangThaiDiemDanhSV() {
        return GetListStatusByGroupType("4");
    }

    public static List<StatusDTO> TrangThaiDiemDanhFC() {
        return GetListStatusByGroupType("5");
    }

    public static List<StatusDTO> ChiNhanh() {
        return GetListStatusByGroupType("6");
    }

    public static List<StatusDTO> LoaiMonHoc() {
        return GetListStatusByGroupType("7");
    }

    public static List<StatusDTO> LoaiPhieuThu() {
        return GetListStatusByGroupType("8");
    }

    public static List<StatusDTO> DanhMucQuanHe() {
        return GetListStatusByGroupType("9");
    }
    public static List<StatusDTO> GetListStatusByGroupType(String id) {
        List<StatusDTO> ls = new ArrayList<StatusDTO>();
        // Gọi DAO của status hàm getByGroupType
        try {
			ls=DanhMucTinhTrangDAO.getStatusByGrouptypeActiveTrue(id);
		} catch (NumberFormatException | InterruptedException | ExecutionException | IOException e) {
			e.printStackTrace();
		}
        return ls;
    }

    public static String GetNameStatus(String id) {
        String name = "";
        // Gọi DAO của status hàm getByID rồi truyền value name_status cho biến name
        StatusDTO st = new StatusDTO();
        try {
			st=DanhMucTinhTrangDAO.getStatusById(id);
		} catch (NumberFormatException | InterruptedException | ExecutionException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        name=st.getName_status();
        return name;
    }
    public static String extractCookie(HttpServletRequest req) {
        String userlogin="";
        Cookie[] c = req.getCookies();
        if(c!=null){
            for (Cookie item:c) {
                if (item.getName().equals("sid"))
                     userlogin= item.getValue();
            }
        }
        return userlogin;
    }
}
