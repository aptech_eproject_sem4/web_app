package c1808g1.aem_web.common;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.Date;

import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

public class FileDinhKem {
    private String name_file;
    private String path_file;
    private String type_file; // field này dành cho list File, còn đơn thì k cần quan tâm

    public FileDinhKem(String name_file, String path_file, String type_file) {
        super();
        this.name_file = name_file;
        this.path_file = path_file;
        this.type_file = type_file;
    }

    public FileDinhKem() {
        super();
    }

    public static final String PATHFILE_AVATAR = "upload/avatar/";
    public static final String PATHFILE_DONTU = "upload/dontu/";
    public static final String PATHFILE_DIEMTHI = "upload/diemthi/";
    public static final String PATHFILE_HETHONG = "upload/fileHeThong/";

    public static FileDinhKem SaveObjectFileDonTu(MultipartFile file,String pathFolder,@RequestParam(required = false) String typeFile) throws IOException {
        FileDinhKem objectFile = new FileDinhKem();
        String pathContainFile = GetPathContainFile() + pathFolder;
        String fileName = file.getOriginalFilename();
        String extension = GetExtensionFile(fileName);
        String newNameFile = new Date().getTime() + "." + extension;
        InputStream is = null;
        try {
            is = file.getInputStream();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return null;
        }
        Files.copy(is, Paths.get(pathContainFile + newNameFile), StandardCopyOption.REPLACE_EXISTING);
        objectFile.name_file=fileName;
        objectFile.path_file=pathFolder+newNameFile;
        objectFile.type_file=typeFile;
        return objectFile;
    }
    public static String SaveFileSingle(MultipartFile file,String pathFolder) throws IOException {
        String pathContainFile = GetPathContainFile() + pathFolder;
        String fileName = new Date().getTime()+"_"+file.getOriginalFilename();
        InputStream is = null;
        try {
            is = file.getInputStream();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return null;
        }
        Files.copy(is, Paths.get(pathContainFile + fileName), StandardCopyOption.REPLACE_EXISTING);
        return pathFolder+fileName;
    }
    public static boolean DeleteFile(String pathFile){
        String pathContainFile = GetPathContainFile();
        String fullPath=pathContainFile+pathFile;
        File fileDelete=new File(fullPath);
        return fileDelete.delete();
    }
    public static boolean DeleteObjectFileSingle(FileDinhKem file){
        String pathContainFile = GetPathContainFile();
        String fullPath=pathContainFile+file.path_file;
        File fileDelete=new File(fullPath);
        return fileDelete.delete();
    }
    public static String GetExtensionFile(String fileName) {
        int index = fileName.lastIndexOf('.');
        if (index > 0) {
            String extension = fileName.substring(index + 1);
            return extension;
        }
        return "";
    }
    public static String GetPathContainFile(){
        Path root = FileSystems.getDefault().getPath("").toAbsolutePath();
        String pathContainFile = root.toString() + "/src/main/resources/static/";
        return pathContainFile;
    }
    
}
