package c1808g1.aem_web.models;

public class GroupType {
    private String id;
	private String name_group;
    public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName_group() {
		return name_group;
	}
	public void setName_group(String name_group) {
		this.name_group = name_group;
	}

    public GroupType(String id, String name_group) {
        super();
        this.id=id;
        this.name_group=name_group;
    }
    public GroupType() {
        super();
    }
}
