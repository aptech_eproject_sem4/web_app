package c1808g1.aem_web.models;


import org.springframework.web.multipart.MultipartFile;

import lombok.Data;

@Data
public class ExcelForm {
    private MultipartFile file;
}
