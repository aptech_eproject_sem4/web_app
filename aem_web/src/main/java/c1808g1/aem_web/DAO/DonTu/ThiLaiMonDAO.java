package c1808g1.aem_web.DAO.DonTu;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.net.http.HttpClient.Version;
import java.net.http.HttpResponse.BodyHandlers;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;

import c1808g1.Models.DonTu.RegisExamDTO;
import c1808g1.aem_web.common.JSONUtils;
import c1808g1.aem_web.common.danhMucDungChung;

public class ThiLaiMonDAO {
	static String mainUri = danhMucDungChung.IP_API + "dontu/thilaimonapi/";
	private static final HttpClient client = HttpClient.newBuilder().version(Version.HTTP_2).build();
	// APIClient client=new APIClient();

	public static List<RegisExamDTO> getAllRegisExam()
			throws InterruptedException, ExecutionException, JsonParseException, JsonMappingException, IOException {
		String subUri = mainUri + "getAll";
		HttpRequest req = HttpRequest.newBuilder(URI.create(subUri)).GET().build();
		CompletableFuture<HttpResponse<String>> response = client.sendAsync(req, BodyHandlers.ofString());
		response.thenAccept(res -> System.out.println(res));
		List<RegisExamDTO> regisexam = JSONUtils.convertFromJsonToList(response.get().body(),
				new TypeReference<List<RegisExamDTO>>() {
				});
		return regisexam;
	}

	public static RegisExamDTO getRegisExamById(String id) throws InterruptedException, ExecutionException, IOException {
		String subUri = mainUri + "getRegisExamById/";
		HttpRequest req = HttpRequest.newBuilder(URI.create(subUri + String.valueOf(id))).GET().build();
		CompletableFuture<HttpResponse<String>> response = client.sendAsync(req, BodyHandlers.ofString());
		response.thenAccept(res -> System.out.println(res));
		if (response.get().statusCode() == 204) {
			System.out.println("Product Not Avaialble");
			return null;
		} else {
			RegisExamDTO regisexam = JSONUtils.covertFromJsonToObject(response.get().body(), RegisExamDTO.class);
			return regisexam;
		}
	}

	public static RegisExamDTO create(RegisExamDTO h) throws InterruptedException, ExecutionException, IOException {
		String subUri = mainUri + "create";
		String inputJson = JSONUtils.covertFromObjectToJson(h);
		HttpRequest request = HttpRequest.newBuilder(URI.create(subUri)).header("Content-Type", "application/json")
				.POST(HttpRequest.BodyPublishers.ofString(inputJson)).build();
		CompletableFuture<HttpResponse<String>> response = client.sendAsync(request,
				HttpResponse.BodyHandlers.ofString());
		System.out.println(response.get().body());
		RegisExamDTO regisexam=JSONUtils.covertFromJsonToObject(response.get().body(), RegisExamDTO.class);
		return regisexam;
	}
	
	public static RegisExamDTO update(RegisExamDTO h) throws InterruptedException, ExecutionException, IOException {
		String subUri = mainUri + "update";
		String inputJson = JSONUtils.covertFromObjectToJson(h);
		HttpRequest request = HttpRequest.newBuilder(URI.create(subUri)).header("Content-Type", "application/json")
				.PUT(HttpRequest.BodyPublishers.ofString(inputJson)).build();
		CompletableFuture<HttpResponse<String>> response = client.sendAsync(request,
				HttpResponse.BodyHandlers.ofString());
		if (response.get().statusCode() == 204) {
			System.out.println("Product Not Avaialble");
			return null;
		} else {
			c1808g1.Models.DonTu.RegisExamDTO regisexam=JSONUtils.covertFromJsonToObject(response.get().body(), RegisExamDTO.class);
			return regisexam;
		}		
	}
	
	public static RegisExamDTO delete(String id) throws InterruptedException, ExecutionException, IOException {
		String subUri = mainUri + "delete/";
		HttpRequest request = HttpRequest.newBuilder(URI.create(subUri+String.valueOf(id))).DELETE().build();;
		CompletableFuture<HttpResponse<String>> response = client.sendAsync(request,
				HttpResponse.BodyHandlers.ofString());
		if (response.get().statusCode() == 500) {
			System.out.println("Product Not Avaialble");
			return null;
		} else {
			RegisExamDTO regisexam=JSONUtils.covertFromJsonToObject(response.get().body(), RegisExamDTO.class);
			return regisexam;
		}		
	}
}
