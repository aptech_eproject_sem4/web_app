package c1808g1.aem_web.DAO.HeThong;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.net.http.HttpClient.Version;
import java.net.http.HttpResponse.BodyHandlers;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;

import c1808g1.Models.HeThong.PermissionRoleDTO;
import c1808g1.aem_web.common.JSONUtils;
import c1808g1.aem_web.common.danhMucDungChung;

public class PermissionRoleDao {
	static String mainUri = danhMucDungChung.IP_API + "hethong/permission_role_api/";
	private static final HttpClient client = HttpClient.newBuilder().version(Version.HTTP_2).build();
	// APIClient client=new APIClient();

	public static List<PermissionRoleDTO> getAllPermissionRole()
			throws InterruptedException, ExecutionException, JsonParseException, JsonMappingException, IOException {
		String subUri = mainUri + "getAll";
		HttpRequest req = HttpRequest.newBuilder(URI.create(subUri)).GET().build();
		CompletableFuture<HttpResponse<String>> response = client.sendAsync(req, BodyHandlers.ofString());
		response.thenAccept(res -> System.out.println(res));
		List<PermissionRoleDTO> prole = JSONUtils.convertFromJsonToList(response.get().body(),
				new TypeReference<List<PermissionRoleDTO>>() {
				});
		return prole;
	}
	public static PermissionRoleDTO getPermissionRoleById(String id) throws InterruptedException, ExecutionException, IOException {
		String subUri = mainUri + "getPermissionRoleById/";
		HttpRequest req = HttpRequest.newBuilder(URI.create(subUri + String.valueOf(id))).GET().build();
		CompletableFuture<HttpResponse<String>> response = client.sendAsync(req, BodyHandlers.ofString());
		response.thenAccept(res -> System.out.println(res));
		if (response.get().statusCode() == 204) {
			System.out.println("Product Not Avaialble");
			return null;
		} else {
			PermissionRoleDTO prole = JSONUtils.covertFromJsonToObject(response.get().body(), PermissionRoleDTO.class);
			return prole;
		}
	}

	public static PermissionRoleDTO create(PermissionRoleDTO h) throws InterruptedException, ExecutionException, IOException {
		String subUri = mainUri + "create";
		String inputJson = JSONUtils.covertFromObjectToJson(h);
		HttpRequest request = HttpRequest.newBuilder(URI.create(subUri)).header("Content-Type", "application/json")
				.POST(HttpRequest.BodyPublishers.ofString(inputJson)).build();
		CompletableFuture<HttpResponse<String>> response = client.sendAsync(request,
				HttpResponse.BodyHandlers.ofString());
		System.out.println(response.get().body());
		PermissionRoleDTO prole=JSONUtils.covertFromJsonToObject(response.get().body(), PermissionRoleDTO.class);
		return prole;
	}
	
	public static PermissionRoleDTO update(PermissionRoleDTO h) throws InterruptedException, ExecutionException, IOException {
		String subUri = mainUri + "update";
		String inputJson = JSONUtils.covertFromObjectToJson(h);
		HttpRequest request = HttpRequest.newBuilder(URI.create(subUri)).header("Content-Type", "application/json")
				.PUT(HttpRequest.BodyPublishers.ofString(inputJson)).build();
		CompletableFuture<HttpResponse<String>> response = client.sendAsync(request,
				HttpResponse.BodyHandlers.ofString());
		if (response.get().statusCode() == 204) {
			System.out.println("Product Not Avaialble");
			return null;
		} else {
			PermissionRoleDTO prole=JSONUtils.covertFromJsonToObject(response.get().body(), PermissionRoleDTO.class);
			return prole;
		}		
	}
	public static List<PermissionRoleDTO> getPermissionByRoleid(int id) throws InterruptedException, ExecutionException, IOException {
		String subUri = mainUri + "getPermissionByRoleid/";
		HttpRequest request = HttpRequest.newBuilder(URI.create(subUri+String.valueOf(id))).GET().build();
		CompletableFuture<HttpResponse<String>> response = client.sendAsync(request, BodyHandlers.ofString());
		response.thenAccept(res -> System.out.println(res));
		if (response.get().statusCode() == 204) {
			System.out.println("Product Not Avaialble");
			return null;
		} else {
			List<PermissionRoleDTO> prole = JSONUtils.convertFromJsonToList(response.get().body(),
			new TypeReference<List<PermissionRoleDTO>>() {
			});
			return prole;
		}
		
	}
	public static List<PermissionRoleDTO> deletePermissionRoleByRoleid(int id) throws InterruptedException, ExecutionException, IOException {
		String subUri = mainUri + "deletePermissionRoleByRoleid/";
		HttpRequest request = HttpRequest.newBuilder(URI.create(subUri+String.valueOf(id))).DELETE().build();;
		CompletableFuture<HttpResponse<String>> response = client.sendAsync(request,
				HttpResponse.BodyHandlers.ofString());
		if (response.get().statusCode() == 500) {
			System.out.println("Product Not Avaialble");
			return null;
		} else {
			List<PermissionRoleDTO> prole=JSONUtils.convertFromJsonToList(response.get().body(),
					new TypeReference<List<PermissionRoleDTO>>() {
					});
			return prole;
		}		
	}
	
	public static List<PermissionRoleDTO> deletePermissionRoleByControllerid(String id) throws InterruptedException, ExecutionException, IOException {
		String subUri = mainUri + "deletePermissionRoleByControllerid/";
		HttpRequest request = HttpRequest.newBuilder(URI.create(subUri+String.valueOf(id))).DELETE().build();;
		CompletableFuture<HttpResponse<String>> response = client.sendAsync(request,
				HttpResponse.BodyHandlers.ofString());
		if (response.get().statusCode() == 500) {
			System.out.println("Product Not Avaialble");
			return null;
		} else {
			List<PermissionRoleDTO> prole=JSONUtils.convertFromJsonToList(response.get().body(),
					new TypeReference<List<PermissionRoleDTO>>() {
					});
			return prole;
		}		
	}
	
	public static PermissionRoleDTO delete(String id) throws InterruptedException, ExecutionException, IOException {
		String subUri = mainUri + "delete/";
		HttpRequest request = HttpRequest.newBuilder(URI.create(subUri+String.valueOf(id))).DELETE().build();;
		CompletableFuture<HttpResponse<String>> response = client.sendAsync(request,
				HttpResponse.BodyHandlers.ofString());
		if (response.get().statusCode() == 500) {
			System.out.println("Product Not Avaialble");
			return null;
		} else {
			PermissionRoleDTO prole=JSONUtils.covertFromJsonToObject(response.get().body(), PermissionRoleDTO.class);
			return prole;
		}		
	}



}
