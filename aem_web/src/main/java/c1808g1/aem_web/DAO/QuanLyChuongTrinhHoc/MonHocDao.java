package c1808g1.aem_web.DAO.QuanLyChuongTrinhHoc;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.net.http.HttpClient.Version;
import java.net.http.HttpResponse.BodyHandlers;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;

import c1808g1.Models.QuanLyChuongTrinhHoc.SubjectDTO;
import c1808g1.aem_web.common.JSONUtils;
import c1808g1.aem_web.common.danhMucDungChung;

public class MonHocDao {
	static String mainUri = danhMucDungChung.IP_API + "quanlychuongtrinhhoc/subjectapi/";
	private static final HttpClient client = HttpClient.newBuilder().version(Version.HTTP_2).build();
	// APIClient client=new APIClient();

	public static List<SubjectDTO> getAllSubject()
			throws InterruptedException, ExecutionException, JsonParseException, JsonMappingException, IOException {
		String subUri = mainUri + "getAll";
		HttpRequest req = HttpRequest.newBuilder(URI.create(subUri)).GET().build();
		CompletableFuture<HttpResponse<String>> response = client.sendAsync(req, BodyHandlers.ofString());
		response.thenAccept(res -> System.out.println(res));
		List<SubjectDTO> sub = JSONUtils.convertFromJsonToList(response.get().body(),
				new TypeReference<List<SubjectDTO>>() {
				});
		return sub;
	}

	public static List<SubjectDTO> getSubjectOrderByOrderNumberASC()
			throws InterruptedException, ExecutionException, JsonParseException, JsonMappingException, IOException {
		String subUri = mainUri + "getSubjectOrderByOrderNumberASC";
		HttpRequest req = HttpRequest.newBuilder(URI.create(subUri)).GET().build();
		CompletableFuture<HttpResponse<String>> response = client.sendAsync(req, BodyHandlers.ofString());
		response.thenAccept(res -> System.out.println(res));
		List<SubjectDTO> sub = JSONUtils.convertFromJsonToList(response.get().body(),
				new TypeReference<List<SubjectDTO>>() {
				});
		return sub;
	}

	public static SubjectDTO getSubjectById(String id) throws InterruptedException, ExecutionException, IOException {
		String subUri = mainUri + "getSubjectById/";
		HttpRequest req = HttpRequest.newBuilder(URI.create(subUri + String.valueOf(id))).GET().build();
		CompletableFuture<HttpResponse<String>> response = client.sendAsync(req, BodyHandlers.ofString());
		response.thenAccept(res -> System.out.println(res));
		if (response.get().statusCode() == 204 || response.get().statusCode() == 404
				|| response.get().statusCode() == 500) {
			System.out.println("Product Not Avaialble");
			return null;
		} else {
			SubjectDTO sub = JSONUtils.covertFromJsonToObject(response.get().body(), SubjectDTO.class);
			return sub;
		}
	}

	public static List<SubjectDTO> checkSubjectByValue(String searchValue)
			throws InterruptedException, ExecutionException, IOException {
		String subUri = mainUri + "checkSubjectByValue/";
		HttpRequest req = HttpRequest.newBuilder(URI.create(subUri + String.valueOf(searchValue))).GET().build();
		CompletableFuture<HttpResponse<String>> response = client.sendAsync(req, BodyHandlers.ofString());
		response.thenAccept(res -> System.out.println(res));
		if (response.get().statusCode() == 204 || response.get().statusCode() == 500
				|| response.get().statusCode() == 40) {
			System.out.println("Product Not Avaialble");
			return null;
		} else {
			List<SubjectDTO> scofc = JSONUtils.convertFromJsonToList(response.get().body(),
					new TypeReference<List<SubjectDTO>>() {
					});
			return scofc;
		}
	}

	public static List<SubjectDTO> getSubjectByMultipleParameter(int idSemester, String idStatus, String searchValue)
			throws InterruptedException, ExecutionException, JsonParseException, JsonMappingException, IOException {
		String subUri = mainUri + "getSubjectByMultipleParameter/";
		String fullUri = null;
		String fullUrifinal = null;
		if (String.valueOf(idSemester) == "null" || String.valueOf(idSemester) == null
				|| String.valueOf(idSemester) == "" || idSemester == 0) {
			idSemester = 0;
		}
		if (idStatus == null || idStatus == "" || idStatus == "null" || idStatus == "0") {
			idStatus = "0";
		}
		if (searchValue == null || searchValue == "" || searchValue == "null") {
			searchValue = "";
		}

		fullUri = subUri + String.valueOf(idSemester) + "/" + idStatus + "/" + searchValue;
		String lastChar = fullUri.substring(fullUri.length() - 1);
		if (lastChar.equals("/")) {
			fullUrifinal = fullUri + "0";
		} else {
			fullUrifinal = subUri + String.valueOf(idSemester) + "/" + idStatus + "/" + searchValue;
		}
		HttpRequest req = HttpRequest.newBuilder(URI.create(fullUrifinal)).GET().build();
		CompletableFuture<HttpResponse<String>> response = client.sendAsync(req, BodyHandlers.ofString());
		response.thenAccept(res -> System.out.println(res));
		if (response.get().statusCode() == 204 || response.get().statusCode() == 500
				|| response.get().statusCode() == 404) {
			System.out.println("Product Not Avaialble");
			return null;
		} else {
			List<SubjectDTO> sub = JSONUtils.convertFromJsonToList(response.get().body(),
					new TypeReference<List<SubjectDTO>>() {
					});
			return sub;
		}
	}

	public static SubjectDTO create(SubjectDTO h) throws InterruptedException, ExecutionException, IOException {
		String subUri = mainUri + "create";
		String inputJson = JSONUtils.covertFromObjectToJson(h);
		HttpRequest request = HttpRequest.newBuilder(URI.create(subUri)).header("Content-Type", "application/json")
				.POST(HttpRequest.BodyPublishers.ofString(inputJson)).build();
		CompletableFuture<HttpResponse<String>> response = client.sendAsync(request,
				HttpResponse.BodyHandlers.ofString());
		System.out.println(response.get().body());
		SubjectDTO sub = JSONUtils.covertFromJsonToObject(response.get().body(), SubjectDTO.class);
		return sub;
	}

	public static SubjectDTO update(SubjectDTO h) throws InterruptedException, ExecutionException, IOException {
		String subUri = mainUri + "update";
		String inputJson = JSONUtils.covertFromObjectToJson(h);
		HttpRequest request = HttpRequest.newBuilder(URI.create(subUri)).header("Content-Type", "application/json")
				.PUT(HttpRequest.BodyPublishers.ofString(inputJson)).build();
		CompletableFuture<HttpResponse<String>> response = client.sendAsync(request,
				HttpResponse.BodyHandlers.ofString());
		if (response.get().statusCode() == 204 || response.get().statusCode() == 500
				|| response.get().statusCode() == 404) {
			System.out.println("Product Not Avaialble");
			return null;
		} else {
			SubjectDTO sub = JSONUtils.covertFromJsonToObject(response.get().body(), SubjectDTO.class);
			return sub;
		}
	}

	public static SubjectDTO delete(String id) throws InterruptedException, ExecutionException, IOException {
		String subUri = mainUri + "delete/";
		HttpRequest request = HttpRequest.newBuilder(URI.create(subUri + String.valueOf(id))).DELETE().build();
		;
		CompletableFuture<HttpResponse<String>> response = client.sendAsync(request,
				HttpResponse.BodyHandlers.ofString());
		if (response.get().statusCode() == 500 || response.get().statusCode() == 204
				|| response.get().statusCode() == 404) {
			System.out.println("Product Not Avaialble");
			return null;
		} else {
			SubjectDTO sub = JSONUtils.covertFromJsonToObject(response.get().body(), SubjectDTO.class);
			return sub;
		}
	}

}
