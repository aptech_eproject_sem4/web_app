package c1808g1.aem_web.DAO;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.net.http.HttpClient.Version;
import java.net.http.HttpResponse.BodyHandlers;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;

import c1808g1.Models.UserLoginDTO;
import c1808g1.aem_web.common.JSONUtils;
import c1808g1.aem_web.common.danhMucDungChung;

public class LoginDAO {
	static String mainUri = danhMucDungChung.IP_API + "loginapi/";
	private static final HttpClient client = HttpClient.newBuilder().version(Version.HTTP_2).build();

	public static UserLoginDTO checkUserLogin(String id_role, String email, String password) throws InterruptedException, ExecutionException, JsonParseException, JsonMappingException, IOException{
		String subUri = mainUri + "checkLogin/";
		HttpRequest req = HttpRequest.newBuilder(URI.create(subUri+String.valueOf(id_role)+"/"+email+"/"+password)).GET().build();
		CompletableFuture<HttpResponse<String>> response = client.sendAsync(req, BodyHandlers.ofString());
		response.thenAccept(res -> System.out.println(res));
		if (response.get().statusCode() == 204||response.get().statusCode() == 500||response.get().statusCode() == 404) {
			System.out.println("Product Not Avaialble");
			return null;
		} else {
			UserLoginDTO usdto = JSONUtils.covertFromJsonToObject(response.get().body(), UserLoginDTO.class);
			return usdto;
		}
	}
	
	public static String resetPassword(String id_role, String id_user, String oldpass, String newpass) throws InterruptedException, ExecutionException, JsonParseException, JsonMappingException, IOException{
		String subUri = mainUri + "rspass/";
		HttpRequest req = HttpRequest.newBuilder(URI.create(subUri+String.valueOf(id_role)+"/"+id_user+"/"+oldpass+"/"+newpass)).GET().build();
		CompletableFuture<HttpResponse<String>> response = client.sendAsync(req, BodyHandlers.ofString());
		response.thenAccept(res -> System.out.println(res));
		if (response.get().statusCode() == 204||response.get().statusCode() == 500||response.get().statusCode() == 404) {
			System.out.println("Not available");
			return response.get().body();
		} else {
			System.out.println("Success");
			return response.get().body();
		}
	}
}
