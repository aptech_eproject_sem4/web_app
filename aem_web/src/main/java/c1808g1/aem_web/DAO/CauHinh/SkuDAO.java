package c1808g1.aem_web.DAO.CauHinh;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.net.http.HttpClient.Version;
import java.net.http.HttpResponse.BodyHandlers;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;

import c1808g1.Models.CauHinh.SkuDTO;
import c1808g1.aem_web.common.JSONUtils;
import c1808g1.aem_web.common.danhMucDungChung;

public class SkuDAO {
	static String mainUri = danhMucDungChung.IP_API + "cauhinh/skuapi/";
	private static final HttpClient client = HttpClient.newBuilder().version(Version.HTTP_2).build();
	// APIClient client=new APIClient();

	public static List<SkuDTO> getAllSku()
			throws InterruptedException, ExecutionException, JsonParseException, JsonMappingException, IOException {
		String subUri = mainUri + "getAll";
		HttpRequest req = HttpRequest.newBuilder(URI.create(subUri)).GET().build();
		CompletableFuture<HttpResponse<String>> response = client.sendAsync(req, BodyHandlers.ofString());
		response.thenAccept(res -> System.out.println(res));
		List<SkuDTO> sku = JSONUtils.convertFromJsonToList(response.get().body(),
				new TypeReference<List<SkuDTO>>() {
				});
		return sku;
	}

	public static SkuDTO getSkuById(String id) throws InterruptedException, ExecutionException, IOException {
		String subUri = mainUri + "getSkuById/";
		HttpRequest req = HttpRequest.newBuilder(URI.create(subUri + String.valueOf(id))).GET().build();
		CompletableFuture<HttpResponse<String>> response = client.sendAsync(req, BodyHandlers.ofString());
		response.thenAccept(res -> System.out.println(res));
		if (response.get().statusCode() == 204||response.get().statusCode() == 404||response.get().statusCode() == 500) {
			System.out.println("Product Not Avaialble");
			return null;
		} else {
			SkuDTO sku = JSONUtils.covertFromJsonToObject(response.get().body(), SkuDTO.class);
			return sku;
		}
	}
	
	public static List<SkuDTO> getSkuByType(Boolean type)
			throws InterruptedException, ExecutionException, JsonParseException, JsonMappingException, IOException {
		String subUri = mainUri + "getSkuByType/";
		HttpRequest req = HttpRequest.newBuilder(URI.create(subUri+ String.valueOf(type))).GET().build();
		CompletableFuture<HttpResponse<String>> response = client.sendAsync(req, BodyHandlers.ofString());
		response.thenAccept(res -> System.out.println(res));
		List<SkuDTO> sku = JSONUtils.convertFromJsonToList(response.get().body(),
				new TypeReference<List<SkuDTO>>() {
				});
		return sku;
	}

	public static SkuDTO create(SkuDTO h) throws InterruptedException, ExecutionException, IOException {
		String subUri = mainUri + "create";
		String inputJson = JSONUtils.covertFromObjectToJson(h);
		HttpRequest request = HttpRequest.newBuilder(URI.create(subUri)).header("Content-Type", "application/json")
				.POST(HttpRequest.BodyPublishers.ofString(inputJson)).build();
		CompletableFuture<HttpResponse<String>> response = client.sendAsync(request,
				HttpResponse.BodyHandlers.ofString());
		System.out.println(response.get().body());
		SkuDTO sku=JSONUtils.covertFromJsonToObject(response.get().body(), SkuDTO.class);
		return sku;
	}
	
	public static SkuDTO update(SkuDTO h) throws InterruptedException, ExecutionException, IOException {
		String subUri = mainUri + "update";
		String inputJson = JSONUtils.covertFromObjectToJson(h);
		HttpRequest request = HttpRequest.newBuilder(URI.create(subUri)).header("Content-Type", "application/json")
				.PUT(HttpRequest.BodyPublishers.ofString(inputJson)).build();
		CompletableFuture<HttpResponse<String>> response = client.sendAsync(request,
				HttpResponse.BodyHandlers.ofString());
		if (response.get().statusCode() == 204) {
			System.out.println("Product Not Avaialble");
			return null;
		} else {
			SkuDTO sku=JSONUtils.covertFromJsonToObject(response.get().body(), SkuDTO.class);
			return sku;
		}		
	}
	
	public static SkuDTO delete(String id) throws InterruptedException, ExecutionException, IOException {
		String subUri = mainUri + "delete/";
		HttpRequest request = HttpRequest.newBuilder(URI.create(subUri+String.valueOf(id))).DELETE().build();;
		CompletableFuture<HttpResponse<String>> response = client.sendAsync(request,
				HttpResponse.BodyHandlers.ofString());
		if (response.get().statusCode() == 500) {
			System.out.println("Product Not Avaialble");
			return null;
		} else {
			SkuDTO sku=JSONUtils.covertFromJsonToObject(response.get().body(), SkuDTO.class);
			return sku;
		}		
	}
}
