package c1808g1.aem_web.DAO.QuanLyHoSo;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.net.http.HttpClient.Version;
import java.net.http.HttpResponse.BodyHandlers;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;

import c1808g1.Models.QuanLiHoSo.GradeSalaryFcDTO;
import c1808g1.aem_web.common.JSONUtils;
import c1808g1.aem_web.common.danhMucDungChung;

public class GradeSalaryFcDao {
	static String mainUri = danhMucDungChung.IP_API + "quanlyhoso/grade_salary_fc_api/";
	private static final HttpClient client = HttpClient.newBuilder().version(Version.HTTP_2).build();
	// APIClient client=new APIClient();

	public static List<GradeSalaryFcDTO> getAllGradeSalaryFc()
			throws InterruptedException, ExecutionException, JsonParseException, JsonMappingException, IOException {
		String subUri = mainUri + "getAll";
		HttpRequest req = HttpRequest.newBuilder(URI.create(subUri)).GET().build();
		CompletableFuture<HttpResponse<String>> response = client.sendAsync(req, BodyHandlers.ofString());
		response.thenAccept(res -> System.out.println(res));
		List<GradeSalaryFcDTO> grasafc = JSONUtils.convertFromJsonToList(response.get().body(),
				new TypeReference<List<GradeSalaryFcDTO>>() {
				});
		return grasafc;
	}

	public static GradeSalaryFcDTO getGradeSalaryFcById(String id) throws InterruptedException, ExecutionException, IOException {
		String subUri = mainUri + "getGradeSalaryFcById/";
		HttpRequest req = HttpRequest.newBuilder(URI.create(subUri + String.valueOf(id))).GET().build();
		CompletableFuture<HttpResponse<String>> response = client.sendAsync(req, BodyHandlers.ofString());
		response.thenAccept(res -> System.out.println(res));
		if (response.get().statusCode() == 204||response.get().statusCode() == 404||response.get().statusCode() == 500) {
			System.out.println("Product Not Avaialble");
			return null;
		} else {
			GradeSalaryFcDTO grasafc = JSONUtils.covertFromJsonToObject(response.get().body(), GradeSalaryFcDTO.class);
			return grasafc;
		}
	}
	public static List<GradeSalaryFcDTO> getGradeFcByFCId(String id) throws InterruptedException, ExecutionException, IOException {
		String subUri = mainUri + "getGradeFCByFCId/";
		HttpRequest req = HttpRequest.newBuilder(URI.create(subUri + String.valueOf(id))).GET().build();
		CompletableFuture<HttpResponse<String>> response = client.sendAsync(req, BodyHandlers.ofString());
		response.thenAccept(res -> System.out.println(res));
		if (response.get().statusCode() == 204||response.get().statusCode() == 404||response.get().statusCode() == 500) {
			System.out.println("Product Not Avaialble");
			return null;
		} else {
			List<GradeSalaryFcDTO> grasafc = JSONUtils.convertFromJsonToList(response.get().body(),
					new TypeReference<List<GradeSalaryFcDTO>>() {
					});
			return grasafc;
		}
	}

	public static GradeSalaryFcDTO create(GradeSalaryFcDTO h) throws InterruptedException, ExecutionException, IOException {
		String subUri = mainUri + "create";
		String inputJson = JSONUtils.covertFromObjectToJson(h);
		HttpRequest request = HttpRequest.newBuilder(URI.create(subUri)).header("Content-Type", "application/json")
				.POST(HttpRequest.BodyPublishers.ofString(inputJson)).build();
		CompletableFuture<HttpResponse<String>> response = client.sendAsync(request,
				HttpResponse.BodyHandlers.ofString());
		System.out.println(response.get().body());
		GradeSalaryFcDTO grasafc=JSONUtils.covertFromJsonToObject(response.get().body(), GradeSalaryFcDTO.class);
		return grasafc;
	}
	
	public static GradeSalaryFcDTO update(GradeSalaryFcDTO h) throws InterruptedException, ExecutionException, IOException {
		String subUri = mainUri + "update";
		String inputJson = JSONUtils.covertFromObjectToJson(h);
		HttpRequest request = HttpRequest.newBuilder(URI.create(subUri)).header("Content-Type", "application/json")
				.PUT(HttpRequest.BodyPublishers.ofString(inputJson)).build();
		CompletableFuture<HttpResponse<String>> response = client.sendAsync(request,
				HttpResponse.BodyHandlers.ofString());
		if (response.get().statusCode() == 204) {
			System.out.println("Product Not Avaialble");
			return null;
		} else {
			GradeSalaryFcDTO grasafc=JSONUtils.covertFromJsonToObject(response.get().body(), GradeSalaryFcDTO.class);
			return grasafc;
		}		
	}
	
	public static GradeSalaryFcDTO delete(String id) throws InterruptedException, ExecutionException, IOException {
		String subUri = mainUri + "delete/";
		HttpRequest request = HttpRequest.newBuilder(URI.create(subUri+String.valueOf(id))).DELETE().build();;
		CompletableFuture<HttpResponse<String>> response = client.sendAsync(request,
				HttpResponse.BodyHandlers.ofString());
		if (response.get().statusCode() == 500) {
			System.out.println("Product Not Avaialble");
			return null;
		} else {
			GradeSalaryFcDTO grasafc=JSONUtils.covertFromJsonToObject(response.get().body(), GradeSalaryFcDTO.class);
			return grasafc;
		}		
	}



}
