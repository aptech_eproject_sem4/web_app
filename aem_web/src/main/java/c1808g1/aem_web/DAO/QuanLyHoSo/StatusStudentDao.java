package c1808g1.aem_web.DAO.QuanLyHoSo;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.net.http.HttpClient.Version;
import java.net.http.HttpResponse.BodyHandlers;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;

import c1808g1.Models.QuanLiHoSo.StatusStudentDTO;
import c1808g1.aem_web.common.JSONUtils;
import c1808g1.aem_web.common.danhMucDungChung;

public class StatusStudentDao {
	static String mainUri = danhMucDungChung.IP_API + "quanlyhoso/status_student_api/";
	private static final HttpClient client = HttpClient.newBuilder().version(Version.HTTP_2).build();
	// APIClient client=new APIClient();

	public static List<StatusStudentDTO> getAllStatusStudent()
			throws InterruptedException, ExecutionException, JsonParseException, JsonMappingException, IOException {
		String subUri = mainUri + "getAll";
		HttpRequest req = HttpRequest.newBuilder(URI.create(subUri)).GET().build();
		CompletableFuture<HttpResponse<String>> response = client.sendAsync(req, BodyHandlers.ofString());
		response.thenAccept(res -> System.out.println(res));
		List<StatusStudentDTO> stastd = JSONUtils.convertFromJsonToList(response.get().body(),
				new TypeReference<List<StatusStudentDTO>>() {
				});
		return stastd;
	}

	public static List<StatusStudentDTO> findStatusStudentByIdStudent(String id)
			throws InterruptedException, ExecutionException, JsonParseException, JsonMappingException, IOException {
		String subUri = mainUri + "findStatusStudentByIdStudent/";
		HttpRequest req = HttpRequest.newBuilder(URI.create(subUri+String.valueOf(id))).GET().build();
		CompletableFuture<HttpResponse<String>> response = client.sendAsync(req, BodyHandlers.ofString());
		response.thenAccept(res -> System.out.println(res));
		if (response.get().statusCode() == 204||response.get().statusCode() == 404||response.get().statusCode() == 500) {
			System.out.println("Product Not Avaialble");
			return null;
		} else {
			List<StatusStudentDTO> stastd = JSONUtils.convertFromJsonToList(response.get().body(),
					new TypeReference<List<StatusStudentDTO>>() {
					});
			return stastd;
		}
	}
	
	public static StatusStudentDTO getLatestStatusStudentByStudentId(String student_id)
			throws InterruptedException, ExecutionException, JsonParseException, JsonMappingException, IOException {
		String subUri = mainUri + "getLatestStatusStudentByStudentId/";
		HttpRequest req = HttpRequest.newBuilder(URI.create(subUri+String.valueOf(student_id))).GET().build();
		CompletableFuture<HttpResponse<String>> response = client.sendAsync(req, BodyHandlers.ofString());
		response.thenAccept(res -> System.out.println(res));
		if (response.get().statusCode() == 204||response.get().statusCode() == 404||response.get().statusCode() == 500) {
			System.out.println("Product Not Avaialble");
			return null;
		} else {
			StatusStudentDTO stastd = JSONUtils.covertFromJsonToObject(response.get().body(), StatusStudentDTO.class);
			return stastd;
		}
	}
	
	public static StatusStudentDTO getLatestStatusStudentByStudentIdAndStatusId(String student_id,String status_id)
			throws InterruptedException, ExecutionException, JsonParseException, JsonMappingException, IOException {
		String subUri = mainUri + "getLatestStatusStudentByStudentIdAndStatusId/";
		HttpRequest req = HttpRequest.newBuilder(URI.create(subUri+String.valueOf(student_id)+"/"+String.valueOf(status_id))).GET().build();
		CompletableFuture<HttpResponse<String>> response = client.sendAsync(req, BodyHandlers.ofString());
		response.thenAccept(res -> System.out.println(res));
		if (response.get().statusCode() == 204||response.get().statusCode() == 404||response.get().statusCode() == 500) {
			System.out.println("Product Not Avaialble");
			return null;
		} else {
			StatusStudentDTO stastd = JSONUtils.covertFromJsonToObject(response.get().body(), StatusStudentDTO.class);
			return stastd;
		}
	}

	public static StatusStudentDTO getStatusStudentById(String id)
			throws InterruptedException, ExecutionException, IOException {
		String subUri = mainUri + "getStatusStudentById/";
		HttpRequest req = HttpRequest.newBuilder(URI.create(subUri + String.valueOf(id))).GET().build();
		CompletableFuture<HttpResponse<String>> response = client.sendAsync(req, BodyHandlers.ofString());
		response.thenAccept(res -> System.out.println(res));
		if (response.get().statusCode() == 204||response.get().statusCode() == 404||response.get().statusCode() == 500) {
			System.out.println("Product Not Avaialble");
			return null;
		} else {
			StatusStudentDTO stastd = JSONUtils.covertFromJsonToObject(response.get().body(), StatusStudentDTO.class);
			return stastd;
		}
	}

	public static StatusStudentDTO create(StatusStudentDTO h)
			throws InterruptedException, ExecutionException, IOException {
		String subUri = mainUri + "create";
		String inputJson = JSONUtils.covertFromObjectToJson(h);
		HttpRequest request = HttpRequest.newBuilder(URI.create(subUri)).header("Content-Type", "application/json")
				.POST(HttpRequest.BodyPublishers.ofString(inputJson)).build();
		CompletableFuture<HttpResponse<String>> response = client.sendAsync(request,
				HttpResponse.BodyHandlers.ofString());
		System.out.println(response.get().body());
		StatusStudentDTO stastd = JSONUtils.covertFromJsonToObject(response.get().body(), StatusStudentDTO.class);
		return stastd;
	}

	public static StatusStudentDTO update(StatusStudentDTO h)
			throws InterruptedException, ExecutionException, IOException {
		String subUri = mainUri + "update";
		String inputJson = JSONUtils.covertFromObjectToJson(h);
		HttpRequest request = HttpRequest.newBuilder(URI.create(subUri)).header("Content-Type", "application/json")
				.PUT(HttpRequest.BodyPublishers.ofString(inputJson)).build();
		CompletableFuture<HttpResponse<String>> response = client.sendAsync(request,
				HttpResponse.BodyHandlers.ofString());
		if (response.get().statusCode() == 204) {
			System.out.println("Product Not Avaialble");
			return null;
		} else {
			StatusStudentDTO stastd = JSONUtils.covertFromJsonToObject(response.get().body(), StatusStudentDTO.class);
			return stastd;
		}
	}

	public static StatusStudentDTO delete(String id) throws InterruptedException, ExecutionException, IOException {
		String subUri = mainUri + "delete/";
		HttpRequest request = HttpRequest.newBuilder(URI.create(subUri + String.valueOf(id))).DELETE().build();

		CompletableFuture<HttpResponse<String>> response = client.sendAsync(request,
				HttpResponse.BodyHandlers.ofString());
		if (response.get().statusCode() == 500) {
			System.out.println("Product Not Avaialble");
			return null;
		} else {
			StatusStudentDTO stastd = JSONUtils.covertFromJsonToObject(response.get().body(), StatusStudentDTO.class);
			return stastd;
		}
	}

	public static List<StatusStudentDTO> getListStudentStatusByStudentId(String id)
			throws InterruptedException, ExecutionException, JsonParseException, JsonMappingException, IOException {
		String subUri = mainUri + "getListStudentStatusByStudentId/" + id;
		HttpRequest req = HttpRequest.newBuilder(URI.create(subUri)).GET().build();
		CompletableFuture<HttpResponse<String>> response = client.sendAsync(req, BodyHandlers.ofString());
		response.thenAccept(res -> System.out.println(res));
		List<StatusStudentDTO> stastd = JSONUtils.convertFromJsonToList(response.get().body(),
				new TypeReference<List<StatusStudentDTO>>() {
				});
		return stastd;
	}

}
