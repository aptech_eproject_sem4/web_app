package c1808g1.aem_web.DAO.QuanLyHoSo;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.net.http.HttpClient.Version;
import java.net.http.HttpResponse.BodyHandlers;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;

import c1808g1.Models.QuanLiHoSo.ScoreStudentDTO;
import c1808g1.Models.QuanLiHoSo.StudentDTO;
import c1808g1.aem_web.common.JSONUtils;
import c1808g1.aem_web.common.danhMucDungChung;

public class SinhVienDao {
	static String mainUri = danhMucDungChung.IP_API + "quanlyhoso/sinhvienapi/";
	private static final HttpClient client = HttpClient.newBuilder().version(Version.HTTP_2).build();
	// APIClient client=new APIClient();

	public static List<ScoreStudentDTO> getAllScoreStudent()
			throws InterruptedException, ExecutionException, JsonParseException, JsonMappingException, IOException {
		String subUri = mainUri + "getAllScoreStudent";
		HttpRequest req = HttpRequest.newBuilder(URI.create(subUri)).GET().build();
		CompletableFuture<HttpResponse<String>> response = client.sendAsync(req, BodyHandlers.ofString());
		response.thenAccept(res -> System.out.println(res));
		List<ScoreStudentDTO> scostd = JSONUtils.convertFromJsonToList(response.get().body(),
				new TypeReference<List<ScoreStudentDTO>>() {
				});
		return scostd;
	}

	public static ScoreStudentDTO getScoreStudentById(String id)
			throws InterruptedException, ExecutionException, IOException {
		String subUri = mainUri + "getScoreStudentById/";
		HttpRequest req = HttpRequest.newBuilder(URI.create(subUri + String.valueOf(id))).GET().build();
		CompletableFuture<HttpResponse<String>> response = client.sendAsync(req, BodyHandlers.ofString());
		response.thenAccept(res -> System.out.println(res));
		if (response.get().statusCode() == 204) {
			System.out.println("Product Not Avaialble");
			return null;
		} else {
			ScoreStudentDTO scostd = JSONUtils.covertFromJsonToObject(response.get().body(), ScoreStudentDTO.class);
			return scostd;
		}
	}

	public static ScoreStudentDTO createScoreStudent(ScoreStudentDTO h)
			throws InterruptedException, ExecutionException, IOException {
		String subUri = mainUri + "createScoreStudent";
		String inputJson = JSONUtils.covertFromObjectToJson(h);
		HttpRequest request = HttpRequest.newBuilder(URI.create(subUri)).header("Content-Type", "application/json")
				.POST(HttpRequest.BodyPublishers.ofString(inputJson)).build();
		CompletableFuture<HttpResponse<String>> response = client.sendAsync(request,
				HttpResponse.BodyHandlers.ofString());
		System.out.println(response.get().body());
		ScoreStudentDTO scostd = JSONUtils.covertFromJsonToObject(response.get().body(), ScoreStudentDTO.class);
		return scostd;
	}

	public static ScoreStudentDTO updateScoreStudent(ScoreStudentDTO h)
			throws InterruptedException, ExecutionException, IOException {
		String subUri = mainUri + "updateScoreStudent";
		String inputJson = JSONUtils.covertFromObjectToJson(h);
		HttpRequest request = HttpRequest.newBuilder(URI.create(subUri)).header("Content-Type", "application/json")
				.PUT(HttpRequest.BodyPublishers.ofString(inputJson)).build();
		CompletableFuture<HttpResponse<String>> response = client.sendAsync(request,
				HttpResponse.BodyHandlers.ofString());
		if (response.get().statusCode() == 204) {
			System.out.println("Product Not Avaialble");
			return null;
		} else {
			ScoreStudentDTO scostd = JSONUtils.covertFromJsonToObject(response.get().body(), ScoreStudentDTO.class);
			return scostd;
		}
	}

	public static ScoreStudentDTO deleteScoreStudent(String id)
			throws InterruptedException, ExecutionException, IOException {
		String subUri = mainUri + "deleteScoreStudent/";
		HttpRequest request = HttpRequest.newBuilder(URI.create(subUri + String.valueOf(id))).DELETE().build();
		;
		CompletableFuture<HttpResponse<String>> response = client.sendAsync(request,
				HttpResponse.BodyHandlers.ofString());
		if (response.get().statusCode() == 500) {
			System.out.println("Product Not Avaialble");
			return null;
		} else {
			ScoreStudentDTO scostd = JSONUtils.covertFromJsonToObject(response.get().body(), ScoreStudentDTO.class);
			return scostd;
		}
	}

	////////////////////////////////////////////////////////////////

	public static List<StudentDTO> getAllStudent()
			throws InterruptedException, ExecutionException, JsonParseException, JsonMappingException, IOException {
		String subUri = mainUri + "getAllStudent";
		HttpRequest req = HttpRequest.newBuilder(URI.create(subUri)).GET().build();
		CompletableFuture<HttpResponse<String>> response = client.sendAsync(req, BodyHandlers.ofString());
		response.thenAccept(res -> System.out.println(res));
		List<StudentDTO> td = JSONUtils.convertFromJsonToList(response.get().body(),
				new TypeReference<List<StudentDTO>>() {
				});
		return td;
	}

	public static StudentDTO getStudentById(String id) throws InterruptedException, ExecutionException, IOException {
		String subUri = mainUri + "getStudentById/";
		HttpRequest req = HttpRequest.newBuilder(URI.create(subUri + String.valueOf(id))).GET().build();
		CompletableFuture<HttpResponse<String>> response = client.sendAsync(req, BodyHandlers.ofString());
		response.thenAccept(res -> System.out.println(res));
		if (response.get().statusCode() == 204 || response.get().statusCode() == 500
				|| response.get().statusCode() == 404) {
			System.out.println("Product Not Avaialble");
			return null;
		} else {
			StudentDTO std = JSONUtils.covertFromJsonToObject(response.get().body(), StudentDTO.class);
			return std;
		}
	}

	public static StudentDTO getStudentByEmail(String email)
			throws InterruptedException, ExecutionException, IOException {
		String subUri = mainUri + "getStudentByEmail/";
		HttpRequest req = HttpRequest.newBuilder(URI.create(subUri + String.valueOf(email))).GET().build();
		CompletableFuture<HttpResponse<String>> response = client.sendAsync(req, BodyHandlers.ofString());
		response.thenAccept(res -> System.out.println(res));
		if (response.get().statusCode() == 204 || response.get().statusCode() == 500
				|| response.get().statusCode() == 404) {
			System.out.println("Product Not Avaialble");
			return null;
		} else {
			StudentDTO std = JSONUtils.covertFromJsonToObject(response.get().body(), StudentDTO.class);
			return std;
		}
	}

	public static StudentDTO getStudentBySchoolEmail(String school_email)
			throws InterruptedException, ExecutionException, IOException {
		String subUri = mainUri + "getStudentBySchoolEmail/";
		HttpRequest req = HttpRequest.newBuilder(URI.create(subUri + String.valueOf(school_email))).GET().build();
		CompletableFuture<HttpResponse<String>> response = client.sendAsync(req, BodyHandlers.ofString());
		response.thenAccept(res -> System.out.println(res));
		if (response.get().statusCode() == 204|| response.get().statusCode() == 500
				|| response.get().statusCode() == 404) {
			System.out.println("Product Not Avaialble");
			return null;
		} else {
			StudentDTO std = JSONUtils.covertFromJsonToObject(response.get().body(), StudentDTO.class);
			return std;
		}
	}

	public static StudentDTO getStudentByMobileMac(String mobile)
			throws InterruptedException, ExecutionException, IOException {
		String subUri = mainUri + "getStudentByMobileMac/";
		HttpRequest req = HttpRequest.newBuilder(URI.create(subUri + mobile)).GET().build();
		CompletableFuture<HttpResponse<String>> response = client.sendAsync(req, BodyHandlers.ofString());
		response.thenAccept(res -> System.out.println(res));
		if (response.get().statusCode() == 204 || response.get().statusCode() == 500
				|| response.get().statusCode() == 404) {
			System.out.println("Product Not Avaialble");
			return null;
		} else {
			StudentDTO std = JSONUtils.covertFromJsonToObject(response.get().body(), StudentDTO.class);
			return std;
		}
	}

	public static StudentDTO createStudent(StudentDTO h) throws InterruptedException, ExecutionException, IOException {

		String subUri = mainUri + "createStudent";
		String inputJson = JSONUtils.covertFromObjectToJson(h);
		HttpRequest request = HttpRequest.newBuilder(URI.create(subUri)).header("Content-Type", "application/json")
				.POST(HttpRequest.BodyPublishers.ofString(inputJson)).build();
		CompletableFuture<HttpResponse<String>> response = client.sendAsync(request,
				HttpResponse.BodyHandlers.ofString());
		System.out.println(response.get().body());
		StudentDTO std = JSONUtils.covertFromJsonToObject(response.get().body(), StudentDTO.class);
		return std;
	}

	public static StudentDTO updateStudent(StudentDTO h) throws InterruptedException, ExecutionException, IOException {
		String subUri = mainUri + "updateStudent";
		String inputJson = JSONUtils.covertFromObjectToJson(h);
		HttpRequest request = HttpRequest.newBuilder(URI.create(subUri)).header("Content-Type", "application/json")
				.PUT(HttpRequest.BodyPublishers.ofString(inputJson)).build();
		CompletableFuture<HttpResponse<String>> response = client.sendAsync(request,
				HttpResponse.BodyHandlers.ofString());
		if (response.get().statusCode() == 204) {
			System.out.println("Product Not Avaialble");
			return null;
		} else {
			StudentDTO std = JSONUtils.covertFromJsonToObject(response.get().body(), StudentDTO.class);
			return std;
		}
	}

	public static StudentDTO deleteStudent(String id) throws InterruptedException, ExecutionException, IOException {
		String subUri = mainUri + "deleteStudent/";
		HttpRequest request = HttpRequest.newBuilder(URI.create(subUri + String.valueOf(id))).DELETE().build();
		;
		CompletableFuture<HttpResponse<String>> response = client.sendAsync(request,
				HttpResponse.BodyHandlers.ofString());
		if (response.get().statusCode() == 500) {
			System.out.println("Product Not Avaialble");
			return null;
		} else {
			StudentDTO std = JSONUtils.covertFromJsonToObject(response.get().body(), StudentDTO.class);
			return std;
		}
	}

	// Get all list student filter

	public static List<StudentDTO> getAllStudentFilter()
			throws InterruptedException, ExecutionException, JsonParseException, JsonMappingException, IOException {
		String subUri = mainUri + "getListStudentFilter";
		HttpRequest req = HttpRequest.newBuilder(URI.create(subUri)).GET().build();
		CompletableFuture<HttpResponse<String>> response = client.sendAsync(req, BodyHandlers.ofString());
		response.thenAccept(res -> System.out.println(res));
		List<StudentDTO> td = JSONUtils.convertFromJsonToList(response.get().body(),
				new TypeReference<List<StudentDTO>>() {
				});
		return td;
	}

	public static List<StudentDTO> getAllStudentFilter(String course_family, String course_id, String class_id,
			String searchValue, String from_date, String to_date, Integer page, String status)
			throws InterruptedException, ExecutionException, JsonParseException, JsonMappingException, IOException {
		String subUri = mainUri + "getListStudentFilterParams?course_family=" + course_family + "&course_id="
				+ course_id + "&current_class=" + class_id + "&name_search=" + searchValue + "&from_date=" + from_date
				+ "&to_date=" + to_date + "&page=" + String.valueOf(page) + "&status=" + status;
		HttpRequest req = HttpRequest.newBuilder(URI.create(subUri)).GET().build();
		CompletableFuture<HttpResponse<String>> response = client.sendAsync(req, BodyHandlers.ofString());
		response.thenAccept(res -> System.out.println(res));
		List<StudentDTO> td = JSONUtils.convertFromJsonToList(response.get().body(),
				new TypeReference<List<StudentDTO>>() {
				});
		return td;
	}
}
