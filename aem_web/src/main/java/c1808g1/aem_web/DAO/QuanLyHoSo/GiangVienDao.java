package c1808g1.aem_web.DAO.QuanLyHoSo;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.net.http.HttpClient.Version;
import java.net.http.HttpResponse.BodyHandlers;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;

import c1808g1.Models.QuanLiHoSo.EmployeeDTO;
import c1808g1.Models.QuanLiHoSo.FCDTO;
import c1808g1.Models.QuanLiHoSo.ScoreFCDTO;
import c1808g1.Models.QuanLyChuongTrinhHoc.SubjectDTO;
import c1808g1.aem_web.common.JSONUtils;
import c1808g1.aem_web.common.danhMucDungChung;

public class GiangVienDao {
	static String mainUri = danhMucDungChung.IP_API + "quanlyhoso/giangvienapi/";
	private static final HttpClient client = HttpClient.newBuilder().version(Version.HTTP_2).build();
	// APIClient client=new APIClient();

	public static List<ScoreFCDTO> getAllScoreFC()
			throws InterruptedException, ExecutionException, JsonParseException, JsonMappingException, IOException {
		String subUri = mainUri + "getAllScoreFC";
		HttpRequest req = HttpRequest.newBuilder(URI.create(subUri)).GET().build();
		CompletableFuture<HttpResponse<String>> response = client.sendAsync(req, BodyHandlers.ofString());
		response.thenAccept(res -> System.out.println(res));
		List<ScoreFCDTO> scofc = JSONUtils.convertFromJsonToList(response.get().body(),
				new TypeReference<List<ScoreFCDTO>>() {
				});
		return scofc;
	}

	public static List<ScoreFCDTO> getScoreFCOrderByDatecreateDESC()
			throws InterruptedException, ExecutionException, JsonParseException, JsonMappingException, IOException {
		String subUri = mainUri + "getScoreFCOrderByDatecreateDESC";
		HttpRequest req = HttpRequest.newBuilder(URI.create(subUri)).GET().build();
		CompletableFuture<HttpResponse<String>> response = client.sendAsync(req, BodyHandlers.ofString());
		response.thenAccept(res -> System.out.println(res));
		List<ScoreFCDTO> scofc = JSONUtils.convertFromJsonToList(response.get().body(),
				new TypeReference<List<ScoreFCDTO>>() {
				});
		return scofc;
	}

	public static List<ScoreFCDTO> getScoreFCByMultipleParameter(String subject_id, String fc_id)
			throws InterruptedException, ExecutionException, JsonParseException, JsonMappingException, IOException {
		String subUri = mainUri + "getScoreFCByMultipleParameter/";
		String fullUri = null;
		String fullUrifinal = null;
		if (String.valueOf(subject_id) == "null" || String.valueOf(subject_id) == null
				|| String.valueOf(subject_id) == "" || String.valueOf(subject_id) == "0") {
			subject_id = "0";
		}
		if (String.valueOf(fc_id) == "null" || String.valueOf(fc_id) == null || String.valueOf(fc_id) == ""
				|| String.valueOf(fc_id) == "0") {
			fc_id = "0";
		}
		fullUri = subUri + String.valueOf(subject_id) + "/" + String.valueOf(fc_id);
		String lastChar = fullUri.substring(fullUri.length() - 1);
		if (lastChar.equals("/")) {
			fullUrifinal = fullUri + "0";
		} else {
			fullUrifinal = fullUri;
		}
		HttpRequest req = HttpRequest.newBuilder(URI.create(fullUrifinal)).GET().build();
		CompletableFuture<HttpResponse<String>> response = client.sendAsync(req, BodyHandlers.ofString());
		response.thenAccept(res -> System.out.println(res));
		if (response.get().statusCode() == 204 || response.get().statusCode() == 500
				|| response.get().statusCode() == 404) {
			System.out.println("Product Not Avaialble");
			return null;
		} else {
			List<ScoreFCDTO> scofc = JSONUtils.convertFromJsonToList(response.get().body(),
					new TypeReference<List<ScoreFCDTO>>() {
					});
			return scofc;
		}
	}

	public static List<ScoreFCDTO> getScoreFCBySubjectIdAndSearchValue(String subject_id, String searchValue)
			throws InterruptedException, ExecutionException, JsonParseException, JsonMappingException, IOException {
		String subUri = mainUri + "getScoreFCBySubjectIdAndSearchValue/";
		String fullUri = null;
		String fullUrifinal = null;
		fullUri = subUri + String.valueOf(subject_id) + "/" + String.valueOf(searchValue);
		fullUrifinal = fullUri;
		HttpRequest req = HttpRequest.newBuilder(URI.create(fullUrifinal)).GET().build();
		CompletableFuture<HttpResponse<String>> response = client.sendAsync(req, BodyHandlers.ofString());
		response.thenAccept(res -> System.out.println(res));
		if (response.get().statusCode() == 204 || response.get().statusCode() == 500
				|| response.get().statusCode() == 404) {
			System.out.println("Product Not Avaialble");
			return null;
		} else {
			List<ScoreFCDTO> scofc = JSONUtils.convertFromJsonToList(response.get().body(),
					new TypeReference<List<ScoreFCDTO>>() {
					});
			return scofc;
		}
	}

	public static List<ScoreFCDTO> getScoreFCByFCIdAndSearchValue(String fc_id, String searchValue)
			throws InterruptedException, ExecutionException, JsonParseException, JsonMappingException, IOException {
		String subUri = mainUri + "getScoreFCByFCIdAndSearchValue/";
		String fullUri = null;
		String fullUrifinal = null;
		fullUri = subUri + String.valueOf(fc_id) + "/" + String.valueOf(searchValue);
		fullUrifinal = fullUri;
		HttpRequest req = HttpRequest.newBuilder(URI.create(fullUrifinal)).GET().build();
		CompletableFuture<HttpResponse<String>> response = client.sendAsync(req, BodyHandlers.ofString());
		response.thenAccept(res -> System.out.println(res));
		if (response.get().statusCode() == 204 || response.get().statusCode() == 500
				|| response.get().statusCode() == 404) {
			System.out.println("Product Not Avaialble");
			return null;
		} else {
			List<ScoreFCDTO> scofc = JSONUtils.convertFromJsonToList(response.get().body(),
					new TypeReference<List<ScoreFCDTO>>() {
					});
			return scofc;
		}
	}


	public static ScoreFCDTO getScoreFCById(String id) throws InterruptedException, ExecutionException, IOException {
		String subUri = mainUri + "getScoreFCById/";
		HttpRequest req = HttpRequest.newBuilder(URI.create(subUri + String.valueOf(id))).GET().build();
		CompletableFuture<HttpResponse<String>> response = client.sendAsync(req, BodyHandlers.ofString());
		response.thenAccept(res -> System.out.println(res));
		if (response.get().statusCode() == 204 || response.get().statusCode() == 500
				|| response.get().statusCode() == 404) {
			System.out.println("Product Not Avaialble");
			return null;
		} else {
			ScoreFCDTO scofc = JSONUtils.covertFromJsonToObject(response.get().body(), ScoreFCDTO.class);
			return scofc;
		}
	}

	public static ScoreFCDTO createScoreFC(ScoreFCDTO h) throws InterruptedException, ExecutionException, IOException {
		String subUri = mainUri + "createScoreFC";
		String inputJson = JSONUtils.covertFromObjectToJson(h);
		HttpRequest request = HttpRequest.newBuilder(URI.create(subUri)).header("Content-Type", "application/json")
				.POST(HttpRequest.BodyPublishers.ofString(inputJson)).build();
		CompletableFuture<HttpResponse<String>> response = client.sendAsync(request,
				HttpResponse.BodyHandlers.ofString());
		System.out.println(response.get().body());
		ScoreFCDTO scofc = JSONUtils.covertFromJsonToObject(response.get().body(), ScoreFCDTO.class);
		return scofc;
	}

	public static ScoreFCDTO updateScoreFC(ScoreFCDTO h) throws InterruptedException, ExecutionException, IOException {
		String subUri = mainUri + "updateScoreFC";
		String inputJson = JSONUtils.covertFromObjectToJson(h);
		HttpRequest request = HttpRequest.newBuilder(URI.create(subUri)).header("Content-Type", "application/json")
				.PUT(HttpRequest.BodyPublishers.ofString(inputJson)).build();
		CompletableFuture<HttpResponse<String>> response = client.sendAsync(request,
				HttpResponse.BodyHandlers.ofString());
		if (response.get().statusCode() == 204) {
			System.out.println("Product Not Avaialble");
			return null;
		} else {
			ScoreFCDTO scofc = JSONUtils.covertFromJsonToObject(response.get().body(), ScoreFCDTO.class);
			return scofc;
		}
	}

	public static ScoreFCDTO deleteScoreFC(String id) throws InterruptedException, ExecutionException, IOException {
		String subUri = mainUri + "deleteScoreFC/";
		HttpRequest request = HttpRequest.newBuilder(URI.create(subUri + String.valueOf(id))).DELETE().build();
		;
		CompletableFuture<HttpResponse<String>> response = client.sendAsync(request,
				HttpResponse.BodyHandlers.ofString());
		if (response.get().statusCode() == 500) {
			System.out.println("Product Not Avaialble");
			return null;
		} else {
			ScoreFCDTO scofc = JSONUtils.covertFromJsonToObject(response.get().body(), ScoreFCDTO.class);
			return scofc;
		}
	}

/////////////////////////////////////////////////////////////

	public static List<FCDTO> getAllFC()
			throws InterruptedException, ExecutionException, JsonParseException, JsonMappingException, IOException {
		String subUri = mainUri + "getAllFC";
		HttpRequest req = HttpRequest.newBuilder(URI.create(subUri)).GET().build();
		CompletableFuture<HttpResponse<String>> response = client.sendAsync(req, BodyHandlers.ofString());
		response.thenAccept(res -> System.out.println(res));
		List<FCDTO> fc = JSONUtils.convertFromJsonToList(response.get().body(), new TypeReference<List<FCDTO>>() {
		});
		return fc;
	}

	public static FCDTO getFCById(String id) throws InterruptedException, ExecutionException, IOException {
		String subUri = mainUri + "getFCById/";
		HttpRequest req = HttpRequest.newBuilder(URI.create(subUri + String.valueOf(id))).GET().build();
		CompletableFuture<HttpResponse<String>> response = client.sendAsync(req, BodyHandlers.ofString());
		response.thenAccept(res -> System.out.println(res));
		if (response.get().statusCode() == 204 || response.get().statusCode() == 500
				|| response.get().statusCode() == 404) {
			System.out.println("Product Not Avaialble");
			return null;
		} else {
			FCDTO fc = JSONUtils.covertFromJsonToObject(response.get().body(), FCDTO.class);
			return fc;
		}
	}


	public static List<ScoreFCDTO> getScoreFCBySearchValue(String searchValue)
			throws InterruptedException, ExecutionException, JsonParseException, JsonMappingException, IOException {
		String subUri = mainUri + "getScoreFCBySearchValue/";
		String fullUri = null;
		String fullUrifinal = null;
		fullUri = subUri + String.valueOf(searchValue);
		fullUrifinal = fullUri;
		HttpRequest req = HttpRequest.newBuilder(URI.create(fullUrifinal)).GET().build();
		CompletableFuture<HttpResponse<String>> response = client.sendAsync(req, BodyHandlers.ofString());
		response.thenAccept(res -> System.out.println(res));
		if (response.get().statusCode() == 204 || response.get().statusCode() == 500
				|| response.get().statusCode() == 404) {
			System.out.println("Product Not Avaialble");
			return null;
		} else {
			List<ScoreFCDTO> scofc = JSONUtils.convertFromJsonToList(response.get().body(),
					new TypeReference<List<ScoreFCDTO>>() {
					});
			return scofc;
		}
	}

	
	public static FCDTO getFCByEmail(String email) throws InterruptedException, ExecutionException, IOException {
		String subUri = mainUri + "getFCByEmail/";
		HttpRequest req = HttpRequest.newBuilder(URI.create(subUri + String.valueOf(email))).GET().build();
		CompletableFuture<HttpResponse<String>> response = client.sendAsync(req, BodyHandlers.ofString());
		response.thenAccept(res -> System.out.println(res));
		if (response.get().statusCode() == 204 || response.get().statusCode() == 500
				|| response.get().statusCode() == 404) {
			System.out.println("Product Not Avaialble");
			return null;
		} else {
			FCDTO fc = JSONUtils.covertFromJsonToObject(response.get().body(), FCDTO.class);
			return fc;
		}
	}

	public static List<FCDTO> checkFCByValue(String searchValue)
			throws InterruptedException, ExecutionException, IOException {
		String subUri = mainUri + "checkFCByValue/";
		HttpRequest req = HttpRequest.newBuilder(URI.create(subUri + String.valueOf(searchValue))).GET().build();
		CompletableFuture<HttpResponse<String>> response = client.sendAsync(req, BodyHandlers.ofString());
		response.thenAccept(res -> System.out.println(res));
		if (response.get().statusCode() == 204 || response.get().statusCode() == 500
				|| response.get().statusCode() == 404) {
			System.out.println("Product Not Avaialble");
			return null;
		} else {
			List<FCDTO> scofc = JSONUtils.convertFromJsonToList(response.get().body(),
					new TypeReference<List<FCDTO>>() {
					});
			return scofc;
		}
	}
	
	
	public static List<FCDTO> getFCByMultipleParameter(String status, String searchValue)
			throws InterruptedException, ExecutionException, JsonParseException, JsonMappingException, IOException {
		String subUri = mainUri + "getFCByMultipleParameter/";
		String fullUri = null;
		String fullUrifinal = null;
		if (String.valueOf(status) == "null" || String.valueOf(status) == null || String.valueOf(status) == ""
				|| String.valueOf(status) == "0") {
			status = "0";
		}
		
		if (searchValue == null || searchValue == "" || searchValue == "null") {
			searchValue = "0";
		}

		fullUri = subUri + String.valueOf(status) + "/" + searchValue;
		String lastChar = fullUri.substring(fullUri.length() - 1);
		if (lastChar.equals("/")) {
			fullUrifinal = fullUri + "0";
		} else {
			fullUrifinal = subUri + String.valueOf(status) + "/" + searchValue;
		}
		HttpRequest req = HttpRequest.newBuilder(URI.create(fullUrifinal)).GET().build();
		CompletableFuture<HttpResponse<String>> response = client.sendAsync(req, BodyHandlers.ofString());
		response.thenAccept(res -> System.out.println(res));
		if (response.get().statusCode() == 204 || response.get().statusCode() == 500
				|| response.get().statusCode() == 500) {
			System.out.println("Product Not Avaialble");
			return null;
		} else {
			List<FCDTO> sub = JSONUtils.convertFromJsonToList(response.get().body(),
					new TypeReference<List<FCDTO>>() {
					});
			return sub;
		}
	}

	

	public static FCDTO createFC(FCDTO h) throws InterruptedException, ExecutionException, IOException {
		String subUri = mainUri + "createFC";

		String inputJson = JSONUtils.covertFromObjectToJson(h);

		HttpRequest request = HttpRequest.newBuilder(URI.create(subUri)).header("Content-Type", "application/json")
				.POST(HttpRequest.BodyPublishers.ofString(inputJson)).build();

		CompletableFuture<HttpResponse<String>> response = client.sendAsync(request,
				HttpResponse.BodyHandlers.ofString());
		System.out.println(response.get().body());
		FCDTO fc = JSONUtils.covertFromJsonToObject(response.get().body(), FCDTO.class);
		return fc;
	}

	public static FCDTO updateFC(FCDTO h) throws InterruptedException, ExecutionException, IOException {
		String subUri = mainUri + "updateFC";
		String inputJson = JSONUtils.covertFromObjectToJson(h);
		HttpRequest request = HttpRequest.newBuilder(URI.create(subUri)).header("Content-Type", "application/json")
				.PUT(HttpRequest.BodyPublishers.ofString(inputJson)).build();
		CompletableFuture<HttpResponse<String>> response = client.sendAsync(request,
				HttpResponse.BodyHandlers.ofString());
		if (response.get().statusCode() == 204) {
			System.out.println("Product Not Avaialble");
			return null;
		} else {
			FCDTO fc = JSONUtils.covertFromJsonToObject(response.get().body(), FCDTO.class);
			return fc;
		}
	}

	public static FCDTO deleteFC(String id) throws InterruptedException, ExecutionException, IOException {
		String subUri = mainUri + "deleteFC/";
		HttpRequest request = HttpRequest.newBuilder(URI.create(subUri + String.valueOf(id))).DELETE().build();
		;
		CompletableFuture<HttpResponse<String>> response = client.sendAsync(request,
				HttpResponse.BodyHandlers.ofString());
		if (response.get().statusCode() == 500) {
			System.out.println("Product Not Avaialble");
			return null;
		} else {
			FCDTO fc = JSONUtils.covertFromJsonToObject(response.get().body(), FCDTO.class);
			return fc;
		}
	}

}
