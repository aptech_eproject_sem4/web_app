package c1808g1.aem_web.DAO.QuanLyHoSo;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.net.http.HttpClient.Version;
import java.net.http.HttpResponse.BodyHandlers;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;

import c1808g1.Models.QuanLiHoSo.EmployeeDTO;
import c1808g1.aem_web.common.JSONUtils;
import c1808g1.aem_web.common.danhMucDungChung;

public class NhanVienDao {
	static String mainUri = danhMucDungChung.IP_API + "quanlyhoso/nhanvienapi/";
	private static final HttpClient client = HttpClient.newBuilder().version(Version.HTTP_2).build();
	// APIClient client=new APIClient();

	public static List<EmployeeDTO> getAllEmployee()
			throws InterruptedException, ExecutionException, JsonParseException, JsonMappingException, IOException {
		String subUri = mainUri + "getAll";
		HttpRequest req = HttpRequest.newBuilder(URI.create(subUri)).GET().build();
		CompletableFuture<HttpResponse<String>> response = client.sendAsync(req, BodyHandlers.ofString());
		response.thenAccept(res -> System.out.println(res));
		List<EmployeeDTO> emp = JSONUtils.convertFromJsonToList(response.get().body(),
				new TypeReference<List<EmployeeDTO>>() {
				});
		return emp;
	}

	public static EmployeeDTO getEmployeeById(String id) throws InterruptedException, ExecutionException, IOException {
		String subUri = mainUri + "getEmployeeById/";
		HttpRequest req = HttpRequest.newBuilder(URI.create(subUri + String.valueOf(id))).GET().build();
		CompletableFuture<HttpResponse<String>> response = client.sendAsync(req, BodyHandlers.ofString());
		response.thenAccept(res -> System.out.println(res));
		if (response.get().statusCode() == 204 || response.get().statusCode() == 500
				|| response.get().statusCode() == 404) {
			System.out.println("Product Not Avaialble");
			return null;
		} else {
			EmployeeDTO emp = JSONUtils.covertFromJsonToObject(response.get().body(), EmployeeDTO.class);
			return emp;
		}
	}
	
	public static EmployeeDTO getEmployeeByEmail(String email) throws InterruptedException, ExecutionException, IOException {
		String subUri = mainUri + "getEmployeeByEmail/";
		HttpRequest req = HttpRequest.newBuilder(URI.create(subUri + String.valueOf(email))).GET().build();
		CompletableFuture<HttpResponse<String>> response = client.sendAsync(req, BodyHandlers.ofString());
		response.thenAccept(res -> System.out.println(res));
		if (response.get().statusCode() == 204 || response.get().statusCode() == 500
				|| response.get().statusCode() == 404) {
			System.out.println("Product Not Avaialble");
			return null;
		} else {
			EmployeeDTO emp = JSONUtils.covertFromJsonToObject(response.get().body(), EmployeeDTO.class);
			return emp;
		}
	}

	public static List<EmployeeDTO> getEmployeeByMultipleParameter(Boolean status, String searchValue)
			throws InterruptedException, ExecutionException, JsonParseException, JsonMappingException, IOException {
		String subUri = mainUri + "getEmployeeByMultipleParameter/";
		String fullUri = null;
		String fullUrifinal = null;
		if (String.valueOf(status) == "null" || String.valueOf(status) == null || String.valueOf(status) == ""
				|| String.valueOf(status) == "0") {
			status = null;
		}
		if (searchValue == null || searchValue == "" || searchValue == "null") {
			searchValue = "";
		}

		fullUri = subUri + String.valueOf(status) + "/" + searchValue;
		String lastChar = fullUri.substring(fullUri.length() - 1);
		if (lastChar.equals("/")) {
			fullUrifinal = fullUri + "0";
		} else {
			fullUrifinal = subUri + String.valueOf(status) + "/" + searchValue;
		}
		HttpRequest req = HttpRequest.newBuilder(URI.create(fullUrifinal)).GET().build();
		CompletableFuture<HttpResponse<String>> response = client.sendAsync(req, BodyHandlers.ofString());
		response.thenAccept(res -> System.out.println(res));
		if (response.get().statusCode() == 204 || response.get().statusCode() == 500
				|| response.get().statusCode() == 500) {
			System.out.println("Product Not Avaialble");
			return null;
		} else {
			List<EmployeeDTO> sub = JSONUtils.convertFromJsonToList(response.get().body(),
					new TypeReference<List<EmployeeDTO>>() {
					});
			return sub;
		}
	}

	public static EmployeeDTO create(EmployeeDTO h) throws InterruptedException, ExecutionException, IOException {
		String subUri = mainUri + "create";
		String inputJson = JSONUtils.covertFromObjectToJson(h);
		HttpRequest request = HttpRequest.newBuilder(URI.create(subUri)).header("Content-Type", "application/json")
				.POST(HttpRequest.BodyPublishers.ofString(inputJson)).build();
		CompletableFuture<HttpResponse<String>> response = client.sendAsync(request,
				HttpResponse.BodyHandlers.ofString());
		System.out.println(response.get().body());
		EmployeeDTO emp = JSONUtils.covertFromJsonToObject(response.get().body(), EmployeeDTO.class);
		return emp;
	}

	public static EmployeeDTO update(EmployeeDTO h) throws InterruptedException, ExecutionException, IOException {
		String subUri = mainUri + "update";
		String inputJson = JSONUtils.covertFromObjectToJson(h);
		HttpRequest request = HttpRequest.newBuilder(URI.create(subUri)).header("Content-Type", "application/json")
				.PUT(HttpRequest.BodyPublishers.ofString(inputJson)).build();
		CompletableFuture<HttpResponse<String>> response = client.sendAsync(request,
				HttpResponse.BodyHandlers.ofString());
		if (response.get().statusCode() == 204) {
			System.out.println("Product Not Avaialble");
			return null;
		} else {
			EmployeeDTO emp = JSONUtils.covertFromJsonToObject(response.get().body(), EmployeeDTO.class);
			return emp;
		}
	}

	public static EmployeeDTO delete(String id) throws InterruptedException, ExecutionException, IOException {
		String subUri = mainUri + "delete/";
		HttpRequest request = HttpRequest.newBuilder(URI.create(subUri + String.valueOf(id))).DELETE().build();
		;
		CompletableFuture<HttpResponse<String>> response = client.sendAsync(request,
				HttpResponse.BodyHandlers.ofString());
		if (response.get().statusCode() == 500) {
			System.out.println("Product Not Avaialble");
			return null;
		} else {
			EmployeeDTO emp = JSONUtils.covertFromJsonToObject(response.get().body(), EmployeeDTO.class);
			return emp;
		}
	}

}
