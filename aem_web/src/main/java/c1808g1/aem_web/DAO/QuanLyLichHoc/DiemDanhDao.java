package c1808g1.aem_web.DAO.QuanLyLichHoc;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.net.http.HttpClient.Version;
import java.net.http.HttpResponse.BodyHandlers;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;

import c1808g1.Models.QuanLyLichHoc.AttendanceDTO;
import c1808g1.Models.QuanLyLichHoc.AttendanceFCDTO;
import c1808g1.Models.QuanLyLichHoc.AttendanceStudentDTO;
import c1808g1.aem_web.common.JSONUtils;
import c1808g1.aem_web.common.danhMucDungChung;

public class DiemDanhDao {
	static String mainUri = danhMucDungChung.IP_API + "quanlylichhoc/diemdanhapi/";
	private static final HttpClient client = HttpClient.newBuilder().version(Version.HTTP_2).build();
	// APIClient client=new APIClient();

	public static List<AttendanceDTO> getAllAttendance()
			throws InterruptedException, ExecutionException, JsonParseException, JsonMappingException, IOException {
		String subUri = mainUri + "getAllAttendance";
		HttpRequest req = HttpRequest.newBuilder(URI.create(subUri)).GET().build();
		CompletableFuture<HttpResponse<String>> response = client.sendAsync(req, BodyHandlers.ofString());
		response.thenAccept(res -> System.out.println(res));
		List<AttendanceDTO> atd = JSONUtils.convertFromJsonToList(response.get().body(),
				new TypeReference<List<AttendanceDTO>>() {
				});
		return atd;
	}

	public static AttendanceDTO getAttendanceById(String id) throws InterruptedException, ExecutionException, IOException {
		String subUri = mainUri + "getAttendanceById/";
		HttpRequest req = HttpRequest.newBuilder(URI.create(subUri + String.valueOf(id))).GET().build();
		CompletableFuture<HttpResponse<String>> response = client.sendAsync(req, BodyHandlers.ofString());
		response.thenAccept(res -> System.out.println(res));
		if (response.get().statusCode() == 204) {
			System.out.println("Product Not Avaialble");
			return null;
		} else {
			AttendanceDTO atd = JSONUtils.covertFromJsonToObject(response.get().body(), AttendanceDTO.class);
			return atd;
		}
	}

	public static AttendanceDTO createAttendance(AttendanceDTO h) throws InterruptedException, ExecutionException, IOException {
		String subUri = mainUri + "createAttendance";
		String inputJson = JSONUtils.covertFromObjectToJson(h);
		HttpRequest request = HttpRequest.newBuilder(URI.create(subUri)).header("Content-Type", "application/json")
				.POST(HttpRequest.BodyPublishers.ofString(inputJson)).build();
		CompletableFuture<HttpResponse<String>> response = client.sendAsync(request,
				HttpResponse.BodyHandlers.ofString());
		System.out.println(response.get().body());
		AttendanceDTO atd=JSONUtils.covertFromJsonToObject(response.get().body(), AttendanceDTO.class);
		return atd;
	}
	
	public static AttendanceDTO updateAttendance(AttendanceDTO h) throws InterruptedException, ExecutionException, IOException {
		String subUri = mainUri + "updateAttendance";
		String inputJson = JSONUtils.covertFromObjectToJson(h);
		HttpRequest request = HttpRequest.newBuilder(URI.create(subUri)).header("Content-Type", "application/json")
				.PUT(HttpRequest.BodyPublishers.ofString(inputJson)).build();
		CompletableFuture<HttpResponse<String>> response = client.sendAsync(request,
				HttpResponse.BodyHandlers.ofString());
		if (response.get().statusCode() == 204) {
			System.out.println("Product Not Avaialble");
			return null;
		} else {
			AttendanceDTO atd=JSONUtils.covertFromJsonToObject(response.get().body(), AttendanceDTO.class);
			return atd;
		}		
	}
	
	public static AttendanceDTO deleteAttendance(String id) throws InterruptedException, ExecutionException, IOException {
		String subUri = mainUri + "deleteAttendance/";
		HttpRequest request = HttpRequest.newBuilder(URI.create(subUri+String.valueOf(id))).DELETE().build();;
		CompletableFuture<HttpResponse<String>> response = client.sendAsync(request,
				HttpResponse.BodyHandlers.ofString());
		if (response.get().statusCode() == 500) {
			System.out.println("Product Not Avaialble");
			return null;
		} else {
			AttendanceDTO atd=JSONUtils.covertFromJsonToObject(response.get().body(), AttendanceDTO.class);
			return atd;
		}		
	}


/////////////////////////////////////////////////////////////////////
	
	
	
	public static List<AttendanceFCDTO> getAllAttendanceFC()
			throws InterruptedException, ExecutionException, JsonParseException, JsonMappingException, IOException {
		String subUri = mainUri + "getAllAttendanceFC";
		HttpRequest req = HttpRequest.newBuilder(URI.create(subUri)).GET().build();
		CompletableFuture<HttpResponse<String>> response = client.sendAsync(req, BodyHandlers.ofString());
		response.thenAccept(res -> System.out.println(res));
		List<AttendanceFCDTO> atdfc = JSONUtils.convertFromJsonToList(response.get().body(),
				new TypeReference<List<AttendanceFCDTO>>() {
				});
		return atdfc;
	}

	public static AttendanceFCDTO getAttendanceFCById(String id) throws InterruptedException, ExecutionException, IOException {
		String subUri = mainUri + "getAttendanceFCById/";
		HttpRequest req = HttpRequest.newBuilder(URI.create(subUri + String.valueOf(id))).GET().build();
		CompletableFuture<HttpResponse<String>> response = client.sendAsync(req, BodyHandlers.ofString());
		response.thenAccept(res -> System.out.println(res));
		if (response.get().statusCode() == 204) {
			System.out.println("Product Not Avaialble");
			return null;
		} else {
			AttendanceFCDTO atdfc = JSONUtils.covertFromJsonToObject(response.get().body(), AttendanceFCDTO.class);
			return atdfc;
		}
	}

	public static AttendanceFCDTO createAttendanceFC(AttendanceFCDTO h) throws InterruptedException, ExecutionException, IOException {
		String subUri = mainUri + "createAttendanceFC";
		String inputJson = JSONUtils.covertFromObjectToJson(h);
		HttpRequest request = HttpRequest.newBuilder(URI.create(subUri)).header("Content-Type", "application/json")
				.POST(HttpRequest.BodyPublishers.ofString(inputJson)).build();
		CompletableFuture<HttpResponse<String>> response = client.sendAsync(request,
				HttpResponse.BodyHandlers.ofString());
		System.out.println(response.get().body());
		AttendanceFCDTO atdfc=JSONUtils.covertFromJsonToObject(response.get().body(), AttendanceFCDTO.class);
		return atdfc;
	}
	
	public static AttendanceFCDTO updateAttendanceFC(AttendanceFCDTO h) throws InterruptedException, ExecutionException, IOException {
		String subUri = mainUri + "updateAttendanceFC";
		String inputJson = JSONUtils.covertFromObjectToJson(h);
		HttpRequest request = HttpRequest.newBuilder(URI.create(subUri)).header("Content-Type", "application/json")
				.PUT(HttpRequest.BodyPublishers.ofString(inputJson)).build();
		CompletableFuture<HttpResponse<String>> response = client.sendAsync(request,
				HttpResponse.BodyHandlers.ofString());
		if (response.get().statusCode() == 204) {
			System.out.println("Product Not Avaialble");
			return null;
		} else {
			AttendanceFCDTO atdfc=JSONUtils.covertFromJsonToObject(response.get().body(), AttendanceFCDTO.class);
			return atdfc;
		}		
	}
	
	public static AttendanceFCDTO deleteAttendanceFC(String id) throws InterruptedException, ExecutionException, IOException {
		String subUri = mainUri + "deleteAttendanceFC/";
		HttpRequest request = HttpRequest.newBuilder(URI.create(subUri+String.valueOf(id))).DELETE().build();;
		CompletableFuture<HttpResponse<String>> response = client.sendAsync(request,
				HttpResponse.BodyHandlers.ofString());
		if (response.get().statusCode() == 500) {
			System.out.println("Product Not Avaialble");
			return null;
		} else {
			AttendanceFCDTO atdfc=JSONUtils.covertFromJsonToObject(response.get().body(), AttendanceFCDTO.class);
			return atdfc;
		}		
	}


////////////////////////////////////////////////////////////
	
	public static List<AttendanceStudentDTO> getAllAttendanceStudentFC()
			throws InterruptedException, ExecutionException, JsonParseException, JsonMappingException, IOException {
		String subUri = mainUri + "getAllAttendanceStudent";
		HttpRequest req = HttpRequest.newBuilder(URI.create(subUri)).GET().build();
		CompletableFuture<HttpResponse<String>> response = client.sendAsync(req, BodyHandlers.ofString());
		response.thenAccept(res -> System.out.println(res));
		List<AttendanceStudentDTO> atdstd = JSONUtils.convertFromJsonToList(response.get().body(),
				new TypeReference<List<AttendanceStudentDTO>>() {
				});
		return atdstd;
	}

	public static AttendanceStudentDTO getAttendanceStudentById(String id) throws InterruptedException, ExecutionException, IOException {
		String subUri = mainUri + "getAttendanceStudentById/";
		HttpRequest req = HttpRequest.newBuilder(URI.create(subUri + String.valueOf(id))).GET().build();
		CompletableFuture<HttpResponse<String>> response = client.sendAsync(req, BodyHandlers.ofString());
		response.thenAccept(res -> System.out.println(res));
		if (response.get().statusCode() == 204) {
			System.out.println("Product Not Avaialble");
			return null;
		} else {
			AttendanceStudentDTO atdstd = JSONUtils.covertFromJsonToObject(response.get().body(), AttendanceStudentDTO.class);
			return atdstd;
		}
	}

	public static AttendanceStudentDTO createAttendanceStudent(AttendanceStudentDTO h) throws InterruptedException, ExecutionException, IOException {
		String subUri = mainUri + "createAttendanceStudent";
		String inputJson = JSONUtils.covertFromObjectToJson(h);
		HttpRequest request = HttpRequest.newBuilder(URI.create(subUri)).header("Content-Type", "application/json")
				.POST(HttpRequest.BodyPublishers.ofString(inputJson)).build();
		CompletableFuture<HttpResponse<String>> response = client.sendAsync(request,
				HttpResponse.BodyHandlers.ofString());
		System.out.println(response.get().body());
		AttendanceStudentDTO atdstd=JSONUtils.covertFromJsonToObject(response.get().body(), AttendanceStudentDTO.class);
		return atdstd;
	}
	
	public static AttendanceStudentDTO updateAttendanceStudent(AttendanceStudentDTO h) throws InterruptedException, ExecutionException, IOException {
		String subUri = mainUri + "updateAttendanceStudent";
		String inputJson = JSONUtils.covertFromObjectToJson(h);
		HttpRequest request = HttpRequest.newBuilder(URI.create(subUri)).header("Content-Type", "application/json")
				.PUT(HttpRequest.BodyPublishers.ofString(inputJson)).build();
		CompletableFuture<HttpResponse<String>> response = client.sendAsync(request,
				HttpResponse.BodyHandlers.ofString());
		if (response.get().statusCode() == 204) {
			System.out.println("Product Not Avaialble");
			return null;
		} else {
			AttendanceStudentDTO atdstd=JSONUtils.covertFromJsonToObject(response.get().body(), AttendanceStudentDTO.class);
			return atdstd;
		}		
	}
	
	public static AttendanceStudentDTO deleteAttendanceStudent(String id) throws InterruptedException, ExecutionException, IOException {
		String subUri = mainUri + "deleteAttendanceStudent/";
		HttpRequest request = HttpRequest.newBuilder(URI.create(subUri+String.valueOf(id))).DELETE().build();;
		CompletableFuture<HttpResponse<String>> response = client.sendAsync(request,
				HttpResponse.BodyHandlers.ofString());
		if (response.get().statusCode() == 500) {
			System.out.println("Product Not Avaialble");
			return null;
		} else {
			AttendanceStudentDTO atdstd=JSONUtils.covertFromJsonToObject(response.get().body(), AttendanceStudentDTO.class);
			return atdstd;
		}		
	}


}
