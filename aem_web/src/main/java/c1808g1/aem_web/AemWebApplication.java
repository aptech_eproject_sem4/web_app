package c1808g1.aem_web;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
@SpringBootApplication
public class AemWebApplication {

	public static void main(String[] args) {
		SpringApplication.run(AemWebApplication.class, args);
	}

}
